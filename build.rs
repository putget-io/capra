fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("proto/keymap.proto")?;
    tonic_build::compile_protos("proto/supervisor.proto")?;
    Ok(())
}
