

# PUTS and DELETES

*DELETEs are handled almost identically to PUTs, because from a GET's point of view they're
 just a tombstone, and from the backend point of view we have to pass them through just the
 same as we would a PUT*

Semantics: Strict write ordering within a key (i.e. after we respond to a PUT/DELETE, any subequent
PUT/DELETE to the same key will reach the backend in the same order they were originally sent).  This
is *very* important, because otherwise someone doing a PUT,DELETE sequence on a key could easily
end up with the opposite end state than they expected.

## Keeping write ordering consistent

As long as there are dirty writes to a key (i.e. payloads in a journal waiting
to be expired), then **all subsequent writes to the same key must be sent to the same journal**.

This happens in the request handler when it calls into KeyMap::begin_write -- if there is no
existing KeyState, we create one with the preferred journal for the core the handler
is running on.  If there *is* and existing KeyState, then writes go to whichever journal
the previous writes went to (which may be one belonging to another core, which is fine because
all our stuff is mutex-safe anyway: the affinity of journals to cores is just a soft preference).

We must keep the KeyState for a dirty key not just until it is expired (i.e. written to backend)
but until the journal is trimmed (i.e. tail pointer is written to disk).  If we didn't do that,
then on a crash/replay, we might find that we had old dirty keys in one journal which had been
expired but not trimmed, but also newer dirty keys in another journal.

We must associate a key with a journal as soon as an incoming write *starts*, not just once
it's persistent.  For this we have the 'projected' field of KeyState: initially a new key
has a KeyState with no reference to persistent data in the journal, but an incremented
projected count to keep it alive.

We must be careful when expiring Streams from a Journal in parallel, that we do not concurrently
expire two Streams for the same Key (if we do, then we lose ordering).  That is done with OrderedWaitMap

## What about inter-key ordering?

Ordering of writes across multiple keys is maintained from the perspective of applications using the proxy,
but *not* respected in the way we send writes to the backend.

That is to say, from the frontend frame of reference, if there are two ordered writes A, B, then
a reader can safely assume that if B is seen then A will also be seen.

However, from the backend frame of reference, if two ordered writes A,B were sent to the frontend,
then a reader reads *directly from the backend* then the writes could land in either order, and
reading B does not imply that A will also be readable.

Because writes to the backend are *not* ordered across different keys, applications that try to use 'lock' objects
(e.g. restic) will only look correct from the perspective of instances going through the proxy.  Instances
which talk directly to the backend may e.g. see the lock object disappear while other activity is still
going on, or see things start changing before the lock object appears.

In practice this is pretty simple:
- Applications that (ab)use the storage system for synchronisation must either write
  entirely through the proxy or not at all: you can't have some instances going
  through the proxy and other instances talking directly to the backend.
- You can bend this rule if you are careful to ensure the proxy's write queue
  has drained in between executions.  For example, you might run a backup
  through the proxy, and later do a restore directly from the backend: this will
  be safe *if* the proxy's write queue is drained in between.

# GetObject GETs

Semantics: We observe strict GET-after-PUT ordering (i.e. once we've sent the response to a PUT, all GET
requests received subsequently will see the PUT).

When a GET request starts, we check if the key is dirty (i.e. has writes in the journal
not yet written back to the backend):
  - If the key is clean, then we issue a HEAD to the backend
to get the ETag, and then conditionally serve the GET from `DCache` (see Read Caching) or
proxy the original GET request to the backend if the content was not in cache.
  - If the key is dirty, the object contents are read from the journal on disk.  To ensure
    these are not trimmed while the GET is underway, `ChunkJournal::read`
    returns a `ReadStream` object which includes a logical lock on the region of the
    journal being used to serve the GET response.
    
Blocking trims for in-progress GET requests introduces a risk of readers blocking writers if the
journal is full.  This could happen in practice if a pathologically slow reader is reading a very large object.
Some mechanism could be introduced to recognise if a particular object is the last one in the journal (i.e.
reading it will block trim), and in that case block the read until after the object is expired.  Alternatively,
tail updates could consult the set of `ReadStream` in flight, and apply a timeout-kill policy on streams
which are blocking writes.  Most S3 clients retry requests at least a few times, so occasionally killing
an in-flight GET in this rare case is unlikely to break applications.

# HeadObject HEADs

HEAD requests are simply handled as a get but without the body.  All the same rules for ordering
apply, including serving them out of the journal for dirty objects.

# ListObjectV2 GETs

To respond to an object listing request, the cache has to send the listing onwards to the backend,
and then superimpose any dirty state from the journal into the results before sending back to the
client.  This is straightforward for a single request, but when multiple requests use continuation
tokens, we have to keep some state around between requests.  Continuation tokens are opaque identifiers,
so we store a mapping of the continuation token to the last object in that response.  When we subsequently
receive another ListObjects that uses the continuation token, we retrieve the mapping of token to key, and
use that key as if it had been passed as a Start-After parameter.

We cannot reliably respect the user's Max-Keys setting, because the backend will give us that many
keys, and we don't know until seeing the backend response how many additional keys we will add to
the response to reflect dirty cache contents.  We /could/ iteratively call to the backend with a varied
Max-Keys, at the cost of 1 (or N, if things keep changing) additional round trip backend latencies,
to deliver the exact number of keys the client asked for.

Listings that include dirty objects generates some read IO,because ListObjects results
include object metadata (ETag, LastModified) which is not held in RAM, but requires
a 4k read to the journal for each object whose metadata is fetched.  This is equivalent
to the IO that we do to satisfy a HEAD request for a dirty object.

To summarize, there are three paths:
- Happy: backend listobjects gives a range that overlaps with no dirty keys,
  we just pass the result through.
- Happy: backend gives us a range that when combined with dirty keys still fits
  within Max-keys, we add in our keys and send the response onward.
- Tricky/slow: a continuation-token is used, or the sum of backend results and
  dirty keys exceeds max-keys and requires re-fetching a smaller number of keys
  from the backend.

In well written applications, object listings should never be on a hot path, so
some performance impact ought to be acceptable.  Correctness is the most important
thing, because they may use object listings to drive things like backups
or data migrations.  We absolutely must not skip any keys!

# DeleteObject DELETE

A `DeleteObject` is just like a `PutObject`, but without a body.  All the same ordering
rules apply to DELETEs as to PUTs.

DeleteObjects POST
==================

The S3 bulk deletion verb simply sends an XML body with a list of keys to delete.

At time of writing, this is simply unpicked into a bunch of individual delete operations, which are
journalled as if they arrived in DeleteObject requests.  This is suboptimal:
- Each key uses at least `IO_ALIGN` (4kb) of journal space, because each key gets its own StreamHeader.
- The number of HTTP requests to the backend during writeback will be far higher than if
  we kept the deletes batched together.
  
Journaling all the keys en-masse would be a relatively invasive change, as it would break
the 1:1 relationship between a stream and a key.

Journal
=======

A 'chunk' is a single IO.  Chunks are 4k-aligned and their size is padded to a 4k boundary (i.e.
we never issue a write smaller than 4k).

A Stream is a collection of chunks.  The chunks for many streams are interleaved within a single
journal.

Chunks and and do complete in a different order than their writes were issued.  Streams wait on their
own chunks, not for overall journal progress, so a Stream can complete even if there are still incomplete
writes in flight behind it.  Consequentially, replay must cope with 'gaps' in the journal (i.e. seeing
a gap doesn't mean we're done with replay).

This journal makes no attempt to coalesce many small writes into larger writes.  That is completely
intentional: we want an incoming small S3 PUT to map directly to a single, immediate write() of
the same size (well, plus a head and padded to 4k).  Of course, the linux block layer is still free
to coalesce adjacent small writes.

Stream Header + Footer
----------------------

StreamHeaders always fit within one chunk, because S3 key lengths are bounded to 1023 bytes and our smallest
chunks are 4k.  This gives us the useful behavior of being able to read the header in a single IO without
having to worry about streaming through multiple objects to get it.

StreamFooters are written after the stream content, and may be split across multiple chunks.  It wouldn't make sense
to give the footer its own chunk, because it's only a 16 byte MD5 digest (what S3 uses as an ETag).

The ETag isn't known until the end of a PUT, so this part has to go into a footer rather than a header if we
are to write objects without buffering them somewhere else first.  In principle, all the metadata could be
written in one go at the end of a stream instead of splitting between header and footer, but that would complicate
various code paths that benefit from knowing the key immediately at the start of a read (esp. expiry and replay).

To fulfil a HeadObject or ListObject, information from both the header and footer is needed.  To avoid having
to issue multiple IOs per key to read it, the salient fields from StreamHeader and StreamFooter are stored
in memory for all dirty keys as part of KeyState.


Replay
------

On replay, we have three goals:
- Populate KeyMap with the most recent journal location of all dirty keys
- Find the head position of the journal (i.e. where to write the next chunk)
- Seal off any torn streams that were left open at termination of the prior writer.

During replay, we only need to read chunks that contain StreamHeaders - the bucket and key
in the StreamHeader are needed to populate KeyMap with a reference to the journal offset that
contains the most recent write to a key.  The resulting IO pattern is non-contiguous sequential
4k reads.

On completion of replay, we must 'seal' any streams that were left open on disk (i.e. a STREAM_BEGIN
but no STREAM_CANCEL or STREAM_END).  If we didn't do that, the expirer would be unable to distinguish
between an ongoing write (which it should wait for) and a torn stream (which would cause it to hang
forever, or until the stream ID was reused by anothers stream).

We do not have to seal a torn stream if the replayed content already included a subsequent STREAM_BEGIN
that re-uses the stream ID of the torn stream - this is sufficient to cue the expirer that the first stream
is torn and shoudl be aborted.

Expiry/writeback
----------------

Expiry of journal entries (i.e. writing them back to the backend) is similar to replay.  A single
task proceeds sequentially through the chunks in the journal, reading ChunkHeaders and StreamHeaders.

When a new stream PUT is seen, a separate StreamExpirer task is spawned, with a channel for receiving Offsets.  The main
expirer task then feeds this channel with the Offset reference to chunks when it encounters a ChunkHeader
whose stream_id matches the in-flight stream.  The StreamExpirer reads the full chunk at the offsets it is
given via the channel, and feeds the chunk bodies through to the backend.  The StreamExpirer doesn't consider
any of its chunks to be expired (i.e. elegible for trim) until the entire request to the backend has succeeded.

The in-memory state about dirty keys is only read as an optimization, to skip writeback of values that are
already superceded by a more recent write or delete.  For the most part, the expiry process operates
entirely by streaming from disk, to be as decoupled from the write side as possible.

A separate Tailer class is responsible for receiving notifications from StreamExpirers when they finish
with a stream, and advancing the tail of the journal accordingly.

Throttling expiry vs frontend writes
------------------------------------

Clearly, expiry competes for resources with the frontend, and the frontend is what the end user sees
as the performance of the system.  The rate (and especially the latency) of expiry is less impactful, so
we treat it as a lower priority, unless the journal is close to being full, in which case expiry runs
as fast as it can to try and make room for incoming writes.

Because expiry and frontend writes use very similar CPU resources (dominated by AWSv4 auth and its SHA-256
operations), we establish a very direct relationship between their rates.  The backend will throttle itself
to the highest rate of the frontend writes it has seen (based on rate of head pointer increase), minus
the current rate of frontend writes.  If there are no frontend writes, the expirer stops throttling
and runs as fast as it can.

The resulting behaviour on a system with bursty frontend writes is that the expirer pretty much stops
reading new chunks while a frontend burst is happening, and then resumes expiry when the frontend
quiets down.  If you plot the two rates on a chart, you'll see a frontend bump followed by a expiry
bump, where the latter is usually lower and flatter as we expect to be deployed in situations where
we can accept writes faster than our backend can.

On failure to expire
--------------------

In the event of the backend being unavailable or a particular write operation failing, the journal
cannot be trimmed past the point where the problematic stream's chunks are.  In practice that means
that while a backend is inaccessible, the journal will get fuller and fuller until eventually
frontend writes stall.

This is perfectly reasonable behaviour when the backend is unavailable.  However, if writes are failing
for some other reason (e.g. we don't have the right auth parameters) or are intrinsically invalid (e.g.
we have writes for a bucket that no longer exists on the backend), the behaviour is more nuanced/opinionated:
- Auth errors are considered transitory: expiry will stall until correct parameters are provided
- 404 NotFound errors on writes are considered permanent: if a bucket simply doesn't exist, then
  we will drop a write and proceed.  Clearly this involves losing user data, but it's data that was
  essentially 'written to nowhere' as it addresses a nonexistent bucket.

Implementation of the above fine-grained behaviour is incomplete at time of writing, so in general
any backend errors are liable to stall journal expiry with retries of the failed writeback, until
it eventually completes.

Read caching
============

A basic content-addressable LRU cache of object bodies called `DCache` is used to accelerate
repeated GETs of the same object contents, or GETs of the most recently written back objects.

The `DCache` interface is written with a persistent cache backend (like local scratch disk) in mind,
but at time of writing stores all values in memory with a fixed 1GB space limit.  This primitive cache
is good enough to handle the case where an application reads the same value repeatedly in a short
space of time (e.g. all nodes in a clustered application read the same object).

The data cache key is the object ETag (md5sum), and values are inserted in two places:
- during writeback, before trimming an object's stream from the `ChunkJournal`
- during GET, if the GET is proxied to the backend (i.e. a cache miss), then the backend's
  response is copied out into the DCache at the same time as being returned to the client
  that requested it.
  
When serving a GET, if the key is dirty then the body is served from the dirty journalled
data -- the `DCache` is not involved in this case.  If the key is clean, then a `HEAD` is sent
to the backend to check the ETag.  If the ETag is found in `DCache`, then the client
receives a response from cache.  If the ETag is not found in `DCache`, then the client's
request is proxied to the backend, and the response content is used to prime a `DCache` entry
for any subsequent reads that match the same ETag.

At time of writing there is no TTL caching of object metadata: once an object is written
back, any subsequent reads must hit the backend with at least a HEAD to ensure they are
serving fresh data.  Adding metadata caching would be especially beneficial for workloads
that use `Cache-Control` headers.

Drain mode
==========

To enable cleanly decommissioning a Capra node, the `--drain` flag modifies write behaviour:
- If the key is clean, then the write skips the journal and is proxied directly to the backend
- If the key is dirty, then the write is journalled.

The result is that the journal gradually empties, as dirty keys are written back and no new
keys can be dirtied.

A client can still spam many writes to the same key to prevent it ever expiring and thereby
prevent the journal fully draining.  To handle that kind of pathological case, drain mode would
need an additional behaviour, such as blocking new writes to a key if a current dirty value
is in the process of being expired.

Using drain mode on an empty journal results in all operations being proxied to the backend,
this could be useful if a simple S3-aware proxy is needed.  It's also handy for testing
the proxying code by effectively disabling the journal.

Clustered mode
============

Clustered mode enables running multiple Capra services in a cache-coherent cluster.  The distribution
of metadata and data is handled separately:
- Metadata in the `KeyMap` is sharded across nodes deterministically according to the
  hash of the key.
- Data in the `ChunkJournal` is written to whichever journal is local to the node that happens
  to receive the write request, unless the key is already dirty, in which case the client request
  is proxied to whichever node owns the journal that already contains dirty state for the key.
  
Inter-node `KeyMap` communication is done with GRPC.  When a `Shard` is on the same node as
the request handler querying a key, the GRPC hop is skipped and results returned directly.  `Shard` is
a trait defining the interface to keys, and is implemented by `KeyMapShard` (the local data) and by
`KeyMapShardRemote` (a GRPC client to a `KeyMapShard` on another node).

An additional set of GRPC calls are used to coordinate the lifetime of nodes within a cluster.  There is
no fine-grained recovery from node failures: when one node restarts and announces itself to the cluster,
all other nodes will restart as well, and go through replay together before opening their
frontend S3 ports.  The `supervisor` module handles this.

The cluster inventory is static and stored in a YAML file, passed with `--cluster-map` to the server
process at startup.