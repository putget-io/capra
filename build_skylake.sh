#!/bin/bash
set -e

RUSTFLAGS="-Ctarget-cpu=skylake" cargo build --release --features=ringhash
mkdir -p target/skylake
mv target/release/capra target/skylake
