FROM docker.io/library/rust:latest as builder

RUN mkdir /build
WORKDIR /build

RUN apt-get update -y
RUN apt-get install -y musl-tools

RUN rustup target add x86_64-unknown-linux-musl
RUN rustup component add rustfmt

COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# Cargo doesn't have a way to just download/build dependencies, so
# we hack around it (https://github.com/rust-lang/cargo/issues/2644)
RUN mkdir -p src/lib \
&& mkdir benches \
&& echo "//nothing" > src/lib/lib.rs \
&& echo "fn main(){}" > src/main.rs \
&& echo "//nothing" > benches/auth.rs \
&& echo "//nothing" > benches/hashes.rs \
&& echo "//nothing" > benches/simple_pool.rs \
&& cargo build --bin=capra --release --target=x86_64-unknown-linux-musl
RUN rm -rf src/ benches/ target/x86_64-unknown-linux-musl/release/capra

COPY ./build.rs ./build.rs
ADD ./src ./src
ADD ./proto ./proto
ADD ./benches ./benches
RUN find ./src -exec touch {} \;
RUN cargo build --release --target=x86_64-unknown-linux-musl

FROM alpine:latest

RUN apk update \
    && apk add --no-cache ca-certificates tzdata \
    && rm -rf /var/cache/apk/*

COPY --from=builder /build/target/x86_64-unknown-linux-musl/release/capra /usr/bin/capra
