Capra: An Object Storage Accelerator
====================================

What is this?
-------------

**This is a spare time project: test thoroughly before using it for any data you care about**

Capra is S3-compatible gateway with a persistent write-back cache.  This type
of cache is sometimes known as a burst buffer: PUT operations complete very quickly
by writing data to fast local storage before draining it later to a slower backend.

With a fast SSD, Capra delivers sub-millisecond write latencies for small objects,
and runs in only a few hundred megabytes of RAM.

This might be useful for:
 - Fast access from edge locations to remote clouds and datacenters, for example
   shipping backups or logs from a colocation facility to your main datacenter, or using
   S3-based backup systems like `restic` from your home network to your cloud of choice.
 - Accelerating performance workloads that are bounded by storage write latency, if 
   you have HPC compute jobs that are held back by a slow storage system.
 - Reducing FaaS function runtimes, if your functions need to write out to storage
   before terminating (especially if you're paying by the millisecond)


Running capra in a container
----------------------------

The details of this depend very much on your environment: examples
below are for running as a superuser on CentOS.

Create a local data location:

    mkdir -p /var/lib/capra/data

Create an environment file at `/var/lib/capra/key.env` with your
crednential environment variables (in a production environment, you
would use whatever mechanism you use for other container secrets):

    AWS_ACCESS_KEY_ID=...
    AWS_SECRET_ACCESS_KEY=...
    CAPRA_LOG=INFO

Then run the container, specifying a port and backend of your choice (in this example the port is `1234` and the backend is Wasabi)

    podman run -p 1234:1234 -v /var/lib/capra/data:/data/ \
        --env-file=/var/lib/capra/key.env \
        -it registry.gitlab.com/putget-io/capra:latest \
        capra server -j /data/journal.bin -l 0.0.0.0:1234 -b http://s3.eu-central-1.wasabisys.com

If selinux is enabled, you may need to use additional settings to enable the container
to write to the data location (or `setenforce 0` if you just want to turn it off in
a development environment).

To enable the faster io_uring backend, you may need extra security settings that depend on
the exact container environment you're in.  For quick testing, you can pass
`--privileged --ulimit memlock=-1:-1` to make it work.

Running on bare metal
---------------------

Download a binary from the [releases](https://gitlab.com/putget-io/capra/-/releases) page, or
by building using Cargo (see Development section)

Before running, set these environment variables:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION` (_optional, default is us-east-1_)

Then invoke capra in server mode:

    capra server -l <listen ip>:<port> -b <backend url> -j <journal file>

For example, to listen on port 8800, use my-s3-backend as your backend, and
store your journal in /var/lib:

    capra server -l 0.0.0.0:8800 -b http://my-s3-backend.com -j /var/lib/journal.bin

Once that's running, you can point any clients that were using `my-s3-backend.com` to the
capra proxy (port 8800 in this example), and their traffic will be proxied.

CAPRA_VHOST & DNS
-----------------

By default, Capra accepts path-style S3 requests, as well as virtualhost style requests
to localhost.  To accept virtualhost requests from elsewhere, set the `CAPRA_VHOST` environment
variable to the domain name clients will use to access the service -- this might be the FQDN
of the server, or it might be some other DNS name like that of a k8s service.

If none of the above made any sense, just set the `CAPRA_VHOST` environment variable to the
output of `hostname -f`.

Options
-------

- `-w <n>` sets the worker thread count.  This defaults to one thread per available
  CPU core.  Using a smaller number of threads may deliver more deterministic
  performance, at the expense of limiting max throughput.

- `-j path[,path...]` comma-separated list of journal paths: use more than one
  to spread IO across multiple files.  This is useful for spanning multiple
  physical devices, or for improving parallism across many CPU cores.  Consider
  matching `-w` (thread count) with the number of journal shards on large
  core count machines, to reduce contention.

- `-e <rio|tokio>` selects the IO engine to use for disk access.  `rio` gives
  better performance but requires a kernel with io_uring support (recent 5.x).  `tokio` works
  on more platforms but has much lower performance.  The
  default is to auto-detect whether io_uring is available.

- `-r <bytes/sec>` limits the writeback speed.  Useful if you have a limited
  bandwidth to your backend over a contended WAN link and want to avoid
  saturating it.
  
- `-d` activates *drain mode* wherein new keys are not written to the journal,
  but existing journal contents are still expired.  This is useful for decommissioning
  a Capra server (once the journal is drained, clients can switch back to talking
  directly to the backend, without risking missing any recent writes).

- `-u` disables authentication of incoming requests.  Useful if you
  are running in a trusted environment (e.g. listening on localhost
  on a single user server) and want to avoid the CPU/latency cost
  of AWSv4 auth.

- `-n` disables ETag calculation on PUT requests.  The S3 protocol uses
  the legacy MD5 algorithm, which is comparatively slow even on modern
  CPUs, and adds about 2ms of latency to a 1 MB PUT.  Some applications
  doing simple PUT/GET operations by key do not care about the ETag, so
  can optionally get better performance and lower power consumption
  by disabling it.  Objects still end up with an Etag when they are
  written back to the backend, but GETs which hit dirty objects
  will see all zeros for ETag

Using Capra with s3fs
---------------------

Running S3FS on top of Capra can provide a fast filesystem on top of a remote
storage backend.

Run the Capra server with the appropriate backend and credentials, and then invoke
s3fs similarly to the following, in which Capra is running on localhost:1234:

    s3fs -f examplebucket: /mnt/examplebucket -o url=http://localhost:1234,host=http://localhost:1234,listobjectsv2,passwd_file=${HOME}/.passwd-s3fs,nomultipart,nocopyapi,sigv4,use_path_request_style

Note the various configuration options to have s3fs use the appropriate subset of
S3 operations.  The `use_path_request_style` option is not mandatory, but useful
in local test environments where you might be addressing the server by IP address.

Authentication and Authorization
--------------------------------

At present, authentication is very simple: a single AWSv4 key pair is loaded
from the environment when `capra server` starts.  This key pair is used to authenticate incoming
requests, and also used when writing & reading data to the backend.

This is good enough for many use cases: if the Capra deployment belongs
to a particular application, it will simply use the same credentials that the application
uses.

Once a client request is authenciated, Capra does not do any authorization checks itself: 
if a client tries to GET something they don't have the right to, then they will get
a 403 when Capra tries to request it from the backend on their behalf.  If a client
sends a PUT to something they don't have the right to write, Capra will accept it
into the journal, but that write will never make it to the backend.  The application
may get confused if it acts this way, but no security barriers will be breached.

Clearly this is not sufficient for deploying Capra as a central shared resource, and
at present that use case is discouraged (and not just because I don't want to write
my own IAM implementation).  If you have N applications, there are great operational
advantages to giving each one its own Capra instance to provide deterministic performance
to each application: Capra is built to be very resource-light to enable this model.


Caveats
-------

Only AWSv4 authentication is supported - there is no fallback for very old clients
that might try to use AWSv2.

HTTPS is supported for backends, but not for frontend connections.  That means 
it's fine to use backends over the public internet, but you should generally
keep your application->capra traffic on private networks (or put capra behind
a k8s ingress that does the HTTPS part).

The frontend is not aware of multi-part uploads, and will pass these directly
through to the backend.  This is usually harmless, but means that multipart uploads
do not respect the same write-ordering rules as regular write requests (PUTs and
DELETEs).  Try to avoid mixing multi-part uploads with regular PUTs to the same
key, and avoid doing multi-part uploads to the same key that was recently the
target of a DELETE (in case that DELETE is still in the journal and
could land on the backend after the multipart upload, deleting it).

If your write journal shard size is very small (<5 GB) then
PUTs for objects larger than a single journal shard skip the cache and are proxied
directly to the backend.  For example, a single 4 GB PUT to a system using 1 GB journal
shards will not be written into the cache.  This can cause ordering issues with
smaller write requests to the same key, including DELETEs.  This can be avoided by
using journal shards >5 GB: this is the default, and a smaller journal will only
have been created by custom `capra format` or if `capra server` was run for the first
time when there was less than 5 GB of space available.

If the total size of concurrent PUTs to a journal shard exceeds the total size of
the journal, then these requests may deadlock (all waiting for some space to free, but
none can be freed because it's all occupied by incomplete requests).  This is a bug and
will be fixed, but after fixing this use case will necessarily be rather slow because
incoming writes will wait for space to be available: if this happens, you need a bigger
journal, smaller writes, or to skip the cache for this type of bulky workload.

CopyObject requests are passed directly to the backend.  If you use CopyObject on
an object that is in the journal but not yet flushed to the backend, you will
receive a 404 response.  Applications that use CopyObject should only be run through
the cache if they exclusively copy objects written to the backend elsewhere, rather than
objects written via the cache.

The frontend is not versioning-aware, and might get confused if its sees PUTs/GETs that
refer to versions.  Running atop a versioned bucket is perfectly safe, but operations
that refer to versions should not be used on clients of the cache.  For example, if
a PUT to the cache is followed by a ListObjects, the new object will not be reflected
in the ListObjects result (because the ListObjects request is proxied to the backend),
whereas a plain GetObject or ListObjectsV2 /will/ see the new object (because these requests
are handled by the cache itself).

The Last-Modified attribute of a written object will initially reflect the time of the
original PUT.  When the object is flushed from the journal to the backend,
the Last-Modified will update to reflect the time of flush.  There is no way in the S3
protocol to set Last-Modified, so any write back cache will exhibit the same behaviour.

Daisy-chaining proxies will work for basic PUTs and GETs, but ListObjectsV2 requests may
experience issues.  This is because of a magic string that the cache uses to
distinguish its own continuation tokens from those of a backend.  This is pretty
easy to fix (by making the token configurable) if chaining proxies is needed.


Frequently asked questions
--------------------------

#### What about high availability?

HA is not built in, but you can get there quickly if running in
a kubernetes environment: put Capra in an n=1 ReplicaSet behind a Service, and use
a PersistentVolume for its journal storage.

Adding built-in active/passive HA in future would be quite straightforward, as all the disk access
is already log structured, so would map well to a simple replication protocol.

Consider whether your use case needs HA: for some batch use cases, like nightly backups, it might be
acceptable to run a non-HA proxy, and in the rare event of that particular server failing, rerun
the backups.  Similarly, in a batch compute environment, you might find that a proxy server failure
is rare enough that you don't mind just rerunning a job when it happens.

#### What about scaling out for more throughput than a single node?

Capra has a clustered mode, activated with the `--cluster-map` argument and
a configuration file.  There aren't polished docs for clustered mode, but
there are some examples in the `scripts/` directory in git.

Development-only options
------------------------

These are documented to *discourage* use - these are development flags, not hacks
to make it run faster!

- `-m` runs with no journal at all.  This is only useful for benchmarking networking,
and CPU load.  It will effectively drop all PUTs (while sending 200 responses that make clients think
their data is persistent).

- `-s` runs with no fsync() calls.  This flag enables fair benchmarking comparisons with certain other storage systems which
  do not provide data safety by default, and enables indicative benchmarking on consumer-grade NAND
  devices as if they were enterprise battery-backed devices.  Without fsync,
  200 responses to a PUT do not guarantee an object is persistent: in the
  event of a power failure, a few milliseconds of "time travel" will occur
  where the most recent PUTs are lost.  This may be acceptable for some
  backup/caching type workloads, but in general is unsafe.

Development
-----------

### Building a container

If you just want to generate a container, you can build it without
an external environmental dependencies other than `podman`/`docker`:

    podman build .

If you have a local Rust environment, standard Cargo commands are used for builds:

    cargo build --release

### Dependencies

* **Rust 1.51**: use `rustup` to stay up to date, this project will
  track the most recent stable Rust.
* **Linux/Unix**: the disk I/O code uses features from `std::os::unix` and is not tested on other platforms.

### Features: ringhash

The default `sha2` crate has excellent performance on architectures that
provide hardware acceleration for SHA-2 algorithms.  This includes AMD processors
since 2017 (>=Zen architecture), and Intel CPUs since 2019 (>=Ice Lake).

On older CPUs, the alternative `ring` crate provides better performance for
SHA256 digests, which improves Capra's throughput on large objects.

To use `ring` for hashing, enable the `ringhash` feature:

    cargo build --release --features=ringhash

### Features: rio_engine

*This is enabled by default*

The rio (io_uring) backend is far faster than the generic tokio backend.  This feature
flag exists to remove rio from the build in case one doesn't want the dependency.

### Features: rados_backend

You must have `librados-devel` or your distro's equivalent installed to build with this feature.

The rados backend is an experimental mode where Capra provides an S3 frontend and caching, but uses
a Ceph cluster's low level protocol as its backend, instead of using another S3 service
as the backend.

The result is a bit like a toy Rust version of Ceph RGW, with just basic put/get/list/delete support,
but with a performance boost for workloads where Capra's caching helps.

To try this:
- Set environment variable `CAPRA_RADOS_POOL` to a pool in your Ceph cluster
- Put a `ceph.conf` in your working directory that includes a working `admin` credential
- use `"-b rados:"` on the server command line

The rados backend maps each S3 object to a single RADOS object, so will only work well
for reasonably small objects (by default Ceph limits object size to 128MB).

### Architecture-optimized builds

The rust compiler produces generic x86_64 binaries by default.  Better
performance is available by specifying a particular CPU featureset.  Different
CPUs may also work better with different digest libraries.

The ``build_skylake.sh`` script has an example of a configuration that
generates higher performing binaries for older Intel CPUs.

Unit tests
----------

Invoke unit test with a standard ``cargo test``

Things to know about unit tests:
- Tests that run async use short timeouts (more like a second than a minute) on
  the expectation of being run on a reasonably fast machine and drive.  This is
  a simple way to detect deadlocks.  If your machine or drive is slow you may have issues.
- Some tests rely on a writing into the tmp path, these will only create files within /tmp/test,
  with subdirectories named after the test.

Performance tests with ``bench`` subcommand
-------------------------------------------

Invoke benchmarks with ``cargo bench``

The tests in the `auth` benchmark are useful for measuring the CPU limit
on hashing performance, to understand the upper bound on per-core throughput
that you'll get.

Copyright & License
-------------------

Copyright: John Spray

License terms: **GPLv3**, see file `LICENSE`.
