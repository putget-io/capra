

FLAGS="-s -n -u"
BACKEND="-b null"

cargo run --release -- format --path=/dev/nvme0n1p4 && taskset -c 4-7,20-23 cargo run --release -- server -e rio -j /dev/nvme0n1p4 -l 127.0.0.1:1234 $BACKEND --cluster-map ./2nodes_1.toml $FLAGS &
cargo run --release -- format --path=/dev/nvme0n1p5 && taskset -c 8-11,24-27 cargo run --release -- server -e rio -j /dev/nvme0n1p5 -l 127.0.0.1:1235 $BACKEND --cluster-map ./2nodes_2.toml $FLAGS &
