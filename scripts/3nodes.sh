

FLAGS="-s -n -u"
BACKEND="-b null"


cargo run --release -- format --path=/dev/nvme0n1p4 && taskset -c 4-7,20-23 cargo run --release -- server -e rio -j /dev/nvme0n1p4 -l 127.0.0.1:1234 $BACKEND --cluster-map ./cluster1.toml $FLAGS &
cargo run --release -- format --path=/dev/nvme0n1p5 && taskset -c 8-11,24-27 cargo run --release -- server -e rio -j /dev/nvme0n1p5 -l 127.0.0.1:1235 $BACKEND --cluster-map ./cluster2.toml $FLAGS &
cargo run --release -- format --path=/dev/nvme0n1p6 && taskset -c 12-15,28-31 cargo run --release -- server -e rio -j /dev/nvme0n1p6 -l 127.0.0.1:1236 $BACKEND --cluster-map ./cluster3.toml $FLAGS &

#RUST_BACKTRACE=full CAPRA_LOG=WARN taskset -c 0-3,16-19 cargo run --release -- bench -e http://127.0.0.1:1234,http://127.0.0.1:1235,http://127.0.0.1:1236 -l 127.0.0.1 bucket2 -s 8000000 -c 10000 -w 16
