use hostname;
use std::net::{IpAddr, SocketAddr};
use toml;

use serde::Deserialize;

pub const DEFAULT_S3_PORT: u16 = 8080;
pub const DEFAULT_GRPC_PORT: u16 = 4321;
pub const DEFAULT_SUPERVISOR_PORT: u16 = 4421;

pub type NodeId = u32;

// The part of a Node that is deserialized from inner dict in cluster map
#[derive(Deserialize, PartialEq, Eq)]
struct NodeConfig {
    address: IpAddr,
    s3_port: u16,
    grpc_port: u16,
    supervisor_port: u16,
}

#[derive(PartialEq, Eq)]
pub struct Node {
    id: NodeId,
    hostname: String,
    config: NodeConfig,
}

impl Node {
    pub fn get_id(&self) -> NodeId {
        self.id
    }

    pub fn get_hostname(&self) -> &str {
        &self.hostname
    }

    pub fn get_grpc_url(&self) -> String {
        // TODO: https support (simple here, but requires cert distribution etc)
        format!("http://{}:{}", self.config.address, self.config.grpc_port)
    }

    pub fn get_supervisor_url(&self) -> String {
        // TODO: https support (simple here, but requires cert distribution etc)
        format!(
            "http://{}:{}",
            self.config.address, self.config.supervisor_port
        )
    }

    pub fn get_grpc_address(&self) -> SocketAddr {
        SocketAddr::new(self.config.address, self.config.grpc_port)
    }

    pub fn get_s3_address(&self) -> SocketAddr {
        SocketAddr::new(self.config.address, self.config.s3_port)
    }

    pub fn get_supervisor_address(&self) -> SocketAddr {
        SocketAddr::new(self.config.address, self.config.supervisor_port)
    }
}

impl std::fmt::Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}:{}", self.id, self.hostname)
    }
}

pub struct ClusterMap {
    my_id: NodeId,
    nodes: Vec<Node>,
}

pub enum LoadError {
    Io(std::io::Error),
    Syntax(toml::de::Error),
    Logic(String),
}

impl From<std::io::Error> for LoadError {
    fn from(err: std::io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<toml::de::Error> for LoadError {
    fn from(err: toml::de::Error) -> Self {
        Self::Syntax(err)
    }
}

impl std::fmt::Debug for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Io(e) => e.fmt(f),
            Self::Syntax(e) => e.fmt(f),
            Self::Logic(e) => write!(f, "Bad ClusterMap: {}", e),
        }
    }
}

impl ClusterMap {
    /// Construct a single-node map referring only to localhost
    pub fn new_local() -> Self {
        // Ports don't matter, as we'll never attempt to connect to ourselves
        let nodes = vec![Node {
            id: 0,
            hostname: "localhost".into(),
            config: NodeConfig {
                address: "127.0.0.1".parse::<IpAddr>().unwrap(),
                s3_port: DEFAULT_S3_PORT,
                grpc_port: DEFAULT_GRPC_PORT,
                supervisor_port: DEFAULT_SUPERVISOR_PORT,
            },
        }];
        Self { my_id: 0, nodes }
    }

    pub fn from_file(path: &std::path::Path) -> Result<Self, LoadError> {
        let content = std::fs::read_to_string(path)?;
        Self::from_string(&content)
    }

    pub fn from_string(s: &str) -> Result<Self, LoadError> {
        let top_v = s.parse::<toml::Value>()?;
        let mut nodes: Vec<Node> = Vec::new();
        let mut my_id: Option<NodeId> = None;

        let my_hostname = hostname::get()?
            .into_string()
            .expect("Non-unicode hostname!?");

        if let toml::Value::Table(mut table) = top_v {
            // toml's Map type doesn't have an into_iter, so copy out keys and do a remove per key below
            let keys: Vec<String> = table.keys().map(|s| s.clone()).collect();

            for (i, hostname) in keys.iter().enumerate() {
                let v = table.remove(hostname).unwrap();
                if hostname == &my_hostname && my_id.is_none() {
                    my_id = Some(i as NodeId);
                }

                if let toml::Value::Table(node_table) = v {
                    if node_table.get("self").is_some() {
                        my_id = Some(i as NodeId);
                    }

                    let node_config = toml::Value::Table(node_table).try_into::<NodeConfig>()?;
                    nodes.push(Node {
                        id: i as NodeId,
                        hostname: hostname.to_string(),
                        config: node_config,
                    });
                } else {
                    return Err(LoadError::Logic(format!(
                        "Unexpected type in section {}",
                        hostname
                    )));
                }
            }
        } else {
            return Err(LoadError::Logic("Missing top-level sections".into()));
        }

        if nodes.is_empty() {
            return Err(LoadError::Logic("No nodes declared".into()));
        }

        let my_id = my_id.ok_or_else(|| LoadError::Logic("No node match this host".into()))?;

        Ok(Self { nodes, my_id })
    }

    pub fn get_id(&self) -> NodeId {
        self.my_id
    }

    pub fn get_nodes(&self) -> &Vec<Node> {
        &self.nodes
    }

    /// This panics on an invalid NodeId, which is okay because the cluster map
    /// is also the only place that NodeIds should come from, and at time of writing
    /// nodes are always globally synchronised at startup to use the same cluster map.
    pub fn get_node(&self, node_id: NodeId) -> &Node {
        &self.nodes[node_id as usize]
    }

    pub fn my_node(&self) -> &Node {
        &self.nodes[self.my_id as usize]
    }

    pub fn leader_node(&self) -> &Node {
        &self.nodes[0]
    }

    pub fn am_leader(&self) -> bool {
        self.my_id == 0
    }

    pub fn followers(&self) -> Vec<&Node> {
        // FIXME: return an iterator instead to be a bit less inefficient
        self.nodes.iter().filter(|n| n.id > 0).collect()
    }

    pub fn lookup_peer(&self, id: NodeId, hostname: &str) -> Option<&Node> {
        if id as usize >= self.nodes.len() {
            None
        } else {
            let node = &self.nodes[id as usize];
            if node.hostname == hostname {
                Some(node)
            } else {
                None
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::server::cluster_map::ClusterMap;

    #[test]
    fn test_load_cluster_map() {
        let config = r#"[host1]
address="1.2.3.4"
grpc_port = 1234
supervisor_port = 1243
s3_port = 1235
self=true
[host2]
address="3.4.5.6"
grpc_port = 2345
supervisor_port = 1243
s3_port = 1235
"#;
        let cm = ClusterMap::from_string(config).expect("Load error");
        assert_eq!(cm.get_nodes().len(), 2);
        assert_eq!(cm.get_id(), 0);
        assert_eq!(cm.get_nodes()[0].get_grpc_url(), "http://1.2.3.4:1234");
    }
}
