use env_logger;
use std::sync::Once;

static TEST_INIT_ONCE: Once = Once::new();

/// Initialization where one test might conflict with another
/// if they both e.g. try to init a logger.
pub fn global_init() {
    TEST_INIT_ONCE.call_once(|| {
        env_logger::init();
    });
}
