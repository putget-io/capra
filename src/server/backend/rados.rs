use log::*;
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

use async_trait::async_trait;
use chrono::{SecondsFormat, Utc};
use digest::Digest;
use futures::stream::FuturesUnordered;
use futures::{Sink, SinkExt, Stream, StreamExt, TryStreamExt};
use http::{Method, Response, StatusCode};
use hyper::body::Bytes;
use hyper::Body;

use super::{Backend, BackendPutStream, Error};
use crate::server::backend::{BackendPutStreamHandle, ErrorKind};
use crate::server::dcache::DCache;
use crate::server::frontend::request::{S3Request, S3Verb};
use crate::server::journal::StreamHeader;
use crate::server::s3_types::{
    DeleteObjectsRequest, ListObjectsV2Request, ListObjectsV2Response, Object, DEFAULT_MAX_KEYS,
};
use crate::util::http::{empty_response, status_response};
use capralib::auth::{Authorizer, SecretMap};

use crate::server::decoder::{AuthValidator, ChunkedBodyDecoder, PlainAuthValidator};
use capralib::encoder::{DecodeError, DecodeErrorKind};

pub struct RadosBackend {
    rados: tokio::sync::Mutex<Option<ceph::ceph::Rados>>,
    pool_name: String,
    secrets: SecretMap,
}

const ETAG_ATTR: &str = "etag";

// When connecting an incoming stream to a RADOS WriteSink, how large
// the Coalescer chunk size should be.
const RADOS_WRITE_SIZE: usize = 4 * 1024 * 1024;

const RADOS_OPS_PER_STREAM: usize = 4;

/// Bind an Arc together with a stream that refers to it.
struct StreamBinder<T, S> {
    stream: *mut S,
    parent: *const T,
}

unsafe impl<T, S> Send for StreamBinder<T, S> {}

impl<'a, T: 'static, S> StreamBinder<T, S> {
    fn new<F>(parent: Arc<T>, stream_fn: F) -> Self
    where
        F: FnOnce(&'static T) -> S,
    {
        unsafe {
            let parent = Arc::into_raw(parent);
            let static_parent: &'static T = parent.as_ref().unwrap();
            let stream = Box::new(stream_fn(static_parent));
            Self {
                stream: Box::into_raw(stream),
                parent,
            }
        }
    }
}

impl<T, S: futures::Stream> Stream for StreamBinder<T, S> {
    type Item = S::Item;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let inner = unsafe { Pin::new_unchecked(self.stream.as_mut().unwrap()) };
        inner.poll_next(cx)
    }
}

impl<T, I, S: futures::Sink<I>> Sink<I> for StreamBinder<T, S> {
    type Error = S::Error;

    fn poll_ready(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let inner = unsafe { Pin::new_unchecked(self.stream.as_mut().unwrap()) };
        inner.poll_ready(cx)
    }

    fn start_send(self: Pin<&mut Self>, item: I) -> Result<(), Self::Error> {
        let inner = unsafe { Pin::new_unchecked(self.stream.as_mut().unwrap()) };
        inner.start_send(item)
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let inner = unsafe { Pin::new_unchecked(self.stream.as_mut().unwrap()) };
        inner.poll_flush(cx)
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let inner = unsafe { Pin::new_unchecked(self.stream.as_mut().unwrap()) };
        inner.poll_close(cx)
    }
}

impl<T, S> Drop for StreamBinder<T, S> {
    fn drop(&mut self) {
        let stream = unsafe { Box::from_raw(self.stream) };
        drop(stream);
        let parent = unsafe { Arc::from_raw(self.parent) };
        drop(parent);
    }
}

struct Coalescer<S: Stream<Item = Result<Vec<u8>, DecodeError>>> {
    size: usize,
    buf: Vec<u8>,
    inner: Pin<Box<S>>,
    exhausted: bool,
}

impl<S: Stream<Item = Result<Vec<u8>, DecodeError>>> Coalescer<S> {
    fn new(inner: S, size: usize) -> Self {
        Self {
            size,
            inner: Box::pin(inner),
            buf: Vec::new(),
            exhausted: false,
        }
    }
}

impl<S: Stream<Item = Result<Vec<u8>, DecodeError>>> Stream for Coalescer<S> {
    type Item = Result<Vec<u8>, DecodeError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        loop {
            if self.buf.len() >= self.size || (self.exhausted && !self.buf.is_empty()) {
                let mut other_buf = if self.buf.len() > self.size {
                    let sz = self.size;
                    self.buf.split_off(sz)
                } else {
                    Vec::new()
                };

                std::mem::swap(&mut other_buf, &mut self.buf);
                return Poll::Ready(Some(Ok(other_buf)));
            }

            if self.exhausted {
                return Poll::Ready(None);
            };

            match self.inner.as_mut().poll_next(cx) {
                Poll::Pending => return Poll::Pending,
                Poll::Ready(None) => self.exhausted = true,
                Poll::Ready(Some(Err(e))) => return Poll::Ready(Some(Err(e))),
                Poll::Ready(Some(Ok(bytes))) => {
                    if bytes.len() >= self.size && self.buf.is_empty() {
                        self.buf = bytes;
                    } else {
                        if self.buf.is_empty() {
                            let sz = self.size;
                            self.buf.reserve(sz)
                        }
                        self.buf.extend_from_slice(&bytes)
                    }
                }
            };
        }
    }
}

impl RadosBackend {
    pub fn new() -> Self {
        let mut secrets = SecretMap::new();

        let access_key_id = std::env::var("AWS_ACCESS_KEY_ID").ok();
        let secret_access_key = std::env::var("AWS_SECRET_ACCESS_KEY").ok();

        if access_key_id.is_some() && secret_access_key.is_some() {
            secrets.insert(access_key_id.unwrap(), secret_access_key.unwrap());
        }

        let rados_pool = std::env::var("CAPRA_RADOS_POOL")
            .ok()
            .expect("Must set CAPRA_RADOS_POOL");

        Self {
            rados: tokio::sync::Mutex::new(None),
            pool_name: rados_pool,
            secrets,
        }
    }

    async fn get_ioctx(&self) -> Arc<ceph::ceph::IoCtx> {
        let mut rados_locked = self.rados.lock().await;
        if rados_locked.is_none() {
            *rados_locked = Some(
                ceph::ceph::connect_to_ceph_async("admin", "./ceph.conf")
                    .await
                    .expect("Cannot connect to RADOS"),
            );
        }

        let rados_inner = rados_locked.as_ref().unwrap();
        Arc::new(
            rados_inner
                .get_rados_ioctx(&self.pool_name)
                .expect("Failed to get RADOS IoCtx"),
        )
    }

    fn object_name(&self, stream_header: &StreamHeader<'_>) -> String {
        format!("{}_{}", stream_header.bucket, stream_header.key)
    }

    fn object_name_from_parts(&self, bucket: &str, key: &str) -> String {
        format!("{}_{}", bucket, key)
    }

    fn object_name_from_req(&self, s3req: &S3Request<'_>) -> String {
        format!("{}_{}", s3req.bucket, s3req.key)
    }

    fn parse_object_name(&self, objname: &str) -> (String, String) {
        // FIXME: add proper error handling for malformed object names
        let underscore = objname.find("_").unwrap();
        (
            objname[0..underscore].into(),
            objname[underscore + 1..].into(),
        )
    }

    async fn head(
        &self,
        ioctx: &ceph::ceph::IoCtx,
        object_name: &str,
    ) -> Result<Option<(u64, String)>, Response<Body>> {
        // First we must do a stat to get content-length or return
        // a 404 if object doesn't exist.
        let (content_length, _timestamp) = match ioctx.rados_async_object_stat(&object_name).await {
            Ok((c, t)) => (c, t),
            Err(ceph::error::RadosError::ApiError(nix::errno::Errno::ENOENT)) => {
                return Ok(None);
            }
            Err(e) => {
                error!("RADOS error on stat {}:{}", object_name, e);
                return Err(status_response(StatusCode::INTERNAL_SERVER_ERROR));
            }
        };

        // Retrieve ETag.  Interpret a missing ETag as an incompletely
        // written object (we 'seal' write streams with the ETag)
        let mut etag_bytes = [0 as u8; 32];
        if let Err(io_error) = ioctx
            .rados_async_object_getxattr(&object_name, ETAG_ATTR, &mut etag_bytes)
            .await
        {
            warn!("No ETag on {}: {}, ignoring object", object_name, io_error);
            return Ok(None);
        }

        let etag = String::from_utf8_lossy(&etag_bytes);
        Ok(Some((content_length, etag.to_string())))
    }

    async fn put<S: Stream<Item = Result<Vec<u8>, DecodeError>> + Unpin>(
        &self,
        s3req: &S3Request<'_>,
        in_stream: S,
    ) -> Response<Body> {
        let ioctx = self.get_ioctx().await;
        let object_name = self.object_name_from_req(s3req);
        let content_length = s3req.content_length;

        // Wrap our incoming stream in a Coalescer: otherwise S3 PUT chunking results
        // in undesirably small write IOs to RADOS.
        let mut stream = Coalescer::new(in_stream, RADOS_WRITE_SIZE);

        debug!("RADOS put starting write stream");
        let mut sink =
            ioctx.rados_async_object_write_stream(&object_name, Some(RADOS_OPS_PER_STREAM));

        let mut md5_ctx = md5::Md5::new();

        // Feed whole body through checking for errors (including auth errors)
        // TODO: figure out how to replace this loop with with SinkExt/TryStreamExt helpers, if
        // that is possible. Just using stream.forward doesn't work because of the
        // incompatible error types.  But I don't want to convert between them:
        // I want to cache DecodeErrors before passing anything into the sink.
        debug!("RADOS put starting write proxy");
        let mut bytes_written: u64 = 0;
        while let Some(item) = stream.next().await {
            match item {
                Err(decode_error) => {
                    // TOOD tear down anything we wrote to RADOS!
                    return match decode_error.kind {
                        DecodeErrorKind::Auth(_auth_err) => {
                            status_response(StatusCode::UNAUTHORIZED)
                        }
                        DecodeErrorKind::System(_io_error) => {
                            status_response(StatusCode::INTERNAL_SERVER_ERROR)
                        }
                        _ => status_response(StatusCode::BAD_REQUEST),
                    };
                }
                Ok(bytes) => {
                    bytes_written += bytes.len() as u64;
                    md5_ctx.update(&bytes);
                    debug!("RADOS put feeding {} to sink", bytes.len());
                    if let Err(io_error) = sink.feed(bytes).await {
                        error!("RADOS write error {}", io_error);
                        return status_response(StatusCode::INTERNAL_SERVER_ERROR);
                    }
                }
            }
        }

        // A short PUT!  A chunked request can get here if it
        // lied about content length and then sent a series of
        // valid chunks that didn't add up top it.
        if bytes_written != content_length {
            error!(
                "Malformed PUT to {}/{}: expected {}, got {}",
                s3req.bucket, s3req.key, content_length, bytes_written
            );
            return status_response(StatusCode::BAD_REQUEST);
        }

        debug!("RADOS put flushing sink");
        if let Err(io_error) = sink.flush().await {
            error!("RADOS write error {}", io_error);
            return status_response(StatusCode::INTERNAL_SERVER_ERROR);
        }

        let etag_str_inner = format!("{:x}", md5_ctx.finalize());
        debug!("RADOS put writing xattr");
        let setxattr_r = ioctx
            .rados_async_object_setxattr(&object_name, ETAG_ATTR, etag_str_inner.as_bytes())
            .await;
        if let Err(e) = setxattr_r {
            error!("RADOS setxattr error {}", e);
            return status_response(StatusCode::INTERNAL_SERVER_ERROR);
        }

        debug!("RADOS put responding");

        Response::builder()
            .status(StatusCode::OK)
            .header("ETag", &format!("\"{}\"", &etag_str_inner))
            .body(Body::empty())
            .unwrap()
    }

    // The explicit 'a lifetime is needed because of a strange compiler
    // error on some async functions (https://github.com/rust-lang/rust/issues/63033)
    async fn delete_objects<'a>(
        &'a self,
        mut frontend_auth: Box<dyn Authorizer>,
        s3req: &S3Request<'a>,
        body: Body,
    ) -> Response<Body> {
        let encoded = hyper::body::to_bytes(body).await.unwrap();
        frontend_auth.update(&encoded);

        if let Err(auth_err) = frontend_auth.finalize() {
            error!("401 {}", auth_err.get_reason());
            return status_response(StatusCode::UNAUTHORIZED);
        }

        let decoded =
            match yaserde::de::from_str::<DeleteObjectsRequest>(&String::from_utf8_lossy(&encoded))
            {
                Err(e) => {
                    error!("Deserialization error on DeleteObjects: {}", e);
                    return status_response(StatusCode::BAD_REQUEST);
                }
                Ok(v) => v,
            };

        let ioctx = self.get_ioctx().await;
        let mut exec_futs = FuturesUnordered::new();
        for object in decoded.objects {
            let rados_obj = self.object_name_from_parts(s3req.bucket, &object.key);
            let ioctx = ioctx.clone();
            let fut = async move {
                let r = ioctx.rados_async_object_remove(&rados_obj).await;
                (r, rados_obj)
            };
            exec_futs.push(fut);
        }

        while let Some((result, rados_obj)) = exec_futs.next().await {
            if let Err(e) = result {
                error!("DeleteObjects RADOS error deleting {}: {}", rados_obj, e);
                return status_response(StatusCode::INTERNAL_SERVER_ERROR);
            } else {
                info!("DeleteObjects {}", rados_obj);
            }
        }

        // TODO fill out response body + respect quiet flag
        status_response(StatusCode::OK)
    }
}

// Must explicitly declare ourselves Send+Sync because the Rados
// type from ceph-rust is not.
unsafe impl Send for RadosBackend {}
unsafe impl Sync for RadosBackend {}

#[async_trait]
/// A functionally limited 'toy' RADOS backend that maps S3 objects to single RADOS
/// objects (no striping) and implements a crude S3 compatibility layer to handle
/// proxied S3 requests.
impl Backend for RadosBackend {
    async fn put(&self, stream_header: StreamHeader<'static>) -> BackendPutStreamHandle {
        debug!("RADOS put {}", stream_header);

        // FIXME: in order to stash the ETag efficiently, we need to see the StreamFooter
        // as well.  Currently reads of written-back objects are broken (see the write-through
        // put() below where we write an etag properly and thereby end up with a readable
        // object)

        Box::new(RadosBackendStream::new(
            self.get_ioctx().await,
            stream_header.bucket.to_string(),
            stream_header.key.to_string(),
            &self.object_name(&stream_header),
            stream_header.content_length,
        ))
    }

    fn get_secrets(&self) -> &SecretMap {
        &self.secrets
    }

    async fn proxy(
        &self,
        frontend_auth: Box<dyn Authorizer>,
        _dcache: &Arc<DCache>,
        s3req: &S3Request<'_>,
        body: Body,
    ) -> Response<Body> {
        // TODO hook in dcache snoop

        // We do not implement PutObject here -- rely on PUTs aways flowing through
        // the journal->expirer->Self::put path.
        match s3req.verb {
            S3Verb::GetObject => {
                // This request should have already been authenticated
                assert!(frontend_auth.finalized());

                let ioctx = self.get_ioctx().await;
                let object_name = self.object_name_from_req(s3req);

                let (content_length, etag) = match self.head(&ioctx, &object_name).await {
                    Ok(None) => return status_response(StatusCode::NOT_FOUND),
                    Ok(Some((content_length, etag))) => (content_length, etag),
                    Err(response) => return response,
                };

                // FIXME: combine/overlap stat, getxattr and first read: read a small amount of bytes
                // and then start the stream from after that read.

                // Object exists, proceed to read.
                let response_body = if content_length > 0 {
                    let read_stream = StreamBinder::new(ioctx, move |io| {
                        io.rados_async_object_read_stream(
                            &object_name,
                            None,
                            None,
                            Some(content_length),
                        )
                        // Transform our stream of Vec/RadosResult into a stream of Bytes/Box<Error>
                        .map(|r| r.map(|v| Bytes::from(v)).map_err(|e| Box::new(e)))
                    });

                    Body::wrap_stream(read_stream)
                } else {
                    Body::empty()
                };

                Response::builder()
                    .status(StatusCode::OK)
                    .header("Content-Length", &content_length.to_string())
                    .header("ETag", &format!("\"{}\"", etag))
                    .body(response_body)
                    .unwrap()
            }
            S3Verb::PutObject => {
                if s3req.chunked_mode {
                    let stream = AuthValidator::new(ChunkedBodyDecoder::new(body), frontend_auth)
                        .map(|v| v.map(|v| v.to_vec()));
                    self.put(s3req, stream).await
                } else {
                    let stream = PlainAuthValidator::new(
                        body.into_stream().map_err(|e| e.into()),
                        frontend_auth,
                    )
                    .map(|v| v.map(|v| v.to_vec()));

                    self.put(s3req, stream).await
                }
            }
            S3Verb::ListObjectsV2 => {
                let lorq = match ListObjectsV2Request::from_header(s3req.head) {
                    Err(e) => {
                        error!("Invalid ListObjectsV2 query params: {}", e);
                        return status_response(StatusCode::BAD_REQUEST);
                    }
                    Ok(lorq) => lorq,
                };
                let lors = match self
                    .list(s3req.bucket, frontend_auth.get_secret_id(), &lorq)
                    .await
                {
                    Ok(lors) => lors,
                    Err(response) => return response,
                };

                lors.into_response()
            }
            S3Verb::DeleteObject => {
                let ioctx = self.get_ioctx().await;
                let object_name = self.object_name_from_req(s3req);
                match ioctx.rados_async_object_remove(&object_name).await {
                    Ok(()) => status_response(StatusCode::NO_CONTENT),
                    Err(io_error) => {
                        error!("RADOS rm error on {}: {}", object_name, io_error);
                        status_response(StatusCode::INTERNAL_SERVER_ERROR)
                    }
                }
            }
            S3Verb::DeleteObjects => self.delete_objects(frontend_auth, s3req, body).await,
            S3Verb::HeadBucket => {
                // For this backend, buckets are just a prefix to the object name,
                // so every possible bucket kinda-sorta exists.
                debug!("RADOS answering 200 to HeadBucket {}", s3req.bucket);
                status_response(StatusCode::OK)
            }
            _ => {
                // Oops, caller tried to do something other than a simple
                // put/get/list.
                error!(
                    "Unsupported verb with RADOS backend: {:?} {:?} {}",
                    s3req.verb, s3req.head.method, s3req.head.uri
                );
                status_response(StatusCode::INTERNAL_SERVER_ERROR)
            }
        }
    }

    async fn remote_read(
        &self,
        _frontend_auth_id: Option<&str>,
        bucket: &str,
        method: http::Method,
        path: &str,
        _query: &str,
    ) -> Response<Body> {
        if method == Method::HEAD {
            // Path is like "/key" so trim off first char
            let ioctx = self.get_ioctx().await;
            let object_name = self.object_name_from_parts(bucket, &path[1..]);
            match self.head(&ioctx, &object_name).await {
                Ok(None) => status_response(StatusCode::NOT_FOUND),
                Ok(Some((content_length, etag))) => Response::builder()
                    .status(StatusCode::OK)
                    .header("Content-Length", &content_length.to_string())
                    .header("ETag", &format!("\"{}\"", etag))
                    .body(Body::empty())
                    .unwrap(),
                Err(response) => response,
            }
        } else {
            debug!("No-op 200 {} {} {}", method, bucket, path);
            empty_response(StatusCode::OK).unwrap()
        }
    }

    async fn delete(&self, stream_header: &StreamHeader<'static>) -> Result<(), Error> {
        let object_name = self.object_name(&stream_header);
        debug!("RADOS delete '{}' ({})", object_name, stream_header);
        let result = self
            .get_ioctx()
            .await
            .rados_async_object_remove(&object_name)
            .await;

        // Transform ENOENT into success
        if let Err(e) = &result {
            if let ceph::error::RadosError::ApiError(errno) = e {
                if *errno == nix::errno::Errno::ENOENT {
                    return Ok(());
                }
            }
        }

        result.map_err(|e| {
            warn!("RADOS delete error: {}", e);
            Error::new(ErrorKind::RemoteHttpError)
        })
    }

    async fn list(
        &self,
        bucket: &str,
        _frontend_auth_id: Option<&str>,
        lor: &ListObjectsV2Request,
    ) -> Result<ListObjectsV2Response, Response<Body>> {
        let ioctx = self.get_ioctx().await;
        let stream = ioctx.rados_async_object_list().map_err(|e| {
            error!("RADOS error on object listing: {}", e);
            status_response(StatusCode::INTERNAL_SERVER_ERROR)
        })?;

        let items: Vec<Object> = match stream
            .try_filter_map(|ceph_object| async move {
                let (obj_bucket, key) = self.parse_object_name(&ceph_object.name);
                if obj_bucket != bucket
                    || !key.starts_with(lor.prefix.as_ref().map(|s| s.as_str()).unwrap_or(""))
                {
                    Ok(None)
                } else {
                    // FIXME: do we stat() each object to get this?  Or just accept passing
                    // through dummy values?
                    let last_modified = Utc::now().to_rfc3339_opts(SecondsFormat::Millis, true);

                    Ok(Some(Object {
                        key,
                        last_modified,
                        e_tag: "".into(),
                        size: 0,
                        storage_class: "STANDARD".into(),
                    }))
                }
            })
            .try_collect()
            .await
        {
            Err(e) => {
                error!("RADOS error listing bucket {}: {}", bucket, e);
                return Err(status_response(StatusCode::INTERNAL_SERVER_ERROR));
            }
            Ok(vec) => vec,
        };

        // TODO: pagination + continuation

        Ok(ListObjectsV2Response {
            name: bucket.into(),
            prefix: lor.prefix.clone().unwrap_or("".into()),
            key_count: items.len() as u64,
            max_keys: lor.max_keys.unwrap_or(DEFAULT_MAX_KEYS) as u64,
            delimiter: lor.delimiter.clone().unwrap_or("".into()),
            is_truncated: false,
            next_continuation_token: None,
            continuation_token: lor.continuation_token.clone(),
            start_after: lor.start_after.clone(),
            contents: items,
            common_prefixes: Vec::new(),
        })
    }
}

struct RadosBackendStream {
    bucket: String,
    key: String,
    content_length: u64,
    cursor: u64,
    rados_stream: StreamBinder<ceph::ceph::IoCtx, ceph::ceph::WriteSink<'static>>,
}

impl RadosBackendStream {
    pub fn new(
        ioctx: Arc<ceph::ceph::IoCtx>,
        bucket: String,
        key: String,
        object_name: &str,
        content_length: u64,
    ) -> Self {
        // FIXME: we are conflating our chunk size (kb, designed for local nvme) with
        // the size of ops we send to RADOS (should be mb for situations where latency
        // doesn't matter much, like cache expiry).  Should put a buffering layer
        // in between.  Compensating by having lots of rados ops in flight per stream.
        let concurrency = 32;

        let rados_stream = StreamBinder::new(ioctx, |ioctx| {
            ioctx.rados_async_object_write_stream(object_name, Some(concurrency))
        });

        Self {
            bucket,
            key,
            content_length,
            cursor: 0,
            rados_stream,
        }
    }
}

#[async_trait]
impl BackendPutStream for RadosBackendStream {
    async fn write(&mut self, data: std::io::Result<&[u8]>) -> std::io::Result<()> {
        let data = match data {
            Ok(d) => d,
            Err(_e) => {
                // Drop out: Err in write is a signal that the sender finished unhealthily
                // FIXME: no atomicity of overwrites: if we have already overwritten parts of an object we're
                // going to leave a corrupt mess behind.
                return Ok(());
            }
        };

        // Keeping track of an offset for error checking: the actual
        // write offset is implicit in the rados WriteSink
        if self.cursor + data.len() as u64 > self.content_length {
            return Err(std::io::Error::from_raw_os_error(34));
        }
        self.cursor += data.len() as u64;

        // We must copy because the WriteSink needs to take ownership of
        // the buffer (it may sit on it for a while before actually
        // firing off the rados OP)
        let mut v = Vec::new();
        v.extend_from_slice(data);

        let r = self.rados_stream.feed(v).await;
        if r.is_err() {
            // TODO: neater error types, UNIX errnos just used as a placeholder
            return Err(std::io::Error::from_raw_os_error(5));
        }

        Ok(())
    }

    async fn finalize(mut self: Box<Self>) -> std::io::Result<()> {
        if self.rados_stream.flush().await.is_err() {
            return Err(std::io::Error::from_raw_os_error(5));
        }
        Ok(())
    }

    fn get_key(&self) -> (&str, &str) {
        (&self.bucket, &self.key)
    }

    fn get_content_length(&self) -> u64 {
        self.content_length
    }
}
