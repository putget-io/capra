use async_trait::async_trait;
use futures::StreamExt;
use log::*;
use owning_ref::{OwningHandle, OwningRef};
use std::borrow::{Borrow, Cow};
use std::convert::TryFrom;
use std::ops::{Deref, DerefMut};
use std::option::Option::None;
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};
use std::{env, fmt, io};

use chrono::prelude::*;
use tokio;
use tokio::sync::{OwnedSemaphorePermit, Semaphore};
use tokio::task::JoinHandle;
use tokio_stream::wrappers::ReceiverStream;

use http::request::Builder as RequestBuilder;
use http::uri::Scheme;
use http::Uri;
use hyper::body::Bytes;
use hyper::client::connect::Connect;
use hyper::client::{Client, HttpConnector, ResponseFuture};
use hyper::{Body, Method, Request, Response, Result as HyperResult, StatusCode};
use hyper_tls::HttpsConnector;

use sha2;
use sha2::Digest;
use yaserde;

use super::super::decoder::ChunkedBodyDecoder;
use super::{Backend, BackendHandle, BackendPutStream, Error, ErrorKind};
use crate::server::backend::BackendPutStreamHandle;
use crate::server::dcache::{DCache, ETag, EntryStream};
use crate::server::decoder::{AuthValidator, DecodeError};
use crate::server::frontend::request::{S3Request, S3Verb};
use crate::server::journal::StreamHeader;
use crate::server::s3_types::{ListObjectsV2Request, ListObjectsV2Response};
use crate::util::auth::get_chunk_seed_signature;
use crate::util::auth::{Authorizer, SecretMap, STREAMING_PAYLOAD};
use crate::util::format::dump_buffer;
use crate::util::http::get_etag;
use capralib::encoder::ChunkedEncoder;
use capralib::s3_client_auth::{
    canonical_request_client, sign_request, sign_request_chunked, ClientAuthConfig,
    ClientAuthKeyState, ClientAuthRequestState,
};

const EMPTY_STRING_SHA256: &str =
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

// AWSv4 chunked body encoding
const CHUNK_SIZE: usize = 64 * 1024;

// Important that we have two separate pools to consume from here, so that
// our journal expiry (not latency sensitive) can't crowd out proxied frontend
// operations (latency sensitive).  This could be done in more clever ways with
// a shared pool+prioritization, but this is a moderately robust approach for now.
const MAX_BACKGROUND_REQUESTS: usize = 128;
const MAX_FRONTEND_REQUESTS: usize = 128;

// Wrap hyper Client in a trait so that I can use it in `dyn` references
// where the Client's C can be either HttpConnector or HttpsConnector
trait GenericClient: Send + Sync + 'static {
    fn request(&self, req: Request<Body>) -> ResponseFuture;
}

impl<C: Connect + Send + Sync + Clone + 'static> GenericClient for Client<C, Body> {
    fn request(&self, req: Request<Body>) -> ResponseFuture {
        Client::request(self, req)
    }
}

fn get_client(https: bool) -> Arc<dyn GenericClient> {
    if https {
        let https = HttpsConnector::new();
        let client = Client::builder().build::<_, Body>(https);
        Arc::new(client)
    } else {
        let connector = HttpConnector::new();
        // TODO: expose ServerOptions through here so that we can get the local address
        //if local_address.len() > 0 {
        //    connector.set_local_address(Some(local_address.parse::<IpAddr>()?));
        //}
        let client_builder = &mut Client::builder();
        Arc::new(client_builder.build(connector))
    }
}

pub struct S3Backend {
    // Start of URL, like http://foo.bar:123
    base_url: String,

    // Http Host header
    host: String,

    // If authentication is disabled on the frontend, use this access key
    // to talk to the backend
    default_access_key_id: String,

    // Mapping of access_key_id to secret_access_key
    secrets: SecretMap,

    // Region for AWSv4 auth purposes (we don't care about this ourselves)
    region: String,

    /// Limit the number of concurrent requests from background tasks (e.g expiry)
    // Arc so that we can create OwnedSemaphorePermit and pass it into BackendStream
    background_semaphore: Arc<Semaphore>,

    /// Limit concurrent requests from frontend (e.g. proxied end user requests)
    frontend_semaphore: Semaphore,

    client: Arc<dyn GenericClient>,
}

#[derive(Debug, Clone)]
pub struct ConfigError {
    why: String,
}

impl ConfigError {
    fn new(why: &str) -> Self {
        Self {
            why: why.to_string(),
        }
    }
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ConfigError<{}>", self.why)
    }
}

impl S3Backend {
    // For use during data journal expiry, where we will
    // combine the key from the journal with the bucket's URL
    fn get_url(&self, bucket: &str, key: &str) -> String {
        format!(
            "{}/{}/{}",
            self.base_url,
            bucket,
            key //percent_encoding::percent_encode(key.as_bytes(), &KEY_ENCODE_CHARS) //key
        )
    }

    pub fn get_base_url(&self) -> &String {
        &self.base_url
    }

    pub fn get_auth_config(
        &self,
        frontend_secret_id: Option<&str>,
    ) -> Result<ClientAuthConfig, ConfigError> {
        let secret_id = match frontend_secret_id {
            Some(s) => s.to_string(),
            None => self.default_access_key_id.clone(),
        };

        let secret = match self.secrets.get(&secret_id) {
            Some(secret) => secret.clone(),
            None => {
                return Err(ConfigError::new(&format!(
                    "Access key ID {} not found",
                    secret_id
                )))
            }
        };

        Ok(ClientAuthConfig {
            region: self.region.clone(),
            access_key_id: secret_id,
            secret_access_key: secret,
            host: self.host.clone(),
        })
    }
}

pub struct S3PutStream {
    stream_header: StreamHeader<'static>,
    // Encoder populated if stream_header.content_length > 0
    encoder_tx: Option<tokio::sync::mpsc::Sender<Result<Bytes, DecodeError>>>,
    join_handle: JoinHandle<HyperResult<Response<Body>>>,
    _sem_permit: OwnedSemaphorePermit,
}

fn hex_digits(n: usize) -> usize {
    // Count the number of hex digits in chunk_size
    if n <= 0xf {
        1
    } else if n <= 0xff {
        2
    } else if n <= 0xfff {
        3
    } else if n <= 0xffff {
        4
    } else if n <= 0xfffff {
        5
    } else if n <= 0xffffff {
        6
    } else {
        // Slow fallback for very large chunk sizes
        format!("{:x}", n).len()
    }
}

fn chunk_encoded_size(chunk_size: usize) -> usize {
    hex_digits(chunk_size)
        // ";chunk-signature=26eedac720c9b7d277976e3efca078e31db0dccac3c5a872349f61361b6923c5".len()
        + 81
        + 2
        + chunk_size
        + 2
}

impl S3PutStream {
    fn open(
        backend: &S3Backend,
        stream_header: StreamHeader<'static>,
        sem_permit: OwnedSemaphorePermit,
    ) -> Self {
        // FIXME: This should go in the StreamHeader
        let content_type = "application/octet-stream";

        // If this is empty/None, backend's default auth ID will be used.
        let frontend_auth_id: Option<&str> = if stream_header.owner.is_empty() {
            None
        } else {
            Some(&stream_header.owner)
        };

        let payload_digest = if stream_header.content_length == 0 {
            &EMPTY_STRING_SHA256
        } else {
            &STREAMING_PAYLOAD
        };

        // FIXME: don't unwrap, propagate error for e.g. missing key
        let auth_config = backend.get_auth_config(frontend_auth_id).unwrap();
        let now: DateTime<Utc> = Utc::now();
        let daily_auth_state = ClientAuthKeyState::new(&now, &auth_config);
        let mut request_auth_state =
            ClientAuthRequestState::new(&now, &auth_config.host, payload_digest);

        request_auth_state.signed_header(
            "x-amz-decoded-content-length",
            &stream_header.content_length.to_string(),
        );

        let mut req_builder = Request::builder()
            .method(Method::PUT)
            .uri(backend.get_url(&stream_header.bucket, &stream_header.key))
            .header("content-type", content_type)
            .header("host", &backend.host);

        let client = backend.client.clone();

        let (req, encoder_tx) = if stream_header.content_length > 0 {
            let mut chunked_length = 0;
            let full_chunks: usize = (stream_header.content_length / CHUNK_SIZE as u64) as usize;
            chunked_length += full_chunks * chunk_encoded_size(CHUNK_SIZE);
            let remainder = (stream_header.content_length % CHUNK_SIZE as u64) as usize;
            if remainder > 0 {
                chunked_length += chunk_encoded_size(remainder);
            }
            chunked_length += chunk_encoded_size(0);
            req_builder = req_builder
                .header(
                    "x-amz-decoded-content-length",
                    format!("{}", stream_header.content_length),
                )
                .header("content-length", chunked_length);

            let (req_builder, seed_sig) =
                sign_request_chunked(req_builder, &daily_auth_state, &request_auth_state);

            let (encoder_tx, encoder_rx) = tokio::sync::mpsc::channel(16);

            let encoder = ChunkedEncoder::new(
                ReceiverStream::new(encoder_rx),
                daily_auth_state,
                request_auth_state,
                &seed_sig,
                stream_header.content_length,
                CHUNK_SIZE,
            );

            let body = Body::wrap_stream(encoder);
            let req = req_builder.body(body).unwrap();

            (req, Some(encoder_tx))
        } else {
            let req = req_builder
                .header(
                    "content-length",
                    format!("{}", stream_header.content_length),
                )
                .body(Body::empty())
                .unwrap();

            (req, None)
        };

        let join_handle = tokio::spawn(client.request(req));

        return Self {
            stream_header,
            encoder_tx,
            join_handle,
            _sem_permit: sem_permit,
        };
    }
}

#[async_trait]
impl BackendPutStream for S3PutStream {
    async fn write(&mut self, data: std::io::Result<&[u8]>) -> io::Result<()> {
        // FIXME: gratuitious alloc+copy in copy_from_slice, because
        // our Expirer read path provides &[u8] but hyper's Body
        // structures expect Bytes
        if self.stream_header.content_length == 0 {
            if let Ok(bytes) = data {
                // Allow for zero-length streams to spuriously call write() with
                // an empty array if that makes their code simpler.
                assert_eq!(bytes.len(), 0);
                return Ok(());
            }
        }

        // FIXME: spurious data copy.  This is because we're using a channel to
        // get data into our ChunkedEncoder, and we can't just throw a slice-reference
        // into a channel and forget about it.  This should be refactored.
        let data = data.map_err(|e| e.into()).map(|v| {
            let mut copy = Vec::<u8>::new();
            copy.extend_from_slice(v);
            Bytes::from(copy)
        });

        match self.encoder_tx.as_mut().unwrap().send(data).await {
            Ok(_) => Ok(()),
            Err(e) => {
                debug!("S3PutStream send error: {} (receiver closed?)", e);
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!("Error sending bytes, conn closed? : {}", e),
                ))
            }
        }
    }

    async fn finalize(self: Box<Self>) -> io::Result<()> {
        // FIXME: it doesn't make sense to use io::Result here, when our
        // typical error cases are things like remote server unavailable, or
        // an http-style error.  We're munging errors for the moment.
        debug!("Joining HTTP request thread key={}", self.stream_header.key);
        match self.join_handle.await {
            Ok(hyper_result) => match hyper_result {
                Ok(http_resp) => {
                    debug!(
                        "Backend response: {} key={}",
                        http_resp.status(),
                        self.stream_header.key
                    );
                    if http_resp.status() == 200 {
                        Ok(())
                    } else {
                        Err(io::Error::new(
                            io::ErrorKind::Other,
                            format!("Backend http status {}", http_resp.status()),
                        ))
                    }
                }
                Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
            },
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
        }
    }

    fn get_key(&self) -> (&str, &str) {
        (&self.stream_header.bucket, &self.stream_header.key)
    }

    fn get_content_length(&self) -> u64 {
        self.stream_header.content_length
    }
}

impl S3Backend {
    // TODO: more general configuration of authentication:
    // IMPORTANT: we do no authorization checks!  That responsibility is delegated to the backend,
    // which is perfectly fine when only a single auth ID is in play.  However, once multiple
    // identities are in play, it is not necessarily true that a key written by one user should
    // be readable by another user (without doing a whole IAM/ACL implementation, yuck), so be
    // careful about how to present/document any multi-user auth.
    //
    // e.g. General config mode v1:
    //  - Specify a dict of access key ID to secret_access_key
    //  - Can map some frontend to a different backend bucket
    // e.g. General config mode v2:
    //  - Enable mapping frontend bucket names to backend bucket names
    //  - Enable mapping frontend auth IDs to backend auth IDs
    //  - Enable plugging in some external KV store for access keys

    /// Currently just implement's simple configuration of a single auth ID used for both
    /// frontend and backend access, from AWS_* environment variables
    pub fn configure_simple(base_url: &str) -> Result<BackendHandle, ConfigError> {
        if base_url.ends_with("/") {
            // Other code does naive concatenations of base url with paths, so need
            // it delivered without trailing slash
            return Err(ConfigError::new("Url must not have trailing slash"));
        }

        let uri: Uri = match base_url.parse() {
            Ok(u) => u,
            Err(e) => return Err(ConfigError::new(&format!("Malformed URL: {}", e))),
        };

        let scheme = match uri.scheme() {
            Some(s) => s,
            None => return Err(ConfigError::new("Missing http:// or https:// on URL")),
        };

        let host = match base_url.rsplit("//").next() {
            Some(substr) => substr,
            None => return Err(ConfigError::new(&format!("Malformed URL {}", base_url))),
        };

        let access_key_id = match env::var("AWS_ACCESS_KEY_ID") {
            Ok(aki) => aki,
            Err(_) => return Err(ConfigError::new("AWS_ACCESS_KEY_ID not set")),
        };

        let secret_access_key = match env::var("AWS_SECRET_ACCESS_KEY") {
            Ok(s) => s,
            Err(_) => return Err(ConfigError::new("AWS_SECRET_ACCESS_KEY not set")),
        };

        let region = match env::var("AWS_DEFAULT_REGION") {
            Ok(s) => s,
            Err(_) => String::from("us-east-1"),
        };

        let mut secrets = SecretMap::new();
        secrets.insert(access_key_id.clone(), secret_access_key);

        Ok(Arc::new(Self {
            base_url: base_url.to_string(),
            host: host.to_string(),
            default_access_key_id: access_key_id,
            secrets,
            region,
            background_semaphore: Arc::new(Semaphore::new(MAX_BACKGROUND_REQUESTS)),
            frontend_semaphore: Semaphore::new(MAX_FRONTEND_REQUESTS),
            client: get_client(*scheme == Scheme::HTTPS),
        }))
    }

    fn proxy_prepare(
        &self,
        frontend_auth_id: Option<&str>,
        s3req: &S3Request<'_>,
        has_body: bool,
    ) -> (RequestBuilder, ClientAuthKeyState, ClientAuthRequestState) {
        // Notes on proxying:
        //  - We have to authenticate the request, because
        //    AWS auth signatures are not proxyable (they depend on Host
        //    which may be different on our frontend than our backend),
        //    and because we may use a different user on the backend
        //    than the frontend.
        //  - We have to decode the request body if it is chunked, because
        //    in a chunked request the auth signatures are interwoven
        //    with the body.

        // TODO: implement frontend/backend configs with leading path
        // components (i.e. don't assume path is just /bucket/key)
        let url = format!(
            "{}{}",
            self.get_base_url(),
            s3req.head.uri.path_and_query().unwrap().as_str()
        );

        debug!(
            "Proxy: {} to {}",
            s3req.head.uri.path_and_query().unwrap().as_str(),
            self.get_base_url()
        );

        let mut req_builder = Request::builder().method(&s3req.head.method).uri(url);

        let host: Cow<'_, str> = if s3req.virtualhost {
            // This is a virtualhost request.  We pass the URL through unchanged and compose
            // a new host header by joining the bucket name and the backend's host.

            // Safe to unwrap because the main handler entry point already checked it
            let old_host = s3req.head.headers.get("host").unwrap().to_str().unwrap();
            let mut rewritten_host =
                String::with_capacity(self.host.len() + 1 + s3req.bucket.len());
            rewritten_host.push_str(s3req.bucket);
            rewritten_host.push_str(".");
            rewritten_host.push_str(self.host.as_str());
            let host = rewritten_host.into();
            debug!("proxy: host {} (was {})", host, old_host);
            host
        } else {
            // This is a path-style request.  We pass the URL through unchanged and
            // the host header is set to the plain backend host
            if !s3req.bucket.is_empty() {
                assert!(s3req.head.uri.path()[1..].starts_with(s3req.bucket));
            }
            let host = Cow::from(&self.host);
            debug!("proxy: host {} (not virtualhost request)", host);
            host
        };
        req_builder = req_builder.header("host", &host as &str);

        // Copy headers
        for (k, v) in s3req.head.headers.iter() {
            let k = k.as_str();
            if k == "authorization"
                || k == "x-amz-date"
                || k == "x-amz-content-sha256"
                || k == "host"
            {
                debug!("Not proxying header {}", k);
                continue;
            } else {
                // FIXME: unsafe unwrap
                let string_v = v.to_str().unwrap();
                debug!("Proxying header {}={}", k, string_v);
                req_builder = req_builder.header(k, string_v);
            }
        }

        // FIXME: don't unwrap, propagate error for e.g. missing key
        let auth_config = self.get_auth_config(frontend_auth_id).unwrap();
        let now: DateTime<Utc> = Utc::now();

        let payload_digest: &str = if has_body {
            if s3req.chunked_mode {
                STREAMING_PAYLOAD
            } else {
                if let Some(hv) = s3req.head.headers.get("x-amz-content-sha256") {
                    if let Ok(s) = hv.to_str() {
                        // String value of x-amz-content-sha256 header
                        s
                    } else {
                        // Corrupt x-amz-content-sha256 header
                        // TODO: introduce much earlier validation of the
                        // headers we care about -- I don't want to have
                        // to potentially return 400s any time I try and
                        // read a header as a string in case to_str fails.
                        "CORRUPT_X_AMZ_CONTENT_SHA256"
                    }
                } else {
                    // FIXME: validate presence of this much earlier.
                    "MISSING_CONTENT_HEADER"
                }
            }
        } else {
            // Hash of empty string
            EMPTY_STRING_SHA256
        };

        assert!(has_body || payload_digest == EMPTY_STRING_SHA256);

        let daily_auth_state = ClientAuthKeyState::new(&now, &auth_config);
        let request_auth_state = ClientAuthRequestState::new(&now, &host, &payload_digest);

        debug!("proxy_prepare: payload_digest {}", &payload_digest);
        // Return a Request::Builder with all the auth + headers ready to go,
        // now tag on the appropriate kind of body
        (
            sign_request(
                req_builder,
                &payload_digest,
                &daily_auth_state,
                &request_auth_state,
            ),
            daily_auth_state,
            request_auth_state,
        )
    }
}

/// Note: only use this for background expiry ops.  For requests proxied from
/// the frontend, we just pass the response straight back.
async fn hyper_result_to_backend_result(result: HyperResult<Response<Body>>) -> Result<(), Error> {
    match result {
        Ok(r) => {
            let (head, body) = r.into_parts();
            debug!("Backend response {}", head.status);
            match head.status {
                StatusCode::UNAUTHORIZED => {
                    warn!(
                        "HTTP 401 Unauthorized: {}",
                        String::from_utf8_lossy(&hyper::body::to_bytes(body).await.unwrap())
                    );
                    Err(Error {
                        inner: ErrorKind::AuthError,
                    })
                }
                StatusCode::FORBIDDEN => {
                    warn!(
                        "HTTP 403 Forbidden: {}",
                        String::from_utf8_lossy(&hyper::body::to_bytes(body).await.unwrap())
                    );
                    Err(Error {
                        inner: ErrorKind::AuthError,
                    })
                }
                StatusCode::OK | StatusCode::NO_CONTENT => Ok(()),
                _ => {
                    warn!(
                        "HTTP {:?}: {}",
                        head.status,
                        String::from_utf8_lossy(&hyper::body::to_bytes(body).await.unwrap())
                    );
                    Err(Error {
                        inner: ErrorKind::RemoteHttpError,
                    })
                }
            }
        }
        Err(e) => {
            debug!("Backend non-response error {}", e);
            // We didn't get any response at all
            Err(Error {
                inner: ErrorKind::RemoteHttpError,
            })
        }
    }
}

#[async_trait]
impl Backend for S3Backend {
    fn get_region(&self) -> &str {
        &self.region
    }

    async fn put(&self, stream_header: StreamHeader<'static>) -> BackendPutStreamHandle {
        let sem_guard = self
            .background_semaphore
            .clone()
            .acquire_owned()
            .await
            .unwrap();

        debug!(
            "S3Backend::open: key={} available_permits={}/{}",
            stream_header.key,
            self.background_semaphore.available_permits(),
            MAX_BACKGROUND_REQUESTS
        );

        Box::new(S3PutStream::open(self, stream_header, sem_guard))
    }

    fn get_secrets(&self) -> &SecretMap {
        &self.secrets
    }

    async fn proxy(
        &self,
        frontend_auth: Box<dyn Authorizer>,
        dcache: &Arc<DCache>,
        s3req: &S3Request<'_>,
        body: Body,
    ) -> Response<Body> {
        info!("Proxying {}", s3req);
        // FIXME: within proxy_prepare we calculate authorization
        // header which overlaps a bunch with the seed signature
        // calculation in proxy_chunked_body
        let (req_builder, daily_auth_state, request_auth_state) = self.proxy_prepare(
            frontend_auth.get_secret_id(),
            &s3req,
            s3req.content_length > 0,
        );

        // Handling chunked bodies: in order to proxy these in a streaming
        // way, we have to also used a chunked encoding onward to the
        // backend, because we don't know the content sha up front.

        // Request has a body.  If Chunked we must decode it,
        // otherwise we can pass it through.
        let req = if s3req.chunked_mode {
            let req_body = proxy_chunked_body(
                frontend_auth,
                &req_builder,
                body,
                s3req.content_length,
                daily_auth_state,
                request_auth_state,
            );
            req_builder.body(req_body).unwrap()
        } else {
            req_builder.body(body).unwrap()
        };

        // Note: ordinarily, to authenticate a nonchunked
        // request, we must read to the end to compare the
        // content SHA256 with the x-amz-content-sha256 header.
        // However, when proxying we do not do this: if the content
        // doesn't match the header, the backend will detect that
        // for themselves.  This saves us the overhead of SHA256
        // hashing every byte of the request body when proxying
        // a non-chunked PUT.

        let _permit = self.frontend_semaphore.acquire().await.unwrap();
        let response = self.client.request(req).await;
        match response {
            Ok(http_resp) => {
                debug!("proxying response {}", http_resp.status());
                if http_resp.status() == StatusCode::OK {
                    maybe_intercept_response(dcache, s3req, http_resp)
                } else {
                    http_resp
                }
            }
            Err(e) => {
                error!("error while proxying: {}", e);
                // Give a 503 back to the originating client: this is the best we can do
                // for a generic inability to connect.
                Response::builder()
                    .status(StatusCode::SERVICE_UNAVAILABLE)
                    .body(Body::empty())
                    .unwrap()
            }
        }
    }

    /// GET/HEAD operations on a single object
    async fn remote_read(
        &self,
        frontend_auth_id: Option<&str>,
        bucket: &str,
        method: Method,
        path: &str,
        query: &str,
    ) -> Response<Body> {
        let mut req_builder = Request::builder()
            .method(method)
            .uri(format!("{}/{}{}?{}", self.base_url, bucket, path, query))
            .header("host", &self.host);

        let auth_config = self.get_auth_config(frontend_auth_id).unwrap();
        let now: DateTime<Utc> = Utc::now();
        let daily_auth_state = ClientAuthKeyState::new(&now, &auth_config);
        let request_auth_state =
            ClientAuthRequestState::new(&now, &auth_config.host, &EMPTY_STRING_SHA256);

        req_builder = sign_request(
            req_builder,
            &EMPTY_STRING_SHA256,
            &daily_auth_state,
            &request_auth_state,
        );
        let _ = self.frontend_semaphore.acquire().await.unwrap();
        let req = req_builder.body(Body::empty()).unwrap();
        match self.client.request(req).await {
            Err(e) => {
                // No response received, synthesize an error response
                error!("Error in ListObjectsV2: {}", e);
                return Response::builder()
                    .status(StatusCode::SERVICE_UNAVAILABLE)
                    .body(Body::empty())
                    .unwrap();
            }
            Ok(r) => r,
        }
    }
    async fn delete(&self, stream_header: &StreamHeader<'static>) -> Result<(), Error> {
        // If this is empty/None, backend's default auth ID will be used.
        let frontend_auth_id: Option<&str> = if stream_header.owner.is_empty() {
            None
        } else {
            Some(&stream_header.owner)
        };

        let auth_config = self.get_auth_config(frontend_auth_id).unwrap();
        let now: DateTime<Utc> = Utc::now();
        let daily_auth_state = ClientAuthKeyState::new(&now, &auth_config);
        let request_auth_state =
            ClientAuthRequestState::new(&now, &auth_config.host, &EMPTY_STRING_SHA256);

        let mut req_builder = Request::builder()
            .method(Method::DELETE)
            .uri(self.get_url(&stream_header.bucket, &stream_header.key))
            .header("host", &self.host);

        req_builder = sign_request(
            req_builder,
            &EMPTY_STRING_SHA256,
            &daily_auth_state,
            &request_auth_state,
        );

        let _ = self.background_semaphore.acquire().await.unwrap();
        let req = req_builder.body(Body::empty()).unwrap();
        hyper_result_to_backend_result(self.client.request(req).await).await
    }

    /// Because this is for use on the frontend, instead of an abstract error, we return
    /// an HTTP response on error: that way the client gets an authentic view of what went
    /// wrong when we tried to call through on their behalf.
    async fn list(
        &self,
        bucket: &str,
        frontend_auth_id: Option<&str>,
        lor: &ListObjectsV2Request,
    ) -> Result<ListObjectsV2Response, Response<Body>> {
        // FIXME: currently always using path style bucket addressing, but techincally the AWS
        // docs don't allow for that, they want you to always put the bucket in the hostname (which
        // doesn't work in on prem cases where someone is using an IP address).  Maybe the backend config
        // should have a bool to say which style to use?
        let uri = format!("{}/{}/?{}", self.base_url, bucket, lor.to_query_string());
        let mut req_builder = Request::builder()
            .method(Method::GET)
            .uri(uri)
            .header("host", &self.host);

        let auth_config = self.get_auth_config(frontend_auth_id).unwrap();
        let now: DateTime<Utc> = Utc::now();
        let daily_auth_state = ClientAuthKeyState::new(&now, &auth_config);
        let request_auth_state =
            ClientAuthRequestState::new(&now, &auth_config.host, &EMPTY_STRING_SHA256);

        req_builder = sign_request(
            req_builder,
            &EMPTY_STRING_SHA256,
            &daily_auth_state,
            &request_auth_state,
        );
        let _ = self.frontend_semaphore.acquire().await.unwrap();
        let req = req_builder.body(Body::empty()).unwrap();
        let response = match self.client.request(req).await {
            Err(e) => {
                // No response received, synthesize an error response
                error!("Error in ListObjectsV2: {}", e);
                return Err(Response::builder()
                    .status(StatusCode::SERVICE_UNAVAILABLE)
                    .body(Body::empty())
                    .unwrap());
            }
            Ok(r) => r,
        };

        // On HTTP error status codes, pass the error response back to the frontend
        if response.status() != StatusCode::OK {
            warn!(
                "Non-OK {} response in ListObjectsV2, passing back to frontend",
                response.status()
            );
            return Err(response);
        }

        let body = response.into_body();
        let content = match hyper::body::to_bytes(body).await {
            Err(e) => {
                error!("Error reading body for ListObjectsV2: {}", e);
                return Err(Response::builder()
                    .status(StatusCode::SERVICE_UNAVAILABLE)
                    .body(Body::empty())
                    .unwrap());
            }
            Ok(b) => b,
        };

        let body_str = String::from_utf8_lossy(&content[..]);
        let list_result = yaserde::de::from_str::<ListObjectsV2Response>(body_str.borrow());
        match list_result {
            Ok(r) => Ok(r),
            Err(e) => {
                error!("ListObjectsV2Response decode error {:?}", e);
                dump_buffer(log::Level::Error, body_str.as_bytes());
                Err(Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::empty())
                    .unwrap())
            }
        }
    }
}

struct PlainBodySniff<'a> {
    content_length: u64,
    entry_stream: OwningHandle<OwningRef<Arc<DCache>, DCache>, Box<Option<EntryStream<'a>>>>,
    upstream_body: Pin<Box<Body>>,
}

impl<'a> PlainBodySniff<'a> {
    fn new(dcache: Arc<DCache>, etag: ETag, content_length: u64, upstream_body: Body) -> Self {
        let dcache = OwningRef::new(dcache).map(|dcache| &*dcache);

        let entry_stream = OwningHandle::new_with_fn(dcache, |dcache| unsafe {
            let dcache = dcache.as_ref().unwrap();
            // FIXME: spurious heap allocation to Box the EntryStream, which is only
            // done to satisfy OwningHandle's requirement for a DeRef-able type
            Box::new(dcache.insert(etag.clone(), content_length).ok())
        });

        if entry_stream.deref().is_some() {
            debug!("Sniff: constructed EntryStream");
        } else {
            debug!("Sniff: no EntryStream for {:?}", etag)
        }

        Self {
            content_length,
            upstream_body: Box::pin(upstream_body),
            entry_stream,
        }
    }
}

impl<'a> futures::Stream for PlainBodySniff<'a> {
    type Item = Result<Bytes, hyper::Error>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let r = self.upstream_body.as_mut().poll_next(cx);

        if let Some(entry_stream) = self.entry_stream.deref_mut() {
            if let Poll::Ready(o) = &r {
                if let Some(result) = o {
                    if let Ok(bytes) = result {
                        // This is a normal chunk of bytes, mirror it through to our
                        // DCache stream.
                        debug!("Sniff: taking {} bytes", bytes.len());
                        entry_stream.write(bytes.clone())
                    }
                }
            }
        }

        debug!("Sniff: passing through");

        r
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let n = usize::try_from(self.content_length).unwrap_or(usize::MAX);
        (n, Some(n))
    }
}

fn maybe_intercept_response(
    dcache: &Arc<DCache>,
    s3req: &S3Request<'_>,
    response: Response<Body>,
) -> Response<Body> {
    // When sniffing to populate DCacher, we are only interested in 200 responses to GETs
    if response.status() != StatusCode::OK || s3req.verb != S3Verb::GetObject {
        return response;
    }

    let r = S3Request::parse_content_length(response.headers(), true);
    let (content_length, is_chunked) = match r {
        Err(e) => {
            // If we can't make sense of it enough to sniff, just pass through
            warn!(
                "Failed to parse content length of GET response, passing through ({})",
                e
            );
            return response;
        }
        Ok(v) => v,
    };

    let etag = get_etag(&response);

    if etag.is_some() && !is_chunked {
        let (head, upstream_body) = response.into_parts();
        let etag = etag.unwrap();

        debug!("Intercepting proxy response for etag {:?}", etag);
        let stream = PlainBodySniff::new(dcache.clone(), etag, content_length, upstream_body);
        Response::from_parts(head, Body::wrap_stream(stream))
    } else {
        debug!("Not intercepting proxy response");
        response
    }
}

/// For outgoing PUTs -- re-encode the body of the request to ensure it authenticates
/// against the backend's Host.
fn proxy_chunked_body(
    auth: Box<dyn Authorizer>,
    req_builder: &hyper::http::request::Builder,
    req_body: Body,
    content_length: u64,
    daily_auth_state: ClientAuthKeyState,
    request_auth_state: ClientAuthRequestState,
) -> Body {
    // Backend secret
    let signing_key = &daily_auth_state.signing_key;

    let canonical_request =
        canonical_request_client(req_builder, &request_auth_state, STREAMING_PAYLOAD);

    let seed_sig = get_chunk_seed_signature(
        signing_key,
        &sha2::Sha256::digest(canonical_request.as_bytes()).into(),
        &request_auth_state.x_amz_date,
        &daily_auth_state.scope,
    );

    let decoder = ChunkedBodyDecoder::new(req_body);
    let auth_checked_decoder = AuthValidator::new(decoder, auth);
    let encoded_stream = ChunkedEncoder::new(
        auth_checked_decoder,
        daily_auth_state,
        request_auth_state,
        &seed_sig,
        content_length,
        CHUNK_SIZE,
    );

    Body::wrap_stream(encoded_stream.map(|i| i.map_err(|e| Box::new(e))))
}
