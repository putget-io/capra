use async_trait::async_trait;
use std::convert::Infallible;
use std::fs;
use std::io;
use std::io::Write;
use std::sync::{Arc, Weak};

use crate::util::auth::{Authorizer, SecretMap};
use http::request::Parts;
use hyper::{Body, Response};

use super::super::journal::journal_collection::StreamHeader;
use super::Error;

use super::{Backend, BackendHandle, BackendStream};

use log::*;

pub struct FileBackend {
    path: String,

    // An empty SecretMap to satisfy the interface that expects
    // us to be an s3-alike
    _secrets: SecretMap,
}

impl Backend for FileBackend {
    type S = FileBackendStream;
}

impl FileBackend {
    fn get_path(&self) -> &str {
        
        &self.path
    }
    // underscore because i'm not using this code anywhere yet
    pub fn _new(path: &str) -> Self {
        Self {
            path: path.to_string(),
            _secrets: SecretMap::new(),
        }
    }
}

pub struct FileBackendStream {
    fd: fs::File,

    // This reference is here in case of later adding
    // things like an FD pool that we need to release to,
    // or rate limiting that we need to respect
    _be: Weak<FileBackend>,

    path: String,
    stream_header: StreamHeader<'static>,
}

#[derive(Clone)]
pub struct FileBackendHandle {
    pub backend: Arc<FileBackend>,
}

#[async_trait]
impl BackendHandle for FileBackendHandle {
    type BE = FileBackend;
    type S = FileBackendStream;

    async fn put(&self, stream_header: StreamHeader<'static>) -> Self::S {
        FileBackendStream::open(&self.backend, stream_header)
    }

    fn get_secrets(&self) -> &SecretMap {
        &self.backend._secrets
    }

    async fn proxy(
        &self,
        _frontend_auth: &dyn Authorizer,
        _head: &Parts,
        _content_length: u64,
        _body: Body,
    ) -> Result<Response<Body>, Infallible> {
        panic!("Proxy HTTP->File not implemented");
    }

    async fn delete(&self, _stream_header: &StreamHeader<'static>) -> Result<(), Error> {
        panic!("File deletions not implemented");
    }
}

impl FileBackendStream {
    fn open(be: &Arc<FileBackend>, stream_header: StreamHeader<'static>) -> Self {
        let path = format!("{}/{}", be.get_path(), stream_header.key);
        let fd = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&path)
            .unwrap();

        FileBackendStream {
            fd,
            _be: Arc::downgrade(be),
            path,
            stream_header,
        }
    }
}

#[async_trait]
impl BackendStream for FileBackendStream {
    type BE = FileBackend;

    fn get_key(&self) -> (&str, &str) {
        (&self.stream_header.bucket, &self.stream_header.key)
    }

    fn get_content_length(&self) -> u64 {
        self.stream_header.content_length
    }

    async fn write(&mut self, data: &[u8]) -> io::Result<()> {
        self.fd.write_all(data)?;
        Ok(())
    }

    fn abort(self) {
        // Unlink our perhaps-half-written file
        let r = fs::remove_file(&self.path);
        if let Err(e) = r {
            // The assumption here is that we're aborting because there
            // was already an IO error writing, so if we can't
            // unlink that might be fine too.  But don't use FileBackend
            // in production!
            error!("Failed to abort on FileBackend key {}: {}", self.path, e);
        }
    }

    async fn finalize(self) -> io::Result<()> {
        self.fd.sync_all()?;
        Ok(())
    }
}
