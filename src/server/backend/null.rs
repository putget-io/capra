use async_trait::async_trait;
use http::{Response, StatusCode};
use hyper::Body;

use super::{Backend, BackendPutStream, Error};
use crate::server::backend::BackendPutStreamHandle;
use crate::server::frontend::request::S3Request;
use crate::server::journal::StreamHeader;
use crate::server::s3_types::{ListObjectsV2Request, ListObjectsV2Response};
use crate::util::http::{empty_response, status_response};
use capralib::auth::{Authorizer, SecretMap};

use crate::server::dcache::DCache;
use log::*;
use std::sync::Arc;

pub struct NullBackend {
    secrets: SecretMap,
}

impl NullBackend {
    pub fn new() -> Self {
        let mut secrets = SecretMap::new();

        let access_key_id = std::env::var("AWS_ACCESS_KEY_ID").ok();
        let secret_access_key = std::env::var("AWS_SECRET_ACCESS_KEY").ok();

        if access_key_id.is_some() && secret_access_key.is_some() {
            secrets.insert(access_key_id.unwrap(), secret_access_key.unwrap());
        }

        Self { secrets }
    }
}

#[async_trait]
impl Backend for NullBackend {
    async fn put(&self, stream_header: StreamHeader<'static>) -> BackendPutStreamHandle {
        info!("No-op PUT {}", stream_header);
        Box::new(NullBackendStream {
            bucket: stream_header.bucket.to_string(),
            key: stream_header.key.to_string(),
            content_length: stream_header.content_length,
        })
    }

    fn get_secrets(&self) -> &SecretMap {
        &self.secrets
    }

    async fn proxy(
        &self,
        _frontend_auth: Box<dyn Authorizer>,
        _dcache: &Arc<DCache>,
        s3req: &S3Request<'_>,
        _body: Body,
    ) -> Response<Body> {
        info!("No-op 200 Proxy {}", s3req);
        return status_response(StatusCode::OK);
    }

    async fn remote_read(
        &self,
        _frontend_auth_id: Option<&str>,
        bucket: &str,
        method: http::Method,
        path: &str,
        _query: &str,
    ) -> Response<Body> {
        info!("No-op 200 {} {} {}", method, bucket, path);
        return empty_response(StatusCode::OK).unwrap();
    }

    async fn delete(&self, stream_header: &StreamHeader<'static>) -> Result<(), Error> {
        info!("Dropping DELETE {}", stream_header);
        return Ok(());
    }

    async fn list(
        &self,
        _bucket: &str,
        _frontend_auth_id: Option<&str>,
        _lor: &ListObjectsV2Request,
    ) -> Result<ListObjectsV2Response, Response<Body>> {
        info!("No-op 20 ListObjectsV2");
        return Err(empty_response(StatusCode::OK).unwrap());
    }
}

struct NullBackendStream {
    bucket: String,
    key: String,
    content_length: u64,
}

#[async_trait]
impl BackendPutStream for NullBackendStream {
    async fn write(&mut self, _data: std::io::Result<&[u8]>) -> std::io::Result<()> {
        Ok(())
    }

    async fn finalize(self: Box<Self>) -> std::io::Result<()> {
        Ok(())
    }

    fn get_key(&self) -> (&str, &str) {
        (&self.bucket, &self.key)
    }

    fn get_content_length(&self) -> u64 {
        self.content_length
    }
}
