use async_trait::async_trait;
use std::fmt;
use std::io;
use std::sync::Arc;

use crate::util::auth::{Authorizer, SecretMap};
use hyper::{Body, Response};

use crate::server::journal::StreamHeader;

//pub mod file;
//pub use file::{FileBackend, FileBackendHandle};

pub mod null;
pub mod s3;

#[cfg(feature = "rados_backend")]
pub mod rados;

use crate::server::dcache::DCache;
use crate::server::frontend::request::S3Request;
use crate::server::s3_types::{ListObjectsV2Request, ListObjectsV2Response};

/// A Backend is a destination for writes expired from the journal, or for
/// other operations that bypass the journal entirely.  Typically a backend
/// is a remote S3 bucket.
#[async_trait]
pub trait Backend: Sync + Send {
    async fn put(&self, stream_header: StreamHeader<'static>) -> BackendPutStreamHandle;

    fn get_secrets(&self) -> &SecretMap;

    async fn proxy(
        &self,
        frontend_auth: Box<dyn Authorizer>,
        dcache: &Arc<DCache>,
        s3req: &S3Request<'_>,
        body: Body,
    ) -> Response<Body>;

    async fn remote_read(
        &self,
        frontend_auth_id: Option<&str>,
        bucket: &str,
        method: http::Method,
        path: &str,
        query: &str,
    ) -> Response<Body>;
    async fn delete(&self, stream_header: &StreamHeader<'static>) -> Result<(), Error>;

    async fn list(
        &self,
        bucket: &str,
        frontend_auth_id: Option<&str>,
        lor: &ListObjectsV2Request,
    ) -> Result<ListObjectsV2Response, Response<Body>>;

    // Optional: real S3-ish backends can override this, others like the file
    // backend can leave the dummy default
    fn get_region(&self) -> &str {
        "us-east-1"
    }
}

#[derive(fmt::Debug)]
pub struct Error {
    inner: ErrorKind,
}

#[derive(fmt::Debug)]
pub enum ErrorKind {
    /// e.g. we couldn't open a connection to the backend.  Maybe someone
    /// stepped on the wire.  Caller should probably wait a while and try again.
    RemoteHttpError,

    /// For backends using local file IO.  Local IO errors shouldn't happen, and
    /// the file backend is mainly for debugging, so if you see this then by all means bail out.
    LocalIOError(io::Error),

    /// For example, an error deserializing a ChunkHeader or StreamFooter
    /// (strictly not a backend error, but comes up while expiring to backend)
    LocalIntegrityError,

    /// e.g. some internal channel disconnected unexpectedly, probably due to a
    /// panic in another task.
    InternalError,

    /// Authorization or Authentication errors.  Indicates a configuration error
    /// with the creds the operator has given us, and we should probably stall
    /// the journal and stop trying to expire things until we get some creds
    /// that work (that might mean new creds, or it might mean the permissions
    /// for our existing creds getting fixed out of band)
    AuthError,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        Self { inner: kind }
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::new(ErrorKind::LocalIOError(error))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.inner)
    }
}

/// The flow for a particular object.  Equivalent to a ChunkJournal streams
#[async_trait]
pub trait BackendPutStream: Send {
    async fn write(&mut self, data: std::io::Result<&[u8]>) -> io::Result<()>;
    async fn finalize(self: Box<Self>) -> io::Result<()>;

    fn get_key(&self) -> (&str, &str);
    fn get_content_length(&self) -> u64;
}

pub type BackendHandle = Arc<dyn Backend>;
pub type BackendPutStreamHandle = Box<dyn BackendPutStream>;
