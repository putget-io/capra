use std::convert::Infallible;
use std::path::Path;
use std::process::exit;
use std::sync::{Arc, Mutex};

use core_affinity;
use futures::future::join_all;
use hyper::server::Server;
use hyper::service::{make_service_fn, service_fn};
use std::thread;
use tonic;

use super::backend::null::NullBackend;
use super::backend::s3::S3Backend;
use super::backend::BackendHandle;
#[cfg(feature = "rados_backend")]
use crate::server::backend::rados::RadosBackend;

use super::cluster_map::ClusterMap;
use super::core_tracker::CoreTracker;
use super::frontend::handlers::handle_request;
use super::frontend::HandlerState;
use super::journal::chunk_journal::ChunkJournalInner;
use super::journal::chunk_journal::{open_journal, HeadAdvancer};
use super::journal::expirer::Expirer;
use super::journal::journal_collection::JournalCollection;
use super::journal::{chunk_journal, JournalId};
use super::options::ServerOptions;

use super::io_engine;
use super::key_map::api::key_map_shard_server;
use super::key_map::{KeyMap, KeyMapShard, KeyMapShardService};
use crate::util::auth::{AuthorizedRequest, Authorizer, UnsignedRequest};

use log::*;

pub struct FatalError {
    msg: String,
}

impl std::fmt::Display for FatalError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

/// This is the logical lifetime of a Capra server, confusingly named beacuse
/// it is *not* the process lifetime.  The Leader/Follower instance has the lifetime
/// of the process, and they start a Process for each epoch (i.e. Process is dropped
/// on cluster restart, while linux process remains alive).
pub struct Process {
    options: ServerOptions,
    cluster_map: Arc<ClusterMap>,
    io_builder: io_engine::Builder,
    journals: Arc<JournalCollection>,
    journal_advancers: Vec<HeadAdvancer>,

    // Populated in Initial->Start transition
    runtime: Option<tokio::runtime::Runtime>,
    local_shard: Arc<KeyMapShard>,

    // Populated on Start->Replay transition
    key_map: Option<Arc<KeyMap>>,
    replay_jh: Option<tokio::task::JoinHandle<()>>,

    // Populated on Replay->Active transition
    server_running: Option<thread::JoinHandle<()>>,
}

impl Process {
    pub fn start(
        cluster_map: Arc<ClusterMap>,
        io_builder: io_engine::Builder,
        options: ServerOptions,
    ) -> Result<Self, FatalError> {
        let mut journals = Vec::new();
        let mut advancers = Vec::new();
        for (i, journal_path) in (&options.journal_shards).iter().enumerate() {
            // Auto-creation of journal shards with fixed default size
            let ensured = ChunkJournalInner::ensure(&Path::new(journal_path));
            if let Err(e) = ensured {
                error!("Failed to create journal at {}: {}", journal_path, e);
                exit(-1);
            }

            let io_engine = io_builder(&journal_path, options.sloppy_sync, options.memory_bench);

            let (journal, advancer) = open_journal(
                i as JournalId,
                &journal_path,
                io_engine,
                options.memory_bench,
            )
            .expect("I/O error opening journal");
            journals.push(journal);
            advancers.push(advancer);
        }

        let journals = Arc::new(JournalCollection::new(journals));

        let runtime = Self::start_runtime(&options, &journals);
        info!("Started runtime");

        let local_shard = Arc::new(KeyMapShard::new());

        // Start our KeyMap server (on top of any empty shard) as soon as we
        // start our runtime.
        let key_map_shard_service = KeyMapShardService::new(local_shard.clone());
        let my_node = &cluster_map.get_nodes()[cluster_map.get_id() as usize];
        let grpc_addr = my_node.get_grpc_address();
        let _key_map_server = runtime.spawn(async move {
            tonic::transport::Server::builder()
                .add_service(key_map_shard_server::KeyMapShardServer::new(
                    key_map_shard_service,
                ))
                .serve(grpc_addr)
                .await
                .expect("Error from gRPC server!");
        });

        Ok(Self {
            options,
            cluster_map,
            io_builder,
            journals,
            journal_advancers: advancers,
            runtime: Some(runtime),
            local_shard,
            key_map: None,
            replay_jh: None,
            server_running: None,
        })
    }

    pub fn shutdown(&mut self) {
        info!("Shutting down...");
        // Implicitly drops any content of self.runtime
        self.runtime = None;

        if let Some(run_thread) = self.server_running.take() {
            run_thread.join().expect("Process thread panicked!");
        }
    }

    fn runtime(&mut self) -> &mut tokio::runtime::Runtime {
        self.runtime.as_mut().unwrap()
    }

    pub async fn replay(
        key_map: Arc<KeyMap>,
        journals: Arc<JournalCollection>,
        runtime_handle: tokio::runtime::Handle,
    ) {
        let mut futs = Vec::new();
        for j in journals.all() {
            info!("Loading journal shard {}", j.get_path());
            let j = j.clone();
            let k = key_map.clone();
            futs.push(runtime_handle.spawn(async move {
                let r = j.replay(&k).await;
                match r.as_ref() {
                    Err(e) => {
                        error!("Error replaying journal {}: {}", j.get_id(), e);
                    }
                    _ => {}
                }
                (r, j)
            }));
        }
        let replay_results = join_all(futs).await;
        for r in replay_results {
            match r {
                Ok(r) => {
                    let (replay_result, journal) = r;
                    match replay_result {
                        Ok(_) => {
                            info!(
                                "Loaded {} of dirty data from journal shard {}",
                                bytefmt::format(journal.get_head() - journal.get_tail()),
                                journal.get_path()
                            );
                        }
                        Err(e) => {
                            // This is the error from ChunkJournal::replay: an I/O error or
                            // a corruption.
                            // FIXME: it is probably better to continue running while perhaps
                            // having lost some writes, than to stop the world.
                            panic!("Replay error: {}", e);
                        }
                    }
                }
                Err(e) => {
                    // This is the error on tokio::spawn, some kind of runtime issue
                    panic!("Replay task error: {}", e);
                }
            }
        }
    }

    fn start_runtime(
        options: &ServerOptions,
        journals: &Arc<JournalCollection>,
    ) -> tokio::runtime::Runtime {
        let cpu_cores = core_affinity::get_core_ids().unwrap();

        // Tokio would pick the core count as the default anyway, but do it explicitly here
        // so that we have the thread count for reference in checks below.
        let worker_count = options.workers.unwrap_or(cpu_cores.len());

        info!(
            "Using {} threads ({} cores available)",
            worker_count,
            cpu_cores.len()
        );

        if worker_count > cpu_cores.len() {
            warn!(
                "Thread count {} exceeds CPU core count {}, performance may degrade",
                worker_count,
                cpu_cores.len()
            );
        }

        if journals.len() > worker_count {
            // Totally legal, but weird and probably not what the user intended
            warn!(
                "More journal shards than IO threads: excess shards will not be used for writes."
            );
        }

        // We need a counter to index the round-robin assignment of home shards to threads,
        // because core Ids may not be contiguous or start at zero.
        let thread_counter = Mutex::new(0 as usize);

        // FIXME: it appears that worker_threads is only a *hint* to tokio: if we
        // request fewer threads than the CPU count, extra worker threads get
        // started once we are under load.  That means our logic for thread-local stuff
        // needs to cope with unexpected workers starting later in the process's lifetime.

        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(worker_count)
            .on_thread_start({
                debug!("on_thread_start");
                let jc = journals.clone();
                move || {
                    let mut thread_counter = thread_counter.lock().unwrap();

                    // For first N workers on an N core environment, bind them to a CPU core
                    // (even if worker count is lower, tokio may spawn more)
                    if *thread_counter < cpu_cores.len() {
                        let my_cpu = cpu_cores[*thread_counter];
                        core_affinity::set_for_current(my_cpu);
                        CoreTracker::register_thread(my_cpu.id);
                    }

                    // Assign a 'home' journal to each worker thread as it starts.  If there are
                    // more threads than journals, multiple threads will share (which obviously
                    // hurts locality, but you may not care if runtime is dominated by authentication/hashing
                    // so we allow this usage style)
                    // (rely on journal ID's being assigned sequentially from zero to infer one)
                    let home_journal_id = (*thread_counter % jc.len()) as JournalId;
                    let chunk_journal = jc.get(home_journal_id);
                    chunk_journal::register_thread(chunk_journal);

                    *thread_counter += 1;
                }
            })
            .enable_io()
            .build()
            .unwrap()
    }

    pub fn start_replay<F, G>(&mut self, on_complete: F)
    where
        F: FnOnce() -> G + Send + Sync + 'static,
        G: std::future::Future + Send + Sync + 'static,
    {
        let rt_handle = self.runtime().handle().clone();

        let (key_map, _key_map_bg) =
            KeyMap::start(self.cluster_map.clone(), &self.local_shard, &rt_handle);

        self.key_map = Some(key_map.clone());

        let journals = self.journals.clone();
        let runtime = self.runtime.as_mut().unwrap().handle().clone();

        self.replay_jh = Some(rt_handle.spawn(async {
            Self::replay(key_map, journals, runtime).await;
            on_complete().await;
        }));
    }

    pub fn start_server(&mut self) {
        let rt_handle = self.runtime.as_mut().unwrap().handle().clone();
        let key_map = self.key_map.as_ref().unwrap().clone();

        // Load the backend object that Expirer will write back into
        let backend: BackendHandle = if self.options.backend == "null" {
            Arc::new(NullBackend::new())
        } else if self.options.backend.starts_with("rados:") {
            #[cfg(feature = "rados_backend")]
            {
                Arc::new(RadosBackend::new())
            }
            #[cfg(not(feature = "rados_backend"))]
            {
                panic!("RADOS backend not compiled in, requires feature 'rados_backend'")
            }
        } else {
            match S3Backend::configure_simple(&self.options.backend) {
                Ok(b) => b,
                Err(e) => {
                    error!("{}", e);
                    exit(-1);
                }
            }
        };

        // Construct a DCache that will be shared between the Expirer (priming it)
        // and the S3 frontend (reading from it and also priming it)
        let dcache = Arc::new(super::dcache::DCache::new(
            // Use a gig of memory for the DCache
            // TODO make dcache size configurable
            1024 * 1024 * 1024,
            128 * 1024 * 1024,
        ));

        // Spawn expirer tasks for all journals
        for journal in self.journals.all() {
            if self.options.memory_bench {
                // To avoid noisy errors, simply do not instantiate expirers if we are
                // in the dev mode that disables writes to journal.
                continue;
            }
            let journal = journal.clone();
            let exp_res = Expirer::open(
                key_map.clone(),
                journal.clone(),
                (self.io_builder)(journal.get_path(), false, false),
                backend.clone(),
                self.options.expiry_rate,
                dcache.clone(),
            );
            let (mut expirer, mut tailer) = match exp_res {
                Ok((expirer, tailer)) => (expirer, tailer),
                Err(e) => {
                    error!("Failed to open expirer for journal {}: {}", journal, e);
                    return;
                }
            };

            rt_handle.spawn(async move {
                let io_result = expirer.consume().await;
                if let Err(e) = io_result {
                    // FIXME: a dead expirer should not make us panic completely,
                    // maybe we should kick into a degraded mode where the frontend
                    // just does what it can?
                    panic!("Expirer IO error: {}", e);
                }
            });

            rt_handle.spawn(async move {
                tailer.consume().await;
            });
        }

        let mut advancers = Vec::new();
        std::mem::swap(&mut advancers, &mut self.journal_advancers);
        for mut advancer in advancers {
            rt_handle.spawn(async move {
                advancer.consume().await;
            });
        }

        let handler_state = HandlerState {
            key_map,
            journal_collection: self.journals.clone(),
            backend,
            cluster_map: self.cluster_map.clone(),
            dcache,
            options: self.options.clone(),
        };

        // Store a handle to the thread in which we run the blocking server.
        // (later shut down by calling .shutdown() on the runtime and then join the thread)
        assert!(self.server_running.is_none());
        self.server_running = Some(thread::spawn(move || {
            if handler_state.options.unsigned {
                rt_handle.spawn(serve::<UnsignedRequest>(handler_state));
            } else {
                rt_handle.spawn(serve::<AuthorizedRequest>(handler_state));
            }
        }))
    }
}

async fn serve<A: Authorizer + Send + 'static>(handler_state: HandlerState) {
    info!("Running S3 service on {}", handler_state.options.listen);
    // handler_state itself could derive Clone because all its members are Arcs,
    // but we clone this thing on every request, so let's just have one reference
    // count that we gratuitiously hammer.
    let handler_state = Arc::new(handler_state);

    let listen = handler_state.options.listen;
    let make_service = make_service_fn(move |_| {
        let handler_state = handler_state.clone();

        async move {
            Ok::<_, Infallible>(service_fn(move |req| {
                let handler_state = handler_state.clone();
                handle_request::<A>(handler_state, req)
            }))
        }
    });

    let server = Server::bind(&listen).tcp_nodelay(true).serve(make_service);

    if let Err(e) = server.await {
        error!("server error: {}", e);
    }
}

impl std::fmt::Debug for Process {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "<Process>")
    }
}
