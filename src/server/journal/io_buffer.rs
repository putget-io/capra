use std::alloc;
use std::fmt;
use std::num::NonZeroU32;
use std::ptr::{slice_from_raw_parts_mut, NonNull};

#[cfg(feature = "rio_engine")]
use rio::AsIoVec;
#[cfg(feature = "rio_engine")]
use std::convert::AsMut;

pub const IO_ALIGN: usize = 0x1000;

pub struct IOBuffer {
    buffer_ptr: NonNull<u8>,
    size: NonZeroU32,
}

unsafe impl Send for IOBuffer {}
unsafe impl Sync for IOBuffer {}

/// A page-aligned memory buffer for use in O_DIRECT disk IO.
/// Size must fit in 4 bytes (it would be pathological to have IO buffers
/// bigger than this) and may not be zero.
/// An Option<> around this has zero cost.
impl IOBuffer {
    pub fn new(sz: usize) -> IOBuffer {
        assert!(sz <= u32::MAX as usize);
        assert!(sz > 0);
        let buffer_ptr = unsafe {
            let ptr = alloc::alloc_zeroed(alloc::Layout::from_size_align(sz, IO_ALIGN).unwrap());
            ptr as *mut u8
        };
        return IOBuffer {
            buffer_ptr: NonNull::new(buffer_ptr).unwrap(),
            size: NonZeroU32::new(sz as u32).unwrap(),
        };
    }

    pub fn len(&self) -> usize {
        self.size.get() as usize
    }

    pub fn as_slice(&self) -> &[u8] {
        unsafe { &*slice_from_raw_parts_mut(self.buffer_ptr.as_ptr(), self.size.get() as usize) }
    }

    pub fn as_slice_mut(&self) -> &mut [u8] {
        unsafe {
            &mut *slice_from_raw_parts_mut(self.buffer_ptr.as_ptr(), self.size.get() as usize)
        }
    }
}

impl fmt::Display for IOBuffer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:p}:{:#06x}", self.buffer_ptr, self.size)
    }
}

#[cfg(feature = "rio_engine")]
impl AsIoVec for IOBuffer {
    fn into_new_iovec(&self) -> libc::iovec {
        return libc::iovec {
            iov_base: self.buffer_ptr.as_ptr() as *mut _,
            iov_len: self.size.get() as usize,
        };
    }
}

#[cfg(feature = "rio_engine")]
impl AsMut<[u8]> for IOBuffer {
    fn as_mut(&mut self) -> &mut [u8] {
        return self.as_slice_mut();
    }
}

impl Drop for IOBuffer {
    fn drop(&mut self) {
        unsafe {
            alloc::dealloc(
                self.buffer_ptr.as_ptr(),
                alloc::Layout::from_size_align(self.size.get() as usize, IO_ALIGN).unwrap(),
            )
        }
    }
}

#[cfg(test)]
use std::mem;

#[test]
fn test_sizes() {
    assert_eq!(
        mem::size_of::<IOBuffer>(),
        mem::size_of::<Option<IOBuffer>>()
    )
}
