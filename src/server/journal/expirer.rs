use std::collections::{HashMap, HashSet};
use std::io::Cursor;
use std::sync::Arc;
use std::time::{Duration, Instant};

use bincode;
use log::*;
use partial_min_max::{max, min};
use tokio::sync::mpsc::{Receiver, Sender, UnboundedReceiver, UnboundedSender};
use tokio::sync::watch;
use tokio::sync::{mpsc, OwnedSemaphorePermit, Semaphore};
use tokio::task::JoinHandle;
use tokio::time::sleep;

use crate::server::backend::{BackendHandle, BackendPutStream, Error, ErrorKind};
use crate::server::journal::wait_map::{OrderedWaitHandle, OrderedWaitMap};
use crate::server::journal::{StreamFooter, StreamHeader, STREAM_FOOTER_SIZE};
use crate::server::key_map::{IndexKey, PersistentValueOp};
use crate::server::range_set::RangeSet;
use crate::server::types::{ObjectOp, Offset, OffsetList};
use crate::util::round_to_pow2;

use super::super::key_map::KeyMap;
use super::chunk_journal::{ChunkHeader, StreamId, STREAM_CANCEL, STREAM_END, STREAM_START};
use super::chunk_journal::{ChunkJournal, DiskLayout};
use super::io_buffer::{IOBuffer, IO_ALIGN};
use crate::server::dcache::{DCache, ETag, EntryStream};
use crate::server::io_engine::IOEngine;
use crate::server::journal::chunk_journal::CONCURRENT_STREAMS_PER_JOURNAL;
use hyper::body::Bytes;

// How often should expirer re-adjust its rate-limiting?
// (also the sleep used when waiting for new data)
const RATE_INTERVAL: Duration = Duration::from_millis(1000);

// How many results can be in the queue to process for
// keymap expiry and tail pointer updates.
const RESULT_CHANNEL_DEPTH: usize = 128;

// How many streams will we try to expire concurrently?  Backend has its
// own limit on how many requests it'll send concurrently, but this limit
// applies a bit earlier and prevents us from spawning an unbounded number
// of background expirer tasks all waiting on the backend (in order to
// bound our memory consumption).
const MAX_CONCURRENT_EXPIRIES: usize = CONCURRENT_STREAMS_PER_JOURNAL * 2;

struct StreamExpirer {
    io: Arc<dyn IOEngine>,
    backend: BackendHandle,
    stream_id: StreamId,
    content_length: u64,
    layout: DiskLayout,
    result_tx: Sender<StreamExpirerResult>,

    // We build up the list of offsets as we receive them from
    // Expirer via our channel.  We need this list to
    // retry if the backend has an error, and ultimately
    // to pass out in our result to cue Tailer that these
    // chunks are elegible for expiry
    offsets: OffsetList,
    complete: bool,

    // Why is this unbounded?  Because of overlapping streams to the same key.  One of them
    // will wait (via OrderedWaitMap) for the other, and in the meantime it must be able
    // to buffer up all of its chunks to avoid stalling the main Expirer read loop.
    chunk_rx: UnboundedReceiver<Option<Offset>>,

    // At end of stream, we may see StreamFooter split across two chunks.  If this
    // happens, this vec gets allocated and used to accumulate the streamfooter bytes.
    footer: Vec<u8>,

    // If this stream was a PUT, we will have an ETag retrieved
    // from the KeyMap at stream start by Expirer, and can use
    // this ETag to populate DCache.
    etag: Option<ETag>,
    dcache: Arc<DCache>,
}

unsafe impl Send for StreamExpirer {}
unsafe impl Sync for StreamExpirer {}

// The StreamExpirer feeds back to the Expirer, to tell it
// which chunks can now be released (i.e. iterate through
// these chunks in order and if they're == tail, then
// advance tail)
struct StreamExpirerResult {
    // Key is optional because we are sometimes expiring some
    // invalid chunks, and do not have a key.
    key: Option<(String, String)>,

    offsets: OffsetList,
}

impl StreamExpirerResult {
    pub fn new(bucket: &str, key: &str, offsets: OffsetList) -> Self {
        Self {
            key: Some((bucket.to_string(), key.to_string())),
            offsets,
        }
    }

    pub fn with_invalid_chunks(offsets: OffsetList) -> Self {
        Self { key: None, offsets }
    }
}

unsafe impl Send for StreamExpirerResult {}
unsafe impl Sync for StreamExpirerResult {}

struct StreamExpirerHandle {
    stream_id: StreamId,
    chunk_tx: UnboundedSender<Option<Offset>>,
    task: Option<JoinHandle<()>>,
}

impl StreamExpirerHandle {
    fn push_chunk(&self, offset: Offset) {
        // This should always succeed, because we only drop the receiver
        // when dropping StreamExpirer, and Expirer won't call push_chunk
        // on a StreamExpirer it has dropped.
        if let Err(e) = self.chunk_tx.send(Some(offset)) {
            panic!("Error in push_chunk: {}", e);
        }
    }

    fn abort(&mut self) {
        debug!("StreamExpirerHandle stream={} abort()", self.stream_id);
        // Doesn't make sense to call end before spawn
        assert!(self.task.is_some());

        // Signal consume() to drop out early - this will cause it to
        // drop its BackendStream and thereby abort any in progress request
        // to the backend.
        let _result = self.chunk_tx.send(None);

        // XXX: see comments in drop() about how we can't abort the task
        // without the tailer stalling.
        //let jh = self.task.take().unwrap();
        //jh.abort();

        // When this task aborts it will end up dropping its OrderedWaitHandle,
        // allowing subsequent writes to the same key to proceed
        // (although even without aborting it will eventually win the lock on
        //  its key when any preceding StreamExpirer finishes - not aborting, just
        // means that this StreamExpirer will hang around longer than it really
        // needs to)
    }

    /// Call after the last push_chunk of a healthy stream, to distinguish
    /// between that case (in which we will let our StreamExpirer run to completion
    /// in the background) and an abort (in which we will abort() the JoinHandle
    /// to force it to relinquish its OrderedWaitHandle.
    fn complete(&mut self) {
        debug!("StreamExpirerHandle stream={} complete()", self.stream_id);
        // Doesn't make sense to call end before spawn
        assert!(self.task.is_some());

        if let Err(e) = self.chunk_tx.send(None) {
            // If the StreamExpirer half has already dropped, that's probably a sign
            // that something bad happened like a panic.  They shouldn't drop
            // until they've seen all data (on backend issues they retry and stay alive)
            panic!(
                "StreamExpirerHandle stream={} channel error in complete(): {}",
                self.stream_id, e
            );
        }

        // Drop our JoinHandle so that on drop we do not abort it: let consume() run
        // to completion in the background now that we've given it all the chunks we
        // expect to give it.
        self.task = None;
    }

    fn spawn(
        &mut self,
        mut key_handle: OrderedWaitHandle<IndexKey>,
        expirer: StreamExpirer,
        stream_header: StreamHeader<'static>,
        sem_permit: OwnedSemaphorePermit,
    ) {
        assert!(self.task.is_none());
        self.task = Some(tokio::spawn(async move {
            // Hold semaphore permit until task drops
            let _permit = sem_permit;

            // Don't proceed to write back stream until any other writes for this key are complete
            key_handle.enter().await;

            expirer.consume(stream_header).await;
        }));
    }
}

impl Drop for StreamExpirerHandle {
    fn drop(&mut self) {
        // If our JoinHandle is still hanging around, it means we didn't go through
        // the happy path of a call to complete() -- kill the background task that it
        // relinquishes its OrderedWaitHandle
        // XXX: actually do NOT do this, because we need the background task to stay alive
        // and feed expired Offsets through to the Tailer.  This is annoying - I'd rather
        // terminate it early, but that can't happen until we have a better way of ensuring
        // those Offsets eventually make it to the Tailer.
        // if let Some(jh) = &self.task {
        //     jh.abort();
        // }
    }
}

impl StreamExpirer {
    fn new(
        io: Arc<dyn IOEngine>,
        backend: BackendHandle,
        stream_id: StreamId,
        content_length: u64,
        layout: DiskLayout,
        result_tx: Sender<StreamExpirerResult>,
        etag: Option<ETag>,
        dcache: Arc<DCache>,
    ) -> (Self, StreamExpirerHandle) {
        // Channel for Expirer to send us chunks as they
        // come in.  We use a bounded channel to avoid unbounded
        // memory use (the main Expirer loop will block and stop
        // reading new chunks if the StreamExpirers are not progressing)
        let (chunk_tx, chunk_rx) = mpsc::unbounded_channel();

        (
            Self {
                io,
                backend,
                stream_id,
                content_length,
                layout,
                result_tx,
                chunk_rx,
                offsets: OffsetList::new(),
                // Whether our Handle has notified us that incoming data is complete (we may still be sending it)
                complete: false,
                footer: Vec::new(),
                etag,
                dcache,
            },
            StreamExpirerHandle {
                stream_id,
                chunk_tx,
                task: None,
            },
        )
    }

    async fn consume_chunk(
        &mut self,
        offset: &Offset,
        backend_stream: &mut dyn BackendPutStream,
        dcache_stream: Option<&mut EntryStream<'_>>,
        byte_count: &mut u64,
    ) -> Result<(), Error> {
        let disk_location = self.layout.offset_to_disk(offset.location);
        let disk_size = round_to_pow2(offset.size as usize, IO_ALIGN);
        let buffer = IOBuffer::new(disk_size);

        debug!(
            "StreamExpirer stream={} consume_chunk {}",
            self.stream_id, offset
        );

        let read_result = self.io.read_at(buffer.as_slice_mut(), disk_location).await;

        if let Err(e) = read_result {
            error!(
                "I/O error reading at disk location {:#10x}, aborting",
                disk_location
            );
            return Err(e.into());
        }

        let chunk_content = &buffer.as_slice()[0..offset.size as usize];
        let mut cursor = Cursor::new(chunk_content);
        // FIXME: unhandled errors from decode
        let chunk_header: ChunkHeader = bincode::deserialize_from(&mut cursor).unwrap();
        assert_eq!(chunk_header.location, offset.location);

        debug!(
            "StreamExpirer stream={} key={}:{} read at {:#10x}:{:#6x} flags={:#02x}",
            self.stream_id,
            backend_stream.get_key().0,
            backend_stream.get_key().1,
            offset.location,
            offset.size,
            chunk_header.stream_state
        );

        // Advance cursor past StreamHeader if on first chunk
        if *byte_count == 0 {
            let _stream_header: StreamHeader = bincode::deserialize_from(&mut cursor).unwrap();
        }

        // TODO: read beyond the stream body to the stream footer, and validate
        // that what we have just read matches the digest, before sending the last
        // body byte to the backend.  (That's a nice-to-have for integrity, it's not
        // a must - we don't have to know the etag to expire an object)

        // Mid-stream, we are passing through all the chunk body, but at the end
        // of the stream we must stop before the stream footer: compare our
        // position with content_length to determine how much to read.
        let bytes_in_chunk = chunk_content.len() - cursor.position() as usize;
        let bytes_remaining_in_stream_body = self.content_length - *byte_count;
        let read_body_bytes = min(bytes_remaining_in_stream_body, bytes_in_chunk as u64) as usize;

        let stream_body_bytes = &chunk_content
            [cursor.position() as usize..cursor.position() as usize + read_body_bytes];

        let trailing_bytes = &chunk_content[cursor.position() as usize + read_body_bytes..];
        if !trailing_bytes.is_empty() {
            if self.footer.is_empty() {
                self.footer.reserve_exact(STREAM_FOOTER_SIZE);
            }
            self.footer.extend_from_slice(trailing_bytes);

            if self.footer.len() == STREAM_FOOTER_SIZE {
                let _footer: StreamFooter = match bincode::deserialize(self.footer.as_slice()) {
                    Ok(f) => f,
                    Err(e) => {
                        error!(
                            "Corrupt StreamFooter reading stream={} chunk {}: {}",
                            self.stream_id, offset, e
                        );

                        // Ignore result, if we can't send an error it just means
                        // the receiver (Body) is already dropped for some other reason.
                        let _result = backend_stream
                            .write(Err(std::io::Error::from_raw_os_error(22)))
                            .await;
                        return Err(Error::new(ErrorKind::LocalIntegrityError));
                    }
                };
                // TODO: validate stream content vs footer etag
            }
        }

        if let Some(dcache_stream) = dcache_stream {
            dcache_stream.write(Bytes::copy_from_slice(stream_body_bytes));
        }

        match backend_stream.write(Ok(stream_body_bytes)).await {
            Ok(_) => {
                *byte_count += read_body_bytes as u64;
                Ok(())
            }
            Err(e) => {
                // A warn rather than an error: backends dropping connections isn't rare,
                // and we have retry around this.
                warn!(
                    "I/O error writing to backend stream for chunk at offset {}: {}",
                    offset, e
                );
                // FIXME: update backend interface to return a proper backend error
                Err(Error::new(ErrorKind::RemoteHttpError))
            }
        }
    }

    // The main work of a StreamExpirer: read chunks in off our channel
    // and use them to read from the journal and send the data we read
    // onward to the backend.
    async fn try_write_back(&mut self, stream_header: &StreamHeader<'static>) -> Result<(), Error> {
        let mut byte_count: u64 = 0;

        // FIXME: gratuitous clone of StreamHeader, improve the backend interface to take
        // a reference instead
        debug!(
            "StreamExpirer stream={} opening writeback for {} content_length={:#06x}",
            self.stream_id, stream_header, stream_header.content_length
        );
        let mut backend_stream = self.backend.put(stream_header.clone()).await;

        // Spurious clone to satisfy borrow checker: otherwise EntryStream is considered
        // to refer to &self, via self.dcache.  Conflicts with consume_chunk being &mut self,
        // which in turn is to enable it to update self.footer.
        let dcache = self.dcache.clone();

        let mut dcache_stream = if let Some(etag) = self.etag {
            dcache.insert(etag, stream_header.content_length).ok()
        } else {
            None
        };

        // If we're retrying, there will be chunks in self.offsets already
        for o in self.offsets.clone() {
            let r = self
                .consume_chunk(
                    &o,
                    &mut *backend_stream,
                    dcache_stream.as_mut(),
                    &mut byte_count,
                )
                .await;
            if r.is_err() {
                return r;
            }
        }

        while !self.complete {
            debug!(
                "StreamExpirer stream={} awaiting chunk (byte_count={})",
                self.stream_id, byte_count
            );
            let rx = self.chunk_rx.recv().await;
            let offset = match rx {
                Some(o) => match o {
                    Some(o) => {
                        self.offsets.push(o);
                        o
                    }
                    None => {
                        self.complete = true;
                        break;
                    }
                },
                None => {
                    error!("StreamExpirer stream={} aborted", self.stream_id);
                    return Err(Error::new(ErrorKind::InternalError));
                }
            };

            let r = self
                .consume_chunk(
                    &offset,
                    &mut *backend_stream,
                    dcache_stream.as_mut(),
                    &mut byte_count,
                )
                .await;
            if r.is_err() {
                return r;
            }
        }

        // This is not an error case!  If a journal stream gives us
        // fewer bytes than its content length, that means it was cancelled.
        if byte_count < backend_stream.get_content_length() {
            warn!(
                "Aborting backend stream for {} byte_count={}/{}",
                stream_header,
                byte_count,
                backend_stream.get_content_length()
            );

            // This was truncated, but we've done all we can with it, so
            // consider the chunks consumed.  This handles the case
            // of a STREAM_CANCEL'd chunk from e.g. a 401 PUT request.
            Ok(())
        } else {
            debug!("StreamExpirer stream={} finalizing...", self.stream_id);
            backend_stream.finalize().await.map_err(|e| e.into())
        }
    }

    async fn consume(mut self, stream_header: StreamHeader<'static>) {
        loop {
            match self.try_write_back(&stream_header).await {
                Ok(_) => {
                    break;
                }
                Err(e) => {
                    warn!(
                        "StreamExpirer stream={} waiting to retry {} ({})",
                        self.stream_id, stream_header, e
                    );
                    sleep(Duration::from_millis(1000)).await;
                }
            }
        }

        // If we successfully consumed these chunks, then send
        // the list of chunks through our completion queue to enable
        // the journal's tail pointer to advance past them.
        debug!(
            "StreamExpirer stream={} writeback complete {}",
            self.stream_id, stream_header
        );
        let tx_result = self
            .result_tx
            .send(StreamExpirerResult::new(
                &stream_header.bucket,
                &stream_header.key,
                self.offsets,
            ))
            .await;
        assert!(tx_result.is_ok())
    }
}

// TODO: implement persistence of expired keys so that we can
// minimize double-expiring on restarts (this is for the case
// where we cannot advance the tail pointer for some slow-expiring
// chunk back there, but we may have read way far ahead and expired
// many more objects since)
// ...OR... implement an 'expiry window' concept, and on restart
// go to the backend and check whether things in the window were expired.

pub struct Expirer {
    io: Arc<dyn IOEngine>,
    journal: ChunkJournal,

    read_at: u64,

    // Local copy of the head pointer, only updated when we think
    // we might be out of data, to avoid unnecessary atomics.
    head: u64,

    results_tx: Sender<StreamExpirerResult>,

    backend: BackendHandle,

    rate_cap: Option<f64>,

    stream_expirers: HashMap<StreamId, StreamExpirerHandle>,

    // Streams that are in-progress but not being sent to a StreamExpirer, typically
    // because they're a superseded version of the object
    ignore_streams: HashSet<StreamId>,

    // There is a bunch of IndexKey construction and copy overhead
    // in maintaining this map.  Maybe we should aim to tag all the per-key state
    // onto KeyState and pass around Arc<Keystate> instead of fat keys
    serializer_map: Arc<OrderedWaitMap<IndexKey>>,

    expiries_sem: Arc<Semaphore>,

    // Access to the KeyMap lets us:
    // - Check if a version we're expiring is already superceded
    // - Retrieve the etag for an object before we read as far as
    //   the StreamFooter.
    key_map: Arc<KeyMap>,

    // Expirer offers object contents to the DCache before dropping them
    dcache: Arc<DCache>,
}

// Companion to Expirer: process expiry results and use them to update
// the tail of a Journal
pub struct Tailer {
    key_map: Arc<KeyMap>,
    journal: ChunkJournal,

    // We are the authority for tail: keep a local copy that we'll compare
    // chunk results against, and only write through atomically to self.journal
    // when the value changes.
    tail: u64,

    results_rx: Receiver<StreamExpirerResult>,
}

struct TailSetter {
    journal: ChunkJournal,
    key_map: Arc<KeyMap>,
    last_v: u64,
    rx: watch::Receiver<u64>,
}

/// A simple rate reducer for updates to the tail pointer: ensure that
/// no matter how frequently the tail is logically updated (may be as frequent
/// as each object expired), we only send expensive (header write I/O) tail
/// updates to the journal every so often)
impl TailSetter {
    pub fn new(j: ChunkJournal, km: Arc<KeyMap>) -> (Self, watch::Sender<u64>) {
        let v = j.get_tail();
        let (tx, rx) = watch::channel(v);
        (
            Self {
                journal: j,
                key_map: km,
                last_v: v,
                rx,
            },
            tx,
        )
    }

    pub async fn consume(mut self) {
        debug!("TailSetter: starting consume");
        loop {
            self.rx.changed().await.unwrap();
            let mut v = *self.rx.borrow();
            if v == self.last_v {
                continue;
            }

            debug!("TailSetter: awoke on value {}", v);

            // Great, we have a new value, but don't apply it right away, as
            // we might get some more updates right after and prefer to group
            // them together
            sleep(Duration::from_millis(1000)).await;

            self.last_v = *self.rx.borrow();
            v = *self.rx.borrow();
            debug!(
                "TailSetter journal={} passing through value {:#10x}",
                self.journal.get_id(),
                v
            );
            self.journal.set_tail(self.last_v).await;
            self.last_v = v;

            self.key_map.trim(self.journal.get_id(), v).await;
        }
    }
}

impl Tailer {
    // Consume StreamExpirerResults and use them to update our Journal's tail pointer
    pub async fn consume(&mut self) {
        let mut dirty_tail = self.tail;
        let mut expired_ranges: RangeSet<u64> = RangeSet::new();

        debug!(
            "tailer journal={} starting consume with tail={:#10x}",
            self.journal.get_id(),
            dirty_tail
        );

        let (tail_setter, tail_tx) = TailSetter::new(self.journal.clone(), self.key_map.clone());
        tokio::spawn(async move {
            tail_setter.consume().await;
        });

        loop {
            let ser = self.results_rx.recv().await;
            let ser = match ser {
                Some(ser) => ser,
                None => {
                    // None means all senders were dropped, which shouldn't
                    // happen because there's a sender stored in Expirer:
                    // this indicates that there was a panic in the Expirer,
                    // so we'll panic too
                    panic!("Tailer: result queue sender dropped!  Did expirer panic?");
                }
            };

            // This is a notification to the KeyMap that the key can be dropped
            // once trimming proceeds past the last chunk in `expired_chunks`.
            if let Some((bucket, key)) = ser.key {
                let last_offset = ser.offsets.last().unwrap().location;
                self.key_map
                    .expire(self.journal.get_id(), &bucket, &key, last_offset)
                    .await;
            }

            for chunk in ser.offsets {
                debug!(
                    "tailer journal={} saw chunk {}",
                    self.journal.get_id(),
                    chunk
                );
                assert!(chunk.location >= self.tail);
                let chunk_disk_size = round_to_pow2(chunk.size as usize, IO_ALIGN);
                if chunk.location == dirty_tail {
                    debug!(
                        "tailer journal={} tail={:#010x} sequential advance to {:#010x}, ranges={}",
                        self.journal.get_id(),
                        dirty_tail,
                        dirty_tail + chunk_disk_size as u64,
                        expired_ranges
                    );
                    dirty_tail += chunk_disk_size as u64;

                    // If this completion moved our tail forward, there may be
                    // entries in expired_chunks that can now move it forward further.
                    match expired_ranges.pop_if(dirty_tail) {
                        None => {}
                        Some(up_to) => {
                            debug!("tailer journal={} tail={:#010x} consuming pending up to {:#010x}, ranges={}",
                                   self.journal.get_id(), dirty_tail, up_to, expired_ranges);
                            assert!(up_to > dirty_tail);
                            dirty_tail = up_to;
                        }
                    }
                } else {
                    debug!(
                        "tailer journal={} tail={:#010x} not advancing for {}, ranges={}",
                        self.journal.get_id(),
                        dirty_tail,
                        chunk,
                        expired_ranges
                    );
                    expired_ranges.insert(chunk.location, chunk.location + chunk_disk_size as u64);
                }
            }

            if dirty_tail != self.tail {
                self.tail = dirty_tail;

                tail_tx.send(self.tail).unwrap();

                debug!(
                    "tailer journal={} sending for journal tail={:#10x}",
                    self.journal.get_id(),
                    self.tail
                );
            }
        }
    }
}

// Note: this is generic on the interface J, but it assumes that
// whatever implementation of J will have written the ChunkJournal
// on-disk format (or will never write anything at all).
impl Expirer {
    pub fn open(
        key_map: Arc<KeyMap>,
        journal: ChunkJournal,
        io_engine: Arc<dyn IOEngine>,
        backend: BackendHandle,
        rate_cap: Option<f64>,
        dcache: Arc<DCache>,
    ) -> std::io::Result<(Self, Tailer)> {
        // FIXME Assumption: that we are running before requests are being processed,
        // and therefore tail+head are stable and it is not racy to read them separately
        // (fix this by making it somehow more explicit in Journal's interface)
        let tail = journal.get_tail();
        let head = journal.get_head();

        let (results_tx, results_rx) = mpsc::channel::<StreamExpirerResult>(RESULT_CHANNEL_DEPTH);

        let journal_2 = journal.clone();

        // Forbid negative rates
        assert!(rate_cap.is_none() || rate_cap.unwrap() >= 0.0);

        Ok((
            Self {
                journal,
                read_at: tail,
                head,
                io: io_engine,
                results_tx,
                backend,
                rate_cap,
                stream_expirers: HashMap::new(),
                ignore_streams: HashSet::new(),
                serializer_map: Arc::new(OrderedWaitMap::<IndexKey>::new()),
                expiries_sem: Arc::new(Semaphore::new(MAX_CONCURRENT_EXPIRIES)),
                key_map: key_map.clone(),
                dcache,
            },
            Tailer {
                key_map,
                journal: journal_2,
                tail,
                results_rx,
            },
        ))
    }

    /// When an Offset isn't being fed through a StreamExpirer, pass it here
    /// instead to tip off the tailer that we can advance past it.
    async fn skip_chunk(&mut self, chunk_offset: Offset) {
        let mut offsets = OffsetList::new();
        offsets.push(chunk_offset);
        let tx_result = self
            .results_tx
            .send(StreamExpirerResult::with_invalid_chunks(offsets))
            .await;
        assert!(tx_result.is_ok()); // Send error means Tailer panicked
    }

    async fn ignore_stream(&mut self, chunk_offset: Offset, chunk_header: ChunkHeader) {
        self.skip_chunk(chunk_offset).await;
        if chunk_header.stream_state & STREAM_END == 0 {
            self.ignore_streams.insert(chunk_header.stream_id);
        }
    }

    async fn consume_chunk(
        &mut self,
        chunk_header: ChunkHeader,
        chunk_offset: Offset,
        cursor: Cursor<&[u8]>,
    ) {
        // Otherwise if it's intra-stream, then look up StreamExpirer
        // and call push_chunk with the Offset, then advance
        // read_at by the chunk's on-disk size (i.e. read_at now
        // points either to the next header, or to the head)
        if chunk_header.stream_state & STREAM_START != 0 {
            // FIXME: naive unwrap on external data!
            let stream_header: StreamHeader = bincode::deserialize_from(cursor).unwrap();

            debug!(
                "StreamHeader location={:#10x} {:?}",
                chunk_header.location, stream_header
            );

            if self.ignore_streams.remove(&chunk_header.stream_id) {
                warn!(
                    "Removing stale ignore of stream {} at {} for STREAM_START on the same ID",
                    chunk_header.stream_id, chunk_offset
                );
            }

            let key_map_entry = self
                .key_map
                .get(&stream_header.bucket, &stream_header.key)
                .await;
            let etag = if stream_header.op == ObjectOp::Put {
                match key_map_entry {
                    Some((_gjid, pv)) => {
                        if pv.chunks.is_empty() || pv.chunks[0] != chunk_offset {
                            info!(
                                "Stream superceded by more recent write, dropping: {}",
                                stream_header
                            );
                            self.ignore_stream(chunk_offset, chunk_header).await;
                            return;
                        } else {
                            // This is the KeyMap entry for the stream we are looking at.  Read
                            // its etag.
                            match pv.op {
                                PersistentValueOp::Put(o) => Some(o.etag),
                                PersistentValueOp::Delete(_) => None,
                            }
                        }
                    }
                    None => {
                        // If an expiring object isn't in the KeyMap, this tells us
                        // in advance that this stream will not end with a successful
                        // stream end.  Ignore the rest of the stream.
                        info!(
                            "Dropping STREAM_START: current object not found in KeyMap: {}",
                            stream_header
                        );
                        self.ignore_stream(chunk_offset, chunk_header).await;
                        return;
                    }
                }
            } else {
                // Not a PUT, don't care about looking up etag
                None
            };

            // Potentially wait if we have lots of other streams in flight
            let sem_guard = Semaphore::acquire_owned(self.expiries_sem.clone())
                .await
                .unwrap();

            let mut ser_handle = OrderedWaitMap::acquire(
                &self.serializer_map,
                IndexKey::new(&stream_header.bucket, &stream_header.key),
            );

            match stream_header.op {
                ObjectOp::Put => {
                    let (new_expirer, mut new_handle) = StreamExpirer::new(
                        self.io.clone(),
                        self.backend.clone(),
                        chunk_header.stream_id,
                        stream_header.content_length,
                        // FIXME: gratuitous copy of layout.  For expendience during CHunkJournal refactor.
                        *self.journal.get_layout(),
                        self.results_tx.clone(),
                        etag.map(|v| v.into()),
                        self.dcache.clone(),
                    );

                    new_handle.spawn(ser_handle, new_expirer, stream_header, sem_guard);
                    new_handle.push_chunk(chunk_offset);

                    if chunk_header.stream_state & STREAM_END != 0 {
                        // A single-chunk stream which is both START and END
                        if let Some(mut existing) =
                            self.stream_expirers.remove(&chunk_header.stream_id)
                        {
                            error!("Unclean end of stream {}", chunk_header.stream_id);
                            existing.abort();
                        }

                        new_handle.complete();
                    } else {
                        // A normal multi-chunk stream: put the stream's expirer
                        // into our map for handling future chunks.
                        let existing = self
                            .stream_expirers
                            .insert(chunk_header.stream_id, new_handle);
                        if let Some(mut existing_expirer) = existing {
                            error!("Unclean end of stream {}", chunk_header.stream_id);
                            existing_expirer.abort();
                        }
                    }
                }
                ObjectOp::Delete => {
                    // A delete should always be single chunk (just a StreamHeader)
                    assert_ne!(chunk_header.stream_state & STREAM_END, 0);

                    // TODO: some kind of rate limiting, either here or in S3Backend,
                    // so that when we rattle through a lot of deletions in the journal,
                    // we're not opening too many thousands of concurrent backend connections.

                    // TODO: maybe coalesce DELETES into DeleteObjects operations?

                    // TODO: these streams will VERY frequently fit in 4k, so it's redundant
                    // to send the StreamExpirer an offset that they'll have to read again: we
                    // should have a fast path for passing a StreamExpirer our 4k buffer we just read.

                    let results_tx = self.results_tx.clone();
                    let backend = self.backend.clone();
                    tokio::spawn(async move {
                        // Hold this Semaphore permit until the task drops
                        let _permit = sem_guard;

                        ser_handle.enter().await;

                        loop {
                            match backend.delete(&stream_header).await {
                                Ok(_) => break,
                                Err(e) => {
                                    warn!("Waiting to retry {} ({})", stream_header, e);
                                    sleep(Duration::from_millis(1000)).await;
                                }
                            }
                        }
                        let mut offset_list = OffsetList::new();
                        offset_list.push(chunk_offset);
                        let tx_r = results_tx
                            .send(StreamExpirerResult::new(
                                &stream_header.bucket,
                                &stream_header.key,
                                offset_list,
                            ))
                            .await;
                        assert!(tx_r.is_ok());
                    });
                }
            };
        } else if chunk_header.stream_state & STREAM_CANCEL != 0 {
            let existing = self.stream_expirers.remove(&chunk_header.stream_id);
            if let Some(mut expirer) = existing {
                debug!(
                    "Stream ID {} aborted, dropping expirer",
                    chunk_header.stream_id
                );

                expirer.abort()
            } else {
                debug!(
                    "Stream ID {} not found for STREAM_CANCEL chunk",
                    chunk_header.stream_id
                );
            }

            // Having aborted a StreamExpirer, or not found one, tell tailer to advance
            // past this chunk
            self.skip_chunk(chunk_offset).await;
        } else if chunk_header.stream_state & STREAM_END != 0 {
            // We remove the StreamExpirer from the map as soon as we see the end of the stream,
            // even though the StreamExpirer itself is still running in another task, consuming
            // the list of offsets and reading from disk.  That task is still holding a semaphore
            // guard against `expiries_sem` until it finishes its work, and an OrderedWaitMap
            // handle to guard against other operations starting on the same key.
            let existing = self.stream_expirers.remove(&chunk_header.stream_id);
            if let Some(mut existing) = existing {
                existing.push_chunk(chunk_offset);
                existing.complete();
            } else {
                error!(
                    "Stream END chunk for non-started stream {} at {}",
                    chunk_header.stream_id, chunk_offset
                );
                self.skip_chunk(chunk_offset).await;
            }
        } else {
            // Mid-stream chunk: ID should exist in map
            let expirer = self.stream_expirers.get(&chunk_header.stream_id);
            match expirer {
                Some(stream_expirer) => {
                    debug!(
                        "Expirer journal={} pushing chunk {} for stream {}",
                        self.journal.get_id(),
                        chunk_offset,
                        chunk_header.stream_id
                    );
                    stream_expirer.push_chunk(chunk_offset);
                }
                None => {
                    error!(
                        "Stream mid chunk for non-started stream {}",
                        chunk_header.stream_id
                    );
                    self.skip_chunk(chunk_offset).await;
                }
            }
        }
    }

    // Consume ChunkHeaders from the head of the Journal, and submit
    // them into a pool of StreamExpirers
    pub async fn consume(&mut self) -> std::io::Result<()> {
        debug!("Expirer.consume for {}", self.journal);
        let chunk_header_buf = IOBuffer::new(IO_ALIGN);

        let mut checkpoint_read_at = self.read_at;
        let mut checkpoint_head = self.journal.get_head();
        let mut checkpoint_t = Instant::now();
        let mut allowance: u64 = 0;

        // High water mark of writer rate in bytes/sec
        // Initialized with a conservative guess.
        // The reader's target rate will be the writer_hwm minus
        // the writer's current rate (i.e. "take up the slack")
        let mut writer_hwm: f64 = 1.0 * 1024.0 * 1024.0;

        // Just for edge-triggering a log message when available
        // data goes to zero
        let mut idle = self.head == self.read_at;

        loop {
            assert!(self.head >= self.read_at);

            // First check if any data is available
            if self.head == self.read_at {
                // Maybe there is data, we just need to reload the head pointer?
                self.head = self.journal.get_head();
                if self.head <= self.read_at {
                    // TODO: for the truly idle state, it would be nice to
                    // have a cleaner wait mechanism like a condvar.
                    debug!(
                        "Expirer.{}: sleeping for 0 bytes available head={:#10x} (mid-stream  for {} streams, {} keys in wait map)",
                        self.journal.get_id(),
                        self.head,
                        self.stream_expirers.len(),
                        self.serializer_map.len()
                    );
                    if !idle {
                        idle = true;
                        info!(
                            "Expirer journal={} going idle head={:#010x}",
                            self.journal.get_id(),
                            self.head
                        )
                    }
                    sleep(RATE_INTERVAL).await;
                    continue;
                } else {
                    if idle {
                        idle = false;
                        info!(
                            "Expirer journal={} awaking at head={:#010x} for {:#010x} bytes available",
                            self.journal.get_id(),
                            self.head,
                            self.head - self.read_at
                        );
                    }
                }
            }

            let mut now = Instant::now();
            let update_rate = if now > checkpoint_t + RATE_INTERVAL {
                // If we have been running longer than RATE_INTERVAL since last
                // rate check, then drop any remaining allowance and recalculate.
                debug!(
                    "Expirer.allowance updating for elapsed time (current allowance={})",
                    allowance
                );
                true
            } else if allowance == 0 {
                // If we have exhausted our allowance, sleep until our
                // next rate check interval.
                debug!("Expirer.allowance exhausted, waiting");
                if now < checkpoint_t + RATE_INTERVAL {
                    let sleep_for = RATE_INTERVAL - (now - checkpoint_t);
                    debug!(
                        "Expirer.{} Sleeping {}ms for allowance exhausted",
                        self.journal.get_id(),
                        sleep_for.as_millis()
                    );
                    sleep(sleep_for).await;
                    now = Instant::now();
                }
                true
            } else {
                // Normal continuous operation: we have allowance available
                // and will proceed along the journal.
                false
            };

            if update_rate {
                let new_head = self.journal.get_head();
                self.head = new_head;
                let elapsed = now - checkpoint_t;

                let reader_rate: f64 =
                    (self.read_at - checkpoint_read_at) as f64 / elapsed.as_secs_f64();
                let writer_rate: f64 = (new_head - checkpoint_head) as f64 / elapsed.as_secs_f64();
                writer_hwm = max(writer_rate, writer_hwm);

                if writer_rate == 0.0 && self.rate_cap.is_none() {
                    allowance = u64::MAX;
                    debug!(
                        "Expirer.allowance unlimited for idle writer (reader rate={})",
                        reader_rate
                    );
                } else {
                    debug!(
                        "Expirer.allowance={} (from writer rate {}, hwm {}) (reader rate={})",
                        allowance, writer_rate, writer_hwm, reader_rate
                    );
                    let mut target_rate = writer_hwm - writer_rate;
                    target_rate = match self.rate_cap {
                        Some(v) => min(target_rate, v),
                        None => target_rate,
                    };

                    allowance = (target_rate * RATE_INTERVAL.as_secs_f64()) as u64;
                }

                checkpoint_t = now;
                checkpoint_head = self.head;
                checkpoint_read_at = self.read_at;

                // If we have a zero allowance ever after freshly updating it, it indicates
                // that we should pause.  Sleep here to avoid spinning on the update of the rate.
                if allowance == 0 {
                    sleep(RATE_INTERVAL).await;
                    continue;
                }
            }

            let layout = self.journal.get_layout();

            // Issue 4k IO at self.read_at, read ChunkHeader
            debug!(
                "Expirer journal={} read at {:#10x}",
                self.journal.get_id(),
                self.read_at
            );
            self.io
                .read_at(
                    chunk_header_buf.as_slice_mut(),
                    layout.offset_to_disk(self.read_at),
                )
                .await?;

            let mut cursor = Cursor::new(chunk_header_buf.as_slice());
            let chunk_header: ChunkHeader = match bincode::deserialize_from(&mut cursor) {
                Ok(ch) => ch,
                Err(e) => {
                    // This is a panic, because the writer side should never write garbage, and
                    // the replay process should catch any pre-existing garbage on disk.  Expirer
                    // should never see it.
                    panic!(
                        "Corrupt ChunkHeader read at journal={} {:010x}: {}",
                        self.journal.get_id(),
                        self.read_at,
                        e
                    );
                }
            };

            if chunk_header.location != self.read_at {
                error!(
                    "Expirer journal={} ate tail at {:#10x} (chunk is {:#10x})",
                    self.journal.get_id(),
                    self.read_at,
                    chunk_header.location
                );
                // This is a bug, but only crash out in testing.
                debug_assert!(false);
                sleep(Duration::from_millis(500)).await;
            }

            debug!(
                "ChunkHeader journal={} location={:#10x} stream={} state={:04x} size={:#06x} blocks={}",
                self.journal.get_id(),
                chunk_header.location,
                chunk_header.stream_id,
                chunk_header.stream_state,
                chunk_header.data_size,
                chunk_header.blocks
            );

            // FIXME: confusing convention: ChunkHeader's data_size does not include
            // the header itself, but Offset's size does include the header: we should
            // pick one convention and go with it.
            let chunk_offset = Offset::new(
                self.read_at,
                chunk_header.data_size as usize
                    + bincode::serialized_size(&chunk_header).unwrap() as usize,
            );

            let bytes_consumed = chunk_header.blocks as u64 * IO_ALIGN as u64;
            self.read_at += bytes_consumed;
            allowance -= min(allowance, bytes_consumed);

            if self.ignore_streams.contains(&chunk_header.stream_id) {
                self.skip_chunk(chunk_offset).await;
                if chunk_header.stream_state & (STREAM_END | STREAM_CANCEL) != 0 {
                    debug!(
                        "Done ignoring stream {} at {}",
                        chunk_header.stream_id, chunk_offset
                    );
                    self.ignore_streams.remove(&chunk_header.stream_id);
                }
            } else {
                // Normal case: we are not skipping this stream
                self.consume_chunk(chunk_header, chunk_offset, cursor).await;
            }
        }
    }
}
