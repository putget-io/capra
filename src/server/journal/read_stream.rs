use futures::task::{Context, Poll};
use futures::Future;
use std::pin::Pin;

use hyper::body::Bytes;

use crate::server::journal::chunk_journal::{ChunkJournal, JournalLockGuard};
use crate::server::journal::io_buffer::{IOBuffer, IO_ALIGN};
use crate::server::types::{Offset, OffsetList};
use crate::util::round_to_pow2;
use capralib::simple_pool::{ItemHandle, SimplePool};

use super::chunk_journal::CHUNK_HEADER_SIZE;
use crate::server::journal::StreamHeader;
use capralib::format::dump_buffer;
use log::error;
use std::io::Cursor;

type IOResult = (
    Offset,
    Pin<Box<(dyn Future<Output = (ItemHandle<IOBuffer>, std::io::Result<usize>)> + Send)>>,
);

pub struct ReadStream {
    journal: ChunkJournal,
    buffers: SimplePool<IOBuffer>,
    _guard: JournalLockGuard,
    offset_list: OffsetList,
    next: usize,
    ios: Vec<IOResult>,
    first: bool,
}

impl ReadStream {
    pub fn new(
        journal: ChunkJournal,
        guard: JournalLockGuard,
        buffers: SimplePool<IOBuffer>,
        offset_list: OffsetList,
    ) -> Self {
        let concurrency = buffers.get_capacity();
        Self {
            journal,
            buffers,
            _guard: guard,
            offset_list,
            next: 0,
            ios: Vec::with_capacity(concurrency),
            first: true,
        }
    }

    // Once all our IO is done, handle an IOBuffer that is
    // allegedly populated with the bytes from a given Offset
    pub fn unpack_buffer(
        &self,
        offset: &Offset,
        buffer: &IOBuffer,
        io_result: std::io::Result<usize>,
        first: bool,
    ) -> <ReadStream as futures::Stream>::Item {
        let size_aligned = round_to_pow2(offset.size as usize, IO_ALIGN);

        // Check IO was successfully completed
        match io_result {
            Ok(sz) => {
                if sz != size_aligned {
                    error!(
                        "Short read {:#10x} at offset {} (wanted {:#10x})",
                        sz, offset, size_aligned
                    );
                    return Err(Box::new(std::io::Error::from_raw_os_error(5)));
                }
            }
            Err(e) => return Err(Box::new(e)),
        };

        let cursor = Cursor::new(&buffer.as_slice()[CHUNK_HEADER_SIZE..]);
        let payload_starts_at: usize = if first {
            let stream_header: StreamHeader = match bincode::deserialize_from(cursor) {
                Ok(sh) => sh,
                Err(e) => {
                    error!(
                        "journal={} offset={} corrupt StreamHeader: {}",
                        self.journal.get_id(),
                        offset,
                        e
                    );
                    dump_buffer(log::Level::Error, &buffer.as_slice());
                    return Err(Box::new(std::io::Error::from_raw_os_error(5)));
                }
            };
            let stream_header_len = bincode::serialized_size(&stream_header).unwrap();
            CHUNK_HEADER_SIZE + stream_header_len as usize
        } else {
            CHUNK_HEADER_SIZE
        };

        let data_payload = &buffer.as_slice()[payload_starts_at..offset.size as usize];

        // Data copy from IOBuffer to Bytes: this is partly because we're used with Request<Body>
        // request handlers (Body needs Bytes), and partly because we want our precious IOBuffers
        // back ASAP for use elsewhere.
        // TODO: It might make more sense to use non-aligned buffers
        // and buffered I/O on the read path: that way we could read directly into Bytes,
        // and workloads that like to repeatedly read dirty objects would benefit from
        // the page cache.
        Ok(Bytes::copy_from_slice(data_payload))
    }
}

impl futures::Stream for ReadStream {
    type Item = Result<Bytes, Box<dyn std::error::Error + Send + Sync>>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        // While we have buffers available and reads still to issue, issue those reads
        while self.next < self.offset_list.len() {
            let next_buffer = self.buffers.acquire_smart();

            let mut pinned = Box::pin(next_buffer);
            let buffer = match pinned.as_mut().poll(cx) {
                Poll::Ready(buffer) => buffer,
                Poll::Pending => {
                    break;
                }
            };
            drop(pinned);

            let offset = self.offset_list[self.next];
            self.next += 1;

            let layout = self.journal.get_layout();
            let disk_location = layout.offset_to_disk(offset.location);
            let size_aligned = round_to_pow2(offset.size as usize, IO_ALIGN);

            let io = self.journal.get_io().clone();
            let fut = async move {
                let mut read_into = &mut (buffer.as_slice_mut()[0..size_aligned]);
                let r = io.read_at(&mut read_into, disk_location).await;
                (buffer, r)
            };

            self.ios.push((offset, Box::pin(fut)));
        }

        // No IOs in flight?
        if self.ios.is_empty() {
            // If we weren't at the end of things to dispatch, we should always
            // have an IO in flight.
            assert!(self.next >= self.offset_list.len());

            return Poll::Ready(None);
        }

        // If the first IO in flight is ready, we are ready
        let fut = &mut self.ios[0].1;
        match fut.as_mut().poll(cx) {
            Poll::Ready(join_result) => {
                let (buffer, io_result) = join_result;

                let offset = &self.ios[0].0;

                let r = self.unpack_buffer(offset, &*buffer, io_result, self.first);
                self.first = false;

                self.ios.remove(0);
                Poll::Ready(Some(r))
            }
            Poll::Pending => Poll::Pending,
        }
    }
}
