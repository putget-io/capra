use std::borrow::Borrow;
use std::cell::RefCell;
use std::cmp::{Eq, PartialEq};
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::fmt;
use std::io;
use std::io::prelude::*;
use std::io::Cursor;
use std::mem;
use std::num::NonZeroU16;
use std::os::unix::io::AsRawFd;
use std::sync::{atomic, Arc};

use parking_lot::Mutex;

use async_trait::async_trait;
use bincode;
use futures::{Future, FutureExt};
use log::*;
use partial_min_max::max;
use partial_min_max::min;
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::Notify;
use tokio::time::{sleep, Duration};

use crate::server::journal::{JournalId, StreamFooter, StreamHeader, STREAM_FOOTER_SIZE};
use crate::server::types::{Offset, OffsetList};
use crate::util;
use capralib::simple_pool::ItemHandle;

use super::super::core_tracker::CoreTracker;
use super::super::key_map::KeyMap;
use super::super::range_set::RangeSet;
use super::io_buffer::{IOBuffer, IO_ALIGN};
use crate::server::io_engine::IOEngine;
use capralib::simple_pool::SimplePool;
use std::fs::OpenOptions;

use crate::server::journal::read_stream::ReadStream;
use digest;
use md5;
use nix::sys::statfs::statfs;
use nix::NixPath;
use std::path::Path;
use std::pin::Pin;
use std::time::Instant;

thread_local!(static THREAD_JOURNAL: RefCell<Option<ChunkJournal>> = RefCell::new(None));

const IO_BUFFER_SIZE: usize = 0x8000;

pub const STREAM_START: u8 = 0x1;
pub const STREAM_END: u8 = 0x4;
pub const STREAM_CANCEL: u8 = 0x8;

// New journals don't start at zero, to reduce risk of misinterpreting
// an uninitialized buffer as a valid ChunkHeader (paranoia, early dev)
const INITIAL_HEAD: u64 = IO_ALIGN as u64;

/// The space reserved for the journal header in on-disk
/// layout.  The actual journal header's serialized size
/// must be <= this.
pub const JOURNAL_HEADER_SPACE: u64 = 0x4000;

/// How far ahead of our contiguously committed region we will write, i.e.
/// how far ahead of `head`.  Generous to enable lots of parallel IO.
const DIRTY_WINDOW: u64 = 100_000_000;

// Default journal size is big enough to accomodate the maximum
// S3 put size (5GB) plus overhead
pub const DEFAULT_JOURNAL_SIZE: u64 =
    1024 * 1024 * 1024 * 5 + JOURNAL_HEADER_SPACE + RESERVED_CAPACITY;

// Refuse to create pathologically small journals
pub const MIN_JOURNAL_SIZE: u64 = 100 * 1024 * 1024;

// How many StreamIds we'll hand out concurrently per journal
pub const CONCURRENT_STREAMS_PER_JOURNAL: usize = 128;

// How many IO_BUFFER_SIZE slabs are allocate per journal
const BUFFERS_PER_JOURNAL: usize = 0x1000;

// Max buffers available to a single BigStream.  Fewer may be available if
// there are other consumers of the journal's buffer pool, such as SmallStream IOs.
const BUFFERS_PER_STREAM: usize = BUFFERS_PER_JOURNAL / CONCURRENT_STREAMS_PER_JOURNAL;

// Max buffers that will be acquired by a single read operation.
// FIXME: think harder about buffer allowances, whether some prioritization is needed
// for reads vs writes
const BUFFERS_PER_READ: usize = BUFFERS_PER_STREAM;

// We must reserve some space (i.e. head is never allowed to come closer than
// this to the tail) in order to guarantee that replay() can write STREAM_CANCEL
// if needed.
const RESERVED_CAPACITY: u64 = (CONCURRENT_STREAMS_PER_JOURNAL * IO_ALIGN) as u64;

trait IOFuture: Future<Output = io::Result<()>> + Send {}
impl<T> IOFuture for T where T: Future<Output = io::Result<()>> + Send {}

#[derive(Serialize, Deserialize)]
pub struct JournalHeader {
    // Total space to use, including JournalHeader
    size: u64,

    // The oldest data that may not be overwritten
    tail: u64,

    // How far forward we *have* written
    head: u64,
}

impl JournalHeader {
    pub fn new(size: u64, head: u64, tail: u64) -> Self {
        Self { head, tail, size }
    }
}

impl fmt::Display for JournalHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "JournalHeader<{:#10x}/{:#10x}>", self.tail, self.head)
    }
}

#[derive(Clone, Copy, Serialize, Deserialize, Hash, Eq)]
pub struct StreamId(pub NonZeroU16);

impl PartialEq for StreamId {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl fmt::Display for StreamId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#06x}", self.0)
    }
}

const CHUNK_MAGIC: u64 = 0xfeedbeeffaded1ce;

#[derive(Serialize, Deserialize)]
pub struct ChunkHeader {
    // FIXME: this is gratuitous use of disk space, but makes debugging easier
    // and makes our replay head-searching a bit less unsafe.
    pub magic: u64,

    // Storing the location enables readers of a wrapping journal to determine
    // whether a chunk is logically 'next', or if it is just detritus from the
    // last time we used this space.  It also acts as a de-facto magic string
    // to let the reader validate that they are getting what they expect and
    // not some other weird bytes.
    pub location: u64,

    pub stream_id: StreamId,

    pub stream_state: u8,

    // Size of the chunk on disk (i.e. how far to the start of the next chunk)
    pub blocks: u8,

    // Size of the stream user data within the chunk, not including header.
    // If (blocks*IO_ALIGN)-sizeof(chunkheader) is greater than size,
    // then the trailing bytes should be zeros and readers must skip them.
    pub data_size: u32,
}

/// Serialized size
pub(super) const CHUNK_HEADER_SIZE: usize = 24;

#[derive(Copy, Clone)]
pub struct DiskLayout {
    // The on-disk size of the journal, including journal header
    raw_size: u64,
}

impl DiskLayout {
    pub fn new(raw_size: u64) -> Self {
        assert_eq!(raw_size & (IO_ALIGN - 1) as u64, 0x0);
        assert_eq!(JOURNAL_HEADER_SPACE & (IO_ALIGN - 1) as u64, 0x0);
        assert!(raw_size >= JOURNAL_HEADER_SPACE + IO_ALIGN as u64);
        Self { raw_size }
    }

    fn data_size(&self) -> u64 {
        self.raw_size - JOURNAL_HEADER_SPACE
    }
}

impl DiskLayout {
    // - Journal offsets start at zero and go up to infinity
    // - Disk positions start after the header and wrap according
    // to the journal length.
    pub fn offset_to_disk(&self, location: u64) -> u64 {
        JOURNAL_HEADER_SPACE + (location % self.data_size())
    }
}

pub fn register_thread(chunk_journal: ChunkJournal) {
    THREAD_JOURNAL.with(move |rc| {
        let chunk_journal: ChunkJournal = chunk_journal.clone();
        let mut rc = rc.borrow_mut();
        *rc = Some(chunk_journal);
    });
}

pub fn get_for_thread() -> ChunkJournal {
    THREAD_JOURNAL.with(|rc| {
        let opt = rc.borrow();
        let journal_option = (*opt).borrow();
        let journal = match journal_option {
            Some(journal) => journal.clone(),
            None => panic!("No journal initialized on this thread!"),
        };
        journal
    })
}

/// Responsible for consuming a channel of stream completions, and
/// using these to update the persistent head pointer of the journal.
/// This is done in a background channel consumer rather than at the point
/// a request handler is waiting on persistence of their stream, to avoid
/// adding latency to the client-facing request handler.
pub struct HeadAdvancer {
    journal: ChunkJournal,
    completions_rx: Receiver<Offset>,
    committed_ranges: RangeSet<u64>,
    head: u64,
}

impl HeadAdvancer {
    pub async fn consume(&mut self) {
        self.head = self.journal.get_head();

        loop {
            self.consume_one().await
        }
    }
    pub async fn consume_one(&mut self) {
        let offset = match self.completions_rx.recv().await {
            Some(ol) => ol,
            None => {
                panic!("HeadAdvancer: completion queue closed");
            }
        };

        let mut dirty_head = self.head;

        debug!(
            "Head journal={} saw offset {} (dirty head {:#10x})",
            self.journal.get_id(),
            offset,
            dirty_head
        );
        let ondisk_size = util::round_to_pow2(offset.size as usize, IO_ALIGN);
        if offset.location == dirty_head {
            debug!(
                "Head journal={} advancing offset {} (dirty head {:#10x}) size {:#10x}",
                self.journal.get_id(),
                offset,
                dirty_head,
                ondisk_size
            );
            dirty_head += ondisk_size as u64;

            match self.committed_ranges.pop_if(dirty_head) {
                None => {}
                Some(up_to) => {
                    assert!(up_to > dirty_head);
                    dirty_head = up_to;
                }
            }
        } else {
            self.committed_ranges
                .insert(offset.location, offset.location + ondisk_size as u64);
        }

        if dirty_head != self.head {
            debug!(
                "Head journal={} advancing {:#10x} -> {:#10x}, pending ranges {}",
                self.journal.get_id(),
                self.head,
                dirty_head,
                self.committed_ranges
            );
            self.head = dirty_head;
            self.journal.set_head(self.head);
        } else {
            debug!(
                "Head journal={} not advancing yet ({:#10x}), pending ranges: {}",
                self.journal.get_id(),
                self.head,
                self.committed_ranges
            );
        }
    }
}

pub type ChunkJournal = Arc<ChunkJournalInner>;

pub fn open_journal(
    id: JournalId,
    path: &str,
    io_engine: Arc<dyn IOEngine>,
    disabled: bool,
) -> std::io::Result<(ChunkJournal, HeadAdvancer)> {
    let (inner, completions_rx) = ChunkJournalInner::open(id, path, io_engine, disabled)?;

    let journal = ChunkJournal::new(inner);

    let advancer = HeadAdvancer {
        journal: journal.clone(),
        completions_rx,
        committed_ranges: RangeSet::new(),
        // The real load of the head value is at the start of HeadAdvancer::consume (which
        // happens after replay() to get the real value), but populating the journal's
        // initial head here is useful for tests that skip consume and just call consume_one.
        head: journal.get_head(),
    };

    Ok((journal, advancer))
}

// We care about alignment because we'll allocate a small-n group of
// these at startup, for use by different cores, and wouldn't want
// to risk them overlapping in cache lines
#[repr(align(64))]
pub struct ChunkJournalInner {
    // Non-hot path
    path: String,

    // > Writer state
    id: JournalId,
    stream_ids: SimplePool<StreamId>,

    // Where we will write the next data.
    cursor: Mutex<u64>,

    // Everything up to here is committed (i.e. it is safe for Expirer to read
    // up to here)
    head: atomic::AtomicU64,

    // State for background advancement of head
    completions_tx: Sender<Offset>,
    // < Writer state

    // > Shared reader + writer state
    disabled: bool,
    io: Arc<dyn IOEngine>,
    layout: DiskLayout,
    // < Shared

    // > Reader/expirer state
    tail: atomic::AtomicU64,
    tail_locks: Mutex<(u64, JournalLocks)>,
    tail_setter_wait: Notify,
    // < Reader
    buffers: SimplePool<IOBuffer>,
}

unsafe impl Send for ChunkJournalInner {}
unsafe impl Sync for ChunkJournalInner {}

impl<'a> fmt::Display for ChunkJournalInner {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{} {}", self.get_id(), self.get_path())
    }
}

pub struct JournalLockGuard {
    // FIXME: this Arc to ChunkJournalInner shouldn't really
    // be necessary (our guards always go along with a journal
    // reference), but I had lifetime issues trying to
    // pass the guard and the journal together into a tokio::spawn
    journal: ChunkJournal,
    offset: u64,
}

impl Drop for JournalLockGuard {
    fn drop(&mut self) {
        self.journal.read_lock_release(self.offset);
    }
}

struct JournalLocks {
    // Reference count for each locked location.  Empty if no
    // reads are in flight.
    locks: BTreeMap<u64, usize>,
    lowest: Option<u64>,
}

impl JournalLocks {
    fn acquire(&mut self, offset: u64) {
        *self.locks.entry(offset).or_insert(0) += 1;
    }

    fn release(&mut self, offset: u64) {
        let n: &mut usize = match self.locks.get_mut(&offset) {
            Some(n) => n,
            None => {
                // Buggy but non-fatal condition
                error!("JournalLock entry not found releasing {:#10x}", offset);
                debug_assert!(false);
                return;
            }
        };

        *n -= 1;
        if *n == 0 {
            self.locks.remove(&offset);

            // Maybe update self.lowest
            if Some(offset) == self.lowest {
                // We were the last lock on the current lowest location, update it.
                self.lowest = match self.locks.keys().next() {
                    Some(v) => Some(*v),
                    None => None,
                };
            }
        }
    }

    fn lowest(&self) -> Option<u64> {
        self.lowest
    }

    fn new() -> Self {
        Self {
            locks: BTreeMap::new(),
            lowest: None,
        }
    }
}

impl<'a> ChunkJournalInner {
    pub fn get_id(&self) -> JournalId {
        self.id
    }

    pub fn get_path(&self) -> &str {
        &self.path
    }

    pub fn get_head(&self) -> u64 {
        self.head.load(atomic::Ordering::Relaxed)
    }

    pub fn get_tail(&self) -> u64 {
        self.tail.load(atomic::Ordering::Relaxed)
    }

    pub fn get_layout(&self) -> &DiskLayout {
        &self.layout
    }

    // Enable ReadStream to get an IOEngine handle
    pub(super) fn get_io(&self) -> &Arc<dyn IOEngine> {
        &self.io
    }

    /// Streams can't be longer than the journal!
    pub fn get_max_stream_length(&self) -> u64 {
        self.layout.data_size() - RESERVED_CAPACITY
    }

    pub async fn set_tail(&self, tail: u64) {
        assert!(tail > self.get_tail());

        let must_wait = {
            let mut guard = self.tail_locks.lock();

            // Advance projected_tail: this will stop any entrants to tail_locks in
            // read_lock_aquire from granting new locks behind this location.
            {
                let projected_tail = &mut guard.0;
                assert!(tail > *projected_tail);
                *projected_tail = tail;
                debug!(
                    "set_tail journal={} projecting {:#10x}",
                    self.get_id(),
                    tail
                );
            }

            // We must wait if there are locks, and the earliest one is before our
            // new tail location.
            {
                let locks = &mut guard.1;
                match locks.lowest() {
                    Some(v) => v < tail,
                    None => false,
                }
            }
        };

        if must_wait {
            debug!("set_tail: trim is waiting for tail locks to release");
            // We must wait until these locks are released: read_lock_release cooperates
            // with us to signal when that has happened
            self.tail_setter_wait.notified().await;
            debug!("set_tail: trim finished waiting for tail locks");
        }

        // Important: we must persist the new tail location into the journal header
        // before updating our self.tail - advancing our tail will permit the header
        // to advance into this space.
        self.write_journal_header(self.head.load(atomic::Ordering::Relaxed), tail)
            .await;

        // FIXME: this may actually be pretty inefficient, if the Expirer is calling
        // from a different core than the journal's home core (where most of its PUT
        // flow is), and the tail and head pointers are on the same cache line: this
        // operation will dirty the home core's cache of the head pointer, that it
        // touches every time it does an insert (our latency sensitive path).
        // We should probably have two separate structures for journals: the writer-owned
        // state (head etc) and the reader-owned state (tail)
        self.tail.store(tail, atomic::Ordering::Relaxed);

        debug!("set_tail journal={} complete {:#10x}", self.get_id(), tail);
    }

    /// If this returns None, it means the offset is already expired
    /// and you may not read it.  If this returns a JournalLockGuard,
    /// you are safe to call read() and read_header() at this offset and
    /// any ahead of it, until that guard is dropped.
    fn read_lock_acquire(self: &ChunkJournal, offset: u64) -> Option<JournalLockGuard> {
        if self.disabled {
            return None;
        }

        let mut guard = self.tail_locks.lock();
        let projected_tail = guard.0;
        let locks = &mut guard.1;

        if projected_tail > offset {
            debug!(
                "reject journal={} lock on {:#10x} (vs {:#10x}",
                self.get_id(),
                offset,
                projected_tail
            );
            None
        } else {
            locks.acquire(offset);
            Some(JournalLockGuard {
                journal: self.clone(),
                offset,
            })
        }
    }

    pub async fn read(self: ChunkJournal, offset_list: OffsetList) -> Option<ReadStream> {
        let guard = self.read_lock_acquire(offset_list[0].location);

        // Drop out if we couldn't lock the location: we cannot fulfil this read
        let guard = if guard.is_none() {
            return None;
        } else {
            guard.unwrap()
        };

        let max_size = util::round_to_pow2(
            offset_list.iter().max_by_key(|o| o.size).unwrap().size as usize,
            IO_ALIGN,
        );
        debug!(
            "ChunkJournal.read: reading {} offsets, max size {:#10x}",
            offset_list.len(),
            max_size
        );

        let buffers =
            SimplePool::new_steal(&self.buffers, min(offset_list.len(), BUFFERS_PER_READ)).await;

        let stream = ReadStream::new(self.clone(), guard, buffers, offset_list);

        Some(stream)
    }

    fn read_lock_release(&self, offset: u64) {
        let mut guard = self.tail_locks.lock();
        let projected_tail = guard.0;
        let locks = &mut guard.1;
        locks.release(offset);

        if offset < projected_tail {
            debug!("Completed read in region pending trim: {:#10x}", offset);
            // set_tail was waiting for us to complete
            let lowest = locks.lowest();
            if lowest.is_none() || lowest.unwrap() >= projected_tail {
                // set_tail may now proceed, no more locks behind the intended
                // new value of projected_tail.
                debug!(
                    "read_lock_release offset={:#10x}, kicking set_tail water",
                    offset
                );
                self.tail_setter_wait.notify_one();
            }
        } else {
            debug!(
                "Completed read offset={:#10x} (projected_tail={:#10x})",
                offset, projected_tail
            );
        }
    }

    // TODO reinstate or remove: this used to be needed for HEAD requests before
    // KeyState was expanded to hold full object metadata in RAM
    pub async fn _read_header(
        &self,
        offset_list: &OffsetList,
    ) -> std::io::Result<StreamHeader<'static>> {
        assert!(offset_list.len() > 0);

        let chunk_0_offset = offset_list[0].location;
        debug!(
            "ChunkJournal.read_header: reading chunk at offset {:#10x}",
            chunk_0_offset
        );
        let chunk_bytes = IOBuffer::new(IO_ALIGN);
        let sz = self
            .io
            .read_at(
                chunk_bytes.as_slice_mut(),
                self.layout.offset_to_disk(chunk_0_offset),
            )
            .await?;

        assert_eq!(sz, IO_ALIGN);

        let mut cursor = Cursor::new(chunk_bytes.as_slice());
        let chunk_header: ChunkHeader = bincode::deserialize_from(&mut cursor).unwrap();
        assert_eq!(chunk_header.location, chunk_0_offset);

        let stream_header: StreamHeader = bincode::deserialize_from(cursor).unwrap();
        debug!(
            "ChunkJournal.read_header: read StreamHeader: {} {}",
            stream_header.content_length, stream_header.key
        );

        Ok(stream_header)
    }

    pub fn format(path: &Path, size: u64) -> std::io::Result<()> {
        assert_eq!(size & (IO_ALIGN - 1) as u64, 0x0);
        assert!(size > 0);

        let header_ser =
            bincode::serialize(&JournalHeader::new(size, INITIAL_HEAD, INITIAL_HEAD)).unwrap();

        // Write the header and zero the rest of JOURNAL_HEADER_SPACE
        let mut file = OpenOptions::new().write(true).open(&path).unwrap();
        file.write_all(&header_ser[..])?;
        let zero_pad_n = JOURNAL_HEADER_SPACE as usize - header_ser.len();
        let mut zero_pad: Vec<u8> = Vec::with_capacity(zero_pad_n);
        zero_pad.resize(zero_pad_n, 0);
        file.write_all(&zero_pad)?;

        // Trim the rest of the device
        let trim_begin = JOURNAL_HEADER_SPACE as libc::off_t;
        let mut trim_len = size as libc::off_t - trim_begin;

        // In theory linux lets you call fallocate on block devices to get
        // to fast zero+discard, but in practice fallocate is rather slow (slower
        // than simply writing zeroes) whereas the BLKDISCARD ioctl is nearly instant
        // on an NVME device.
        let args: [u64; 2] = [trim_begin as u64, trim_len as u64];
        let res = unsafe { ioctls::blkdiscard(file.as_raw_fd(), &args) };
        if res == -1 {
            // -1 is what we get if calling blkdiscard on a non-flash device.  Fall back
            // to fallocating zeros.

            // This is going to be WAY too slow to do a whole drive if someone calls it on
            // a whole spinning disk, so just write a modest amount of zeroed space to erase
            // any previous journal content that might otherwise get replayed on first
            // startup.
            trim_len = min(4 * 1024 * 1024 * 1024, trim_len);

            warn!(
                "DISCARD unavailable (non-flash device?), falling back zeroing first {}",
                bytefmt::format(trim_len as u64)
            );

            // On flash drives, fallocate can be slower than just writing zeros from userspace, but
            // on this code path we're probably on a spinning disk or virtualized disk, so take
            // the fallocate route to at least give the kernel/driver a chance to optimize.
            nix::fcntl::fallocate(
                file.as_raw_fd(),
                nix::fcntl::FallocateFlags::FALLOC_FL_ZERO_RANGE
                    | nix::fcntl::FallocateFlags::FALLOC_FL_KEEP_SIZE,
                trim_begin,
                trim_len,
            )
            .map_err(|e| std::io::Error::from_raw_os_error(e.as_errno().unwrap() as i32))
        } else if res != 0 {
            Err(std::io::Error::from_raw_os_error(res))
        } else {
            Ok(())
        }
    }

    fn set_head(&self, v: u64) {
        self.head.store(v, atomic::Ordering::Relaxed)
    }

    pub fn create(path: &Path, size: u64) -> std::io::Result<()> {
        assert_eq!(size & (IO_ALIGN - 1) as u64, 0x0);
        assert!(size > 0);
        let pave_write_size: usize = 1024 * 1024;
        let mut zeros: Vec<u8> = Vec::with_capacity(pave_write_size);
        zeros.resize(pave_write_size, 0);

        info!(
            "Creating journal at {} with size {}",
            path.display(),
            bytefmt::format(size)
        );
        let mut file = OpenOptions::new().create(true).write(true).open(&path)?;

        // Serialize and write header
        let header_ser = bincode::serialize(&JournalHeader {
            size,
            head: INITIAL_HEAD,
            tail: INITIAL_HEAD,
        })
        .unwrap();
        file.write_all(&header_ser[..])?;

        // Fallocate zeros to the desired size of the file
        let r = nix::fcntl::fallocate(
            file.as_raw_fd(),
            nix::fcntl::FallocateFlags::from_bits(0).unwrap(),
            header_ser.len() as libc::off_t,
            size as libc::off_t - header_ser.len() as libc::off_t,
        );
        r.map_err(|e| std::io::Error::from_raw_os_error(e.as_errno().unwrap() as i32))
    }

    fn parent_free_bytes(path: &Path) -> nix::Result<u64> {
        let stats = match path.parent() {
            None => {
                return Err(nix::Error::from_errno(nix::errno::from_i32(22)));
            }
            Some(parent) => {
                if parent.len() == 0 {
                    statfs("./")?
                } else {
                    statfs(parent)?
                }
            }
        };
        Ok(stats.blocks_available() * stats.block_size() as u64)
    }

    /// If the journal doesn't exist, create it.
    pub fn ensure(path: &Path) -> std::io::Result<()> {
        let meta = std::fs::metadata(&path);
        if let Err(e) = meta {
            if e.kind() == std::io::ErrorKind::NotFound {
                // Determine size to use based on available space
                let available_bytes = Self::parent_free_bytes(path)
                    .map_err(|e| std::io::Error::from_raw_os_error(e.as_errno().unwrap() as i32))?;

                let size = if available_bytes > DEFAULT_JOURNAL_SIZE {
                    DEFAULT_JOURNAL_SIZE
                } else if available_bytes < MIN_JOURNAL_SIZE {
                    return Err(std::io::Error::from_raw_os_error(28));
                } else {
                    let mut size = available_bytes / 2;
                    size = size - (size & (IO_ALIGN as u64 - 1));
                    size
                };

                Self::create(path, size)
            } else {
                error!("{}", e.raw_os_error().unwrap());
                return Err(e);
            }
        } else {
            Ok(())
        }
    }

    async fn write_journal_header(&self, head: u64, tail: u64) {
        let header = &JournalHeader {
            size: self.layout.raw_size,
            head: head as u64,
            tail: tail as u64,
        };
        debug!("write_journal_header: {}", header);
        let header_ser = bincode::serialize(&header).unwrap();

        let buf = IOBuffer::new(JOURNAL_HEADER_SPACE as usize);
        buf.as_slice_mut()[0..header_ser.len()].copy_from_slice(&header_ser[..]);

        self.io.write_at_sync(buf.as_slice(), 0).await.unwrap();
    }

    /// The no_direct flag is only for tests, which are likely to run on tmp filesystems
    /// that do not support O_DIRECT
    pub fn open(
        id: JournalId,
        path: &str,
        io_engine: Arc<dyn IOEngine>,
        disabled: bool,
    ) -> std::io::Result<(Self, Receiver<Offset>)> {
        let journal_header = if !disabled {
            debug!("Opening journal at {}", path,);

            // Temporary FD to read the header synchronously
            let mut header_fd = OpenOptions::new().read(true).open(&path).unwrap();

            let mut buf: Vec<u8> = Vec::with_capacity(JOURNAL_HEADER_SPACE as usize);
            buf.resize(JOURNAL_HEADER_SPACE as usize, 0);
            header_fd.read(&mut buf[..])?;
            // FIXME: handle decode failure
            let journal_header: JournalHeader = bincode::deserialize(&buf[..]).unwrap();
            debug!("Loaded journal header {}", journal_header);

            journal_header
        } else {
            debug!("Opening null journal on CPU {}", CoreTracker::get_cpu_id());

            let journal_header = JournalHeader::new(0, 0, 0);
            journal_header
        };

        let mut free_streams: Vec<StreamId> = Vec::new();
        for i in 1..(CONCURRENT_STREAMS_PER_JOURNAL as u16 + 1) {
            free_streams.push(StreamId(NonZeroU16::new(i).unwrap()));
        }

        // Note that WriteBackground reserves a space here at the point
        // it starts an I/O, so it needs some slack (the factor of 2) to
        // avoid writers getting hung up waiting for HeadAdvancer to consume
        // from the channel.
        let (completions_tx, completions_rx) =
            channel(CONCURRENT_STREAMS_PER_JOURNAL * BUFFERS_PER_STREAM * 2);

        Ok((
            Self {
                id,
                stream_ids: SimplePool::new(free_streams),
                io: io_engine,
                cursor: Mutex::new(journal_header.head),
                head: atomic::AtomicU64::new(journal_header.head),
                tail: atomic::AtomicU64::new(journal_header.tail),
                tail_locks: Mutex::new((journal_header.tail, JournalLocks::new())),
                tail_setter_wait: Notify::new(),
                layout: DiskLayout::new(journal_header.size),
                path: path.to_string(),
                completions_tx,
                buffers: SimplePool::new_with(BUFFERS_PER_JOURNAL, || {
                    IOBuffer::new(IO_BUFFER_SIZE)
                }),
                disabled,
            },
            completions_rx,
        ))
    }

    /// Once main replay process has finished locating all the chunks for
    /// a stream, issue any additional reads required to read the stream footer.
    // (The 'a scattered across args is to prevent an E0700 compile error, which I believe
    // is a compiler bug - in future this might work without the 'a)
    async fn replay_finish_stream(
        &'a self,

        key_map: &'a KeyMap,
        offset_list: OffsetList,
        stream_header: StreamHeader<'static>,

        // The header of the last chunk in the stream.
        chunk_header: &'a ChunkHeader,
        // The first 4k of the last chunk, which may also be the first chunk
        // for short streams.
        buffer: &'a IOBuffer,
    ) -> std::io::Result<()> {
        assert_eq!(chunk_header.stream_state & STREAM_END, STREAM_END);

        // The footer location may be within the buffer we have, if it was for a tiny
        // single chunk write, or if it happened to fall within the first 4k of the
        // last chunk in a multi-chunk stream.
        // If that's the case, we can load the footer without any extra I/O.  Otherwise
        // we need to identify the 1 or 2 chunks that contain the footer, and read
        // from them.
        let footer: StreamFooter = if chunk_header.data_size as usize + CHUNK_HEADER_SIZE
            <= buffer.len()
            && chunk_header.data_size as usize >= STREAM_FOOTER_SIZE
        {
            debug!(
                "Reading StreamFooter inline for {}:{} at {}",
                stream_header.bucket,
                stream_header.key,
                offset_list.last().unwrap()
            );
            // We have the whole chunk and the chunk's data payload is big enough to contain
            // an un-fragmented footer.  Read it from the buf.
            let footer_cursor = Cursor::new(
                &buffer.as_slice()
                    [CHUNK_HEADER_SIZE + chunk_header.data_size as usize - STREAM_FOOTER_SIZE..],
            );
            bincode::deserialize_from(footer_cursor).unwrap()
        } else {
            // We must issue 1 or 2 more I/Os to read the stream footer.
            let header_sz = bincode::serialized_size(&stream_header).unwrap();

            let chunk_data_sz = IO_BUFFER_SIZE - CHUNK_HEADER_SIZE;
            let footer_at = header_sz as u64 + stream_header.content_length;

            let chunk_of_footer = (footer_at / chunk_data_sz as u64) as usize;
            assert!(chunk_of_footer < offset_list.len());
            let offset_within = CHUNK_HEADER_SIZE + (footer_at % chunk_data_sz as u64) as usize;
            let mut footer_buffer: [u8; STREAM_FOOTER_SIZE] = [0; STREAM_FOOTER_SIZE];
            if offset_within <= IO_BUFFER_SIZE - STREAM_FOOTER_SIZE {
                // Footer is within a single chunk
                debug!(
                    "Reading StreamFooter (1 extra I/O) for {}:{} at {}",
                    stream_header.bucket,
                    stream_header.key,
                    offset_list.last().unwrap()
                );
                let raw_offset = offset_list[chunk_of_footer].location + offset_within as u64;
                let read_bytes = self
                    .io
                    .read_at_unaligned(&mut footer_buffer, self.layout.offset_to_disk(raw_offset))
                    .await?;
                assert_eq!(read_bytes, STREAM_FOOTER_SIZE);
            } else {
                // Footer is within a single chunk
                debug!(
                    "Reading split StreamFooter (2 extra I/Os) for {}:{} at {}",
                    stream_header.bucket,
                    stream_header.key,
                    offset_list.last().unwrap()
                );
                // Footer is split between chunks
                assert!(chunk_of_footer + 1 < offset_list.len());

                let read1_size = IO_BUFFER_SIZE - offset_within;
                let read1_offset = offset_list[chunk_of_footer].location + offset_within as u64;
                let r1 = self
                    .io
                    .read_at_unaligned(
                        &mut footer_buffer[0..read1_size],
                        self.layout.offset_to_disk(read1_offset),
                    )
                    .await?;

                let read2_size = STREAM_FOOTER_SIZE - read1_size;
                let read2_offset =
                    offset_list[chunk_of_footer + 1].location + CHUNK_HEADER_SIZE as u64;
                let r2 = self
                    .io
                    .read_at_unaligned(
                        &mut footer_buffer[read1_size..],
                        self.layout.offset_to_disk(read2_offset),
                    )
                    .await?;

                // FIXME: coinsider whether we need to handle short reads here (might only
                // happen if we are interrupted by a signal during replay?)
                assert_eq!(r1, read1_size);
                assert_eq!(r2, read2_size);
            }
            bincode::deserialize(&footer_buffer).unwrap()
        };

        key_map
            .replay(self.get_id(), offset_list, stream_header, footer)
            .await;
        Ok(())
    }

    fn replay_load_chunkheader(
        &self,
        read_at: u64,
        cursor: &mut Cursor<&[u8]>,
    ) -> Option<ChunkHeader> {
        let ch_result: bincode::Result<ChunkHeader> = bincode::deserialize_from(cursor);
        match ch_result {
            Ok(h) => {
                if h.magic != CHUNK_MAGIC {
                    debug!("replay gap at {:#10x} (hit bad magic)", read_at);
                    None
                } else if h.location != read_at {
                    debug!(
                        "replay gap at {:#10x} (hit chunk location {:#10x})",
                        read_at, h.location
                    );
                    None
                } else {
                    Some(h)
                }
            }
            Err(_) => {
                debug!("replay gap at {:#10x} (hit junk)", read_at);
                None
            }
        }
    }

    pub async fn replay(&'a self, key_map: &KeyMap) -> std::io::Result<()> {
        let buf = IOBuffer::new(IO_ALIGN);

        // Need to track enough state about each stream to put it into KeyMap
        struct StreamReplayState {
            header: StreamHeader<'static>,
            offset_list: OffsetList,
        }

        let mut streams: HashMap<StreamId, StreamReplayState> = HashMap::new();

        // Start replaying from tail position.
        let mut first_gap: Option<u64> = None;
        // Where we'll read next
        let mut cursor: u64 = self.get_tail();
        // The location immediately after the last non-gap chunk we saw
        let mut candidate_head = cursor;
        debug!(
            "Starting replay at {:#10x} (expect head >= {:#010x}",
            cursor,
            self.get_head()
        );
        let mut valid_chunks: u64 = 0;

        loop {
            let disk_location = self.layout.offset_to_disk(cursor);

            debug!("replay read at disk={:#010x} ", disk_location);
            let r = self.io.read_at(buf.as_slice_mut(), disk_location).await?;
            debug!("replay read={}", r);

            let mut buffer_cursor = Cursor::new(&buf.as_slice()[..]);
            let chunk_header = match self.replay_load_chunkheader(cursor, &mut buffer_cursor) {
                None => {
                    // Skip over junk entry, if we're still within the write window.
                    if first_gap.is_none() {
                        debug!(
                            "replay journal={} first gap at {:#010x}",
                            self.get_id(),
                            cursor
                        );
                        first_gap = Some(cursor)
                    }
                    let limit = max(first_gap.unwrap(), self.get_head()) + DIRTY_WINDOW;
                    if cursor < limit {
                        debug!(
                            "replay journal={} skipping gap at {:#010x}",
                            self.get_id(),
                            cursor
                        );
                        cursor += IO_ALIGN as u64;
                        continue;
                    } else {
                        debug!(
                            "replay journal={} hit end of dirty window at {:#010x}",
                            self.get_id(),
                            cursor
                        );
                        break;
                    }
                }
                Some(ch) => {
                    valid_chunks += 1;
                    ch
                }
            };

            let chunk_offset = Offset::new(
                cursor,
                chunk_header.data_size as usize
                    + bincode::serialized_size(&chunk_header).unwrap() as usize,
            );

            if chunk_header.stream_state & STREAM_START != 0 {
                let stream_header: StreamHeader = bincode::deserialize_from(buffer_cursor).unwrap();
                let mut offset_list = OffsetList::new();
                offset_list.push(chunk_offset);

                if chunk_header.stream_state & STREAM_END != 0 {
                    self.replay_finish_stream(
                        key_map,
                        offset_list,
                        stream_header,
                        &chunk_header,
                        &buf,
                    )
                    .await?;
                } else {
                    streams.insert(
                        chunk_header.stream_id,
                        StreamReplayState {
                            header: stream_header,
                            offset_list,
                        },
                    );
                };
            } else if chunk_header.stream_state & STREAM_END != 0 {
                match streams.remove(&chunk_header.stream_id) {
                    Some(mut stream) => {
                        stream.offset_list.push(chunk_offset);
                        self.replay_finish_stream(
                            key_map,
                            stream.offset_list,
                            stream.header,
                            &chunk_header,
                            &buf,
                        )
                        .await?;
                    }
                    None => {
                        debug!("STREAM_END for unseen stream {}", chunk_header.stream_id);
                    }
                };
            } else if chunk_header.stream_state & STREAM_CANCEL != 0 {
                match streams.remove(&chunk_header.stream_id) {
                    Some(stream) => {
                        debug!(
                            "STREAM_CANCEL on {} ({})",
                            chunk_header.stream_id, stream.header.key
                        );
                    }
                    None => {}
                };
            } else {
                match streams.get_mut(&chunk_header.stream_id) {
                    Some(stream) => {
                        stream.offset_list.push(chunk_offset);
                    }
                    None => {
                        debug!(
                            "Mid-stream chunk on unseen stream {}",
                            chunk_header.stream_id
                        );
                    }
                };
            }

            cursor += chunk_header.blocks as u64 * IO_ALIGN as u64;
            // This was a valid chunk, so the following location is
            // our potential head pointere (i.e. if this chunk is
            // the last one we see).
            candidate_head = cursor;
        }

        if candidate_head < self.get_head() {
            // Our replay is allowed to go ahead of the persisted head
            // pointer, but it should never be behind it.
            panic!(
                "Replay only reached candidate_head={:#10x} cursor={:#10x} but header was {:#10x}",
                candidate_head,
                cursor,
                self.get_head()
            );
        }

        self.head
            .store(candidate_head, std::sync::atomic::Ordering::SeqCst);
        *(self.cursor.lock()) = candidate_head;

        debug!(
            "journal={} replay completing {} torn streams",
            self.get_id(),
            streams.len()
        );
        for (stream_id, v) in streams {
            // Write STREAM_CANCEL chunks to cap off these torn streams, so that
            // the Expirer doesn't hang on them.  Space is guaranteed to be available
            // for these writes via RESERVED_CAPACITY
            let buffer = self.buffers.acquire().await;
            let (offset, write_size) = self
                .prepare_buffer(&buffer, &stream_id, 0, STREAM_CANCEL, true)
                .now_or_never()
                .unwrap()
                .unwrap();
            let bytes = &buffer.as_slice()[0..write_size as usize];
            warn!(
                "replay journal={} writing cancel for torn stream={} at {:#010x} (key={})",
                self.get_id(),
                stream_id,
                offset.location,
                v.header.key
            );
            let disk_location = self.layout.offset_to_disk(offset.location);
            let result = self.io.write_at_sync(&bytes, disk_location).await;
            assert!(result.is_ok());
        }

        // Advance head to account for any cancellation writes
        self.head
            .store(*(self.cursor.lock()), std::sync::atomic::Ordering::SeqCst);

        debug!(
            "journal={} replay complete, {} valid dirty chunks, head={:#010x}",
            self.get_id(),
            valid_chunks,
            self.head.load(atomic::Ordering::SeqCst)
        );

        // For case where we have replayed past some gaps, then after restart
        // there are a bunch more gaps written, to make sure that a subsequent
        // replay will look far ahead enough to see all data that can have been
        // written within DIRTY_WINDOW.
        self.write_journal_header(self.get_head(), self.get_tail())
            .await;

        Ok(())
    }

    pub async fn begin_smallstream(&'a self, stream_header: &StreamHeader<'_>) -> SmallStream<'a> {
        let stream_id = self.stream_ids.acquire().await;

        let buffer = self.buffers.acquire().await;
        let skip = mem::size_of::<ChunkHeader>();
        let cursor_position = {
            let mut cursor = Cursor::new(&mut buffer.as_slice_mut()[skip..]);
            bincode::serialize_into(&mut cursor, stream_header).unwrap();
            cursor.position()
        };

        SmallStream {
            buffer: Some(buffer),
            stream_id: Some(stream_id),
            journal: self,
            cursor: cursor_position as usize + skip,
        }
    }

    pub async fn write_inline(
        &'a self,
        buffer: IOBuffer,
        write_sz: usize,
        stream_id: &StreamId,
    ) -> (std::io::Result<()>, OffsetList) {
        let (offset, write_bytes) = self
            .prepare_buffer(
                &buffer,
                stream_id,
                write_sz,
                STREAM_START | STREAM_END,
                true,
            )
            .await
            .unwrap();

        let disk_location = self.layout.offset_to_disk(offset.location);
        let bytes = &buffer.as_slice()[0..write_bytes as usize];
        let r = self.io.write_at_sync(&bytes, disk_location).await;
        if r.is_ok() {
            // We assume that if this were an IO error, something is severely broken
            // and stalling the advancement of the head pointer is probably the least
            // of our problems.
            self.completions_tx
                .send(offset)
                .await
                .expect("Completion queue died!");
        }

        self.buffers.release(buffer);

        match r {
            Err(e) => (Err(e), OffsetList::new()),
            Ok(_) => {
                // FIXME maybe for smallstream we should skip doing the default fat
                // offsetlist?  but it's what keymap expects
                let mut offset_list = OffsetList::new();
                offset_list.push(offset);
                (Ok(()), offset_list)
            }
        }
    }

    pub fn is_smallstream(&self, stream_header: &StreamHeader<'_>) -> bool {
        let hsize = bincode::serialized_size(stream_header).unwrap();
        stream_header.content_length + hsize + STREAM_FOOTER_SIZE as u64
            <= (IO_BUFFER_SIZE - mem::size_of::<ChunkHeader>()) as u64
    }

    pub async fn begin_bigstream(&'a self, stream_header: &StreamHeader<'_>) -> BigStream<'a> {
        // FIXME: borrow a slice from stream_state's IO buffer and serialize into
        // that, to avoid this allocation.  Do it in a new() function()
        let header_encoded = bincode::serialize(stream_header).unwrap();
        let stream_id = self.stream_ids.acquire().await;

        // Hand out up to BUFFERS_PER_STREAM (or less if the stream won't need that many)
        let total_stream_size =
            stream_header.content_length + header_encoded.len() as u64 + STREAM_FOOTER_SIZE as u64;
        let content_per_chunk = (IO_BUFFER_SIZE - CHUNK_HEADER_SIZE) as u64;
        let chunk_count = ((total_stream_size - 1) / (content_per_chunk)) as usize + 1;
        let buffer_count = min(BUFFERS_PER_STREAM, chunk_count);
        let stream_buffers = SimplePool::new_steal(&self.buffers, buffer_count).await;

        let mut stream_state = BigStream {
            journal: &self,
            stream_id: Some(stream_id),
            chunks: OffsetList::new(),
            writes_issued: 0,
            cursor: mem::size_of::<ChunkHeader>(),
            current_buffer: None,
            issue_handles: Vec::new(),
            buffers: stream_buffers,
        };

        stream_state.inner_write(&header_encoded[..], false).await;

        stream_state
    }

    /// If space is available, advance the cursor and return its old value.
    /// Else return None (caller must wait and call us again)
    fn maybe_advance_cursor(&self, consume_bytes: u64) -> Option<u64> {
        let head = self.head.load(atomic::Ordering::Acquire);
        let tail = self.tail.load(atomic::Ordering::Acquire);
        {
            let mut cursor_p = self.cursor.lock();
            let cursor: u64 = *cursor_p;

            let ready = {
                let space_available = self.layout.data_size() - (cursor - tail) - RESERVED_CAPACITY;
                if space_available < consume_bytes {
                    debug!(
                        "journal={} blocked on space ({} bytes available), waiting",
                        self.id, space_available
                    );
                    false
                } else if cursor >= head + DIRTY_WINDOW {
                    debug!(
                        "journal={} blocked on dirty window (head={:#010x}, cursor={:#010x}), waiting",
                        self.id, head, cursor
                    );
                    false
                } else {
                    debug!(
                        "journal={} ready for write ({} bytes available)",
                        self.id, space_available
                    );
                    true
                }
            };

            if ready {
                *cursor_p = cursor + consume_bytes;
                Some(cursor)
            } else {
                None
            }
        }
    }

    /// Block until space is available in the journal, then advance the cursor
    /// and return its old value.
    async fn advance_cursor(&self, consume_bytes: u64) -> u64 {
        loop {
            match self.maybe_advance_cursor(consume_bytes) {
                None => {
                    // This is a janky way of waiting, but for the hopefully rare case of a full journal,
                    // it gets the job done without requiring tight synchronisation between here and
                    // the places that set head+tail.
                    sleep(Duration::from_millis(100)).await;
                }
                Some(v) => return v,
            }
        }
    }

    /// Note: only called for larger streaming requests - small single-chunk requests
    /// just use an O_DSYNC file descriptor and issue their IO in a single shot.
    pub async fn sync(&self) -> std::io::Result<()> {
        self.io.fdatasync().await
    }

    /// Issue a write IO wrapped in a task, and return the JoinHandle
    /// (for use in multi-chunk streams)
    /// Use prepare_buffer first to allocate a journal location and write header into the buffer.
    /// The prepare part is separate because it can be used non-blocking, and does not
    /// consume the buffer, whereas this function always succeeds and consumes the buffer.
    async fn write_background(
        &'a self,
        buffer: ItemHandle<IOBuffer>,
        offset: Offset,
        write_bytes: u64,
    ) -> (Offset, BackgroundWrite) {
        let bg_io = self.io.clone();
        let offset_bg = offset.clone();

        let disk_location = self.layout.offset_to_disk(offset.location);

        // TODO: be a little more efficient by releasing buffers directly to self.buffers (not
        // the caller BigStream's releaser) if the stream is near its end and won't need them again.

        let id_bg = self.get_id(); // Copy of journal ID for logging from task
        let fut = async move {
            let bytes = &buffer.as_slice()[0..write_bytes as usize];
            debug!("I/O Enter journal={} @ {}", id_bg, offset_bg);
            let r = bg_io.write_at(&bytes, disk_location).await;
            match &r {
                Ok(_) => {
                    debug!("I/O Success journal={} @ {}", id_bg, offset_bg);
                }
                Err(e) => {
                    error!("I/O Error journal={} offset={}: {}", id_bg, offset_bg, e);
                }
            }
            r
        };

        let completion_permit = self
            .completions_tx
            .clone()
            .reserve_owned()
            .await
            .expect("Completion queue dropped");

        (
            offset,
            BackgroundWrite::new(Box::pin(fut), offset, completion_permit),
        )
    }

    pub async fn prepare_buffer(
        &'a self,
        buffer: &IOBuffer,
        stream_id: &StreamId,
        size: usize,
        flags: u8,
        block_if_full: bool,
    ) -> Option<(Offset, u64)> {
        assert!(size <= buffer.len() - mem::size_of::<ChunkHeader>());

        // How much data (including ChunkHeader) do we want to write?
        let raw_byte_count = size + mem::size_of::<ChunkHeader>();

        // How many IO_ALIGN blocks are needed to write that?
        let mut raw_block_count = raw_byte_count / IO_ALIGN;
        if raw_byte_count % IO_ALIGN > 0 {
            raw_block_count += 1;
        }

        // We will encode the block count as u8 (i.e. 1MB is the max size of a chunk)
        assert!(raw_block_count < 256);
        assert!(size <= u32::MAX as usize);

        // How much on-disk space will we consume (including trailing padding
        // to advance the journal to an IO_ALIGN-aligned location for the next write
        // after ours)
        let consume_bytes: u64 = raw_block_count as u64 * IO_ALIGN as u64;

        // Get the journal cursor location and advance it to where the next entry will
        // be written.
        // (checks there is enough space in the journal)
        let wrote_at = if block_if_full {
            self.advance_cursor(consume_bytes).await
        } else {
            match self.maybe_advance_cursor(consume_bytes) {
                Some(v) => v,
                None => return None,
            }
        };

        let header = ChunkHeader {
            magic: CHUNK_MAGIC,
            location: wrote_at,
            blocks: raw_block_count as u8,
            data_size: size as u32,
            stream_state: flags,
            stream_id: *stream_id,
        };

        let header_slice = &mut buffer.as_slice_mut()[0..mem::size_of::<ChunkHeader>()];
        let mut cursor = Cursor::new(header_slice);
        let serialize_result = bincode::serialize_into(&mut cursor, &header);
        assert!(serialize_result.is_ok());

        Some((Offset::new(wrote_at, raw_byte_count), consume_bytes))
    }
}

#[async_trait]
pub trait JournalStream {
    async fn write(self: &mut Self, incoming_bytes: &[u8]);
    async fn sync(mut self, etag: &digest::Output<md5::Md5>) -> (std::io::Result<()>, OffsetList);
    async fn cancel(mut self);
}

// When we create a future to write a journal chunk in the background,
// we need to make sure that the completion queue always gets pinged
// when we're done, even if the future is hard-dropped rather than
// being awaited.  Wrap the future with this struct to ensure that.
struct BackgroundWrite {
    offset: Offset,
    io_future: Option<Pin<Box<dyn IOFuture + Send>>>,
    completion_permit: Option<tokio::sync::mpsc::OwnedPermit<Offset>>,
    result: Option<std::io::Result<()>>,
}

unsafe impl Send for BackgroundWrite {}
unsafe impl Sync for BackgroundWrite {}

impl BackgroundWrite {
    pub fn new(
        mut io_future: Pin<Box<dyn IOFuture + Send>>,
        offset: Offset,
        completion_permit: tokio::sync::mpsc::OwnedPermit<Offset>,
    ) -> Self {
        // Kick the IO to ensure it is dispatched as early as possible, and
        // we don't wait until someone awaits this to start it.
        let early = io_future.as_mut().now_or_never();
        let io_future = if early.is_none() {
            Some(io_future)
        } else {
            // Early completion of the I/O!  The io_uring layer completed
            // the operation inside the syscall that kicked the ring.
            None
        };

        Self {
            io_future,
            offset,
            completion_permit: Some(completion_permit),
            result: early,
        }
    }

    fn emit_completion(&mut self) {
        self.completion_permit.take().unwrap().send(self.offset);
    }

    pub async fn sync(&mut self) -> std::io::Result<()> {
        // Did we already do an early completion?
        let result = self.result.take();
        if result.is_some() {
            self.emit_completion();
            result.unwrap()
        } else {
            // This assertion will trigger if a caller wrongly calls wait() twice!
            assert!(self.io_future.is_some());
            let r = self.io_future.as_mut().unwrap().await;
            self.io_future = None;
            self.emit_completion();
            r
        }
    }
}

impl Drop for BackgroundWrite {
    fn drop(&mut self) {
        // On drop, if we have a pending result or pending future, we must
        // send our completion event.  This happens if a PUT handler panics.
        if self.result.is_some() || self.io_future.is_some() {
            // Not really a harmful case but useful in development to see where
            // it's happening
            info!("Unclean drop of BackgroundWrite");
            self.emit_completion();
        }
    }
}

/// BigStreams are the general case write IO, for streams that aren't small
/// enough to fit in a SmallStream.  BigStreams submit multiple IOs in the background
/// and only block on them in their sync() method.
pub struct BigStream<'a> {
    journal: &'a ChunkJournalInner,
    // This is only optional during destruction (i.e. you can release it early
    // and None this field to prevent drop from double freeing it)
    stream_id: Option<StreamId>,
    writes_issued: usize,
    chunks: OffsetList,
    cursor: usize,

    // This is an Option because between final dispatch and final sync,
    // there is no current buffer
    current_buffer: Option<ItemHandle<IOBuffer>>,

    // Join handles for the tasks in which individual IOs run
    issue_handles: Vec<BackgroundWrite>,

    // Important: the last member in the struct, therefore dropped
    // last (after current_buffer and issue_handles which must release
    // any IOBuffers they hold first)
    buffers: SimplePool<IOBuffer>,
}

impl Drop for BigStream<'_> {
    fn drop(&mut self) {
        // On successfully written streams we will already
        // have cleared all our in-flight buffers.  Log some
        // warnings if we're in the unclean path.
        if self.current_buffer.is_some() {
            warn!("BigStream::drop: 1 current_buffer");
        }

        if !self.issue_handles.is_empty() {
            warn!(
                "BigStream::drop: {} issue_handles",
                self.issue_handles.len()
            );
        }
    }
}

// BigStream is send, because futures referencing it will get
// held across .await calls.
unsafe impl<'a> Send for BigStream<'a> {}

/// SmallStreams avoid some of the overhead of BigStreams, for the
/// special case of streams small enough to fit in a single IO buffer.
pub struct SmallStream<'a> {
    journal: &'a ChunkJournalInner,
    cursor: usize,
    // These Options are only ever None between consumption and drop
    buffer: Option<IOBuffer>,
    stream_id: Option<StreamId>,
}

#[async_trait]
impl<'a> JournalStream for SmallStream<'a> {
    async fn write(self: &mut Self, bytes: &[u8]) {
        let buffer = self.buffer.as_mut().unwrap();
        buffer.as_slice_mut()[self.cursor..self.cursor + bytes.len()].copy_from_slice(bytes);
        self.cursor += bytes.len();
    }

    async fn sync(mut self, etag: &digest::Output<md5::Md5>) -> (std::io::Result<()>, OffsetList) {
        let buffer = self.buffer.take().unwrap();
        let stream_id = self.stream_id.take().unwrap();

        let mut c =
            Cursor::new(&mut buffer.as_slice_mut()[self.cursor..self.cursor + STREAM_FOOTER_SIZE]);

        let footer = StreamFooter::new(etag);
        debug_assert_eq!(
            bincode::serialized_size(&footer).unwrap(),
            STREAM_FOOTER_SIZE as u64
        );
        bincode::serialize_into(&mut c, &footer).expect("Footer encoding error");

        let r = self
            .journal
            .write_inline(
                buffer,
                self.cursor - mem::size_of::<ChunkHeader>(),
                &stream_id,
            )
            .await;
        self.journal.stream_ids.release(stream_id);

        (r.0, r.1)
    }

    async fn cancel(mut self) {
        // No-op: we are a single-chunk operation, so if we
        // didn't sync() then nothing was written to disk and
        // there's nothing to cancel.
    }
}

impl Drop for SmallStream<'_> {
    fn drop(&mut self) {
        if let Some(buffer) = self.buffer.take() {
            self.journal.buffers.release(buffer);
        }
        if let Some(stream_id) = self.stream_id.take() {
            self.journal.stream_ids.release(stream_id);
        }
    }
}

#[async_trait]
impl<'a, 'b> JournalStream for BigStream<'a> {
    async fn write(self: &mut Self, incoming_bytes: &[u8]) {
        self.inner_write(incoming_bytes, false).await
    }

    async fn sync(mut self, etag: &digest::Output<md5::Md5>) -> (std::io::Result<()>, OffsetList) {
        let footer = StreamFooter::new(etag);

        // Footer is tiny.  Serialize it into a stack array to avoid allocating a Vec.
        // (We cannot generally serialize directly into IO buffer because the write might
        //  be split between two buffers if we're near the end of one)
        // This is only using a serializer at all to be somewhat futureproof - when using bincode
        // encoding, we could just pass footer.md5 into inner_write
        let mut footer_bytes: [u8; STREAM_FOOTER_SIZE] = [0; STREAM_FOOTER_SIZE];
        let mut cursor = Cursor::new(&mut footer_bytes[..]);
        bincode::serialize_into(&mut cursor, &footer).expect("Footer serialization error");

        self.inner_write(&footer_bytes, true).await;
        self.issue(STREAM_END).await;
        let sync_r = self.inner_sync().await;

        (sync_r.0, sync_r.1)
    }

    async fn cancel(mut self) {
        // Figure out whether we wrote anything at all so far: if
        // we didn't we can skip writing anything out to disk to cancel.
        if self.writes_issued == 0 {
            debug!(
                "StreamState.cancel[{}]: no IOs, fast cancel",
                self.stream_id.unwrap()
            );
            self.journal
                .stream_ids
                .release(self.stream_id.take().unwrap());
        } else {
            // Write out an empty chunk with the cancel flag
            self.cursor = mem::size_of::<ChunkHeader>();
            self.issue(STREAM_CANCEL).await;

            let (io_result, _) = self.inner_sync().await;

            // On a cancellation, we don't need to expose an error up to
            // the S3 client (we're 401'ing them anyway) but it's still
            // very unexpected.
            if let Err(e) = io_result {
                error!("BigStream I/O error during cancel {}", e);
            }
        }
    }
}

impl<'a, 'b> BigStream<'a> {
    async fn take_buffer(self: &mut Self) {
        debug!(
            "BigStream::take_buffer journal={} stream={} in flight {}/{}",
            self.journal.get_id(),
            self.stream_id.unwrap(),
            self.issue_handles.len(),
            self.buffers.get_capacity()
        );

        // It would be preferable to check for buffer exhaustion via a try_acquire on SimplePool,
        // but cannot because at time of writing tokio's mpsc receiver has no try_recv
        if self.issue_handles.len() >= self.buffers.get_capacity() {
            // Important that if we are out of buffers, we flush something before
            // waiting for a new one.  Otherwise we might have JoinHandles
            // that we aren't calling await on, that won't advance until we do.
            debug!(
                "BigStream::take_buffer journal={} stream={} flushing to release buffers",
                self.journal.get_id(),
                self.stream_id.unwrap()
            );
            let t1 = Instant::now();
            self.flush_n(self.buffers.get_capacity() / 2).await;
            debug!(
                "BigStream::take_buffer journal={} stream={} flushed in {}us",
                self.journal.get_id(),
                self.stream_id.unwrap(),
                (Instant::now() - t1).as_micros()
            )
        }

        self.cursor = mem::size_of::<ChunkHeader>();
        self.current_buffer = Some(self.buffers.acquire_smart().await);
    }

    async fn inner_write(self: &mut Self, incoming_bytes: &[u8], last_data: bool) {
        let mut consumed: usize = 0;

        while consumed < incoming_bytes.len() {
            if self.current_buffer.is_none() {
                self.take_buffer().await;
                debug!(
                    "BigStream::inner_write journal={} stream={} fresh buffer cursor={}",
                    self.journal.get_id(),
                    self.stream_id.unwrap(),
                    self.cursor
                );
            } else {
                debug!(
                    "BigStream::inner_write journal={} stream={} reuse buffer cursor={}",
                    self.journal.get_id(),
                    self.stream_id.unwrap(),
                    self.cursor
                );
            }

            let incoming_bytes_remaining = &incoming_bytes[consumed..];
            let buffer_space_left = IO_BUFFER_SIZE - self.cursor;
            let consume_bytes = min(incoming_bytes_remaining.len(), buffer_space_left);

            debug!(
                "BigStream::inner_write journal={} stream={} incoming_remaining {} buffer_space {} consume_bytes={} (cursor={})",
                self.journal.get_id(),
                self.stream_id.unwrap(),
                incoming_bytes_remaining.len(),
                buffer_space_left,
                consume_bytes,
                self.cursor
            );

            // We will always have some space left in our buffer, because when we fill it,
            // we either replace the buffer, or we have finished the stream.
            assert!(buffer_space_left > 0);

            self.current_buffer.as_ref().map(|b| {
                b.as_slice_mut()[self.cursor..self.cursor + consume_bytes]
                    .copy_from_slice(&incoming_bytes_remaining[0..consume_bytes]);
            });
            self.cursor += consume_bytes;
            consumed += consume_bytes;

            if consume_bytes == buffer_space_left || last_data {
                // last_data is the last call to write()
                // final_data is the last actual IO we issue.
                let final_data = last_data && (consumed >= incoming_bytes.len());

                // Flush!
                debug!(
                    "BigStream::inner_write journal={} stream={} complete buffer (final={})",
                    self.journal.get_id(),
                    self.stream_id.unwrap(),
                    final_data
                );
                if !final_data {
                    // Defer final IO until sync(), so that our caller can defer
                    // their final auth checks (i.e. finalize auth between their last
                    // call to write() and their call to sync())
                    self.issue(0).await;
                    assert!(self.current_buffer.is_none());
                }
            }
        }
    }

    async fn inner_sync(mut self) -> (std::io::Result<()>, OffsetList) {
        // Does NOT guarantee the journal as a whole was written up to this point, just
        // that those parts of it needed for this particular stream were.
        // This is important for replay: replay may have to skip junk regions
        // to find completed chunks further ahead.

        // When we are called, they should have submitted at least one chunk:
        // otherwise in cancel() we skip the sync.
        assert!(self.chunks.len() > 0);

        let mut errors = self.flush_all().await;

        self.journal.sync().await.expect("Journal Sync IO error!");

        // We will issue no more IOs, so release our stream ID ASAP so that someone
        // else can use it.
        self.journal
            .stream_ids
            .release(self.stream_id.take().unwrap());

        let chunks = mem::replace(&mut self.chunks, OffsetList::new());
        if errors.is_empty() {
            debug!("ChunkJournalStream.sync() success");
            (Ok(()), chunks)
        } else {
            // We might have seen multiple errors, but just raise the latest one.
            error!("sync() errors: {}", errors.len());
            (Err(errors.pop().unwrap()), chunks)
        }
    }

    // Wait on all outstanding I/Os.  Call this when our
    // writing is blocked, to ensure we are completing I/Os
    // and thereby releasing buffers, updating head, etc in
    // order to get past whatever condition is blocking us.
    async fn flush_all(self: &'b mut Self) -> Vec<std::io::Error> {
        let mut errors = Vec::new();
        for bgwrite in &mut self.issue_handles {
            match bgwrite.sync().await {
                Ok(_sz) => {}
                Err(e) => {
                    error!(
                        "I/O error on journal={} stream chunk write: {}",
                        self.journal.get_id(),
                        e
                    );
                    errors.push(e)
                }
            };
        }
        self.issue_handles.clear();

        errors
    }

    async fn flush_n(self: &'b mut Self, n: usize) -> Vec<std::io::Error> {
        let n = min(n, self.issue_handles.len());
        if n == 0 {
            // We don't expect to be called in this state, but it's a simple
            // thing to handle gently, so do so.
            return Vec::new();
        }

        // The oldest handles are the likeliest to have completed.  split_off takes the latter
        // part, so grab that and then do a swap to get the first (oldest) part of the vector.
        let mut flush_on = self.issue_handles.split_off(n);
        mem::swap(&mut flush_on, &mut self.issue_handles);
        assert_eq!(flush_on.len(), n);

        let mut errors = Vec::new();
        for mut bgwrite in flush_on {
            let io_r = bgwrite.sync().await;
            match io_r {
                Ok(_) => {}
                Err(e) => {
                    error!(
                        "I/O error on journal={} stream chunk write: {}",
                        self.journal.get_id(),
                        e
                    );
                    errors.push(e)
                }
            }
        }
        errors
    }

    // Write out whatever we have in our current_buffer
    async fn issue(self: &'b mut Self, mut flags: u8) {
        if self.writes_issued == 0 {
            flags |= STREAM_START;
        }

        let buffer = self.current_buffer.take().unwrap();
        assert!(self.current_buffer.is_none());
        debug!(
            "Stream[{}] issue buffer {}",
            self.stream_id.unwrap(),
            buffer
        );

        let mut blocking = false;
        let (offset, write_bytes) = loop {
            match self
                .journal
                .prepare_buffer(
                    &buffer,
                    self.stream_id.as_ref().unwrap(),
                    self.cursor - mem::size_of::<ChunkHeader>(),
                    flags,
                    blocking,
                )
                .await
            {
                Some(v) => break v,
                None => {
                    // Something is blocking writing new chunks!  Flush all our
                    // outstanding I/Os in case we are it.
                    debug!(
                        "BigStream::issue: journal={} stream={} flushing {} IOs for blocked journal",
                        self.journal.get_id(),
                        self.stream_id.as_ref().unwrap(),
                        self.issue_handles.len()
                    );

                    self.flush_all().await;
                    blocking = true;
                }
            }
        };
        debug!(
            "BigStream::issue: journal={} stream={} padded write of {:#06x} at {} flags={:#4x}",
            self.journal.get_id(),
            self.stream_id.unwrap(),
            write_bytes,
            offset,
            flags
        );

        // We have successfully got a journal location to write to: send it.
        let (wrote_at, bg_write) = self
            .journal
            .write_background(buffer, offset, write_bytes)
            .await;

        self.writes_issued += 1;
        self.chunks.push(wrote_at);
        self.issue_handles.push(bg_write);
    }
}

#[cfg(test)]
mod test {
    use std::env;
    use std::fs;
    use std::mem;
    use std::sync::Arc;

    use chrono::Utc;
    use function_name::named;
    use ntest::timeout;

    use crate::server::io_engine::{IOEngine, TokioEngine};
    use crate::server::journal::chunk_journal::INITIAL_HEAD;
    use crate::server::journal::StreamHeader;
    use crate::server::journal::{JournalId, STREAM_FOOTER_SIZE};
    use crate::server::key_map::test::key_map_for_test;
    use crate::server::types::ObjectOp;
    use crate::util::round_to_pow2;

    use super::JournalStream;
    use super::{
        open_journal, ChunkHeader, ChunkJournal, ChunkJournalInner, HeadAdvancer, IO_ALIGN,
        IO_BUFFER_SIZE, JOURNAL_HEADER_SPACE,
    };
    use crate::server::test;
    use tempfile::NamedTempFile;

    const TEST_ID: JournalId = 123;
    const TEST_SIZE: u64 = JOURNAL_HEADER_SPACE + 1024 * 1024 * 10;

    fn test_init(tag: &str) -> (ChunkJournal, HeadAdvancer, NamedTempFile) {
        test::global_init();
        let file = tmp_file(tag);
        let meta = fs::metadata(file.path());
        if meta.is_ok() {
            fs::remove_file(file.path()).unwrap();
        }
        ChunkJournalInner::create(file.path(), TEST_SIZE).expect("Journal creation failed");

        let (j, ha) = test_open(&file);
        (j, ha, file)
    }

    fn test_open(file: &NamedTempFile) -> (ChunkJournal, HeadAdvancer) {
        let sloppy = false;
        let no_direct = true;
        let writes_disabled = false;
        let path = file.path().to_str().unwrap();

        // Use the lower performance backend for unit tests, to avoid depending on
        // an O_DIRECT-enabled filesystem (/tmp frequently doesn't have it)
        let io_engine = Arc::new(TokioEngine::new(path, sloppy, no_direct, writes_disabled));

        open_journal(TEST_ID, path, io_engine, writes_disabled).unwrap()
    }

    fn tmp_file(tag: &str) -> NamedTempFile {
        let dir = env::temp_dir().join("tests").join(tag);
        let meta = fs::metadata(&dir);
        if !meta.is_ok() {
            fs::create_dir_all(&dir).expect(&format!("Failed to create tmp dir for {}", tag));
        }
        NamedTempFile::new_in(&dir).unwrap()
    }

    #[named]
    #[tokio::test]
    #[timeout(1000)]
    async fn test_bigstream() {
        let (j, mut ha, _file) = test_init(function_name!());

        let stream_size = IO_BUFFER_SIZE * 2;
        let mut payload_bytes: Vec<u8> = Vec::with_capacity(stream_size);
        payload_bytes.resize(stream_size, 0);

        let sh = StreamHeader::new(
            ObjectOp::Put,
            Some(&"alice"),
            "foo",
            "bar",
            stream_size as u64,
            Utc::now(),
        );

        let mut expect_ondisk_size =
            stream_size + bincode::serialized_size(&sh).unwrap() as usize + STREAM_FOOTER_SIZE;
        let payload_per_chunk = IO_BUFFER_SIZE - mem::size_of::<ChunkHeader>();
        let mut expect_chunks = expect_ondisk_size / payload_per_chunk;
        let remainder = expect_ondisk_size % payload_per_chunk;
        expect_ondisk_size = expect_chunks * IO_BUFFER_SIZE;
        if remainder > 0 {
            expect_ondisk_size +=
                round_to_pow2(mem::size_of::<ChunkHeader>() + remainder, IO_ALIGN);
            expect_chunks += 1;
        }

        let etag = digest::Output::<md5::Md5>::clone_from_slice(&[0; 16]);

        let mut s = j.begin_bigstream(&sh).await;
        s.write(&payload_bytes[..]).await;
        let (r, offset_list) = s.sync(&etag).await;
        assert!(r.is_ok());
        assert_eq!(offset_list.len(), expect_chunks);

        assert_eq!(j.get_head(), INITIAL_HEAD);
        for _i in 0..expect_chunks {
            ha.consume_one().await;
        }
        assert_eq!(j.get_head(), INITIAL_HEAD + expect_ondisk_size as u64);
    }

    #[named]
    #[tokio::test]
    #[timeout(1000)]
    async fn test_smallstream() {
        let (j, mut ha, _file) = test_init(function_name!());
        let mydata = "foobar".as_bytes();
        let sh = StreamHeader::new(
            ObjectOp::Put,
            Some(&"alice"),
            "foo",
            "bar",
            mydata.len() as u64,
            Utc::now(),
        );

        let etag = digest::Output::<md5::Md5>::clone_from_slice(&[0; 16]);
        let mut stream = j.begin_smallstream(&sh).await;
        stream.write(mydata).await;
        let (r, offset_list) = stream.sync(&etag).await;
        assert!(r.is_ok());
        assert_eq!(offset_list.len(), 1);

        assert_eq!(j.get_head(), INITIAL_HEAD);
        ha.consume_one().await;
        assert_eq!(j.get_head(), INITIAL_HEAD + IO_ALIGN as u64);
    }

    #[named]
    #[tokio::test]
    #[timeout(10000)]
    async fn test_replay() {
        let (j, mut ha, file) = test_init(function_name!());
        let mydata = "foobar".as_bytes();
        let sh = StreamHeader::new(
            ObjectOp::Put,
            Some(&"alice"),
            "foo",
            "bar",
            mydata.len() as u64,
            Utc::now(),
        );
        let mut s = j.begin_bigstream(&sh).await;
        s.write(&mydata).await;

        let etag = digest::Output::<md5::Md5>::clone_from_slice(&[0; 16]);
        let (r, _offset_list) = s.sync(&etag).await;
        assert!(r.is_ok());
        assert_eq!(j.get_head(), INITIAL_HEAD);
        ha.consume_one().await;
        assert_eq!(j.get_head(), INITIAL_HEAD + IO_ALIGN as u64);

        // Drop and recreate journal to replay it
        let key_map = key_map_for_test().await;
        let (j, _ha) = test_open(&file);
        j.replay(&key_map).await.expect("replay error");
        assert_eq!(j.get_head(), INITIAL_HEAD + IO_ALIGN as u64);
        assert_eq!(key_map.len(), 1);
    }

    /// Test that a 'torn' stream is handled correctly on replay
    #[named]
    #[tokio::test]
    #[timeout(10000)]
    async fn test_torn_replay() {
        let (j, mut ha, file) = test_init(function_name!());

        // A payload the size of an IO buffer is guaranteed
        // to be more than one chunk after headers.
        let stream_size = IO_BUFFER_SIZE;

        let mut payload_bytes: Vec<u8> = Vec::with_capacity(stream_size);
        payload_bytes.resize(stream_size, 0);

        // Begin a stream and write slightly fewer bytes than
        // the overall content: sufficient to write at least one chunk but
        // not enough to complete the stream.
        let sh = StreamHeader::new(
            ObjectOp::Put,
            Some(&"alice"),
            "foo",
            "bar",
            stream_size as u64,
            Utc::now(),
        );
        let mut s = j.begin_bigstream(&sh).await;
        s.write(&payload_bytes[0..stream_size - 2]).await;
        assert!(s.writes_issued > 0);

        // Flush the partially written stream
        s.flush_all().await;

        // Validate our single written IO flowed through to HeadAdvancer
        assert_eq!(j.get_head(), INITIAL_HEAD);
        ha.consume_one().await;
        assert_eq!(j.get_head(), INITIAL_HEAD + IO_BUFFER_SIZE as u64);

        // Drop and recreate journal to replay it
        drop(s);
        drop(j);
        let key_map = key_map_for_test().await;
        let (j, _ha) = test_open(&file);
        j.replay(&key_map).await.expect("replay error");

        // The replay should have added a chunk to cancel the torn stream, so
        // expect head to have advanced to reflect that.
        assert_eq!(
            j.get_head(),
            INITIAL_HEAD + IO_BUFFER_SIZE as u64 + IO_ALIGN as u64
        );

        // The torn stream should not have been added to the KeyMap
        assert_eq!(key_map.len(), 0);
    }
}
