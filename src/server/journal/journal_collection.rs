use crate::server::journal::JournalId;

use super::chunk_journal::ChunkJournal;

// #[async_trait]
// pub trait Journal: Send + Sync + fmt::Display {
//     // Read functions for GET requests serviced out of dirty data
//     //async fn lock_offset(&self, )
//     async fn read_header(&self, offset_list: &OffsetList) -> std::io::Result<StreamHeaderRead>;
//     async fn read(&self, offset_list: OffsetList, sender: Sender);
//
//     // Head/tail pointer interface for Expirer to update the journal
//     // when it successfully expires oldest data.
//     fn get_head(&self) -> u64;
//     fn get_tail(&self) -> u64;
//     async fn set_tail(&self, tail: u64);
//     fn get_layout(&self) -> &dyn JournalLayout;
//
//     // Simple getters
//     fn get_id(&self) -> JournalId;
//     fn get_path(&self) -> &str;
// }

// An immutable map of JournalId to Journal instance, for anyone
// who needs to resolve a KeyMap entry's JournalId to something
// they can do IO on.
pub struct JournalCollection {
    journals: Vec<ChunkJournal>,
}

impl JournalCollection {
    pub fn new(journals: Vec<ChunkJournal>) -> Self {
        assert!(journals.len() > 0);
        Self { journals }
    }

    pub fn get(&self, id: JournalId) -> ChunkJournal {
        // Panic if not found: JournalIds are internal runtime
        // state and a JournalId for a non-existent Journal should
        // never have been constructed.
        assert!(id < self.journals.len() as JournalId);
        self.journals[id as usize].clone()
    }

    pub fn len(&self) -> usize {
        self.journals.len()
    }

    pub fn all(&self) -> &Vec<ChunkJournal> {
        &self.journals
    }
}
