use std::borrow::Cow;
use std::fmt;

use chrono::{DateTime, Utc};
use digest::Output;
use md5;
use serde::{Deserialize, Serialize};

use crate::server::cluster_map::NodeId;
use crate::server::s3_types::{BUCKET_LEN_MAX, BUCKET_LEN_MIN, KEY_LEN_MAX};
use crate::server::types::ObjectOp;

pub mod chunk_journal;
pub mod expirer;
pub mod io_buffer;
pub mod journal_collection;

mod read_stream;
mod wait_map;

/// The StreamHeader is everything we need to know to route a Stream to
/// a BackendStream -- if the backend is S3 that means the StreamHeader needs
/// to contain enough information to start the HTTP request.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StreamHeader<'a> {
    // Fixed length fields come before variable length fields
    pub op: ObjectOp,
    pub last_modified: DateTime<Utc>,
    pub content_length: u64,

    pub bucket: Cow<'a, str>,
    pub key: Cow<'a, str>,
    // owner="" means anonymous: not using an Option because that makes
    // Cow behaviour awkward
    pub owner: Cow<'a, str>,
}

impl<'a> StreamHeader<'a> {
    pub fn new(
        op: ObjectOp,
        owner: Option<&'a str>,
        bucket: &'a str,
        key: &'a str,
        content_length: u64,
        last_modified: DateTime<Utc>,
    ) -> Self {
        // StreamHeader is supposed to fit inside a single Chunk, so
        // it is important that our S3-level expectations about
        // the length of strings are met.
        assert!(bucket.len() >= BUCKET_LEN_MIN);
        assert!(bucket.len() <= BUCKET_LEN_MAX);
        assert!(key.len() <= KEY_LEN_MAX);

        // Deletes should never specify a content length
        if let ObjectOp::Delete = op {
            assert_eq!(content_length, 0);
        }

        Self {
            op,
            bucket: Cow::from(bucket),
            key: Cow::from(key),
            content_length,
            owner: Cow::from(owner.unwrap_or(&"")),
            last_modified,
        }
    }
}

impl fmt::Display for StreamHeader<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Stream<{}:{}>", self.bucket, self.key)
    }
}

/// Metadata that is only known once all the body has been received, principally
/// the ETag (an MD5 of the body)
#[derive(Serialize, Deserialize, Debug)]
pub struct StreamFooter {
    md5: [u8; 16],
}

impl StreamFooter {
    pub fn new(digest: &Output<md5::Md5>) -> Self {
        Self {
            md5: *(digest.as_ref()),
        }
    }

    pub fn into(self) -> Output<md5::Md5> {
        Output::<md5::Md5>::clone_from_slice(&self.md5)
    }
}

const STREAM_FOOTER_SIZE: usize = 16;

// Local runtime reference to a particular data journal (not meaningful
// if persisted or sent over the network!)
pub type JournalId = u32;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct GlobalJournalId {
    pub node_id: NodeId,
    pub journal_id: JournalId,
}

impl GlobalJournalId {
    pub fn new(node_id: NodeId, journal_id: JournalId) -> Self {
        GlobalJournalId {
            node_id,
            journal_id,
        }
    }
}

impl fmt::Display for GlobalJournalId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#04x}:{:#04x}", self.node_id, self.journal_id)
    }
}

unsafe impl Send for GlobalJournalId {}
unsafe impl Sync for GlobalJournalId {}
