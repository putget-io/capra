use std::collections::HashMap;
use std::fmt;
use std::hash::Hash;
use std::sync::{Arc, Mutex, Weak};
use tokio::sync::Notify;

use log::*;

/// Structure for concurrent operations on a keyspace
/// where ops to the same key must be serializd, and
/// must execute in the same order they were submitted.
/// Optimises for case where concurrent access to a key
/// is rare.
pub struct OrderedWaitMap<K: Eq + Hash + Clone> {
    keys: Mutex<HashMap<K, Vec<Arc<Notify>>>>,
}

pub struct OrderedWaitHandle<K: Eq + Hash + Clone + fmt::Display> {
    inner: Option<Arc<Notify>>,
    parent: Weak<OrderedWaitMap<K>>,
    key: K,

    // True once we have acquired the lock: controls how we release it
    // on drop (whether we're doing a normal release, or unpicking our
    // notify handle.
    am_authority: bool,
}

impl<K: Eq + Hash + Clone + fmt::Display> Drop for OrderedWaitHandle<K> {
    fn drop(&mut self) {
        if self.am_authority {
            match self.parent.upgrade() {
                None => {}
                Some(p) => p.release(&self.key),
            }
        } else {
            let notify = self.inner.take().unwrap();
            match self.parent.upgrade() {
                None => {}
                Some(p) => p.release_waiter(&self.key, &notify),
            }
        }
    }
}

impl<K: Eq + Hash + Clone + fmt::Display> OrderedWaitHandle<K> {
    pub async fn enter(&mut self) {
        match &self.inner {
            None => {}
            Some(n) => {
                debug!("enter() begin waiting for {}", self.key);
                n.notified().await;
                self.am_authority = true;
                debug!("enter() complete waiting for {}", self.key);
            }
        }
    }
}

impl<K: Eq + Hash + Clone + fmt::Display> OrderedWaitMap<K> {
    pub fn acquire(this: &Arc<Self>, k: K) -> OrderedWaitHandle<K> {
        let mut map = this.keys.lock().unwrap();
        match map.get_mut(&k) {
            None => {
                map.insert(k.clone(), Vec::new());
                OrderedWaitHandle {
                    inner: None,
                    parent: Arc::downgrade(this),
                    key: k,
                    am_authority: true,
                }
            }
            Some(v) => {
                let n = Arc::new(Notify::new());
                v.push(n.clone());
                OrderedWaitHandle {
                    inner: Some(n),
                    parent: Arc::downgrade(this),
                    key: k,
                    am_authority: false,
                }
            }
        }
    }

    fn release(&self, k: &K) {
        let mut map = self.keys.lock().unwrap();

        // Optimistically remove: well formed objectstore workloads
        // should only rarely be doing repeated writes to the same key.
        let (k, mut v) = map.remove_entry(k).unwrap();
        if v.is_empty() {
            return;
        } else {
            let n = v.remove(0);
            n.notify_one();
            map.insert(k, v);
        }
    }

    // Special case used when we have called acquire but want to dispose of
    // the waiter before it has been notified (e.g. on drop)
    fn release_waiter(&self, k: &K, notifier: &Arc<Notify>) {
        let mut map = self.keys.lock().unwrap();

        match map.get_mut(k) {
            None => {
                return;
            }
            Some(e) => {
                let mut new_vec: Vec<Arc<Notify>> = Vec::new();
                for i in e.drain(..) {
                    if Arc::as_ptr(&i) != Arc::as_ptr(notifier) {
                        new_vec.push(i);
                    }
                }
                *e = new_vec;
            }
        }
    }

    pub fn len(&self) -> usize {
        self.keys.lock().unwrap().len()
    }

    pub fn new() -> Self {
        Self {
            keys: Mutex::new(HashMap::new()),
        }
    }
}

#[cfg(test)]
use tokio;

#[tokio::test]
async fn test_simple() {
    let owm: Arc<OrderedWaitMap<String>> = Arc::new(OrderedWaitMap::new());

    // Simplest case:
    let k = String::from("foo");
    let mut h1 = OrderedWaitMap::acquire(&owm, k.clone());
    assert!(!owm.keys.lock().unwrap().is_empty());
    let mut h2 = OrderedWaitMap::acquire(&owm, k.clone());
    let mut h3 = OrderedWaitMap::acquire(&owm, k.clone());

    h1.enter().await;
    drop(h1);
    h2.enter().await;
    drop(h2);
    h3.enter().await;
    drop(h3);

    assert!(owm.keys.lock().unwrap().is_empty());
}

#[tokio::test]
async fn test_early_drop() {
    let owm: Arc<OrderedWaitMap<String>> = Arc::new(OrderedWaitMap::new());

    let k = String::from("foo");
    let mut h1 = OrderedWaitMap::acquire(&owm, k.clone());
    let h2 = OrderedWaitMap::acquire(&owm, k.clone());
    let mut h3 = OrderedWaitMap::acquire(&owm, k.clone());

    h1.enter().await;

    // Having dropped without entering, the waiter list for the key
    // should have been updated such that only h3 is waiting (h1 still holds)
    drop(h2);

    drop(h1);
    h3.enter().await;
    drop(h3);

    assert!(owm.keys.lock().unwrap().is_empty());
}
