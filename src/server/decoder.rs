use log::*;
use std::convert::From;
use std::mem;
use std::pin::Pin;
use std::task::{Context, Poll};

use futures::pin_mut;
use futures::Future;
use futures::StreamExt;

use hyper::body::Bytes;

use crate::util::format::dump_buffer;
use futures::{Stream, TryStreamExt};

use capralib::auth::Authorizer;
use futures::stream::IntoStream;
use hyper::Body;

pub use capralib::encoder::{DecodeError, DecodeErrorKind};

#[derive(PartialEq)]
enum ChunkedBodyState {
    // Not inside a chunk.  Usually because we didn't get bytes yet, but can
    // also happen if we are given exactly a chunk's worth of data and haven't
    // seen the next header yet.
    Idle,

    // We have seen a chunk header and are currently consuming the bytes for
    // that chunk.
    IntraChunk,
}

pub struct ChunkedBodyDecoder {
    raw_body: IntoStream<Body>,

    state: ChunkedBodyState,

    // TODO: validate on input that nobody tries to mess with us
    // by overflowing usize with teh size of an incoming chunk (S3 does not
    // specify a max chunk size, but we can reasonably apply one)

    // When IntraChunk, the size of the current chunk
    chunk_size: usize,
    // When IntraChunk, the number of bytes remaining in the current chunk
    chunk_remaining: usize,
    // When IntraChunk, the signature from the start of the chunk
    chunk_signature: String,

    buffer_cursor: usize,
    buffer: Option<Bytes>,

    input_closed: bool,
}

// Shortest possible header:
// 1;chunk-signature=ad80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648\r\n
const SHORTEST_CHUNK_HEADER: usize = 84;

#[derive(Debug)]
struct EncodingError;

// When using a ChunkedBodyDecoder, you're usually going to want to validate
// the signatures: this is a think stream layer that does that.
pub struct AuthValidator<T: Stream<Item = Result<(Bytes, Option<String>), DecodeError>>> {
    // FIXME: play around with avoiding boxing this, can we just use pin_mut inline?
    input: Pin<Box<T>>,
    auth: Box<dyn Authorizer>,
}

impl<T: Stream<Item = Result<(Bytes, Option<String>), DecodeError>>> AuthValidator<T> {
    pub fn new(inner: T, auth: Box<dyn Authorizer>) -> Self {
        Self {
            input: Box::pin(inner),
            auth,
        }
    }
}

impl<T: Stream<Item = Result<(Bytes, Option<String>), DecodeError>>> Stream for AuthValidator<T> {
    type Item = Result<Bytes, DecodeError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let (bytes, chunk_sig) = match self.input.as_mut().poll_next(cx) {
            Poll::Pending => return Poll::Pending,
            Poll::Ready(None) => return Poll::Ready(None),
            Poll::Ready(Some(Err(e))) => return Poll::Ready(Some(Err(e.into()))),
            Poll::Ready(Some(Ok(r))) => r,
        };

        debug!(
            "AuthValidator: saw {} bytes, sig={:?}",
            bytes.len(),
            chunk_sig
        );

        self.auth.update(&bytes);
        if let Some(chunk_sig) = chunk_sig {
            if let Err(e) = self.auth.update_chunked(chunk_sig) {
                Poll::Ready(Some(Err(e.into())))
            } else {
                Poll::Ready(Some(Ok(bytes)))
            }
        } else {
            Poll::Ready(Some(Ok(bytes)))
        }
    }
}

/// Non-chunked equivalent of AuthValidator.  Authorization happens
/// when the incoming stream ends, and auth errors surface as errors
/// in the stream's output
///
/// It is *essential* that callers read the stream
/// all the way to the end to get authentication!
pub struct PlainAuthValidator<T: Stream<Item = Result<Bytes, DecodeError>>> {
    input: Pin<Box<T>>,
    auth: Box<dyn Authorizer>,
}

impl<T: Stream<Item = Result<Bytes, DecodeError>>> PlainAuthValidator<T> {
    pub fn new(inner: T, auth: Box<dyn Authorizer>) -> Self {
        Self {
            input: Box::pin(inner),
            auth,
        }
    }
}

impl<T: Stream<Item = Result<Bytes, DecodeError>>> Stream for PlainAuthValidator<T> {
    type Item = Result<Bytes, DecodeError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        if self.auth.finalized() {
            // This happens if there was an auth error that caused us
            // to yield a Some(Err) instead of a None when the inner stream ended.
            return Poll::Ready(None);
        }

        match self.input.as_mut().poll_next(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(None) => {
                if let Err(e) = self.auth.finalize() {
                    // Authentication error at end of stream!
                    Poll::Ready(Some(Err(e.into())))
                } else {
                    // Healthy end of stream
                    Poll::Ready(None)
                }
            }
            Poll::Ready(Some(Err(e))) => Poll::Ready(Some(Err(e.into()))),
            Poll::Ready(Some(Ok(bytes))) => {
                self.auth.update(&bytes.slice(..));
                Poll::Ready(Some(Ok(bytes)))
            }
        }
    }
}

impl ChunkedBodyDecoder {
    pub fn new(raw_body: hyper::Body) -> Self {
        return Self {
            raw_body: raw_body.into_stream(),
            state: ChunkedBodyState::Idle,
            chunk_size: 0,
            chunk_remaining: 0,
            chunk_signature: String::new(),
            buffer_cursor: 0,
            buffer: None,
            input_closed: false,
        };
    }

    // Returns the number of bytes consumed
    fn try_read_chunk_header(&mut self) -> Result<usize, EncodingError> {
        debug_assert!(self.state == ChunkedBodyState::Idle || self.chunk_remaining == 0);
        debug_assert!(!self.buffer.is_none());

        let unwrapped_buffer = self.buffer.as_ref().unwrap();

        // Optimisation: don't start trying to parse unless we have a shot at
        // getting a complete chunk header
        if unwrapped_buffer.len() - self.buffer_cursor < SHORTEST_CHUNK_HEADER {
            self.state = ChunkedBodyState::Idle;
            return Ok(0);
        }

        // If we see a leading \r\n, skip first two bytes (these are the trailing
        // part of a previous body).
        // TODO: make this stricter by holding state for whether we're on first
        // chunk, rather than uncritically proceeding based on the byte we see.
        if unwrapped_buffer[self.buffer_cursor] == 0x0d
            && unwrapped_buffer[self.buffer_cursor + 1] == 0x0a
        {
            self.buffer_cursor += 2;
        }
        let in_bytes = unwrapped_buffer.slice(self.buffer_cursor..);

        debug!(
            "Trying to read chunk header at {} (bytes available {})",
            self.buffer_cursor,
            in_bytes.len()
        );

        // Previous calls should never leave us in a state where we have
        // a buffer but no available bytes in it.
        assert!(in_bytes.len() > 0);

        let bytes = in_bytes.as_ref();

        // Read up to semicolon
        let mut k = 0;
        let mut semicolon = 0;
        let mut equals = 0;
        let mut newline = 0;
        for byte in bytes {
            if semicolon == 0 && *byte == 0x3b {
                // ';'
                semicolon = k;
            } else if semicolon > 0 && equals == 0 && *byte == 0x3d {
                // '='
                equals = k;
            } else if equals > 0 && *byte == 0x0a {
                // '\n'
                newline = k;
                break;
            }
            k += 1;
        }

        if newline == 0 {
            self.state = ChunkedBodyState::Idle;
            return Ok(0);
        }

        let size_str_bytes = &bytes[0..semicolon];
        let size_str = match std::str::from_utf8(size_str_bytes) {
            Ok(v) => v,
            Err(e) => {
                error!("Bad chunk size {}", e);
                return Err(EncodingError);
            }
        };

        let size: usize = match usize::from_str_radix(size_str, 16) {
            Ok(v) => v,
            Err(_) => {
                error!("Bad size string '{}'", size_str);
                dump_buffer(log::Level::Warn, size_str_bytes);
                error!("All bytes:");
                dump_buffer(log::Level::Warn, bytes);
                return Err(EncodingError);
            }
        };

        let sig_str_bytes = &in_bytes[equals + 1..newline - 1];
        // FIXME: this is wasteful, I don't need to UTF-8 validate this signature text.
        let sig_str = match std::str::from_utf8(sig_str_bytes) {
            Ok(v) => v,
            Err(e) => {
                error!("Bad sig string {}", e);
                error!("semicolon at {}", semicolon);
                error!("equals at {}", equals);
                error!("newline at {}", newline);
                return Err(EncodingError);
            }
        };

        // k is index of \n, advance one past it to get the overall byte count
        let header_bytes_consumed = k + 1;

        let whole_header = &bytes[0..header_bytes_consumed];
        debug!("Whole header read:");
        dump_buffer(log::Level::Debug, whole_header);

        // All done: we have a valid size and signature
        self.state = ChunkedBodyState::IntraChunk;
        self.chunk_size = size;
        self.chunk_signature = sig_str.to_string();
        self.chunk_remaining = self.chunk_size;

        return Ok(header_bytes_consumed);
    }

    // Return a 2 tuple:
    // - First is None if no data is available, otherwise bytes from inside
    //   current chunk (may be zero length Bytes)
    // - Second is None if first is None, also None if we are not at the end
    //   of a chunk, or it is set if we returned the last bytes of a chunk.
    fn read(&mut self) -> (Option<Bytes>, Option<String>) {
        debug!("ChunkedBodyDecoder.read");
        if self.buffer.is_none() {
            return (None, None);
        }

        if self.state != ChunkedBodyState::IntraChunk {
            let head_result = self.try_read_chunk_header();

            if let Ok(head_bytes) = head_result {
                if head_bytes == 0 {
                    // No header available
                    debug!("Chunk header unreadable (waiting for more bytes?)");
                    return (None, None);
                } else {
                    // Great, we got a chunk header
                    debug!(
                        "Read chunk header ({:#06x} bytes), chunk size {:#06x}",
                        head_bytes, self.chunk_size
                    );
                    self.buffer_cursor += head_bytes;
                }
            } else {
                // TODO: adjust function definition to enable us to return an error:
                // the HTTP layer should turn this into a 400.
                panic!("Invalid chunked input")
            }
        }

        let buffer = self.buffer.as_ref().unwrap();

        // Previous block should have returned if it we weren't in intrachunk
        // and didn't get a header to enable us to enter that state.
        assert!(self.state == ChunkedBodyState::IntraChunk);

        // The number of bytes available to parse in our input
        let available_bytes = buffer.len() - self.buffer_cursor;

        if available_bytes >= self.chunk_remaining {
            // We are in a position to return all remaining bytes in the chunk
            debug!(
                "ChunkedEncoding.read: cursor {} buffer len {}",
                self.buffer_cursor,
                buffer.len()
            );
            let result_data =
                buffer.slice(self.buffer_cursor..self.buffer_cursor + self.chunk_remaining);
            let result_signature = mem::take(&mut self.chunk_signature);

            self.buffer_cursor += self.chunk_remaining;
            if self.buffer_cursor >= buffer.len() {
                self.buffer_cursor = 0;
                self.buffer = None;
            }

            self.chunk_size = 0;
            self.chunk_signature.clear();
            self.chunk_remaining = 0;
            self.state = ChunkedBodyState::Idle;

            (Some(result_data), Some(result_signature))
        } else {
            debug!(
                "ChunkedEncoding.read: intra-chunk partial response (chunk_remaining {}, yielding {} bytes)",
                self.chunk_remaining, available_bytes
            );
            // We are returning some incomplete part of the chunk, but consuming
            // the entire buffer
            let result_data =
                buffer.slice(self.buffer_cursor..self.buffer_cursor + available_bytes);
            self.buffer = None;
            self.buffer_cursor = 0;
            self.chunk_remaining -= available_bytes;
            (Some(result_data), None)
        }

        // 10000;chunk-signature=ad80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648
    }

    // This function returns nothing, but it expects and requires that
    // the caller observes the rule that they must call read() to completion
    // in between calls to write(), and may abort if that is not the case.
    fn write(&mut self, in_bytes_raw: Bytes) {
        debug!("ChunkedBodyDecoder.write");
        self.buffer = if self.buffer.is_none() {
            // This is the normal path.  We always do this unless our write() calls
            // happen to split a chunk header, which results in buffering some data
            // and having to concatenate that in the else{} branch of this statement.
            Some(in_bytes_raw)
        } else {
            // Concatenate current buffer and incoming bytes: this happens
            // if the current buffer contains a partial chunk header.

            // FIXME: this allocation could be avoided by maintaining a SmallVec of
            // incomplete chunks, and enabling the header parsing to stitch them together.
            let buffer = self.buffer.as_ref().unwrap();
            let mut combined_bytes: Vec<u8> = Vec::with_capacity(in_bytes_raw.len() + buffer.len());
            combined_bytes.extend_from_slice(&buffer[..]);
            combined_bytes.extend_from_slice(&in_bytes_raw[..]);

            Some(Bytes::from(combined_bytes))
        }
    }

    fn read_input(
        self: &mut Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Option<Poll<Option<<Self as Stream>::Item>>> {
        let next_fut = self.raw_body.next();
        pin_mut!(next_fut);
        match next_fut.poll(cx) {
            Poll::Pending => {
                debug!("ChunkedBodyDecoder read pending");
                Some(Poll::Pending)
            }

            Poll::Ready(Some(bytes_result)) => {
                debug!(
                    "ChunkedBodyDecoder: input bytes {} (err={})",
                    bytes_result.as_ref().map(|b| b.len()).unwrap_or(0),
                    bytes_result.is_err()
                );

                let raw_bytes = match bytes_result {
                    Ok(b) => b,
                    Err(e) => {
                        error!("Source error in ChunkedBodyDecoder: {}", e);
                        return Some(Poll::Ready(Some(Err(DecodeError {
                            kind: DecodeErrorKind::Internal,
                        }))));
                    }
                };
                debug!("ChunkedBodyDecoder raw body bytes {:#06x}", raw_bytes.len());
                self.write(raw_bytes);
                None
            }
            Poll::Ready(None) => {
                let bytes_buffered = self
                    .buffer
                    .as_ref()
                    .map(|v| v.len() - self.buffer_cursor)
                    .unwrap_or(0);
                debug!(
                    "ChunkedBodyDecoder: end of input, {} bytes buffered",
                    bytes_buffered
                );
                self.input_closed = true;
                None
            }
        }
    }

    fn maybe_read(&mut self) -> Option<Poll<Option<<Self as Stream>::Item>>> {
        let (a, b) = self.read();
        if a.is_none() && b.is_none() {
            if self.input_closed {
                debug!("ChunkedBodyDecoder done: unreadable and input closed");
                Some(Poll::Ready(None))
            } else {
                debug!("ChunkedBodyDecoder not yet readable");
                None
            }
        } else {
            debug!(
                "ChunkedBodyDecoder yielding decoded {:#06x}",
                a.as_ref().unwrap().len()
            );
            Some(Poll::Ready(Some(Ok((a.unwrap(), b)))))
        }
    }
}

impl Stream for ChunkedBodyDecoder {
    type Item = Result<(Bytes, Option<String>), DecodeError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        loop {
            if let Some(r) = self.maybe_read() {
                return r;
            }

            if !self.input_closed {
                if let Some(poll) = self.read_input(cx) {
                    return poll;
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::server::decoder::ChunkedBodyDecoder;
    use capralib::format::dump_buffer;
    use futures::StreamExt;
    use log::debug;

    use hyper::body::Bytes;
    use hyper::Body;
    use ntest::timeout;
    use tokio_stream::wrappers::ReceiverStream;

    #[tokio::test]
    #[timeout(10000)]
    async fn test_valid_encoding() {
        // Three chunks: 16 bytes, 32 bytes, 0 bytes.  Signatures are made up because
        // we're not testing auth here.
        let input =
            "10;chunk-signature=ab80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648\
\r\n1234567890abcdef\r\n\
20;chunk-signature=bc80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648\
\r\n1234567890abcdef1234567890abcdef\r\n\
0;chunk-signature=cd80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648\
\r\n\r\n";
        let decoded_size: usize = 48;

        // We iterate exhaustively over all possible split locations (real life input will be
        // split according to the way the kernel chooses to give us data from a network socket)
        for split_loc in 1..(input.len() - 1) {
            let mut decoded = Vec::new();
            let mut signatures = Vec::with_capacity(3);

            // TODO update test to actually send the data in via the stream and not the inner
            // write/read calls
            let (tx, rx) = tokio::sync::mpsc::channel::<hyper::Result<Bytes>>(16);
            let body = Body::wrap_stream(ReceiverStream::new(rx));

            debug!("split_loc: {}", split_loc);
            let mut decoder = ChunkedBodyDecoder::new(body);
            let part_a = Bytes::from(input.as_bytes()[..split_loc].to_vec());
            let part_b = Bytes::from(input.as_bytes()[split_loc..].to_vec());

            tx.send(Ok(Bytes::from(part_a))).await.unwrap();
            tx.send(Ok(Bytes::from(part_b))).await.unwrap();
            std::mem::drop(tx);

            while let Some(item) = decoder.next().await {
                let (bytes, signature) = item.unwrap();
                debug!("yielded {}", bytes.len());
                dump_buffer(log::Level::Info, bytes.as_ref());
                decoded.push(bytes);

                if let Some(s) = signature {
                    signatures.push(s);
                }
            }

            let sum: usize = decoded.iter().map(|i| i.len()).sum();
            assert_eq!(sum, decoded_size);

            assert_eq!(signatures.len(), 3);
            assert_eq!(
                signatures[0],
                "ab80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648"
            );
            assert_eq!(
                signatures[1],
                "bc80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648"
            );
            assert_eq!(
                signatures[2],
                "cd80c730a21e5b8d04586a2213dd63b9a0e99e0e2307b0ade35a65485a288648"
            );
        }
    }
}
