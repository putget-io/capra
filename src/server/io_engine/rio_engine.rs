use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::os::unix::fs::OpenOptionsExt;

use async_trait::async_trait;
use rio;

use super::IOEngine;
use std::path::Path;

use super::super::journal::io_buffer::IOBuffer;

use log::*;
use std::collections::BTreeMap;

pub const IORING_OP_READV: u8 = 1;
pub const IORING_OP_WRITEV: u8 = 2;
pub const IORING_OP_FSYNC: u8 = 3;

pub struct RioEngine {
    ring: rio::Rio,
    fd_dsync: File,
    fd_direct: File,
    fd_unaligned: File,
    writes_disabled: bool,
    sloppy: bool,
}

impl RioEngine {
    fn available_fs(probe_path: &Path, ring: rio::Rio) -> bool {
        // Remove previous probe file an orphaned on was found
        info!("Checking direct I/O works at {}", probe_path.display());
        if std::fs::metadata(probe_path).is_ok() {
            if let Err(e) = std::fs::remove_file(probe_path) {
                // Weird situation: perhaps RO fs with previous probe file?.
                // Bail out as we cannot complete other checks.
                error!(
                    "Cannot remove old probe file at {}: {}",
                    probe_path.display(),
                    e
                );
                return false;
            };
        }

        let fd_direct = match OpenOptions::new()
            .custom_flags(libc::O_DIRECT)
            .write(true)
            .read(true)
            .create(true)
            .open(&probe_path)
        {
            Ok(fd) => fd,
            Err(e) => {
                info!(
                    "Could not open file for O_DIRECT writes at {}: {}",
                    probe_path.display(),
                    e
                );
                return false;
            }
        };
        let buffer = IOBuffer::new(0x1000);
        buffer.as_slice_mut().copy_from_slice(&[0; 0x1000]);

        match ring.write_at(&fd_direct, &buffer.as_slice(), 0).wait() {
            Ok(sz) => {
                if sz != 0x1000 {
                    error!(
                        "Short write ({}) while testing direct writes to {}",
                        sz,
                        probe_path.display()
                    );
                    return false;
                }
            }
            Err(e) => {
                info!(
                    "Failed to do direct write into {}: {}",
                    probe_path.display(),
                    e
                );
                return false;
            }
        }

        true
    }
}

#[async_trait]
impl IOEngine for RioEngine {
    fn new(path: &str, sloppy: bool, no_direct: bool, writes_disabled: bool) -> Self {
        // File handle for small writes (all writes are persistent as soon as they complete)
        let sync_flags =
            if sloppy { 0 } else { libc::O_DSYNC } | if no_direct { 0 } else { libc::O_DIRECT };
        let fd_dsync = OpenOptions::new()
            .custom_flags(sync_flags)
            .write(true)
            .open(&path)
            .unwrap();

        // File handle for large writes and aligned reads
        // (writes must be followed by fsyncs)
        let direct_flags = if no_direct { 0 } else { libc::O_DIRECT };
        let fd_direct = OpenOptions::new()
            .custom_flags(direct_flags)
            .write(true)
            .read(true)
            .open(&path)
            .unwrap();

        // File handle for unaligned reads of StreamFooter during replay
        let fd_unaligned = OpenOptions::new().read(true).open(&path).unwrap();

        Self {
            ring: rio::new().unwrap(),
            fd_dsync,
            fd_direct,
            fd_unaligned,
            writes_disabled,
            sloppy,
        }
    }

    fn available(probe_path: Option<&Path>) -> bool {
        // We need to check:
        //  - io_uring syscalls are available
        //  - io_uring probe includes the ops we need
        //  - (if probe_path is Some) an open FD supports direct IO

        let ring: rio::Rio = match rio::new() {
            Ok(r) => r,
            Err(e) => {
                if e.raw_os_error() == Some(38) {
                    // ENOTIMPLEMENTED
                    // This is totally expected on older kernels.  Log at INFO
                    // level to give the user a hint during startup that they're
                    // on an old kernel.
                    info!("io_uring is not available (pre-5.1 kernel?)");
                } else {
                    // Errors other than ENOTIMPLEMENTED are unexpected here.
                    error!("Unexpected error setting up io_uring: {}", e);
                }
                return false;
            }
        };

        let ops = match ring.probe_ops() {
            Ok(o) => o,
            Err(e) => {
                info!("io_uring probe failed (pre 5.6 kernel?): {}", e);
                return false;
            }
        };
        debug!("io_uring reports {} opcodes available:", ops.len());
        let mut available_ops: BTreeMap<u8, u16> = BTreeMap::new();
        for o in ops {
            debug!("  op: {:#04x} {:#06x}", o.op, o.flags);

            // Op >0 means this isn't a gap in the sparse vector
            // Flags & 0x1 means this op is available
            if o.op > 0 && o.flags & 0x1 > 0 {
                available_ops.insert(o.op, o.flags);
            }
        }

        if available_ops.get(&IORING_OP_READV).is_none() {
            info!("Missing io_uring op: readv");
            return false;
        }

        if available_ops.get(&IORING_OP_WRITEV).is_none() {
            info!("Missing io_uring op: writev");
            return false;
        }

        if available_ops.get(&IORING_OP_FSYNC).is_none() {
            info!("Missing io_uring op: fsync");
            return false;
        }

        // Filesystem-specific checks that don't apply when using a block device
        if let Some(probe_path) = probe_path {
            let r = Self::available_fs(probe_path, ring);

            // Whhether the checks passed or failed, ensure we clean up the probe file
            let _ignore = std::fs::remove_file(probe_path);

            r
        } else {
            // No more checks for block devices: we passed.
            true
        }
    }

    #[inline(always)]
    async fn write_at(&self, buffer: &[u8], at: u64) -> io::Result<()> {
        if self.writes_disabled {
            return Ok(());
        }
        self.ring
            .write_at(&self.fd_direct, &buffer, at)
            .await
            .map(|_sz| ())
    }

    #[inline(always)]
    async fn write_at_sync(&self, buffer: &[u8], at: u64) -> io::Result<()> {
        if self.writes_disabled {
            return Ok(());
        }
        self.ring
            .write_at(&self.fd_dsync, &buffer, at)
            .await
            .map(|_sz| ())
    }

    #[inline(always)]
    async fn read_at(&self, buffer: &mut [u8], at: u64) -> io::Result<usize> {
        self.ring.read_at(&self.fd_direct, &buffer, at).await
    }

    #[inline(always)]
    async fn read_at_unaligned(&self, buffer: &mut [u8], at: u64) -> io::Result<usize> {
        self.ring.read_at(&self.fd_unaligned, &buffer, at).await
    }

    #[inline(always)]
    async fn fdatasync(&self) -> io::Result<()> {
        if self.writes_disabled || self.sloppy {
            return Ok(());
        }
        // FIXME: fdatasync seems to be broken somehow, so using fsync
        self.ring.fsync(&self.fd_direct).await
    }
}
