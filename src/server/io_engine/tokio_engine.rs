use std::io;
use tokio::io::{AsyncReadExt, AsyncSeekExt, AsyncWriteExt, SeekFrom};

use async_trait::async_trait;
use libc;
use std::os::unix::fs::OpenOptionsExt;

use std::fs::OpenOptions;
use tokio::fs::File;

use super::IOEngine;

use capralib::simple_pool::SimplePool;
use log::*;

// How many of each flavour of FD to create per engine
const FD_COUNT: usize = 16;

pub struct TokioEngine {
    fds_dsync: SimplePool<File>,
    fds: SimplePool<File>,
    writes_disabled: bool,
    sloppy: bool,
}

#[async_trait]
impl IOEngine for TokioEngine {
    fn new(path: &str, sloppy: bool, _no_direct: bool, writes_disabled: bool) -> Self {
        // File handle for small writes (all writes are persistent as soon as they complete)
        let fds_dsync = SimplePool::new_with(FD_COUNT, || {
            File::from_std(
                OpenOptions::new()
                    .custom_flags(if sloppy { 0 } else { libc::O_DSYNC })
                    .write(true)
                    .open(&path)
                    .unwrap(),
            )
        });

        // File handle for large writes which will be followed by fsyncs
        let fds = SimplePool::new_with(FD_COUNT, || {
            File::from_std(
                OpenOptions::new()
                    .write(true)
                    .read(true)
                    .open(&path)
                    .unwrap(),
            )
        });

        Self {
            fds_dsync,
            fds,
            writes_disabled,
            sloppy,
        }
    }

    async fn write_at(&self, buffer: &[u8], at: u64) -> io::Result<()> {
        if self.writes_disabled {
            return Ok(());
        }
        let mut fd = self.fds.acquire_smart().await;
        fd.seek(SeekFrom::Start(at)).await.expect("Seek I/O error");
        fd.write_all(buffer).await
    }

    async fn write_at_sync(&self, buffer: &[u8], at: u64) -> io::Result<()> {
        if self.writes_disabled {
            return Ok(());
        }
        let mut fd = self.fds_dsync.acquire_smart().await;
        fd.seek(SeekFrom::Start(at)).await.expect("Seek I/O error");
        fd.write_all(buffer).await
    }

    async fn read_at(&self, buffer: &mut [u8], at: u64) -> io::Result<usize> {
        let mut fd = self.fds.acquire_smart().await;
        fd.seek(SeekFrom::Start(at)).await.expect("Seek I/O error");
        let r = fd.read_exact(buffer).await;
        match r.as_ref() {
            Err(e) => {
                error!("read_at({:#06x}, {:#010x}): {}", buffer.len(), at, e);
            }
            Ok(size) => {
                assert_eq!(*size, buffer.len())
            }
        }
        r
    }

    async fn read_at_unaligned(&self, buffer: &mut [u8], at: u64) -> io::Result<usize> {
        let mut fd = self.fds.acquire_smart().await;
        fd.seek(SeekFrom::Start(at)).await.expect("Seek I/O error");
        fd.read(buffer).await
    }

    async fn fdatasync(&self) -> io::Result<()> {
        if self.writes_disabled || self.sloppy {
            return Ok(());
        }
        let fd = self.fds.acquire_smart().await;
        fd.sync_data().await
    }
}
