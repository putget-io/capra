use async_trait::async_trait;
use nix;
use std::io;
use std::sync::Arc;

use log::info;

// nix doesn't have a method for parsing out st_mode from a stat call,
// so manually do it with these values from linux/include/uapi/linux/stat.h
const S_IFBLK: u32 = 0o060000;
const S_IFMT: u32 = 0o0170000;
const S_IFREG: u32 = 0o100000;

const CAPRA_PROBE_FILE: &str = ".capraprobe";

#[async_trait]
pub trait IOEngine: Send + Sync {
    fn new(path: &str, sloppy: bool, no_direct: bool, writes_disabled: bool) -> Self
    where
        Self: Sized;

    /// Check whether the engine is usable in the current environment
    /// (e.g. kernel new enough for io_uring)
    fn available(_probe_path: Option<&Path>) -> bool
    where
        Self: Sized,
    {
        true
    }

    async fn write_at(&self, buffer: &[u8], at: u64) -> io::Result<()>;
    async fn write_at_sync(&self, buffer: &[u8], at: u64) -> io::Result<()>;
    async fn read_at(&self, buffer: &mut [u8], at: u64) -> io::Result<usize>;
    async fn read_at_unaligned(&self, buffer: &mut [u8], at: u64) -> io::Result<usize>;
    async fn fdatasync(&self) -> io::Result<()>;
}

#[cfg(feature = "rio_engine")]
mod rio_engine;
#[cfg(feature = "rio_engine")]
pub use rio_engine::RioEngine;

mod tokio_engine;
use nix::errno::Errno;
use std::path::{Path, PathBuf};
pub use tokio_engine::TokioEngine;

#[derive(PartialEq, Debug)]
pub enum FileType {
    DoesNotExist,
    Regular,
    Block,
    Other, // Exists, but is something other than a block device or regular file
}

/// When we're given a path input, we need to figure out whether it exists,
/// and if so what sort of file it is.
pub fn resolve_path(path: &Path) -> std::io::Result<FileType> {
    match nix::sys::stat::stat(path) {
        Err(e) => {
            if e.as_errno().unwrap() != Errno::ENOENT {
                // An unexpected error
                return Err(std::io::Error::from_raw_os_error(
                    e.as_errno().unwrap() as i32
                ));
            } else {
                // Doesn't exist -> isn't a block dev
                Ok(FileType::DoesNotExist)
            }
        }
        Ok(meta) => {
            if (meta.st_mode & S_IFMT) == S_IFBLK {
                Ok(FileType::Block)
            } else if (meta.st_mode & S_IFMT) == S_IFREG {
                Ok(FileType::Regular)
            } else {
                Ok(FileType::Other)
            }
        }
    }
}

pub type Builder = fn(&str, bool, bool) -> Arc<dyn IOEngine>;

pub fn get_builder(which: &Option<String>, location: &Path) -> Result<Builder, String> {
    let file_type = resolve_path(location).map_err(|e| e.to_string())?;
    let is_block = match file_type {
        FileType::Block => true,
        FileType::Other => {
            return Err(format!(
                "Journal shard {} is neither a block device nor a regular file",
                location.display()
            ))
        }
        _ => false,
    };

    let probe_file = if is_block {
        None
    } else {
        let probe_dir: PathBuf = match location.parent() {
            None => location.to_owned(),
            Some(p) => p.to_owned(),
        };

        Some(probe_dir.join(Path::new(CAPRA_PROBE_FILE)))
    };
    let probe_file = probe_file.as_ref().map(|path_buf| path_buf.as_path());

    let engine_name = match which {
        None => {
            #[cfg(feature = "rio_engine")]
            let default = if RioEngine::available(probe_file) {
                "rio"
            } else {
                "tokio"
            };
            #[cfg(not(feature = "rio_engine"))]
            let default = "tokio";

            info!("Auto-selected IOEngine: {}", default);

            default
        }
        Some(s) => s.as_str(),
    };

    match engine_name {
        "tokio" => Ok(|path, sloppy, writes_disabled| {
            Arc::new(TokioEngine::new(&path, sloppy, false, writes_disabled))
        }),
        #[cfg(feature = "rio_engine")]
        "rio" => {
            // If this engine was selected explicitly, must do the available() check
            // (if we picked it as the default then we already checked this above)
            if which.is_some() && !RioEngine::available(probe_file) {
                return Err(format!(
                    "Engine '{}' not usable on this system",
                    engine_name
                ));
            } else {
                Ok(|path, sloppy, writes_disabled| {
                    Arc::new(RioEngine::new(&path, sloppy, false, writes_disabled))
                })
            }
        }
        _ => Err(format!("Engine '{}' not found", engine_name)),
    }
}
