use std::error::Error;
use std::str::FromStr;

use capralib::auth::QUERY_ENCODE_CHARS;
use http::request::Parts;
use hyper::{Body, Response, StatusCode};
use log::*;
use std::fmt::Formatter;
#[cfg(test)]
use yaserde;
/// This module is for types that map directly to the S3 protocol, for shared use
/// by both the frontend and backend implementations.
use yaserde_derive::{YaDeserialize, YaSerialize};

// Per ListObjectsV2 documentation, if a client omits max_keys, a server paginates to 1000
pub const DEFAULT_MAX_KEYS: usize = 1000;

pub const KEY_LEN_MAX: usize = 1024;
pub const BUCKET_LEN_MAX: usize = 63;
pub const BUCKET_LEN_MIN: usize = 3;

//TODO: refactor with Cows to be save a few allocs
//     (this isn't urgent, listings aren't considered hot path)
pub struct ListObjectsV2Request {
    pub prefix: Option<String>,
    pub continuation_token: Option<String>,
    pub start_after: Option<String>,
    pub delimiter: Option<String>,
    pub encoding_type: Option<String>,
    pub fetch_owner: bool,
    pub max_keys: Option<usize>,
}

fn kv_split(kv: &str) -> Option<(&str, &str)> {
    let mut i = kv.split("=");
    let k = match i.next() {
        None => return None,
        Some(k) => k,
    };
    let v = match i.next() {
        None => return None,
        Some(v) => v,
    };

    Some((k, v))
}

#[derive(Debug)]
struct RequestParseError {
    why: String,
}

impl std::fmt::Display for RequestParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.why)
    }
}

impl Error for RequestParseError {}

impl ListObjectsV2Request {
    pub fn from_header(head: &Parts) -> Result<Self, Box<dyn Error>> {
        let mut instance = Self {
            prefix: None,
            continuation_token: None,
            start_after: None,
            delimiter: None,
            max_keys: None,
            encoding_type: None,
            fetch_owner: false,
        };

        let query_string = head.uri.query().unwrap_or("");
        let mut is_v2 = false;

        for kv in query_string.split("&") {
            let (k, v) = match kv_split(kv) {
                None => continue,
                Some((k, v)) => (k, v),
            };
            // This is not needed for all fields, but percent_decode is smart and only
            // allocates a new string if it actually had to decode something.
            let v = percent_encoding::percent_decode_str(v).decode_utf8_lossy();
            if v.is_empty() {
                continue;
            }

            match k {
                "continuation-token" => instance.continuation_token = Some(v.into()),
                "prefix" => {
                    instance.prefix = Some(v.into());
                }
                "start-after" => {
                    instance.start_after = Some(v.into());
                }
                "delimiter" => {
                    instance.delimiter = Some(v.into());
                }
                "max-keys" => {
                    let i = match usize::from_str(&v) {
                        Ok(i) => i,
                        Err(e) => return Err(Box::new(e)),
                    };
                    instance.max_keys = Some(i);
                }
                "encoding-type" => {
                    instance.encoding_type = Some(String::from(v));
                }
                "list-type" => {
                    if v == "2" {
                        is_v2 = true;
                    } else {
                        warn!("Unexpected list-type in GET params: {}", query_string);
                    }
                }
                "fetch-owner" => instance.fetch_owner = bool::from_str(&v)?,
                _ => {
                    warn!("Unexpected query param '{}' on ListObjectsV2", k);
                }
            }
        }

        if is_v2 {
            Ok(instance)
        } else {
            Err(Box::new(RequestParseError {
                why: "no list-type=2".into(),
            }))
        }
    }

    pub fn to_query_string(&self) -> String {
        // Some of these fields are required to be ommitted if unset, whereas
        // others can be included as blank strings (there is no S3 standard, so what
        // works, works).

        let mut parts: Vec<String> = Vec::new();
        if let Some(ct) = self.continuation_token.as_ref() {
            parts.push(format!(
                "continuation-token={}",
                percent_encoding::percent_encode(ct.as_bytes(), QUERY_ENCODE_CHARS)
            ));
        }

        parts.push(format!(
            "delimiter={}",
            percent_encoding::percent_encode(
                self.delimiter.as_ref().map_or("", |s| s as &str).as_bytes(),
                QUERY_ENCODE_CHARS
            )
        ));

        if let Some(mk) = self.max_keys.as_ref() {
            parts.push(format!("max-keys={}", mk));
        }

        parts.push(format!(
            "encoding-type={}",
            self.encoding_type.as_ref().map_or("", |s| s as &str)
        ));

        parts.push(format!("fetch-owner={}", self.fetch_owner.to_string()));

        parts.push("list-type=2".to_string());

        parts.push(format!(
            "prefix={}",
            percent_encoding::percent_encode(
                self.prefix.as_ref().map_or("", |s| s as &str).as_bytes(),
                QUERY_ENCODE_CHARS
            )
        ));

        if let Some(sa) = self.start_after.as_ref() {
            parts.push(format!(
                "start-after={}",
                percent_encoding::percent_encode(sa.as_bytes(), QUERY_ENCODE_CHARS)
            ));
        }

        parts.join("&")
    }
}

#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone, PartialOrd, Eq, Ord)]
#[yaserde(
    rename_all = "PascalCase",
    namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
    default_namespace = "ns",
    prefix = "ns"
)]
pub struct Prefix {
    #[yaserde(child, prefix = "ns", rename = "Prefix")]
    pub prefix: String,
}

/// https://docs.aws.amazon.com/AmazonS3/latest/API/API_Object.html
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
rename_all = "PascalCase"
namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
default_namespace = "ns",
prefix = "ns"
)]
pub struct Object {
    #[yaserde(child, prefix = "ns", rename = "Key")]
    pub key: String,
    #[yaserde(child, prefix = "ns", rename = "LastModified")]
    pub last_modified: String,
    #[yaserde(child, prefix = "ns", rename = "ETag")]
    pub e_tag: String,
    #[yaserde(child, prefix = "ns", rename = "Size")]
    pub size: u64,
    #[yaserde(child, prefix = "ns", rename = "StorageClass")]
    pub storage_class: String,
}

/// https://docs.aws.amazon.com/AmazonS3/latest/API/API_ListObjectsV2.html#API_ListObjectsV2_ResponseSyntax
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
    rename = "ListBucketResult",
    rename_all = "PascalCase",
    namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
    default_namespace = "ns",
    prefix = "ns"
)]
pub struct ListObjectsV2Response {
    #[yaserde(child, prefix = "ns", rename = "Name")]
    pub name: String,
    #[yaserde(child, prefix = "ns", rename = "Prefix")]
    pub prefix: String,
    #[yaserde(child, prefix = "ns", rename = "KeyCount")]
    pub key_count: u64,
    #[yaserde(child, prefix = "ns", rename = "MaxKeys")]
    pub max_keys: u64,
    #[yaserde(child, prefix = "ns", rename = "Delimiter")]
    pub delimiter: String,
    #[yaserde(child, prefix = "ns", rename = "IsTruncated")]
    pub is_truncated: bool,

    #[yaserde(child, prefix = "ns", rename = "NextContinuationToken")]
    pub next_continuation_token: Option<String>,
    #[yaserde(child, prefix = "ns", rename = "ContinuationToken")]
    pub continuation_token: Option<String>,
    #[yaserde(child, prefix = "ns", rename = "StartAfter")]
    pub start_after: Option<String>,

    #[yaserde(child, prefix = "ns", rename = "Contents")]
    pub contents: Vec<Object>,
    #[yaserde(child, prefix = "ns", rename = "CommonPrefixes")]
    pub common_prefixes: Vec<Prefix>,
}

impl ListObjectsV2Response {
    pub fn into_response(self) -> Response<Body> {
        let content = match yaserde::ser::to_string(&self) {
            Ok(e) => e,
            Err(e) => {
                error!("ListObjectsV2Response encoding error! {:?}", e);
                return Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::empty())
                    .unwrap();
            }
        };

        Response::builder()
            .status(StatusCode::OK)
            .body(Body::from(content))
            .unwrap()
    }
}

// https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
rename_all = "PascalCase"
namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
default_namespace = "ns",
prefix = "ns"
)]
pub struct DeleteObjectsObject {
    #[yaserde(child, prefix = "ns", rename = "Key")]
    pub key: String,
}

// https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
    rename = "Delete",
    rename_all = "PascalCase",
    namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
    default_namespace = "ns",
    prefix = "ns"
)]
pub struct DeleteObjectsRequest {
    #[yaserde(child, prefix = "ns", rename = "Object")]
    pub objects: Vec<DeleteObjectsObject>,
    #[yaserde(child, prefix = "ns", rename = "Quiet")]
    pub quiet: bool,
}

#[test]
fn test_list_objects_v2_response() {
    yaserde::de::from_str::<Object>("<Contents><Key>0_0</Key><LastModified>2021-03-08T22:36:59.601Z</LastModified><ETag>&#34;256459291f3d6e5c2f8d04f14058777a&#34;</ETag><Size>1000000</Size><Owner><ID>02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4</ID><DisplayName></DisplayName></Owner><StorageClass>STANDARD</StorageClass></Contents>").unwrap();

    let example = r#"<?xml version="1.0" encoding="UTF-8"?><ListBucketResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/"><Name>fc2data</Name><Prefix></Prefix><KeyCount>2000</KeyCount><MaxKeys>4500</MaxKeys><Delimiter>/</Delimiter><IsTruncated>false</IsTruncated><Contents><Key>0_0</Key><LastModified>2021-03-08T22:36:59.601Z</LastModified><ETag>&#34;256459291f3d6e5c2f8d04f14058777a&#34;</ETag><Size>1000000</Size><Owner><ID>02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4</ID><DisplayName></DisplayName></Owner><StorageClass>STANDARD</StorageClass></Contents><Contents><Key>0_1</Key><LastModified>2021-03-08T22:36:59.603Z</LastModified><ETag>&#34;256459291f3d6e5c2f8d04f14058777a&#34;</ETag><Size>1000000</Size><Owner><ID>02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4</ID><DisplayName></DisplayName></Owner><StorageClass>STANDARD</StorageClass></Contents><Contents><Key>0_10</Key><LastModified>2021-03-08T22:36:59.726Z</LastModified><ETag>&#34;256459291f3d6e5c2f8d04f14058777a&#34;</ETag><Size>1000000</Size><Owner><ID>02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4</ID><DisplayName></DisplayName></Owner><StorageClass>STANDARD</StorageClass></Contents><EncodingType>url</EncodingType></ListBucketResult>"#;

    let decoded = yaserde::de::from_str::<ListObjectsV2Response>(example).unwrap();
    eprintln!("{:?}", decoded);

    assert_eq!(decoded.name, "fc2data");
    assert_eq!(decoded.contents.len(), 3);
    assert_eq!(decoded.contents[0].key, "0_0");

    // Same exact checks, but this time on the decode of what we re-encoded
    let encoded = yaserde::ser::to_string(&decoded).unwrap();
    eprintln!("{:?}", encoded);
    let decoded = yaserde::de::from_str::<ListObjectsV2Response>(&encoded).unwrap();
    assert_eq!(decoded.name, "fc2data");
    assert_eq!(decoded.contents.len(), 3);
    assert_eq!(decoded.contents[0].key, "0_0");
}

#[test]
fn test_deleteobjects_request() {
    let example =  "<Delete><Object><Quiet>true</Quiet><Key>sample1.txt</Key></Object><Object><Key>sample2.txt</Key></Object></Delete>";

    let decoded = yaserde::de::from_str::<DeleteObjectsRequest>(example).unwrap();
    assert_eq!(decoded.objects.len(), 2);
    assert_eq!(decoded.objects[0].key, "sample1.txt");
    assert_eq!(decoded.objects[1].key, "sample2.txt");
}

// https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
    rename = "DeleteResult",
    rename_all = "PascalCase",
    namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
    default_namespace = "ns",
    prefix = "ns"
)]
pub struct DeleteObjectsResponse {
    #[yaserde(child, prefix = "ns", rename = "Deleted")]
    pub deleted: Vec<DeleteObjectsObject>,
    #[yaserde(child, prefix = "ns", rename = "Error")]
    pub errors: Vec<DeleteObjectsError>,
}

// https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObjects.html
#[derive(YaSerialize, YaDeserialize, PartialEq, Debug, Clone)]
#[yaserde(
rename_all = "PascalCase"
namespace = "ns: http://s3.amazonaws.com/doc/2006-03-01/",
default_namespace = "ns",
prefix = "ns"
)]
pub struct DeleteObjectsError {
    #[yaserde(child, prefix = "ns", rename = "Key")]
    pub key: String,
    #[yaserde(child, prefix = "ns", rename = "Code")]
    pub code: String,
    #[yaserde(child, prefix = "ns", rename = "Error")]
    pub error: String,
}
