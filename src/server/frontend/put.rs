use std::convert::Infallible;
use std::sync::Arc;

use digest;
use digest::{Digest, Output};
use futures::{Stream, StreamExt, TryStreamExt};
use http::{Response, StatusCode, Version};
use hyper::Body;
use log::*;
use md5;
use partial_min_max::min;

use crate::server::core_tracker::CoreTracker;
use crate::server::decoder::{ChunkedBodyDecoder, DecodeError};
use crate::server::frontend::handler_state::HandlerState;
use crate::server::frontend::request::S3Request;
use crate::server::journal::chunk_journal::{ChunkJournal, JournalStream};
use crate::server::journal::StreamHeader;
use crate::server::key_map::{PersistentValue, PersistentValueOp, PutValue};
use crate::server::types::{ObjectOp, OffsetList};
use crate::util;
use crate::util::auth::Authorizer;
use crate::util::http::empty_response;
use hyper::body::Bytes;
use md5::Md5;
use rand::{thread_rng, Rng};

// For reasonably large MD5s, it's worth running them in a tokio spawn_blocking
// in order to parallelize the calculation with respect to the SHA256 calculation.
// (Threshold determined by single-worker PUT benchmark on Ryzen 1700 with one
//  server worker and un-chunked PUT bodies)
const BACKGROUND_MD5_THRESHOLD: usize = 0x10000;

/// For testing
fn _maybe_fail(key: &str, every_n: u32) -> Result<(), Result<Response<Body>, Infallible>> {
    let rand: u32 = thread_rng().gen();
    if rand % every_n == 0 {
        error!("Synthetic PUT failure key={}", key);
        return Err(empty_response(StatusCode::INTERNAL_SERVER_ERROR));
    } else {
        Ok(())
    }
}

async fn consume_put_body<
    D: Stream<Item = Result<(Bytes, Option<String>), DecodeError>> + Unpin,
    A: Authorizer,
    S: JournalStream,
>(
    mut decoder: D,
    mut auth: A,
    mut stream_state: S,
    key: &str,
    content_length: u64,
    core_tracker: &CoreTracker,
    no_etags: bool,
) -> Result<(OffsetList, digest::Output<md5::Md5>), Result<Response<Body>, Infallible>> {
    let mut total_decoded_bytes: u64 = 0;

    let mut etag = md5::Md5::new();

    // This loop is a tad complicated compared with just wrapping our
    // incoming stream in an AuthValidator -- this is because we
    // need to manually handle auth in order to:
    //  - overlap it with MD5s when running them in background
    //  - overlap it with IO latency (issue our NVMe writes before
    //    we have done auth checks)
    while let Some(item) = decoder.next().await {
        let (bytes, chunk_sig_opt) = if item.is_err() {
            return Err(util::http::empty_response(StatusCode::BAD_REQUEST));
        } else {
            item.unwrap()
        };

        if bytes.len() == 0 {
            debug!("Zero-sized decoded bytes, null chunk");
        } else {
            debug!("Decoded {:#06x} bytes", bytes.len());
            total_decoded_bytes += bytes.len() as u64;
        }

        let last = total_decoded_bytes >= content_length;
        let first = total_decoded_bytes == bytes.len() as u64;

        core_tracker.touch();

        stream_state.write(bytes.as_ref()).await;
        debug!(
            "Submitting write, content {}/{} (first={} last={})",
            total_decoded_bytes, content_length, first, last
        );

        // Update hashes *after* we've dispatched an IO, to hide latency
        let background_md5 =
            (bytes.len() >= BACKGROUND_MD5_THRESHOLD) && (auth.enabled() && !no_etags);
        if background_md5 {
            // For large enough buffers, hide latency by running MD5 calculation on
            // a background thread in parallel to the SHA256 (auth) calculation.
            let bg_bytes = bytes.clone();
            let mut bg_etag = md5::Md5::new();
            std::mem::swap(&mut bg_etag, &mut etag);
            let bg_f = tokio::task::spawn_blocking(move || {
                bg_etag.update(&bg_bytes[..]);
                bg_etag
            });

            auth.update(&bytes);

            etag = bg_f.await.unwrap();
        } else {
            // Overlap SHA256 and MD5 calculations to try and avoid consuming
            // 2x memory bandwidth.  This doesn't net a performance improvement
            // with small IO buffers but if running with huge IO buffers it should
            // help somewhat.
            let hash_chunk_size: usize = 0x8000;
            for i in (0..bytes.len()).step_by(hash_chunk_size) {
                let upper = min(i + hash_chunk_size, bytes.len());
                auth.update(&bytes[i..upper]);
                if !no_etags {
                    etag.update(&bytes[i..upper]);
                }
            }
        }

        if let Some(chunk_sig) = chunk_sig_opt {
            debug!("End of chunk, sig={}", chunk_sig);
            match auth.update_chunked(chunk_sig) {
                Err(auth_err) => {
                    error!("PUT 401 key={} {}", key, auth_err.get_reason());
                    stream_state.cancel().await;
                    return Err(util::http::empty_response(StatusCode::UNAUTHORIZED));
                }
                Ok(_) => {}
            }
        }

        // Clean request completion: they sent the number of bytes
        // that the content length said they should.  Drop out here
        // instead of trying to read anything more from the body.
        if last {
            if let Err(auth_err) = auth.finalize() {
                error!("PUT 401 key={} {}", key, auth_err.get_reason());
                stream_state.cancel().await;
                return Err(util::http::empty_response(StatusCode::UNAUTHORIZED));
            } else {
                debug!("End of request, auth OK");
            }

            break;
        }
    }

    let etag = if no_etags {
        digest::Output::<md5::Md5>::clone_from_slice(&[0; 16])
    } else {
        etag.finalize()
    };

    debug!("PUT key={} syncing...", key);
    let (io_result, chunks) = stream_state.sync(&etag).await;
    if let Err(e) = io_result {
        error!("PUT 500 key={} IO error in sync: {}", key, e);
        Err(util::http::empty_response(
            StatusCode::INTERNAL_SERVER_ERROR,
        ))
    } else {
        debug!("PUT key={} synced", key);
        Ok((chunks, etag))
    }
}

async fn handle_request_put_local<
    D: Stream<Item = Result<(Bytes, Option<String>), DecodeError>> + Unpin,
    A: Authorizer,
>(
    decoder: D,
    handler_state: &Arc<HandlerState>,
    auth: A,
    s3req: &S3Request<'_>,
    journal: ChunkJournal,
) -> Result<(OffsetList, Output<Md5>), Response<Body>> {
    let core_tracker = CoreTracker::new(s3req.key.to_string());

    let stream_header = StreamHeader::new(
        ObjectOp::Put,
        auth.get_secret_id(),
        s3req.bucket,
        &s3req.key,
        s3req.content_length,
        s3req.started_at,
    );

    let content_r = if journal.is_smallstream(&stream_header) {
        let stream_state = journal.begin_smallstream(&stream_header).await;
        debug!("PUT (small) key={}", s3req.key);

        consume_put_body(
            decoder,
            auth,
            stream_state,
            &s3req.key,
            s3req.content_length,
            &core_tracker,
            handler_state.options.no_etags,
        )
        .await
    } else {
        let stream_state = journal.begin_bigstream(&stream_header).await;
        debug!("PUT (streaming) key={}", s3req.key,);

        consume_put_body(
            decoder,
            auth,
            stream_state,
            &s3req.key,
            s3req.content_length,
            &core_tracker,
            handler_state.options.no_etags,
        )
        .await
    };

    content_r.map_err(|r| r.unwrap())
}

pub async fn handle_request_put<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    auth: A,
    s3req: &S3Request<'_>,
    body: Body,
) -> Result<Response<Body>, Infallible> {
    let (auth, body, journal, km_handle) =
        match handler_state.prepare_write(s3req, auth, body).await {
            Ok(v) => v,
            Err(response) => return Ok(response),
        };

    if s3req.content_length > journal.get_max_stream_length() {
        // This is a warn-able situation because when we skip the journal for a write,
        // it violates our ordering constraints with subsequent/concurrent requests that might
        // go through the journal, e.g. deletes to the same key.
        // Properly configured systems should have journals big enough for their workload's biggest
        // writes.
        warn!("PUT too big for journal, proxying: {}", s3req);

        // No need to hold our slot in the KeyMap while proxying
        std::mem::drop(km_handle);

        return Ok(handler_state
            .backend
            .proxy(Box::new(auth), &handler_state.dcache, s3req, body)
            .await);
    }

    let content_r = if s3req.chunked_mode {
        handle_request_put_local(
            ChunkedBodyDecoder::new(body),
            &handler_state,
            auth,
            s3req,
            journal,
        )
        .await
    } else {
        handle_request_put_local(
            body.into_stream()
                .map(|v| v.map(|v| (v, None::<String>)))
                .map_err(|e| e.into()),
            &handler_state,
            auth,
            s3req,
            journal,
        )
        .await
    };

    let (chunks, md5_digest) = match content_r {
        Ok(i) => i,
        Err(r) => return Ok(r),
    };

    // Start constructing response, don't send it until after KeyMap has been updated.
    let mut resp = Response::builder();
    if s3req.head.version == Version::HTTP_11 {
        resp = resp.header("Connection", "keep-alive");
    }
    resp = resp.header("ETag", &format!("\"{:x}\"", md5_digest));

    // Ordering: important that we insert to KeyMap before transmitting response,
    // to preserve read-after-write (per key) consistency
    handler_state
        .key_map
        .complete_write(
            km_handle,
            PersistentValue {
                chunks,
                op: PersistentValueOp::Put(PutValue {
                    last_modified: s3req.started_at,
                    content_length: s3req.content_length,
                    etag: md5_digest,
                }),
            },
        )
        .await;

    // S3 Put operations return 200.  Returning 204 on empty responses
    // is rejected by some clients (e.g. minio mc) even though it is accepted
    // by the AWS SDK.
    return Ok(resp.status(StatusCode::OK).body(Body::empty()).unwrap());
}
