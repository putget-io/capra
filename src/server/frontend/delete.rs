use core::convert::Infallible;
use core::result::Result;
use core::result::Result::{Err, Ok};
use futures::StreamExt;
use std::sync::Arc;

use digest;
use digest::Digest;
use http::response::Response;
use http::status::StatusCode;
use http::version::Version;
use hyper::body::{Body, Bytes};
use log::*;
use md5;

use crate::server::cluster_map::NodeId;
use crate::server::frontend::handler_state::HandlerState;
use crate::server::frontend::request::S3Request;
use crate::server::journal::chunk_journal::{ChunkJournal, JournalStream};
use crate::server::journal::StreamHeader;
use crate::server::key_map::{DeleteValue, PersistentValue, PersistentValueOp, ProjectionHandle};
use crate::server::s3_types::{
    DeleteObjectsError, DeleteObjectsObject, DeleteObjectsRequest, DeleteObjectsResponse,
};
use crate::server::types::ObjectOp;
use crate::util;
use crate::util::auth::Authorizer;
use crate::util::http::status_response;
use capralib::digest::{Sha256, Sha256Digest};
use capralib::s3_client::S3Client;
use capralib::s3_client_auth::ClientAuthConfig;
use chrono::{DateTime, Utc};
use futures::stream::FuturesUnordered;
use hyper::client::HttpConnector;
use hyper::http::uri::{PathAndQuery, Scheme};
use hyper::{Client, Uri};
use std::collections::HashMap;

pub async fn handle_request_delete<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    mut auth: A,
    s3req: &S3Request<'_>,
) -> Result<Response<Body>, Infallible> {
    if let Err(auth_err) = auth.finalize() {
        error!("401 {}", auth_err.get_reason());
        return util::http::empty_response(StatusCode::UNAUTHORIZED);
    } else {
        debug!("DELETE authenticated OK");
    }

    let (auth, _body, journal, km_handle) = match handler_state
        .prepare_write(s3req, auth, Body::empty())
        .await
    {
        Ok(v) => v,
        Err(response) => return Ok(response),
    };

    let stream_header = StreamHeader::new(
        ObjectOp::Delete,
        auth.get_secret_id(),
        s3req.bucket,
        &s3req.key,
        0,
        s3req.started_at,
    );
    let small_stream = journal.begin_smallstream(&stream_header).await;
    let empty_etag = digest::Output::<md5::Md5>::clone_from_slice(&[0; 16]);
    let (result, chunks) = small_stream.sync(&empty_etag).await;

    match result {
        Ok(_) => {
            handler_state
                .key_map
                .complete_write(
                    km_handle,
                    PersistentValue {
                        chunks,
                        op: PersistentValueOp::Delete(DeleteValue {
                            last_modified: s3req.started_at,
                        }),
                    },
                )
                .await;

            let mut resp = Response::builder();
            if s3req.head.version == Version::HTTP_11 {
                resp = resp.header("Connection", "keep-alive");
            }
            Ok(resp
                .status(StatusCode::NO_CONTENT)
                .body(Body::empty())
                .unwrap())
        }
        Err(e) => {
            error!("DELETE sync error: {}", e);
            util::http::empty_response(StatusCode::INTERNAL_SERVER_ERROR)
        }
    }
}

/// None means this object was remote and needs a remote deletion generated
async fn delete_one(
    secret_id: Option<&str>,
    handler_state: Arc<HandlerState>,
    started_at: DateTime<Utc>,
    bucket: &str,
    object: DeleteObjectsObject,
    journal: ChunkJournal,
    km_handle: ProjectionHandle<'_>,
) -> (String, std::io::Result<()>) {
    // Keys in the XML body of a DeleteObjects request might
    // be URL-encoded: decode them.
    let key = percent_encoding::percent_decode(object.key.as_bytes())
        .decode_utf8_lossy()
        .to_string();

    let stream_header = StreamHeader::new(ObjectOp::Delete, secret_id, bucket, &key, 0, started_at);
    let small_stream = journal.begin_smallstream(&stream_header).await;
    let empty_etag = digest::Output::<md5::Md5>::clone_from_slice(&[0; 16]);
    let (result, chunks) = small_stream.sync(&empty_etag).await;

    match &result {
        Ok(_) => {
            handler_state
                .key_map
                .complete_write(
                    km_handle,
                    PersistentValue {
                        chunks,
                        op: PersistentValueOp::Delete(DeleteValue {
                            last_modified: started_at,
                        }),
                    },
                )
                .await;

            (key, result)
        }
        Err(e) => {
            error!("DeleteObjects sync error: {}", e);
            (key, result)
        }
    }
}

/// DeleteObjects is interesting, because it's a multi-key verb, which means
/// that in a clustered system, we may need to break it up into multiple
/// request to get each key's update to the proper node (if those keys
/// are dirty and therefore bound to a particular journal).
pub async fn handle_request_deleteobjects<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    mut auth: A,
    s3req: &S3Request<'_>,
    body: Body,
) -> Result<Response<Body>, Infallible> {
    if handler_state.options.drain {
        // Assumption: we don't need to check for projected state, because if I am
        // in drain mode then so are all other frontend peers (simple clustering where
        // everyone is always running with the same exact config).
        let dirty = handler_state.key_map.get(s3req.bucket, &s3req.key).await;
        if dirty.is_none() {
            debug!(
                "Drain DeleteObjects: passing through write to {}/{} (clean key)",
                s3req.bucket, s3req.key
            );
            return Ok(handler_state
                .backend
                .proxy(Box::new(auth), &handler_state.dcache, &s3req, body)
                .await);
        } else {
            debug!(
                "Drain DeleteObjects: journalling write to {}/{} because the key is already dirty",
                s3req.bucket, s3req.key
            );
        }
    }

    let encoded = match hyper::body::to_bytes(body).await {
        Ok(e) => e.to_vec(),
        Err(e) => {
            warn!("Error reading DeleteObjects body: {}", e);
            return Ok(status_response(StatusCode::BAD_REQUEST));
        }
    };

    auth.update(&encoded);
    if let Err(auth_err) = auth.finalize() {
        error!("401 {}", auth_err.get_reason());
        return util::http::empty_response(StatusCode::UNAUTHORIZED);
    } else {
        debug!("DELETE authenticated OK");
    }

    let decoded =
        match yaserde::de::from_str::<DeleteObjectsRequest>(&String::from_utf8_lossy(&encoded)) {
            Err(e) => {
                error!("Deserialization error on DeleteObjects: {}", e);
                return Ok(status_response(StatusCode::BAD_REQUEST));
            }
            Ok(v) => v,
        };

    let mut remote_keys: HashMap<NodeId, Vec<DeleteObjectsObject>> = HashMap::new();

    let mut local_writes = FuturesUnordered::new();
    for object in decoded.objects {
        let r = handler_state
            .get_journal_write(s3req.bucket, &object.key)
            .await;
        let r = r.map(|(v1, v2)| (v1, v2.into_owned()));

        match r {
            Ok((journal, km_handle)) => {
                // Local: write the tombstone to our journal.
                let km_handle = km_handle.into_owned();
                let secret_id = auth.get_secret_id();
                let handler_state = handler_state.clone();
                info!("  DeleteObjects: deleting {}/{}", s3req.bucket, object.key);
                local_writes.push(delete_one(
                    secret_id,
                    handler_state,
                    s3req.started_at.clone(),
                    s3req.bucket,
                    object,
                    journal,
                    km_handle,
                ))
            }
            Err(remote_node_id) => {
                // Remote: defer generating a DeleteObject to this peer
                info!(
                    "  DeleteObjects: forwarding {}/{} to node {:#02x}",
                    s3req.bucket, object.key, remote_node_id
                );
                let vec = remote_keys
                    .entry(remote_node_id)
                    .or_insert_with(|| Vec::new());
                vec.push(object);
            }
        };
    }

    // Dispatch a DeleteObjects request for to each peer that had
    // dirty keys referenced by the incoming request.
    let mut remote_writes = FuturesUnordered::new();
    for (peer_id, peer_keys) in remote_keys.into_iter() {
        info!(
            "Forwarding {} deletion keys to peer {:#02x}",
            peer_keys.len(),
            peer_id
        );
        let dor = DeleteObjectsRequest {
            quiet: decoded.quiet,
            objects: peer_keys,
        };

        let peer_node = handler_state.cluster_map.get_node(peer_id);
        let peer_address = peer_node.get_s3_address();

        let uri = Uri::builder()
            .scheme(Scheme::HTTP)
            .path_and_query(PathAndQuery::from_static("/"))
            .authority(peer_address.to_string().as_str())
            .build()
            .unwrap();
        let mut s3_client = if handler_state.options.unsigned {
            let client_auth_config = ClientAuthConfig {
                region: handler_state.backend.get_region().to_string(),
                access_key_id: "".to_string(),
                secret_access_key: "".to_string(),
                host: s3req.domain.to_string(),
            };

            S3Client::new(uri, client_auth_config, true)
        } else {
            // The secret must both exist and have a corresponding secret, or we
            // would have failed authorization earlier.
            let secret_id = auth.get_secret_id().unwrap();
            let secret = handler_state.backend.get_secrets().get(secret_id).unwrap();
            let client_auth_config = ClientAuthConfig {
                region: handler_state.backend.get_region().to_string(),
                access_key_id: secret_id.to_string(),
                secret_access_key: secret.to_string(),
                host: s3req.domain.to_string(),
            };

            S3Client::new(uri, client_auth_config, false)
        };

        // Compose request
        let path_and_query = s3req.head.uri.path_and_query().unwrap().clone();
        let content = yaserde::ser::to_string(&dor).unwrap();
        let mut hasher = Sha256::new();
        let body_bytes = Bytes::from(content);
        hasher.update(&body_bytes);
        let body_digest: Sha256Digest = hasher.finalize().into();
        let req = s3_client.post(path_and_query, body_bytes, &body_digest);

        // Issue request
        let connector = HttpConnector::new();
        let client = Client::builder().build::<_, Body>(connector);
        remote_writes.push(client.request(req))
    }

    let mut dors = DeleteObjectsResponse {
        deleted: Vec::new(),
        errors: Vec::new(),
    };

    while let Some((key, result)) = local_writes.next().await {
        if let Err(e) = result {
            // Local IO Error!
            dors.errors.push(DeleteObjectsError {
                key,
                code: "Undefined".into(),
                error: format!("{}", e),
            });
        } else if !decoded.quiet {
            dors.deleted.push(DeleteObjectsObject { key })
        }
    }

    while let Some(r) = remote_writes.next().await {
        match r {
            Err(e) => {
                // Peer Error!  We cannot provide granular report on which
                // keys were successful or not, so must fail the overall request.
                error!("Peer DeleteObjects error: {}", e);
                return Ok(status_response(StatusCode::INTERNAL_SERVER_ERROR));
            }
            Ok(response) => {
                // No Hyper error, but there might be errors in the response body
                let (header, body) = response.into_parts();
                if header.status != StatusCode::OK {
                    warn!("Peer DeleteObjects error: {:?}", header.status);
                    return Ok(status_response(StatusCode::INTERNAL_SERVER_ERROR));
                }

                let resp_body = match hyper::body::to_bytes(body).await {
                    Ok(e) => e.to_vec(),
                    Err(e) => {
                        warn!("Error reading peer DeleteObjects body: {}", e);
                        return Ok(status_response(StatusCode::INTERNAL_SERVER_ERROR));
                    }
                };

                let peer_dors = match yaserde::de::from_str::<DeleteObjectsResponse>(
                    &String::from_utf8_lossy(&resp_body),
                ) {
                    Err(e) => {
                        error!("Peer deserialization error on DeleteObjects: {}", e);
                        return Ok(status_response(StatusCode::INTERNAL_SERVER_ERROR));
                    }
                    Ok(v) => v,
                };

                dors.errors.extend(peer_dors.errors.into_iter());
                dors.deleted.extend(peer_dors.deleted.into_iter());
            }
        }
    }

    let serialized = yaserde::ser::to_string(&dors).unwrap();
    Ok(Response::builder()
        .status(StatusCode::OK)
        .body(Body::from(serialized))
        .unwrap())
}
