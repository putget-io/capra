use std::sync::Arc;
use std::{
    future::Future,
    net::SocketAddr,
    pin::Pin,
    task::{self, Poll},
};

use http::request::Builder as RequestBuilder;
use http::uri::Scheme;
use http::{Response, StatusCode};
use hyper::client::HttpConnector;
use hyper::Uri;
use hyper::{Body, Client};
use log::*;
use tokio;
use tower;

use crate::server::backend::BackendHandle;
use crate::server::cluster_map::{ClusterMap, NodeId};
use crate::server::dcache::DCache;
use crate::server::frontend::request;
use crate::server::frontend::request::S3Request;
use crate::server::journal::chunk_journal;
use crate::server::journal::chunk_journal::ChunkJournal;
use crate::server::journal::journal_collection::JournalCollection;
use crate::server::key_map::{KeyMap, ProjectionHandle};
use crate::server::options::ServerOptions;
use crate::util;
use capralib::auth::Authorizer;

/// All the state that a request handler may need access to
/// to process a request, such as a JournalCollection (to
/// find journals to write to), a KeyMap (to look up
/// in-cache data by Key) and a Backend (to know where
/// to send non-intercepted requests through to).
pub struct HandlerState {
    pub key_map: Arc<KeyMap>,
    pub cluster_map: Arc<ClusterMap>,
    pub journal_collection: Arc<JournalCollection>,
    pub backend: BackendHandle,
    pub dcache: Arc<DCache>,
    pub options: ServerOptions,
}

impl<'a> HandlerState {
    /// Common setup code for single-key PUT and DELETE handlers.  On Ok response,
    /// the journal is local and a slot in KeyMap has been reserved with
    /// a ProjectionHandle.
    ///
    /// The 'error' variant of the response is a Response indication that
    /// we were able to early-handle the request by proxying it to a peer
    /// or the backend.
    pub async fn prepare_write<A: Authorizer + 'static>(
        &'a self,
        s3req: &'a S3Request<'a>,
        auth: A,
        body: Body,
    ) -> Result<(A, Body, ChunkJournal, ProjectionHandle<'a>), Response<Body>> {
        // In drain mode, do a pre-check for whether the key is dirty, and
        // if it isn't then proxy it straight to the backend.
        if self.options.drain {
            // Assumption: we don't need to check for projected state, because if I am
            // in drain mode then so are all other frontend peers (simple clustering where
            // everyone is always running with the same exact config).
            let dirty = self.key_map.get(s3req.bucket, s3req.key.as_ref()).await;
            if dirty.is_none() {
                debug!(
                    "Drain {:?}: passing through write to {}/{} (clean key)",
                    s3req.verb, s3req.bucket, s3req.key
                );
                return Err(self
                    .backend
                    .proxy(Box::new(auth), &self.dcache, &s3req, body)
                    .await);
            } else {
                debug!(
                    "Drain {:?}: journalling write to {}/{} because the key is already dirty",
                    s3req.verb, s3req.bucket, s3req.key
                );
            }
        }

        match self.get_journal_write(s3req.bucket, &s3req.key).await {
            Ok(v) => Ok((auth, body, v.0, v.1)),
            Err(remote_node_id) => Err(self.proxy_peer(remote_node_id, s3req, body).await),
        }
    }

    pub async fn get_journal_write(
        &'a self,
        bucket: &'a str,
        key: &'a str,
    ) -> Result<(ChunkJournal, ProjectionHandle<'a>), NodeId> {
        let mut journal = chunk_journal::get_for_thread();
        let handle = match self
            .key_map
            .begin_write(bucket, key, journal.get_id())
            .await
        {
            Ok(v) => v,
            Err(global_journal_id) => {
                // This is the case where the shard is telling us that state exists for this
                // key, but on a different node (the node ID is in the GlobalJournalId it returns)
                return Err(global_journal_id.node_id);
            }
        };

        let journal_id = handle.get_journal_id();
        if journal_id != journal.get_id() {
            // If the key was already bound to a different local journal, write to that one instead
            journal = self.journal_collection.get(journal_id);
        }
        Ok((journal, handle))
    }

    /// Redirect an incoming request to a different node.  Use this when
    /// the KeyMap indicates the request's key is already dirty on a remote journal.
    pub async fn proxy_peer(
        &self,
        node_id: NodeId,
        s3req: &request::S3Request<'_>,
        body: Body,
    ) -> Response<Body> {
        // TODO: keep a connection open between peers for forwarded requests?
        // OR: implement grpc reads to remote journals
        // OR: once we have a by-etag read cache, promote things to the cache
        // once someone reads them, and just have remote reads go the cache

        // FIXME: this could get confused if a client is addressing a particular server
        // node by a DNS name not in CAPRA_VHOST.  Maybe we just say that's forbidden?
        let peer_node = self.cluster_map.get_node(node_id);
        let peer_address = peer_node.get_s3_address();

        info!("Proxying to peer {} - {}", peer_node.get_hostname(), s3req);

        //let connector = PeerConnector { addr: peer_address };
        let connector = HttpConnector::new();

        // TODO: respect ServerOptions.local_address to ensure we're going over the right NIC
        //if local_address.len() > 0 {
        //    connector.set_local_address(Some(local_address.parse::<IpAddr>()?));
        //}
        let client = Client::builder().build::<_, Body>(connector);
        let head = s3req.head.clone();

        let uri = Uri::builder()
            .scheme(Scheme::HTTP)
            .path_and_query(head.uri.path_and_query().unwrap().clone())
            .authority(peer_address.to_string().as_str())
            .build()
            .unwrap();

        let mut rb = RequestBuilder::new()
            .method(head.method.clone())
            .uri(uri)
            .version(head.version.clone());
        *(rb.headers_mut().unwrap()) = head.headers.clone();

        // TODO do we care about extensions?
        //*(rb.extensions_mut().unwrap()) = head.extensions.clone();

        let req = rb.body(body).unwrap();
        return match client.request(req).await {
            Ok(r) => r,
            Err(e) => {
                error!(
                    "Failed to proxy to {}: {} ({})",
                    peer_node.get_hostname(),
                    e,
                    s3req
                );
                util::http::status_response(StatusCode::SERVICE_UNAVAILABLE)
            }
        };
    }
}

unsafe impl Send for HandlerState {}
unsafe impl Sync for HandlerState {}

#[derive(Clone)]
struct PeerConnector {
    addr: SocketAddr,
}

// This is given in the docs as a simpler way, but it doesn't compile.
// let connector = tower::service_fn(|_dst| async {
//     tokio::net::TcpStream::connect("127.0.0.1:1337").await
// });
impl tower::Service<Uri> for PeerConnector {
    type Response = tokio::net::TcpStream;
    type Error = std::io::Error;
    // We can't "name" an `async` generated future.
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + Send>>;

    fn poll_ready(&mut self, _: &mut task::Context<'_>) -> Poll<Result<(), Self::Error>> {
        // This connector is always ready, but others might not be.
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _: Uri) -> Self::Future {
        Box::pin(tokio::net::TcpStream::connect(self.addr))
    }
}
