use std::convert::Infallible;
use std::sync::Arc;

use hyper::StatusCode;
use hyper::{Body, Request, Response};
use log::*;

use crate::server::frontend::handler_state::HandlerState;
use crate::server::frontend::request::{S3Request, S3Verb};
use crate::util;
use crate::util::auth::Authorizer;

use super::delete::{handle_request_delete, handle_request_deleteobjects};
use super::get::handle_request_getorhead;
use super::list::handle_request_list;
use super::put;

pub async fn handle_request<'b, A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    req: Request<Body>,
) -> Result<Response<Body>, Infallible> {
    let (head, body) = req.into_parts();

    // Parse out bucket, key, content length etc
    let s3req = match S3Request::new(&head) {
        Ok(r) => r,
        Err(e) => {
            warn!("{}", e);
            return util::http::empty_response(StatusCode::BAD_REQUEST);
        }
    };

    if log_enabled!(log::Level::Trace) {
        for (key, val) in head.headers.iter() {
            let val_decoded = match val.to_str() {
                Ok(v) => v,
                Err(_) => "_unprintable_",
            };
            trace!("header {} {}", key, val_decoded);
        }
    }

    // FIXME: validate that 'bucket' exists on the backend, so that
    // we do not accept PUTs in nonexistent buckets.  Currently such
    // PUTs will result in stalling the journal when expiring.
    // This should be a cache of known buckets on the backend, and
    // read-through HEAD to the backend if we see a PUT for an unknown
    // bucket.  Because buckets can be deleted, the backend still
    // needs to handle that case during expiry, probably by recognising
    // that type of 404 and dropping the journal entry on the floor.

    // Ready to dispatch the request: construct authentication handler
    // (this validates auth credentials as far as it can at construction,
    //  but for requests with a body, there will be subsequent checks
    //  during/after decode of body)
    let auth = match A::new(&head, &handler_state.backend.get_secrets()) {
        Ok(new_auth) => new_auth,
        Err(auth_err) => {
            error!("{}", auth_err.get_reason());
            return util::http::empty_response(StatusCode::UNAUTHORIZED);
        }
    };

    let response = match s3req.verb {
        S3Verb::Other => {
            // Default action is to proxy requests to the backend (i.e. everything
            // but the straightforward PutObject/GetObject requests that we intercept)
            Ok(handler_state
                .backend
                .proxy(Box::new(auth), &handler_state.dcache, &s3req, body)
                .await)
        }
        S3Verb::HeadBucket => {
            // For the moment, we proxy all requests for bucket existence check.
            // This is something we might reasonably cache in future, to help out
            // client utilities that are in the habit of "helpfully" issuing a spurious
            // HeadBucket before a PUT or a GET (guys, it's not necessary)
            Ok(handler_state
                .backend
                .proxy(Box::new(auth), &handler_state.dcache, &s3req, body)
                .await)
        }
        S3Verb::GetObject | S3Verb::HeadObject => {
            // Return data from our journal if the key is dirty, else pass
            // through to the backend.
            handle_request_getorhead(handler_state, auth, &s3req).await
        }
        S3Verb::GetBucketLocation => {
            // The AWS docs are broken for this request type: they
            // first say the response should contain two nested tags,
            // then show an example with just a single toplevel tag.
            // The latter is defacto correct.
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_GetBucketLocation.html

            // TODO: Optimisation: Logically, this would always be a passthrough, but some clients may do this
            // gratuitiously (e.g. minio mc), so it is useful to serve it directly for the benefit
            // of anyone using us as a WAN-latency-reducing frontend.

            let response_body = format!(
                r#"<?xml version="1.0" encoding="UTF-8"?><LocationConstraint>{}</LocationConstraint>"#,
                handler_state.backend.get_region()
            );

            let resp = Response::builder()
                .status(StatusCode::OK)
                .header("Connection", "keep-alive")
                .header("Content-type", "text/xml");

            Ok(resp.body(Body::from(response_body)).unwrap())
        }
        S3Verb::ListObjectsV2 => {
            // Simple path: if there's nothing in the KeyMap that starts with the prefix
            // of the request, then simply proxy the request to the backend

            // General case: send a list request to the backend, then interleave the backend
            // results with any KeyMap entries within the requested prefix
            handle_request_list(handler_state, auth, &s3req).await
        }
        S3Verb::PutObject => put::handle_request_put(handler_state, auth, &s3req, body).await,
        S3Verb::DeleteObject => handle_request_delete(handler_state, auth, &s3req).await,
        S3Verb::DeleteObjects => {
            handle_request_deleteobjects(handler_state, auth, &s3req, body).await
        }
    };

    info!(
        "{} {} {}",
        response.as_ref().unwrap().status().as_u16(),
        s3req.content_length,
        s3req
    );
    response
}
