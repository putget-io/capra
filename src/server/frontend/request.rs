use std::env;
use std::fmt;

use chrono::{DateTime, Duration, Utc};
use http::request::Parts;
use http::{HeaderMap, Method};
use lazy_static::lazy_static;

use crate::server::s3_types::{BUCKET_LEN_MAX, BUCKET_LEN_MIN, KEY_LEN_MAX};
use crate::util::http::get_header_str;
use log::*;
use std::borrow::Cow;

lazy_static! {
    // TODO: move into a more generic configuration system, and support multiple values
    static ref CAPRA_VHOST: Option<String> = env::var("CAPRA_VHOST").ok();
}

#[derive(Debug, PartialEq, Eq)]
pub enum S3Verb {
    PutObject,
    HeadObject,
    HeadBucket,
    GetObject,
    ListObjectsV2,
    DeleteObject,
    DeleteObjects,
    GetBucketLocation,

    // The 'Other' verb is everything that we don't handle in the cache
    Other,
}

pub struct S3Request<'a> {
    pub started_at: DateTime<Utc>,
    // We take a reference to head rather than consuming it, because bucket/key
    // are references to content of the header and we want to be moveable.
    pub head: &'a Parts,

    // Empty string means request is not specific to a bucket
    pub bucket: &'a str,

    // Empty string means request is not specific to key
    pub key: Cow<'a, str>,

    pub content_length: u64,

    // If true, content_length is the decoded content size, not the literal
    // size of the HTTP request body.
    pub chunked_mode: bool,

    // Track whether this was a virtualhost or path-style request: needed
    // when proxying to know how to rewrite Host
    pub virtualhost: bool,

    // The S3 verb, inferred from the structure of the URL
    pub verb: S3Verb,

    // For the header "Host: bucket.foo.com:80", this is "foo.com"
    pub domain: &'a str,
}

fn is_percent_encoded(raw: &str) -> bool {
    let decoded = percent_encoding::percent_decode(raw.as_bytes());
    let a = decoded.count();
    a != raw.len()
}

impl<'a> S3Request<'a> {
    pub fn parse_content_length(
        headers: &HeaderMap,
        expect_length: bool,
    ) -> Result<(u64, bool), String> {
        let mut content_length: u64 = match headers.get("content-length") {
            // TODO error handling for ASCII validity and integer validity
            Some(v) => v.to_str().unwrap().parse().unwrap(),
            None => {
                if expect_length {
                    return Err("No content-length header".into());
                } else {
                    // Tolerate absence, report length as zero
                    0
                }
            }
        };

        let chunked_mode =
            if let Some(amz_decoded_content_length) = headers.get("x-amz-decoded-content-length") {
                // TODO error handling for ASCII validity and integer validity
                content_length = amz_decoded_content_length
                    .to_str()
                    .unwrap()
                    .parse()
                    .unwrap();
                true
            } else {
                false
            };

        Ok((content_length, chunked_mode))
    }

    pub fn new(head: &'a Parts) -> Result<Self, String> {
        let now = Utc::now();

        // Require a content-length for PUTs, for other request types
        // treat absense of the content-length as meaning 0
        let expect_length = head.method == Method::PUT;
        let (content_length, chunked_mode) =
            Self::parse_content_length(&head.headers, expect_length)?;

        let (bucket, key, domain, virtualhost) = match parse_bucket_key(&head) {
            Ok(v) => v,
            Err(e) => {
                // TODO: make sure there's more structured detail about which request
                // failed and/or pass the reason string into the response body
                return Err(format!("Malformed request: {}", e));
            }
        };

        let request_query = head.uri.query().unwrap_or("");

        let verb = if bucket.len() == 0 {
            // For requests that are not for a particular bucket, always proxy them
            // (we only cache/handle concrete data operations within a bucket).
            // (This is things like ListBuckets)
            S3Verb::Other
        } else if (head.method == Method::GET) && key.len() > 0 {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_GetObject.html
            S3Verb::GetObject
        } else if (head.method == Method::HEAD) && key.len() > 0 {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_HeadObject.html
            S3Verb::HeadObject
        } else if (head.method == Method::HEAD) && key.is_empty() {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_HeadBucket.html
            S3Verb::HeadBucket
        } else if head.method == Method::GET
            && key.len() == 0
            && request_query.starts_with("location")
        {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_GetBucketLocation.html
            S3Verb::GetBucketLocation
        } else if head.method == Method::GET && key.len() == 0 {
            let query_string = head.uri.query().unwrap_or("");
            if query_string.contains("list-type=2") {
                // https://docs.aws.amazon.com/AmazonS3/latest/API/API_ListObjectsV2.html
                S3Verb::ListObjectsV2
            } else {
                // Various other bucket-level GET operations, like
                // GET?versioning
                S3Verb::Other
            }
        } else if head.method == Method::PUT && key.len() > 0 && request_query.len() == 0 {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_PutObject.html
            // A PUT that that has a key but no query params is a PutObject.
            // (PUTs that do not have a key are non-object things like PutBucketPolicy)
            // (PUTs that /do/ have query params are more advanced things like PutObjectAcl)
            S3Verb::PutObject
        } else if head.method == Method::DELETE && key.len() > 0 && request_query.len() == 0 {
            // https://docs.aws.amazon.com/AmazonS3/latest/API/API_DeleteObject.html
            S3Verb::DeleteObject
        } else if head.method == Method::POST
            && key.is_empty()
            && request_query.starts_with("delete")
        {
            S3Verb::DeleteObjects
        } else {
            S3Verb::Other
        };

        // Internally, we handle keys as UTF-8.  If the key is URL-encoded,
        // then decode it.
        let key = if is_percent_encoded(key) {
            let str = percent_encoding::percent_decode(key.as_bytes());
            str.decode_utf8_lossy()
        } else {
            Cow::from(key)
        };

        Ok(Self {
            started_at: now,
            head,
            bucket,
            key,
            content_length,
            chunked_mode,
            virtualhost,
            verb,
            domain,
        })
    }

    fn elapsed(&self) -> Duration {
        Utc::now() - self.started_at
    }
}

impl fmt::Display for S3Request<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} {}us {:?} {}:{} {}{}",
            self.head.method,
            self.elapsed().num_microseconds().unwrap(),
            self.verb,
            self.bucket,
            self.key,
            if self.head.uri.query().is_some() {
                "?"
            } else {
                ""
            },
            self.head.uri.query().unwrap_or("")
        )
    }
}

// The minimal URL when using paths for bucket names is
// "/abc/" for a bucket 'a'.
const MIN_BUCKETPATH_LEN: usize = BUCKET_LEN_MIN + 2;

/// Legacy path-style (not virtualhost) way of addressing a bucket
/// (This is still sometimes used for on-prem object stores where it's
///  useful to enable addressing them by IP address)
fn parse_bucket_key_legacy(head: &Parts) -> Result<(&str, &str, &str, bool), String> {
    let request_path = head.uri.path();
    let host = head.headers.get("host").unwrap().to_str().unwrap();

    if request_path.len() < 5 || request_path.as_bytes()[0] != b'/' {
        return Err(format!("Malformed request path {}", request_path));
    }

    let slash_pos = request_path[1..].find('/');
    let bucket: &str = match slash_pos {
        Some(pos) => &request_path[1..pos + 1],
        None => {
            // No extra slash means this is a keyless path, like a ListObjects
            return Ok((&request_path[1..], &"", host, false));
        }
    };

    let key_offset = bucket.len() + 2;
    let key: &str = if key_offset >= request_path.len() {
        &""
    } else {
        &request_path[key_offset..]
    };

    if key.len() > KEY_LEN_MAX {
        return Err(format!("Key too long ({} bytes)", key.len()));
    }

    Ok((bucket, key, host, false))
}

/// Return true if a domain is within our whitelist of known endpoint addresses,
/// i.e. in <foo>.<domain> it is safe to interpret foo as a bucket name.
fn is_endpoint_domain(domain: &str) -> bool {
    if domain == "localhost" || domain == "localhost.localdomain" {
        true
    } else {
        match &*CAPRA_VHOST {
            Some(vhost) => domain == vhost,
            None => false,
        }
    }
}

/// This is NOT picky about the trailing part of the hostname: we only compare
/// the hostname to a whitelist when we need to disambiguate a request whose path
/// could either be path-style or virtualhost-style
fn parse_bucket_key(head: &Parts) -> Result<(&str, &str, &str, bool), String> {
    // If the hostname has a dot in it, and the part after the  dot
    // matches one of our endpoint hostnames...
    let mut host = match get_header_str(&head.headers, "host") {
        None => return Err("Missing or malformed host header".into()),
        Some(v) => v,
    };

    if let Some(i) = host.find(":") {
        // Host headers might have ports in, but we do not consider them
        // for the purposes of extracting bucket names
        host = &host[..i];
    }

    trace!("parse_bucket_key: {} {}", host, head.uri.path());

    // Special case, before trying to parse out a bucket+key, check if this
    // is a non-bucket request (like a "aws s3 ls s3://")
    if head.uri.path() == "/" {
        if is_endpoint_domain(host) {
            return Ok((&"", &"", host, false));
        }
    }

    let (before_dot, after_dot) = match host.find(".") {
        None => return parse_bucket_key_legacy(head),
        Some(v) => {
            if v == 0 || v == host.len() - 1 || v < BUCKET_LEN_MIN || v > BUCKET_LEN_MAX {
                return parse_bucket_key_legacy(head);
            } else {
                (&host[..v], &host[v + 1..])
            }
        }
    };

    let path_key = &head.uri.path()[1..];
    if path_key.len() < MIN_BUCKETPATH_LEN - 1 {
        // This cannot be a path-style request, no need to proceed to checking
        // after_dot.
        return Ok((before_dot, path_key, after_dot, true));
    }

    if is_endpoint_domain(after_dot) {
        // This is a bucket subdomain of a vhost known to us
        return Ok((before_dot, path_key, after_dot, true));
    } else {
        // The hostname is not known to us, so we cannot infer that the leading
        // part is a bucket name.  Fall back to legacy style parsing.
        return parse_bucket_key_legacy(head);
    }
}

#[cfg(test)]
mod test {
    use crate::server::frontend::request::parse_bucket_key;
    use http::Method;
    use hyper::http::request::Parts;
    use hyper::{Body, Request};

    #[test]
    fn test_parse_bucket_key() {
        fn build_head(uri: &str, host: &str) -> Parts {
            let req = Request::builder()
                .method(Method::GET)
                .uri(uri)
                .header("host", host)
                .body(Body::empty())
                .unwrap();
            let (head, _body) = req.into_parts();
            head
        }

        // A virtualhost-style request (e.g. GetObject)
        assert_eq!(
            parse_bucket_key(&build_head(
                "http://mybucket.localhost/mykey",
                "mybucket.localhost"
            )),
            Ok(("mybucket", "mykey", "localhost", true))
        );

        // A path-style request
        let req = Request::builder()
            .method(Method::GET)
            .uri("http://localhost/mybucket/mykey")
            .header("host", "localhost")
            .body(Body::empty())
            .unwrap();
        let (head, _body) = req.into_parts();
        assert_eq!(
            parse_bucket_key(&head),
            Ok(("mybucket", "mykey", "localhost", false))
        );

        // A bucket-less request (e.g. ListBuckets)
        let req = Request::builder()
            .method(Method::GET)
            .uri("http://localhost/")
            .header("host", "localhost")
            .body(Body::empty())
            .unwrap();
        let (head, _body) = req.into_parts();
        assert_eq!(parse_bucket_key(&head), Ok(("", "", "localhost", false)));

        // A key-less virtualhost request (e.g. ListObjects)
        let req = Request::builder()
            .method(Method::GET)
            .uri("http://localhost/")
            .header("host", "mybucket.localhost")
            .body(Body::empty())
            .unwrap();
        let (head, _body) = req.into_parts();
        assert_eq!(
            parse_bucket_key(&head),
            Ok(("mybucket", "", "localhost", true))
        );

        // A key-less legacy/path request (e.g. ListObjects)
        let req = Request::builder()
            .method(Method::GET)
            .uri("http://localhost/mybucket")
            .header("host", "localhost")
            .body(Body::empty())
            .unwrap();
        let (head, _body) = req.into_parts();
        assert_eq!(
            parse_bucket_key(&head),
            Ok(("mybucket", "", "localhost", false))
        );

        // Check that with an unknown hostname in Host, we do not incorrectly infer
        // that the first part of that hostname as as bucket name
        let req = Request::builder()
            .method(Method::GET)
            .uri("http://localhost/mybucket/mykey")
            .header("host", "myservice.mydomain")
            .body(Body::empty())
            .unwrap();
        let (head, _body) = req.into_parts();
        assert_eq!(
            parse_bucket_key(&head),
            Ok(("mybucket", "mykey", "myservice.mydomain", false))
        );
    }
}
