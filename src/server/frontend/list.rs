use core::convert::Infallible;
use std::cmp::Ordering;
use std::sync::Arc;

use chrono::SecondsFormat;
use http::response::Response;
use http::status::StatusCode;
use hyper::body::Body;
use log::*;
use partial_min_max::{max, min};

use crate::server::backend::BackendHandle;
use crate::server::frontend::handler_state::HandlerState;
use crate::server::frontend::request::S3Request;
use crate::server::key_map::{KeyMap, ListCursor, PersistentValue, PersistentValueOp};
use crate::server::s3_types::{
    ListObjectsV2Request, ListObjectsV2Response, Object, Prefix, DEFAULT_MAX_KEYS,
};
use crate::util;
use crate::util::auth::Authorizer;
use percent_encoding::percent_decode;
use std::borrow::Cow;

// ListObjectsV2 documentation doesn't specify a limit, but we shouldn't let clients
// do unbounded-sized requests.
const MAX_MAX_KEYS: usize = 0x4000;

fn pval_to_object(key: &str, persistent: PersistentValue) -> Option<Object> {
    let put_metadata = match &persistent.op {
        PersistentValueOp::Put(p) => p,
        PersistentValueOp::Delete(_) => return None,
    };

    let last_modified = put_metadata
        .last_modified
        .to_rfc3339_opts(SecondsFormat::Millis, true);

    Some(Object {
        key: key.to_string(),
        last_modified,
        e_tag: format!("{:x}", put_metadata.etag),
        size: put_metadata.content_length,
        // TODO: document our storage_class behaviour - anything dirty will always be reported as standard
        storage_class: "STANDARD".to_string(),
    })
}

// Sif we can get away with proxying this request instead of doing the work
// to intercept it and splice in dirty metadata
async fn check_interceptable(
    bucket: &str,
    prefix: &str,
    lorq: &ListObjectsV2Request,
    key_map: &KeyMap,
) -> (bool, Option<ListCursor>) {
    match &lorq.continuation_token {
        // If a continuation token is present, decide whether to intercept
        // based on whether it's one of ours
        Some(ct) => match ListCursor::decode(&ct) {
            Some(v) => (true, Some(v)),
            None => (false, None),
        },

        // No continuation token: decide whether to intercept based on whether
        // we have any dirty keys in the prefix
        None => (
            key_map.prefix_dirty(bucket, prefix).await,
            lorq.start_after
                .as_ref()
                .map(|sa| ListCursor::new(&sa, false)),
        ),
    }
}

#[derive(Clone)]
enum ListItem {
    // A backend object
    Object(Object),

    // A dirty PUT or DELETE
    Dirty((String, PersistentValue)),

    // A common prefix, may be dirty or from backend.
    Prefix(Prefix),
}

impl ListItem {
    fn get_key(&self) -> Cow<'_, str> {
        match self {
            Self::Object(key) => percent_decode(key.key.as_bytes()).decode_utf8_lossy(),
            Self::Prefix(prefix) => percent_decode(prefix.prefix.as_bytes()).decode_utf8_lossy(),
            Self::Dirty((key, _)) => Cow::from(key),
        }
    }
}

impl PartialOrd for ListItem {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        self.get_key().partial_cmp(&rhs.get_key())
    }
}

impl Ord for ListItem {
    fn cmp(&self, rhs: &Self) -> Ordering {
        self.get_key().cmp(&rhs.get_key())
    }
}

impl PartialEq for ListItem {
    fn eq(&self, rhs: &Self) -> bool {
        self.get_key().eq(&rhs.get_key())
    }
}

impl Eq for ListItem {}

async fn fetch_items_backend(
    items: &mut Vec<ListItem>,
    backend: &BackendHandle,
    bucket: &str,
    frontend_auth_id: &Option<&str>,
    lorq: &mut ListObjectsV2Request,
    cursor: &Option<ListCursor>,
    max_keys: usize,
) -> Result<bool, Response<Body>> {
    debug!("intercepted_list: loading backend listing");
    lorq.max_keys = Some(max_keys);
    lorq.start_after = cursor.as_ref().map(|c| c.cursor.clone());
    lorq.continuation_token = None;
    let lors = backend.list(bucket, *frontend_auth_id, &lorq).await?;

    debug!(
        "ListObjectsV2: Backend list gave us {} keys, {} prefixes, continuation {} -> {} (start_after {})",
        lors.contents.len(),
        lors.common_prefixes.len(),
        lors.continuation_token.as_ref().map(|s| s.as_str()).unwrap_or("None"),
        lors.next_continuation_token.as_ref().map(|s| s.as_str()).unwrap_or("None"),
        lors.start_after.as_ref().map(|s| s.as_str()).unwrap_or("None"),
    );
    debug!("Backend common prefixes: {:?}", lors.common_prefixes);

    items.extend(lors.contents.into_iter().map(|o| ListItem::Object(o)));
    items.extend(
        lors.common_prefixes
            .into_iter()
            .map(|p| ListItem::Prefix(p)),
    );
    // Descending sort so we can pop off next item
    items.sort_by(|a, b| b.cmp(&a));
    Ok(lors.next_continuation_token.is_none())
}

async fn fetch_items_frontend(
    items: &mut Vec<ListItem>,
    key_map: &KeyMap,
    bucket: &str,
    prefix: &str,
    delimiter: Option<&str>,
    cursor: Option<&ListCursor>,
    max_keys: usize,
) -> bool {
    debug!("intercepted_list: loading frontend listing");
    let (dirty_keys, dirty_prefixes) = key_map
        .list(bucket, cursor, prefix, delimiter, max_keys)
        .await;
    debug!(
        "ListObjectsV2: frontend list gave us {} keys, {} prefixes",
        dirty_keys.len(),
        dirty_prefixes.len()
    );

    items.extend(dirty_keys.into_iter().map(|v| ListItem::Dirty(v)));
    items.extend(
        dirty_prefixes
            .into_iter()
            .map(|p| ListItem::Prefix(Prefix { prefix: p })),
    );
    // Descending sort so we can pop off next item
    items.sort_by(|a, b| b.cmp(&a));

    items.is_empty()
}

/// When our dirty keys overlap the range being queried, we must splice
/// together backend list results with any dirty items in the KeyMap.
async fn intercepted_list(
    key_map: &KeyMap,
    backend: &BackendHandle,
    bucket: &str,
    frontend_auth_id: Option<&str>,
    mut lorq: ListObjectsV2Request,
    mut cursor: Option<ListCursor>,
) -> Result<ListObjectsV2Response, Response<Body>> {
    // S3 protocol requires echoing request fields back at end: stash them before
    // we start rewriting this request structure.
    let orig_continuation_token = lorq.continuation_token.clone();
    let orig_start_after = lorq.start_after.clone();

    // Clamp max_keys input to 1-MAX_MAX_KEYS, and default it to DEFAULT_MAX_KEYS
    let max_keys = max(
        1,
        min(lorq.max_keys.unwrap_or(DEFAULT_MAX_KEYS), MAX_MAX_KEYS),
    );
    let mut result_items: Vec<ListItem> = Vec::with_capacity(max_keys);
    let mut backend_exhausted = false;
    let mut backend_items: Vec<ListItem> = Vec::with_capacity(max_keys);
    let mut frontend_exhausted = false;
    let mut frontend_items: Vec<ListItem> = Vec::with_capacity(max_keys);

    while result_items.len() < max_keys
        && !(frontend_exhausted
            && backend_exhausted
            && frontend_items.is_empty()
            && backend_items.is_empty())
    {
        if backend_items.is_empty() && !backend_exhausted {
            backend_exhausted = match fetch_items_backend(
                &mut backend_items,
                backend,
                bucket,
                &frontend_auth_id,
                &mut lorq,
                &cursor,
                max_keys,
            )
            .await
            {
                Err(resp) => return Err(resp),
                Ok(exhausted) => exhausted,
            }
        }

        if frontend_items.is_empty() && !frontend_exhausted {
            let prefix: &str = lorq.prefix.as_ref().map(|s| s.as_str()).unwrap_or("");
            let delimiter = lorq.delimiter.as_ref().map(|s| s.as_str());
            frontend_exhausted = fetch_items_frontend(
                &mut frontend_items,
                key_map,
                bucket,
                prefix,
                delimiter,
                cursor.as_ref(),
                max_keys,
            )
            .await
        }

        debug!(
            "intercepted_list: {}/{} prefix={:?} cursor={:?} be={}({}) fe={}({})",
            result_items.len(),
            max_keys,
            lorq.prefix,
            cursor,
            backend_items.len(),
            backend_exhausted,
            frontend_items.len(),
            frontend_exhausted
        );

        // Take one item at a time, that way we go back round the frontend+backend population
        // process each time, and if we see an empty vector we know they're tapped out.
        let next_item =
            if (backend_items.is_empty() && backend_exhausted) && frontend_items.is_empty() {
                // Neither load successfully got any items: we're done.
                break;
            } else if frontend_items.is_empty() {
                backend_items.pop().unwrap()
            } else if backend_items.is_empty() {
                frontend_items.pop().unwrap()
            } else {
                // Frontend and backend items are available, compare them.
                let fe_item = frontend_items.last().unwrap();
                let be_item = backend_items.last().unwrap();

                if fe_item.get_key() == be_item.get_key() {
                    // If frontend and backend report on the same key, drop the backend's item.
                    let _drop = backend_items.pop().unwrap();
                    frontend_items.pop().unwrap()
                } else if fe_item < be_item {
                    frontend_items.pop().unwrap()
                } else {
                    backend_items.pop().unwrap()
                }
            };

        cursor = Some(match &next_item {
            ListItem::Prefix(p) => ListCursor::new(&p.prefix, true),
            _ => ListCursor::new(&next_item.get_key(), false),
        });

        // If this is a dirty delete, don't include it in the output (but do
        // advance cursor past it, as otherwise we would see it again when
        // next fetching frontend items)
        if let ListItem::Dirty((_, value)) = &next_item {
            if let PersistentValueOp::Delete(_) = value.op {
                continue;
            }
        }

        result_items.push(next_item);
    }

    // FIXME: in certain cases where available keys exactly line up with
    // max_keys, we'll return everything without recognising the _exhausted flags.
    // The code above can be finessed to recognise when we've seen all keys, without
    // sending another request to get zero results and recognise it that way.

    let (is_truncated, next_continuation_token) = if frontend_exhausted && backend_exhausted {
        (false, None)
    } else {
        (true, cursor.as_ref().map(|c| c.encode()))
    };

    info!("next_continuation_token: {:?}", next_continuation_token);

    let mut result = ListObjectsV2Response {
        // Elements that are simply echoed from the request
        name: bucket.to_string(),
        prefix: lorq.prefix.unwrap_or_else(|| String::new()),
        continuation_token: orig_continuation_token,
        start_after: orig_start_after,
        delimiter: lorq.delimiter.unwrap_or("".to_string()),

        key_count: result_items.len() as u64,
        max_keys: max_keys as u64,
        is_truncated,
        next_continuation_token,
        common_prefixes: vec![],
        contents: vec![],
    };

    // Map result_items into result.contents and result.common_prefixes
    for i in result_items {
        match i {
            ListItem::Dirty((key, value)) => {
                // Unwrap is safe because we earlier excluded deletes
                result.contents.push(pval_to_object(&key, value).unwrap())
            }
            ListItem::Prefix(prefix) => result.common_prefixes.push(prefix),
            ListItem::Object(o) => result.contents.push(o),
        }
    }

    Ok(result)
}

pub async fn handle_request_list<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    mut auth: A,
    s3req: &S3Request<'_>,
) -> Result<Response<Body>, Infallible> {
    if let Err(auth_err) = auth.finalize() {
        error!("401 {}", auth_err.get_reason());
        return util::http::empty_response(StatusCode::UNAUTHORIZED);
    } else {
        debug!("GET (ListObjects) authenticated OK");
    }

    let lorq = match ListObjectsV2Request::from_header(s3req.head) {
        Err(e) => {
            error!("Invalid ListObjectsV2 query params: {}", e);
            return util::http::empty_response(StatusCode::BAD_REQUEST);
        }
        Ok(lorq) => lorq,
    };

    let prefix: &str = match lorq.prefix.as_ref() {
        Some(s) => s,
        None => "",
    };

    let (do_intercept, cursor) =
        check_interceptable(s3req.bucket, prefix, &lorq, &handler_state.key_map).await;

    if !do_intercept {
        debug!(
            "ListObjectsV2 Proxying ListObjectsV2 on bucket={} prefix={}",
            s3req.bucket, prefix
        );
        return Ok(handler_state
            .backend
            .proxy(Box::new(auth), &handler_state.dcache, s3req, Body::empty())
            .await);
    } else {
        debug!(
            "ListObjectsV2 Intercepting on bucket={} prefix={}",
            s3req.bucket, prefix
        );
        let lors = match intercepted_list(
            &handler_state.key_map,
            &handler_state.backend,
            s3req.bucket,
            auth.get_secret_id(),
            lorq,
            cursor,
        )
        .await
        {
            Ok(lors) => lors,
            Err(resp) => return Ok(resp),
        };

        debug!(
            "ListObjectsV2 Returning {} keys, next_continuation_token='{}'",
            lors.contents.len(),
            lors.next_continuation_token
                .as_ref()
                .map(|s| s.as_str())
                .unwrap_or("")
        );

        // Serialize and issue response
        Ok(lors.into_response())
    }
}

#[cfg(test)]
mod test {
    use std::collections::HashSet;
    use std::io;

    use async_trait::async_trait;
    use chrono::Utc;
    use digest::Output;
    use futures::FutureExt;
    use http::Response;
    use hyper::Body;
    use log::*;
    use md5::Md5;
    use tokio;

    use capralib::auth::{Authorizer, SecretMap};

    use crate::server::backend::{
        Backend, BackendHandle, BackendPutStream, BackendPutStreamHandle, Error,
    };
    use crate::server::frontend::request::S3Request;
    use crate::server::journal::{StreamFooter, StreamHeader};
    use crate::server::key_map::test::key_map_for_test;
    use crate::server::key_map::KeyMap;
    use crate::server::s3_types::{ListObjectsV2Request, ListObjectsV2Response, Object, Prefix};
    use crate::server::test;
    use crate::server::types::{ObjectOp, Offset, OffsetList};

    use super::{check_interceptable, intercepted_list, DEFAULT_MAX_KEYS};
    use crate::server::dcache::DCache;
    use std::sync::Arc;

    struct MockBackendStream {}

    #[async_trait]
    impl BackendPutStream for MockBackendStream {
        async fn write(&mut self, _data: std::io::Result<&[u8]>) -> io::Result<()> {
            Ok(())
        }

        async fn finalize(self: Box<Self>) -> io::Result<()> {
            Ok(())
        }

        fn get_key(&self) -> (&str, &str) {
            ("", "")
        }
        fn get_content_length(&self) -> u64 {
            0
        }
    }

    #[derive(Clone)]
    struct MockBackend {
        secret_map: SecretMap,
        objects: Vec<Object>,
    }

    impl MockBackend {
        fn new(mut objects: Vec<Object>) -> BackendHandle {
            objects.sort_by(|a, b| a.key.cmp(&b.key));
            Arc::new(Self {
                secret_map: SecretMap::new(),
                objects,
            })
        }
    }

    #[async_trait]
    impl Backend for MockBackend {
        async fn put(&self, _stream_header: StreamHeader<'static>) -> BackendPutStreamHandle {
            Box::new(MockBackendStream {})
        }

        fn get_secrets(&self) -> &SecretMap {
            &self.secret_map
        }

        async fn remote_read(
            &self,
            _frontend_auth_id: Option<&str>,
            _bucket: &str,
            _method: http::Method,
            _path: &str,
            _query: &str,
        ) -> Response<Body> {
            unimplemented!();
        }

        async fn proxy(
            &self,
            _frontend_auth: Box<dyn Authorizer>,
            _dcache: &Arc<DCache>,
            _s3req: &S3Request<'_>,
            _body: Body,
        ) -> Response<Body> {
            Response::builder().body(Body::empty()).unwrap()
        }

        async fn delete(&self, _stream_header: &StreamHeader<'static>) -> Result<(), Error> {
            Ok(())
        }

        async fn list(
            &self,
            bucket: &str,
            _frontend_auth_id: Option<&str>,
            lorq: &ListObjectsV2Request,
        ) -> Result<ListObjectsV2Response, Response<Body>> {
            let max_keys = lorq.max_keys.unwrap_or(DEFAULT_MAX_KEYS);
            let mut resp = ListObjectsV2Response {
                name: bucket.to_string(),
                prefix: lorq.prefix.clone().unwrap_or_else(|| String::new()),
                continuation_token: lorq.continuation_token.clone(),
                start_after: lorq.start_after.clone(),
                delimiter: lorq.delimiter.clone().unwrap_or("".to_string()),
                key_count: 0,
                max_keys: max_keys as u64,
                is_truncated: false,
                next_continuation_token: None,
                common_prefixes: vec![],
                contents: vec![],
            };

            // We never pass a continuation token to the backend unless proxying: this
            // mocked version is for testing non-proxied paths like the intercepted list.
            assert!(lorq.continuation_token.is_none());

            let mut common_prefixes = HashSet::<String>::new();
            let mut is_truncated = true;

            let prefix = lorq.prefix.as_ref().map(|s| s.as_str()).unwrap_or("");
            let start_after = lorq.start_after.as_ref().map(|s| s.as_str()).unwrap_or("");

            // For a mock class, we don't care about efficiently, just do a linear pass through all
            let mut i = self.objects.iter();
            while resp.contents.len() + common_prefixes.len() < max_keys {
                let object = match i.next() {
                    None => {
                        debug!("mock list: ran out of objects");
                        is_truncated = false;
                        break;
                    }
                    Some(o) => o,
                };

                if !object.key.starts_with(prefix) {
                    continue;
                } else if object.key.as_str() <= start_after {
                    continue;
                }

                if let Some(delimiter) = &lorq.delimiter {
                    if object.key.len() > prefix.len() {
                        if let Some(i) = object.key[prefix.len()..].find(delimiter) {
                            // Finding the first delimiter /after/ the prefix part of the key,
                            // then the candidate common_prefix match is the part from the start
                            // of the key up to+including that delimiter that we found.
                            let my_prefix = &object.key[0..prefix.len() + i + 1];

                            let new = common_prefixes.insert(my_prefix.to_string());
                            if new {
                                debug!("mock list: including prefix: {}", my_prefix);
                            }

                            continue;
                        }
                    }
                }

                debug!("mock list: including object: {}", object.key);
                resp.contents.push(object.clone());
            }

            resp.key_count = (resp.common_prefixes.len() + resp.contents.len()) as u64;
            resp.common_prefixes = common_prefixes
                .into_iter()
                .map(|s| Prefix { prefix: s })
                .collect();
            resp.is_truncated = is_truncated;

            if is_truncated {
                // Our intercepted list should never look at the contained value,
                // just the Some-ness.
                resp.next_continuation_token = Some("OPAQUE".to_string());
            }

            Ok(resp)
        }
    }

    #[tokio::test]
    async fn test_intercepted_list() {
        const TEST_BUCKET: &str = "bucket";

        test::global_init();

        fn gen_objects(keys: Vec<&str>) -> Vec<Object> {
            keys.iter()
                .map(|k| Object {
                    key: k.to_string(),
                    last_modified: "".into(),
                    e_tag: "".into(),
                    size: 1,
                    storage_class: "".into(),
                })
                .collect()
        }

        async fn gen_key_map(keys: Vec<(&str, bool)>) -> KeyMap {
            let key_map = key_map_for_test().await;

            let now = Utc::now();
            for (k, is_put) in keys {
                let offset_list = OffsetList::from(vec![Offset {
                    location: 0,
                    size: 4096,
                }]);
                let (op, content_length) = if is_put {
                    (ObjectOp::Put, 1024)
                } else {
                    (ObjectOp::Delete, 0)
                };
                let stream_header =
                    StreamHeader::new(op, None, TEST_BUCKET, k, content_length, now);
                let stream_footer = StreamFooter::new(Output::<Md5>::from_slice(&[0; 16]));
                key_map
                    .replay(0, offset_list, stream_header, stream_footer)
                    .now_or_never();
            }

            key_map
        }

        // Act like a client sending ListObjectsV2 requests until the server reports is_truncated=false
        async fn pump_list(
            backend: &BackendHandle,
            key_map: &KeyMap,
            prefix: Option<&str>,
            delimiter: Option<&str>,
            start_after: Option<&str>,
            max_keys: Option<usize>,
        ) -> (Vec<String>, Vec<String>) {
            let mut contents = Vec::new();
            let mut common_prefixes = Vec::new();

            // Counter to catch stuck loops
            let mut i: usize = 0;

            let mut continuation_token = None;
            loop {
                i += 1;
                assert!(i < 1000);

                let req = ListObjectsV2Request {
                    prefix: prefix.map(|s| s.to_string()),
                    delimiter: delimiter.map(|s| s.to_string()),
                    start_after: if continuation_token.is_some() {
                        None
                    } else {
                        start_after.map(|s| s.to_string())
                    },
                    continuation_token: continuation_token.clone(),
                    encoding_type: Some("url".to_string()),
                    fetch_owner: false,
                    max_keys,
                };

                let (do_intercept, cursor) =
                    check_interceptable(TEST_BUCKET, prefix.unwrap_or(""), &req, key_map).await;
                assert!(do_intercept); // This harness is specifically for testing intercepted listings

                let lors = intercepted_list(key_map, backend, TEST_BUCKET, None, req, cursor)
                    .await
                    .unwrap();
                contents.extend(lors.contents.into_iter());
                common_prefixes.extend(lors.common_prefixes.into_iter());
                continuation_token = lors.next_continuation_token;
                if continuation_token.is_none() {
                    break;
                }
            }

            (
                contents.into_iter().map(|o| o.key).collect(),
                common_prefixes.into_iter().map(|p| p.prefix).collect(),
            )
        }

        let mut backend_keys = Vec::<String>::new();
        // Intentionally janky counts to avoid aliasing with max_keys
        let a_subdir1_keys: Vec<String> = (1..73)
            .map(|i| format!("/a/sub(dir).1/file{}", i))
            .collect();
        let a_subdir2_keys: Vec<String> = (1..29)
            .map(|i| format!("/a/sub(dir).2/file{}", i))
            .collect();
        let a_keys: Vec<String> = (1..37).map(|i| format!("/a/topfile{}", i)).collect();

        backend_keys.extend(a_subdir1_keys.iter().map(|s| s.clone()));
        backend_keys.extend(a_subdir2_keys.iter().map(|s| s.clone()));
        backend_keys.extend(a_keys.iter().map(|s| s.clone()));

        let mixed_backend = MockBackend::new(gen_objects(
            backend_keys.iter().map(|s| s.as_str()).collect(),
        ));

        // One deletion at top level
        let onedel = gen_key_map(vec![("/a/topfile2", false)]).await;
        let (contents, common_prefixes) = pump_list(
            &mixed_backend,
            &onedel,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(
            common_prefixes,
            vec!["/a/sub(dir).1/".to_string(), "/a/sub(dir).2/".to_string()]
        );
        assert_eq!(contents.len(), a_keys.len() - 1);

        // Deletions that shadow all backend keys
        let deleteall =
            gen_key_map(backend_keys.iter().map(|k| (k.as_str(), false)).collect()).await;
        let (contents, common_prefixes) = pump_list(
            &mixed_backend,
            &deleteall,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        // Our listing logic can't know that our deletes entirely eliminate a prefix
        // -- it will always act as if the prefix still exists.
        assert_eq!(
            common_prefixes,
            vec!["/a/sub(dir).1/".to_string(), "/a/sub(dir).2/".to_string()]
        );
        assert_eq!(contents.len(), 0);

        // Again, but a flat listing (no delimiter)
        let (contents, common_prefixes) = pump_list(
            &mixed_backend,
            &deleteall,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(
            common_prefixes,
            vec!["/a/sub(dir).1/".to_string(), "/a/sub(dir).2/".to_string()]
        );
        assert_eq!(contents.len(), 0);

        // Insertions: above the prefix, in the prefix, in a subdir
        let insertions = gen_key_map(vec![
            ("aaarootkey", true),
            ("zzzrootkey", true),
            ("/a/aaatopnewkey", true),
            ("/a/zzztopnewkey", true),
            ("/a/aaasubdirnew/newkey", true),
            ("/a/sub(dir).1b/newkey", true),
            ("/a/zzzsubdirnew/newkey", true),
        ])
        .await;
        let (contents, common_prefixes) = pump_list(
            &mixed_backend,
            &insertions,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(
            common_prefixes,
            vec![
                "/a/aaasubdirnew/".to_string(),
                "/a/sub(dir).1/".to_string(),
                "/a/sub(dir).1b/".to_string(),
                "/a/sub(dir).2/".to_string(),
                "/a/zzzsubdirnew/".to_string()
            ]
        );
        assert_eq!(contents.len(), a_keys.len() + 2);

        // Again, but no prefix or delimiter
        let (contents, common_prefixes) =
            pump_list(&mixed_backend, &insertions, None, None, None, Some(10)).await;
        assert_eq!(common_prefixes.len(), 0);
        assert_eq!(contents.len(), backend_keys.len() + insertions.len());

        // Nothing on the backend, bunch of insertions on the frontend
        let empty_backend = MockBackend::new(Vec::new());
        let insertions =
            gen_key_map(backend_keys.iter().map(|k| (k.as_str(), true)).collect()).await;
        let (contents, common_prefixes) = pump_list(
            &empty_backend,
            &insertions,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(
            common_prefixes,
            vec!["/a/sub(dir).1/".to_string(), "/a/sub(dir).2/".to_string(),]
        );
        assert_eq!(contents.len(), a_keys.len());

        // Again, but no prefix or delimiter
        let (contents, common_prefixes) =
            pump_list(&empty_backend, &insertions, None, None, None, Some(10)).await;
        assert_eq!(common_prefixes.len(), 0);
        assert_eq!(contents.len(), insertions.len());

        // A backend listing where the frontend has deletions for the first half of the keys only
        // (tests case where the first paginated pass will wipe out all backend keys, but it's
        //  important that it keeps trying to find the rest of thge keys)
        let flat_backend_keys: Vec<String> = (1..73).map(|i| format!("/a/file{}", i)).collect();
        let flat_backend = MockBackend::new(gen_objects(
            flat_backend_keys.iter().map(|s| s.as_str()).collect(),
        ));
        let deletions = gen_key_map(
            flat_backend_keys[0..23]
                .iter()
                .map(|s| (s.as_str(), false))
                .collect(),
        )
        .await;

        let (contents, common_prefixes) = pump_list(
            &flat_backend,
            &deletions,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(common_prefixes.len(), 0);
        assert_eq!(contents.len(), flat_backend_keys.len() - deletions.len());

        // Frontend deletions for every other key
        let flat_backend_keys: Vec<String> = (1..73).map(|i| format!("/a/file{}", i)).collect();
        let flat_backend = MockBackend::new(gen_objects(
            flat_backend_keys.iter().map(|s| s.as_str()).collect(),
        ));
        let deletions = gen_key_map(
            (0..flat_backend_keys.len())
                .step_by(2)
                .map(|i| (flat_backend_keys[i].as_str(), false))
                .collect(),
        )
        .await;

        let (contents, common_prefixes) = pump_list(
            &flat_backend,
            &deletions,
            Some("/a/"),
            Some("/"),
            None,
            Some(10),
        )
        .await;
        assert_eq!(common_prefixes.len(), 0);
        assert_eq!(contents.len(), flat_backend_keys.len() - deletions.len());

        // Frontend PUTs + DELETEs but nothing on the backend
        let put_delete_dirty = gen_key_map(vec![
            ("key1", true),
            ("key2", true),
            ("key1", false),
            ("key2", false),
        ])
        .await;
        let (contents, common_prefixes) = pump_list(
            &empty_backend,
            &put_delete_dirty,
            None,
            None,
            None,
            Some(10),
        )
        .await;
        assert_eq!(common_prefixes.len(), 0);
        assert_eq!(contents.len(), 0);
    }
}
