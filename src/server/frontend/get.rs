use core::convert::Infallible;
use std::sync::Arc;

use http::method::Method;
use http::response::Response;
use http::status::StatusCode;
use http::version::Version;
use hyper::body::{Body, Bytes};
use log::*;

use crate::server::frontend::handler_state::HandlerState;
use crate::server::key_map::PersistentValueOp;
use crate::util;
use crate::util::auth::Authorizer;

use super::request::S3Request;
use crate::server::dcache::Entry;
use crate::util::http::{empty_response, get_etag};
use partial_min_max::min;
use std::option::Option::None;
use std::pin::Pin;
use std::task::{Context, Poll};

// e.g. Last-Modified: Mon, 17 Dec 2012 02:14:10 GMT
const SPACED_DATETIME_FORMAT: &str = "%a, %d %b %Y %H:%M:%S GMT";

pub async fn handle_request_getorhead<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    mut auth: A,
    s3req: &S3Request<'_>,
) -> Result<Response<Body>, Infallible> {
    if let Err(auth_err) = auth.finalize() {
        error!("401 {} {}", auth_err.get_reason(), s3req);
        return util::http::empty_response(StatusCode::UNAUTHORIZED);
    } else {
        debug!("GET authenticated OK");
    }

    let (global_journal_id, persistent_value) =
        match handler_state.key_map.get(s3req.bucket, &s3req.key).await {
            Some(v) => v,
            None => {
                // Key is clean: pass through the GET to the backend
                debug!("Key {} is clean (not found in KeyMap)", s3req.key);
                return handle_getorhead_clean(handler_state, auth, s3req).await;
            }
        };

    // If the key is dirty and a DELETE was most recent op, return 404.
    if let PersistentValueOp::Delete(_) = persistent_value.op {
        info!(
            "Returning 404 for GET on dirty delete {}/{}",
            s3req.bucket, s3req.key
        );
        return empty_response(StatusCode::NOT_FOUND);
    }

    // The key is dirty and a PUT was the most recent op: we must pass the request
    // to the home node for the journal where the PUT data is to serve it.
    let journal_id = if global_journal_id.node_id == handler_state.cluster_map.get_id() {
        global_journal_id.journal_id
    } else {
        return Ok(handler_state
            .proxy_peer(global_journal_id.node_id, s3req, Body::empty())
            .await);
    };

    let journal = handler_state.journal_collection.get(journal_id);
    let offset_list = persistent_value.chunks;
    let persistent_put = match persistent_value.op {
        PersistentValueOp::Put(v) => v,
        _ => panic!("Unexpected PersistentValueOp"),
    };

    let mut resp = Response::builder().status(StatusCode::OK);

    if s3req.head.version == Version::HTTP_11 {
        resp = resp.header("Connection", "keep-alive");
    }

    // TODO: respect object metadata content type
    //       (this is actually kind of awkward, if we had a PUT
    //        overwriting an object, the metadata may only exist
    //        on the backend, while the most recent data only exists
    //        in our journal.  Doable but slow.)
    // TODO: respect response-content-type header if set on the request
    resp = resp.header("Content-type", "application/octet-stream");

    // TODO: handle 'Range' header
    // TODO: for any If-* headers (like If-Modified-Since), divert
    // the GET to the backend

    resp = resp
        .header(
            "last-modified",
            &persistent_put
                .last_modified
                .format(SPACED_DATETIME_FORMAT)
                .to_string(),
        )
        .header(
            "content-length",
            format!("{}", persistent_put.content_length),
        )
        .header(
            "date",
            &s3req.started_at.format(SPACED_DATETIME_FORMAT).to_string(),
        )
        .header("ETag", format!("\"{:x}\"", persistent_put.etag));

    if s3req.head.method == Method::HEAD {
        // A HEAD is just a GET without the body
        // https://docs.aws.amazon.com/AmazonS3/latest/API/API_HeadObject.html
        Ok(resp.body(Body::empty()).unwrap())
    } else {
        let read_stream = match journal.read(offset_list).await {
            Some(s) => s,
            None => {
                // The region is the journal is unavailable (has already expired, our KeyMap
                // read raced with journal expiry).  We have written this object back
                // to the backend at this point, so handle the GET via the clean path.
                debug!(
                    "Key {} found in KeyMap but offset already expired",
                    s3req.key
                );
                return handle_getorhead_clean(handler_state, auth, s3req).await;
            }
        };

        // Consider... might it be better to try and run this I/O part
        // on the home CPU for the journal?  (our execution context
        // at this point in request handling could be on any worker/cpu)
        Ok(resp.body(Body::wrap_stream(read_stream)).unwrap())
    }
}

struct DCacheEntryStream {
    next: usize,
    entry: Arc<Entry>,
}

impl futures::Stream for DCacheEntryStream {
    type Item = Result<Bytes, Box<dyn std::error::Error + Send + Sync>>;

    fn poll_next(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let r = self
            .entry
            .get_chunks()
            .get(self.next)
            .map(|i| Ok(i.clone()));
        self.next += 1;
        Poll::Ready(r)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let n = self.entry.get_content_length();

        // Munge it down to usize range -- for some reason the Stream interface assumes
        // we'll never need to hint beyond the usize limit.
        let n = min(n, usize::MAX as u64) as usize;

        (n, Some(n))
    }
}

async fn handle_getorhead_clean<A: Authorizer + 'static>(
    handler_state: Arc<HandlerState>,
    auth: A,
    s3req: &S3Request<'_>,
) -> Result<Response<Body>, Infallible> {
    let head_response = handler_state
        .backend
        .remote_read(
            auth.get_secret_id(),
            s3req.bucket,
            hyper::Method::HEAD,
            &format!("/{}", s3req.key),
            "",
        )
        .await;

    // TODO: maybe, if it's cheaper, just always issue a GET, and then
    // only read from the body if we don't have it in cache, else if we
    // do have it in cache, then replace the response's body with our cache
    // stream.

    // Read the ETag out of the HEAD response, and see if we can use it
    // to find a cached copy of the content.
    if let Some(etag) = get_etag(&head_response) {
        let cached = handler_state.dcache.get(&etag);
        if let Some(entry) = cached {
            debug!(
                "Cache HIT  fetching {}/{} (etag {:?})",
                s3req.bucket, s3req.key, etag
            );

            let (head, _) = head_response.into_parts();
            let body = Body::wrap_stream(DCacheEntryStream { next: 0, entry });

            return Ok(http::Response::from_parts(head, body));
        } else {
            debug!(
                "Cache MISS fetching {}/{} (etag {:?})",
                s3req.bucket, s3req.key, etag
            );
        }
    } else {
        // FIXME: cleaner logging in the case of multipart objects, which have ETags
        // that aren't plain md5sums, and will not parse cleanly into our internal ETag
        // (currently we'll simply skip caching them but also log a bunch of warnings)
        warn!(
            "Missing or malformed ETag header on object {}/{}",
            s3req.bucket, s3req.key
        );
    }

    Ok(handler_state
        .backend
        .proxy(Box::new(auth), &handler_state.dcache, s3req, Body::empty())
        .await)
}
