use std::sync::Arc;
use std::time::Duration;

use tonic::{Request, Response};

use crate::server::cluster_map::{ClusterMap, Node};
use crate::server::options::ServerOptions;
use crate::server::process::Process;
use crate::server::supervisor::{lookup_peer, NodeEpoch, NodeState};
use crate::server::{io_engine, supervisor};

use super::api;

use log::*;
use tokio::sync::MutexGuard;

struct FollowerState {
    epoch: NodeEpoch,
    process: Option<Process>,
    state: NodeState,
    next_state: NodeState,
    ready_tx: Option<tokio::sync::mpsc::UnboundedSender<Ready>>,
}

impl FollowerState {
    fn reset(&mut self) {
        if let Some(ready_tx) = self.ready_tx.as_mut() {
            let r = ready_tx.send(Ready::Reset);
            if r.is_err() {
                // the ready_rx gets dropped while main_loop is resetting, so
                // it's fine to drop this call to reset().
                warn!("Dropping reset() call, ready_rx is closed")
            }
        }
    }
}

pub struct FollowerSupervisor {
    options: ServerOptions,
    cluster_map: Arc<ClusterMap>,
    io_builder: io_engine::Builder,

    // State and reset hooks for clustered mode
    state: tokio::sync::Mutex<FollowerState>,
}

#[derive(Debug)]
enum Ready {
    State(ReadyState),
    Reset,
}

#[derive(Debug)]
struct ReadyState {
    state: NodeState,
    next_state: NodeState,
}

impl FollowerSupervisor {
    pub fn new(
        options: ServerOptions,
        cluster_map: Arc<ClusterMap>,
        io_builder: io_engine::Builder,
    ) -> Arc<Self> {
        let state = FollowerState {
            epoch: supervisor::gen_epoch(),
            process: None,
            state: NodeState::Initial,
            next_state: NodeState::Start,
            ready_tx: None,
        };

        Arc::new(Self {
            options,
            cluster_map,
            io_builder,
            state: tokio::sync::Mutex::new(state),
        })
    }

    pub fn block(this: Arc<Self>) -> Result<(), ()> {
        // Start a runtime to run our grpc server
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(1)
            .build()
            .unwrap();

        let follower_server = FollowerServer {
            supervisor: this.clone(),
        };

        let bg_this = this.clone();
        rt.spawn(async move {
            tonic::transport::Server::builder()
                .add_service(api::follower_server::FollowerServer::new(follower_server))
                .serve(bg_this.cluster_map.my_node().get_supervisor_address())
                .await
                .expect("Error from supervisor RPC server")
        });

        rt.block_on(this.main_loop());

        Ok(())
    }

    async fn main_loop(&self) {
        let leader_node = self.cluster_map.leader_node();
        let my_node = self.cluster_map.my_node();

        // At first startup (but not on reset), set a ReportState to the leader.  This
        // is what triggers the leader to handle followers restarting unprompted.
        let mut client = loop {
            // TODO smart backoff
            tokio::time::sleep(Duration::from_millis(100)).await;

            match api::leader_client::LeaderClient::connect(leader_node.get_supervisor_url()).await
            {
                Ok(c) => {
                    break c;
                }
                Err(e) => {
                    info!("Cannot connect to leader {} yet ({})", leader_node, e);
                    continue;
                }
            }
        };

        info!(
            "Constructed client for leader={}, sending ReportState",
            leader_node
        );

        loop {
            let my_epoch = supervisor::gen_epoch();
            let (ready_tx, mut ready_rx) = tokio::sync::mpsc::unbounded_channel();

            // It is safe that we did not have a ready_tx published up to this point,
            // because if we had received a ResetState (and dropped because of no ready_tx),
            // all it would have done is send us to the top of the loop we're about the enter.
            {
                let mut state = self.state.lock().await;
                state.ready_tx = Some(ready_tx);
                state.state = NodeState::Initial;
                state.next_state = NodeState::Start;
            }

            // Send a ReportState(INITIAL) to the leader.
            match client
                .report_state(api::ReportStateRequest {
                    from: Some(supervisor::node_to_proto(my_epoch, my_node)),
                    current: api::NodeState::Initial as i32,
                    ready_for: api::NodeState::Start as i32,
                })
                .await
            {
                Ok(r) => r.into_inner(),
                Err(e) => {
                    info!(
                        "Error reporting state to leader {}: {}, resetting",
                        leader_node, e
                    );
                    continue;
                }
            };

            // Service the ready channel - this will prompt us to either send
            // NotifyState to the leader when we're ready for our next step,
            // or to reset if we see a reset.

            while let Some(rx) = ready_rx.recv().await {
                match rx {
                    Ready::Reset => {
                        warn!("Follower Resetting");
                        break;
                    }
                    Ready::State(s) => {
                        let r = client
                            .report_state(api::ReportStateRequest {
                                from: Some(supervisor::node_to_proto(my_epoch, my_node)),
                                current: s.state as i32,
                                ready_for: s.next_state as i32,
                            })
                            .await;

                        if r.is_err() {
                            // FIXME: retry/sleep when this happens: tolerate
                            // temporary network drops to leader without resetting.

                            // Treat comms errors to leader as resets.
                            break;
                        }
                    }
                }
            }

            // Resetting, clear down ready_tx hook and Process
            {
                let mut state = self.state.lock().await;
                state.ready_tx = None;

                let process = state.process.take();
                if let Some(mut process) = process {
                    info!("Dropping previous Process");
                    // Tokio runtimes cannot be dropped in an async context, so spawn
                    // a non-async task to do it.
                    tokio::task::spawn_blocking(move || {
                        // We are the only reference to this, because of the Mutex around
                        // the Arc, combined with the way we don't clone it anywhere else.
                        process.shutdown();
                    })
                    .await
                    .expect("Failed to shut down Process!");
                }
            }
        }
    }
}

struct FollowerServer {
    supervisor: Arc<FollowerSupervisor>,
}

fn validate_leader(cluster_map: &ClusterMap, peer: &Node) -> Result<(), tonic::Status> {
    if peer != cluster_map.leader_node() {
        Err(tonic::Status::out_of_range(
            "Sending node is not this node's leader",
        ))
    } else {
        Ok(())
    }
}

impl FollowerServer {
    // Initial->Start state transition
    // (go from no runtime, to starting runtime)
    fn initial_start(&self, state: &mut MutexGuard<FollowerState>) -> Result<(), tonic::Status> {
        // Hold state lock across Process init, to avoid the grpc side
        // trying to process requests (e.g. reset) while we're in an
        // in-between state.
        info!(
            "Starting Process (epoch={} on {})",
            state.epoch,
            self.supervisor.cluster_map.my_node()
        );

        match Process::start(
            self.supervisor.cluster_map.clone(),
            self.supervisor.io_builder.clone(),
            self.supervisor.options.clone(),
        ) {
            Ok(p) => {
                state.process = Some(p);

                // Announce that we are ready to replay
                if let Some(ready_tx) = state.ready_tx.as_ref() {
                    let _r = ready_tx.send(Ready::State(ReadyState {
                        state: NodeState::Start,
                        next_state: NodeState::Replay,
                    }));
                }
                Ok(())
            }
            Err(e) => {
                error!("Failed to start: {}", e);
                Err(tonic::Status::internal(format!(
                    "Failed to start Process: {}",
                    e
                )))
            }
        }
    }

    // Start->Replay state transition
    // (given runtime is started here and on all peers, start journal replay)
    async fn start_replay(&self, state: &mut MutexGuard<'_, FollowerState>) -> Result<(), ()> {
        let ready_tx = state.ready_tx.as_ref().unwrap().clone();

        state.state = NodeState::Replay;

        // Unwrap is safe because if we're in the Start->Replay transition, we
        // have definitely already populated this at the start of main_loop
        let process = state.process.as_mut().unwrap();
        process.start_replay(|| async move {
            let _r = ready_tx.send(Ready::State(ReadyState {
                state: NodeState::Replay,
                next_state: NodeState::Active,
            }));
            // Don't care about result, if ready_rx is closed we're resetting.
        });

        Ok(())
    }

    // Initial->Start state transition
    // (given that replay is complete here and on all peers, start our client-facing service)
    fn replay_active(&self, state: &mut MutexGuard<FollowerState>) -> Result<(), tonic::Status> {
        let process = state.process.as_mut().unwrap();
        process.start_server();

        let ready_tx = state.ready_tx.as_ref().unwrap().clone();
        ready_tx
            .send(Ready::State(ReadyState {
                state: NodeState::Active,
                next_state: NodeState::Active,
            }))
            .expect("ready_rx closed?");

        Ok(())
    }
}

#[tonic::async_trait]
impl api::follower_server::Follower for FollowerServer {
    async fn reset(
        &self,
        request: Request<api::ResetRequest>,
    ) -> Result<Response<api::ResetResponse>, tonic::Status> {
        let request = request.into_inner();

        let (from_node, from_epoch) = lookup_peer(&*self.supervisor.cluster_map, request.from)?;
        validate_leader(&*self.supervisor.cluster_map, from_node)?;

        let response = {
            let mut state = self.supervisor.state.lock().await;

            warn!(
                "Resetting for ResetRequest from {} epoch={}",
                from_node, from_epoch
            );
            state.reset();

            api::ResetResponse {
                from: Some(supervisor::node_to_proto(
                    state.epoch,
                    self.supervisor.cluster_map.my_node(),
                )),
                status: api::Status::Ok as i32,
            }
        };

        Ok(Response::new(response))
    }

    async fn set_state(
        &self,
        request: Request<api::SetStateRequest>,
    ) -> Result<Response<api::SetStateResponse>, tonic::Status> {
        let request = request.into_inner();
        let (from_node, _from_epoch) = lookup_peer(&*self.supervisor.cluster_map, request.from)?;
        validate_leader(&*self.supervisor.cluster_map, from_node)?;
        let new_state = decode_enum!(NodeState, request.new);

        let response = {
            let mut state = self.supervisor.state.lock().await;

            if state.state != new_state {
                match (state.state, new_state) {
                    (NodeState::Initial, NodeState::Start) => self.initial_start(&mut state)?,
                    (NodeState::Start, NodeState::Replay) => self
                        .start_replay(&mut state)
                        .await
                        .expect("FIXME handle error"),
                    (NodeState::Replay, NodeState::Active) => self.replay_active(&mut state)?,
                    _ => {
                        error!(
                            "Unexpected SetState {:?} (am {:?}), resetting",
                            new_state, state.state
                        );
                        state.reset();
                        return Err(tonic::Status::invalid_argument("Unexpected state"));
                    }
                }

                state.state = new_state;
            } else {
                // Tolerate this for resends/retries
                info!("Redundant SetState {:?} (already in that state)", new_state);
            }

            api::SetStateResponse {
                from: Some(supervisor::node_to_proto(
                    state.epoch,
                    self.supervisor.cluster_map.my_node(),
                )),
                status: api::Status::Ok as i32,
            }
        };

        Ok(Response::new(response))
    }
}
