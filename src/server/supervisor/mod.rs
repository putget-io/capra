use rand::{thread_rng, Rng};

use crate::server::cluster_map::{ClusterMap, Node};

mod follower;
mod leader;
mod local;

pub use follower::FollowerSupervisor;
pub use leader::LeaderSupervisor;
pub use local::LocalSupervisor;
use tonic::Code;

pub mod api {
    tonic::include_proto!("supervisor");
}

/// A minimal clustering system designed to support safe, robust operation
/// but not dynamic runtime changes or operating with any cluster members offline.  The purpose
/// of this code is to provide an "all or none" start/restart behaviour: if one node restarts,
/// all nodes in the cluster must also restart before normal operation resumes.  This gives very
/// simple replay logic at startup.
///
/// This kind of simple system is usuable (and perhaps desirable) in production where:
///  - zero-downtime restarts aren't required (i.e. you have a scheduled maintenance window
///    for installing updates, and aren't trying to do it during production operations)
///  - zero downtime adding or removing nodes isn't required (as above, this in practice just
///    means you must have a maintenance window for doing such things)
///  - manual intervention on node failures is acceptable (i.e. explicitly removing a node
///    on hardware failure)
///
/// That set of constraints is found, for example, on a bare metal cluster used for
/// back-office/research type applications, with a small (n<10) node count.  Modern enterprise
/// servers, once burnt in, do not fail terribly often: a full node failure in such a small cluster
/// is perhaps a once-annually event (pessimistically).  When such a failure occurs, things
/// like HPC jobs using the Capra cluster would either stall until it was repaired, or
/// simply be re-run after the cluster is repaired.

type NodeEpoch = u64;
// Just re-use the protobuf-derived structures for simple enums
type NodeState = api::NodeState;

fn gen_epoch() -> u64 {
    let mut rand = thread_rng();
    rand.gen()
}

fn node_to_proto(epoch: NodeEpoch, node: &Node) -> api::NodeSelf {
    api::NodeSelf {
        name: node.get_hostname().to_string(),
        id: node.get_id(),
        epoch,
    }
}

fn lookup_peer(
    cluster_map: &ClusterMap,
    peer: Option<api::NodeSelf>,
) -> Result<(&Node, NodeEpoch), tonic::Status> {
    let peer = peer.ok_or_else(|| tonic::Status::invalid_argument("None node field"))?;

    let peer_node = match cluster_map.lookup_peer(peer.id, &peer.name) {
        Some(n) => n,
        None => {
            return Err(tonic::Status::new(
                Code::InvalidArgument,
                "Invalid `from` node",
            ))
        }
    };

    Ok((peer_node, peer.epoch))
}
