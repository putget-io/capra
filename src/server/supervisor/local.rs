use core::result::Result;
use core::result::Result::{Err, Ok};
use std::sync::Arc;

use crate::server::cluster_map::ClusterMap;
use crate::server::io_engine;
use crate::server::options::ServerOptions;
use crate::server::process;

use log::*;

pub struct LocalSupervisor {
    options: ServerOptions,
    cluster_map: Arc<ClusterMap>,
    io_builder: io_engine::Builder,

    // Shutdown hook for single node mode
    shutdown_lock: std::sync::Mutex<bool>,
    shutdown: std::sync::Condvar,
}

struct SimpleWait {
    lock: std::sync::Mutex<bool>,
    cond: std::sync::Condvar,
}

impl SimpleWait {
    fn new() -> Self {
        Self {
            lock: std::sync::Mutex::new(false),
            cond: std::sync::Condvar::new(),
        }
    }

    fn notify(&self) {
        self.cond.notify_one()
    }

    fn wait(&self) {
        let _guard = self
            .cond
            .wait(self.lock.lock().unwrap())
            .expect("SimpleWait::wait poisoned");
    }
}

impl LocalSupervisor {
    pub fn new(
        options: ServerOptions,
        cluster_map: Arc<ClusterMap>,
        io_builder: io_engine::Builder,
    ) -> Arc<Self> {
        Arc::new(Self {
            options,
            io_builder,
            cluster_map,
            shutdown: std::sync::Condvar::new(),
            shutdown_lock: std::sync::Mutex::new(false),
        })
    }

    pub fn block(&self) -> Result<(), ()> {
        // Start Process (includes tokio runtime)
        info!("Starting runtime...");
        let mut p = match process::Process::start(
            self.cluster_map.clone(),
            self.io_builder.clone(),
            self.options.clone(),
        ) {
            Err(e) => {
                error!("{}", e);
                return Err(());
            }
            Ok(p) => p,
        };

        let waiter = Arc::new(SimpleWait::new());
        // Replay and wait
        let waiter_bg = waiter.clone();
        info!("Starting replay...");
        p.start_replay(|| async move { waiter_bg.notify() });
        waiter.wait();

        // Run main server
        info!("Entering server...");
        p.start_server();

        {
            let guard = self.shutdown_lock.lock().unwrap();
            let _guard = self.shutdown.wait(guard).expect("Shudown lock poisoned");
        }

        p.shutdown();

        Ok(())
    }
}
