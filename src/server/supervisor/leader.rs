use std::sync::Arc;
use std::time::Duration;

use tonic::{Request, Response};

use crate::server::cluster_map::{ClusterMap, Node, NodeId};
use crate::server::options::ServerOptions;
use crate::server::process::Process;
use crate::server::{io_engine, supervisor};

use super::api;
use super::{NodeEpoch, NodeState};

use crate::server::supervisor::lookup_peer;
use log::*;

struct NodeStatus {
    id: NodeId,
    epoch: NodeEpoch,
    current: NodeState,
    next: NodeState,
}

#[derive(Debug)]
enum Ready {
    Reset,

    // When all nodes are ready to advance to a new state
    State(NodeState),
    // When the local node's current/ready need updating (e.g. replay is complete)
    MyState((NodeState, NodeState)),
}

struct LeaderState {
    epoch: NodeEpoch,
    process: Option<Process>,
    node_status: Vec<NodeStatus>,
    ready_tx: Option<tokio::sync::mpsc::UnboundedSender<Ready>>,
}

impl LeaderState {
    async fn teardown(&mut self) {
        let process = self.process.take();
        info!("Dropping previous Process");
        if let Some(mut process) = process {
            // Tokio runtimes cannot be dropped in an async context, so spawn
            // a non-async task to do it.
            tokio::task::spawn_blocking(move || {
                // We are the only reference to this, because of the Mutex around
                // the Arc, combined with the way we don't clone it anywhere else.
                process.shutdown();
            })
            .await
            .expect("Failed to shut down Process!");
        }
    }

    fn update_node(&mut self, node: &Node, epoch: u64, state: NodeState, next_state: NodeState) {
        info!(
            "update_node: node {} state {:?} next={:?}",
            node, state, next_state
        );
        let node_status = &mut self.node_status[node.get_id() as usize];

        if node_status.epoch == 0 {
            // First time we've seen this follower since leader
            // came up.
            info!("First ReportState from follower {} epoch {}", node, epoch);
            node_status.epoch = epoch;
        } else {
            if node_status.epoch != epoch {
                warn!(
                    "Node {} epoch changed ({} != {}), resetting",
                    node, node_status.epoch, epoch
                );

                // We continue to fall through and tell the client OK,
                // they did nothing wrong by notifying us of their
                // restart.
                if let Some(ready_tx) = self.ready_tx.as_mut() {
                    ready_tx
                        .send(Ready::Reset)
                        .expect("Leader receiver closed?");
                    return;
                }
            }
        }

        node_status.current = state;
        node_status.next = next_state;

        let mut any_delta = false;
        let mut next_state = None;
        for n in &self.node_status {
            if n.current != n.next {
                any_delta = true;
            }

            match next_state {
                None => next_state = Some(n.next),
                Some(nxt) => {
                    if n.next != nxt {
                        // Not all nodes are ready for the same next state.
                        // Drop out.
                        next_state = None;
                        break;
                    }
                }
            }
        }

        if let Some(next_state) = next_state {
            if any_delta {
                if let Some(ready_tx) = self.ready_tx.as_mut() {
                    ready_tx
                        .send(Ready::State(next_state))
                        .expect("Leader receiver closed?");
                }
            }
        } else {
            info!("Not yet ready for next state");
            for n in &self.node_status {
                info!("Node {} current={:?} next={:?}", n.id, n.current, n.next);
            }
        }
    }
}

pub struct LeaderSupervisor {
    options: ServerOptions,
    cluster_map: Arc<ClusterMap>,
    io_builder: io_engine::Builder,

    // Mutable part
    state: tokio::sync::Mutex<LeaderState>,
}

impl LeaderSupervisor {
    pub fn new(
        options: ServerOptions,
        cluster_map: Arc<ClusterMap>,
        io_builder: io_engine::Builder,
    ) -> Arc<Self> {
        let node_status = cluster_map
            .get_nodes()
            .iter()
            .map(|n| NodeStatus {
                id: n.get_id(),
                epoch: 0,
                current: NodeState::Initial,
                // The leader is immediately ready to start, followers
                // aren't until they report themselves so.
                next: if n.get_id() == 0 {
                    NodeState::Start
                } else {
                    NodeState::Initial
                },
            })
            .collect();

        let state = LeaderState {
            process: None,
            node_status,
            epoch: supervisor::gen_epoch(),
            ready_tx: None,
        };

        Arc::new(Self {
            options,
            cluster_map,
            io_builder,
            state: tokio::sync::Mutex::new(state),
        })
    }

    async fn broadcast_state(
        &self,
        clients: &mut Vec<api::follower_client::FollowerClient<tonic::transport::Channel>>,
        next_state: NodeState,
    ) -> Result<(), ()> {
        let my_epoch = self.state.lock().await.node_status[0].epoch;
        let my_node = self.cluster_map.my_node();

        info!("broadcast_state: {:?}", next_state);

        for node in self.cluster_map.followers() {
            // FIXME: a neater/more robust way of handling the client
            // collection, that ensures at startup that the node ids
            // really are contiguous from 1 (or just HashMaps them)
            info!("Sending set_state to follower {}", node);
            let client = &mut clients[node.get_id() as usize - 1];
            let req = api::SetStateRequest {
                from: Some(supervisor::node_to_proto(my_epoch, my_node)),
                new: next_state as i32,
            };
            match client.set_state(req).await {
                Ok(resp) => {
                    let resp = resp.into_inner();
                    if resp.status != api::Status::Ok as i32 {
                        info!("Peer {} gave bad set_state status {:?}", node, resp.status);
                        self.reset().await;
                    }
                }
                Err(e) => {
                    // The peer's supervisor service is unavailable, assume
                    // this means a cluster restart.
                    // FIXME: tolerate network gaps... followers are guaranteed
                    // to send a ReportState when they start up, which would trigger
                    // our reset.  On an unresponsive client we should just wait
                    // and see.  But we have to do that in a way that ensures
                    // we see the reset (i.e. keep an eye on self.reset) and
                    // not just loop on the client itself.
                    info!("RPC error with peer {}: {}, restarting", node, e);

                    self.reset().await;
                }
            }
        }

        Ok(())
    }

    async fn set_state_local(&self, next_state: NodeState) -> Result<(), ()> {
        let my_node = self.cluster_map.my_node();

        // Hold state lock across Process init, to avoid the grpc side
        // trying to process requests (e.g. reset) while we're in an
        // in-between state.
        let mut state = self.state.lock().await;

        info!(
            "Changing state {:?}->{:?}",
            state.node_status[0].current, next_state
        );

        match next_state {
            NodeState::Start => {
                info!("Starting leader's Process ({})", my_node);
                let process = match Process::start(
                    self.cluster_map.clone(),
                    self.io_builder.clone(),
                    self.options.clone(),
                ) {
                    Ok(p) => p,
                    Err(e) => {
                        error!("Failed to start: {}", e);
                        return Err(());
                    }
                };

                state.process = Some(process);
                let my_epoch = state.epoch;
                state.update_node(my_node, my_epoch, NodeState::Start, NodeState::Replay);
            }
            NodeState::Replay => {
                let ready_tx = state.ready_tx.as_ref().unwrap().clone();
                let process = state.process.as_mut().unwrap();

                // FIXME: we should find a way to avoid this ubiquitous unwrapping of ready_tx,
                // or check it as soon as we take a lock on state perhaps.
                let msg = Ready::MyState((NodeState::Replay, NodeState::Active));
                process.start_replay(|| async move { ready_tx.send(msg) });
            }
            NodeState::Active => {
                let process = state.process.as_mut().unwrap();
                process.start_server();
                state.node_status[0].current = NodeState::Active;
                state.node_status[0].next = NodeState::Active;
            }
            _ => unimplemented!(),
        }

        Ok(())
    }

    async fn reset(&self) {
        let mut state = self.state.lock().await;
        if let Some(ready_tx) = &mut state.ready_tx {
            ready_tx
                .send(Ready::Reset)
                .expect("Leader receiver closed?");
        }
    }

    async fn main_loop(&self) {
        let my_node = self.cluster_map.my_node();

        'cluster: loop {
            // TODO smart backoff
            tokio::time::sleep(Duration::from_millis(100)).await;

            // in the LeaderState so that gRPC handlers can validate incoming
            // requests against it.
            let my_epoch = supervisor::gen_epoch();
            let (ready_tx, mut ready_rx) = tokio::sync::mpsc::unbounded_channel();

            // Reset my epoch and follower state
            {
                let mut state = self.state.lock().await;

                state.epoch = my_epoch;
                state.ready_tx = Some(ready_tx);

                // Treat peers as ready to start immediately.  Treat self
                // as not ready to start until further down where we'll
                // set ourselves as ready once we've initialized clients etc.
                state.node_status[0].epoch = my_epoch;
                state.node_status[0].current = NodeState::Initial;
                state.node_status[0].next = NodeState::Start;
                for s in &mut state.node_status[1..] {
                    s.epoch = 0;
                    s.current = NodeState::Initial;
                    s.next = NodeState::Initial;
                }
            }

            // Open a client to all followers.  Reset (i.e. loop) if any unavailable.
            let mut clients = vec![];
            for node in self.cluster_map.followers() {
                match api::follower_client::FollowerClient::connect(node.get_supervisor_url()).await
                {
                    Ok(c) => clients.push(c),
                    Err(e) => {
                        info!("Cannot connect to follower {} yet ({})", node, e);
                        continue 'cluster;
                    }
                }
            }
            info!("Connected to all followers");

            // FIXME: all these places that we await on a client operation
            // to a follower, should really be pre-empted if we get e.g.
            // a reset-indicating ReportState on the grpc side.  How to
            // handle that?

            // Send a reset to all followers.  Loop with a backoff timeout until
            // we successfully complete the RPC with all followers.
            for node in self.cluster_map.followers() {
                // FIXME: a neater/more robust way of handling the client
                // collection, that ensures at startup that the node ids
                // really are contiguous from 1 (or just HashMaps them)
                info!("Sending reset to follower {}", node);
                let client = &mut clients[node.get_id() as usize - 1];
                let req = api::ResetRequest {
                    from: Some(supervisor::node_to_proto(my_epoch, my_node)),
                };
                match client.reset(req).await {
                    Ok(resp) => {
                        let resp: api::ResetResponse = resp.into_inner();
                        if resp.status != api::Status::Ok as i32 {
                            info!("Peer {} gave bad reset response {:?}", node, resp.status);
                            continue 'cluster;
                        }
                    }
                    Err(e) => {
                        // The peer's supervisor service is unavailable, assume
                        // this means a cluster restart.
                        info!("RPC error with peer {}: {}, restarting", node, e);

                        continue 'cluster;
                    }
                };
            }
            info!("Sent reset to all followers");

            // Service any state updates: initially we will receive ReportStates from
            // all the nodes that we just reset, and once all nodes are ready for the
            // next state they'll get a SetState to proceed.
            while let Some(ready) = ready_rx.recv().await {
                match ready {
                    Ready::Reset => {
                        warn!("Resetting");
                        break; // Fall through to tear down and loop.
                    }
                    Ready::State(next_state) => {
                        if self
                            .broadcast_state(&mut clients, next_state)
                            .await
                            .is_err()
                        {
                            // Treat error as reset
                            warn!("broadcast_state failed, resetting");
                            break;
                        }

                        if self.set_state_local(next_state).await.is_err() {
                            // Treat error as reset
                            warn!("set_state_local failed, resetting");
                            break;
                        }
                    }
                    Ready::MyState((current_state, next_state)) => {
                        let mut state = self.state.lock().await;
                        state.update_node(my_node, my_epoch, current_state, next_state);
                    }
                }
            }

            // Prior to our next turn around the loop, drop our Process
            // if there is one.
            self.state.lock().await.teardown().await;

            //  TODO: break out replay stage separately and gate everyone
            // through that together too to make sure that nobody services end
            // user requests until everybody has replayed.
        }
    }

    pub fn block(this: Arc<Self>) -> Result<(), ()> {
        // Start a runtime to run our grpc server
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(1)
            .build()
            .unwrap();

        let leader_server = LeaderServer {
            supervisor: this.clone(),
        };

        let bg_this = this.clone();
        rt.spawn(async move {
            tonic::transport::Server::builder()
                .add_service(api::leader_server::LeaderServer::new(leader_server))
                .serve(bg_this.cluster_map.my_node().get_supervisor_address())
                .await
                .expect("Error from supervisor RPC server")
        });

        rt.block_on(this.main_loop());

        Ok(())
    }
}

struct LeaderServer {
    supervisor: Arc<LeaderSupervisor>,
}

#[tonic::async_trait]
impl api::leader_server::Leader for LeaderServer {
    async fn report_state(
        &self,
        request: Request<api::ReportStateRequest>,
    ) -> Result<Response<api::ReportStateResponse>, tonic::Status> {
        let request = request.into_inner();

        let my_node = self.supervisor.cluster_map.my_node();

        let (from_node, from_epoch) = lookup_peer(&*self.supervisor.cluster_map, request.from)?;

        let response = {
            let mut state = self.supervisor.state.lock().await;
            state.update_node(
                from_node,
                from_epoch,
                decode_enum!(NodeState, request.current),
                decode_enum!(NodeState, request.ready_for),
            );

            api::ReportStateResponse {
                from: Some(supervisor::node_to_proto(state.epoch, my_node)),
                status: api::Status::Ok as i32,
            }
        };
        Ok(Response::new(response))
    }
}
