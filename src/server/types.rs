use serde::{Deserialize, Serialize};
use smallvec::SmallVec;
/// Module for widely shared types, e.g. those used in both the KeyMap and
/// in the journal.
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::fmt;

/// The types of op we persist, either in KeyMap or in StreamHeader.
/// This is not the whole set of S3 ops: just the ones we persist.
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum ObjectOp {
    Put = 1,
    Delete = 2,
}

#[derive(Clone, Eq, Ord, Copy)]
pub struct Offset {
    pub location: u64,

    // Includes ChunkHeader: i.e. the total number of bytes to request in
    // a read() when reading back.  This is distinct from the actual on-disk
    // size of a chunk, which is rounded up to the next IO_ALIGN
    pub size: u32,
}

impl Offset {
    pub fn new(location: u64, size: usize) -> Self {
        assert!(size <= u32::MAX as usize);
        Self {
            location,
            size: size as u32,
        }
    }
}

impl fmt::Display for Offset {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#010x}:{:#06x}", self.location, self.size)
    }
}
impl fmt::Debug for Offset {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#010x}:{:#06x}", self.location, self.size)
    }
}

impl PartialEq for Offset {
    fn eq(&self, other: &Self) -> bool {
        self.location == other.location
    }
}

impl PartialOrd for Offset {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// We track the chunks in a stream using a simple vector, but to avoid
// allocations on small objects (latency sensitive), we use a SmallVec
// with a heuristic definitino of how many IO_BUFFER_SIZE we consider 'small'
// N.B. if we wanted to really optimize the memory footprint of this, the offsets
// are highly compressible, being small deltas to one another.
pub type OffsetList = SmallVec<[Offset; 4]>;
