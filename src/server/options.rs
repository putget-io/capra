use std::env;
use std::net::SocketAddr;

use args::{Args, ArgsError};
use getopts::Occur;

#[derive(Clone)]
pub struct ServerOptions {
    pub memory_bench: bool,
    pub no_etags: bool,
    pub sloppy_sync: bool,
    pub unsigned: bool,
    pub drain: bool,
    pub journal_shards: Vec<String>,
    pub backend: String,
    pub listen: SocketAddr,
    pub workers: Option<usize>,
    pub expiry_rate: Option<f64>,
    pub engine: Option<String>,
    pub cluster_map: Option<String>,
}

impl ServerOptions {
    pub fn parse(input: &[String]) -> Result<ServerOptions, ArgsError> {
        let mut args = Args::new(&env::args().nth(0).unwrap()[..], "Capra server");

        args.flag("h", "help", "Print the usage menu");

        args.flag("m", "memory-bench", "Disable file IO for benchmarking");

        args.flag("n", "no-etags", "Disable ETag calculation on PUTs");

        args.flag(
            "s",
            "sloppy-sync",
            "Disable strict fsync semantics (risk data loss on crash)",
        );

        args.flag(
            "u",
            "unsigned",
            "Disable AWSv4 signature and content digest checks",
        );

        args.option(
            "j",
            "journal-shards",
            "One or more file paths, comma separated",
            "PATH[,PATH]",
            Occur::Optional,
            Some(String::from("journal.bin")),
        );

        args.option(
            "e",
            "engine",
            "IO Engine (e.g. tokio, rio)",
            "ENGINE",
            Occur::Optional,
            None,
        );

        args.option(
            "c",
            "cluster-map",
            "Cluster map (enable clustered mode)",
            "PATH",
            Occur::Optional,
            None,
        );

        args.option(
            "b",
            "backend",
            "S3 Backend URI or 'null'",
            "URI",
            Occur::Req,
            None,
        );

        args.option(
            "l",
            "listen",
            "Address+port to listen on",
            "ADDRPORT",
            Occur::Optional,
            Some(String::from("0.0.0.0:1234")),
        );

        args.option(
            "w",
            "workers",
            "How many worker threads to spawn?",
            "WORKERS",
            Occur::Optional,
            None,
        );

        args.option(
            "r",
            "expiry-rate",
            "bytes/sec limit on per-journal expiry rate",
            "RATE",
            Occur::Optional,
            None,
        );

        args.flag(
            "d",
            "drain",
            "do not journal writes unless the key is already journalled",
        );

        args.parse(input)?;

        if args.value_of("help")? {
            eprintln!("{}", args.full_usage());
            std::process::exit(0);
        }

        let listen_str: String = args.value_of("listen")?;

        let listen = match listen_str.parse::<SocketAddr>() {
            Ok(s) => s,
            Err(_) => {
                return Err(ArgsError::new(
                    "listen",
                    "Invalid socket spec, should be 1.2.3.4:1234",
                ))
            }
        };

        let shards_str: String = args.value_of("journal-shards")?;

        return Ok(ServerOptions {
            memory_bench: args.value_of("memory-bench")?,
            no_etags: args.value_of("no-etags")?,
            sloppy_sync: args.value_of("sloppy-sync")?,
            drain: args.value_of("drain")?,
            journal_shards: shards_str.split(",").map(|s| s.to_string()).collect(),
            engine: args.optional_value_of("engine")?,
            cluster_map: args.optional_value_of("cluster-map")?,
            backend: args.value_of("backend")?,
            unsigned: args.value_of("unsigned")?,
            listen,
            workers: args.optional_value_of("workers")?,
            expiry_rate: args.optional_value_of("expiry-rate")?,
        });
    }
}
