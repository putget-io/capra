use chrono::{DateTime, Utc};
use parking_lot::Mutex;
use std::collections::HashMap;

use linked_hash_map::LinkedHashMap;

use hyper::body::Bytes;
use log::{debug, error, info, warn};
use std::sync::atomic::{AtomicI64, AtomicU64};
use std::sync::Arc;

pub type ETag = [u8; 16];

struct DCacheInner {
    // Sum of all Entry.content_length, including entries in Writing state (i.e.
    // those Entries act as a reservation)
    bytes: u64,

    // Keep track of EntryStreams in progress, along with the size of each
    pending_streams: HashMap<ETag, u64>,

    // Each Entry is in an Arc, so that read streams can proceed independently
    // of any trimming operations that might remove the entry.
    entries: LinkedHashMap<ETag, Arc<Entry>>,
}

/// The DCache is a content-addressed store, accessed via the S3 ETag (i.e
/// md5sum) of objects.
pub struct DCache {
    // Limit total size of objects in the cache
    max_bytes_total: u64,

    // Limit size of objects that will be cached
    max_bytes_object: u64,

    inner: Mutex<DCacheInner>,
}

pub struct Entry {
    // Use Hyper's Bytes type to make it convenient to stream these through
    // into a hyper's default Body type when serving GETs
    chunks: Vec<Bytes>,
    _inserted_at: DateTime<Utc>,
    content_length: u64,

    hit_count: AtomicU64,
    // Storing date as miliseconds since epoch to use an atomic
    hit_at: AtomicI64,
}

impl Entry {
    pub fn get_chunks(&self) -> &Vec<Bytes> {
        return &self.chunks;
    }

    pub fn get_content_length(&self) -> u64 {
        self.content_length
    }
}

pub struct EntryStream<'a> {
    parent: &'a DCache,
    etag: ETag,
    chunks: Vec<Bytes>,
    content_length: u64,
    bytes: u64,
}

impl<'a> EntryStream<'a> {
    pub fn write(&mut self, bytes: Bytes) {
        if bytes.len() == 0 {
            return;
        }

        self.bytes += bytes.len() as u64;
        self.chunks.push(bytes);

        if self.bytes == self.content_length {
            let mut chunks = Vec::new();
            std::mem::swap(&mut chunks, &mut self.chunks);
            self.parent.complete_insert(&self.etag, chunks)
        } else if self.bytes > self.content_length {
            error!(
                "EntryStream given more bytes than content_length {:#10x} > {:#10x}",
                self.bytes, self.content_length
            );
            // This is a bug, but not worth panic'ing over.  We will end up calling abort_insert
            // on dro.
        }
    }

    // Helper for unit tests to use bare slices when we don't care about
    // the overhead of implicitly alloc'ing on each write
    #[cfg(test)]
    pub fn write_slice(&mut self, data: &[u8]) {
        self.write(Bytes::copy_from_slice(data))
    }
}

impl<'a> Drop for EntryStream<'a> {
    fn drop(&mut self) {
        if self.bytes != self.content_length {
            warn!("Aborting DCache insert for etag {:?}", self.etag);
            self.parent.abort_insert(&self.etag)
        }
    }
}

impl DCache {
    pub fn new(max_bytes_total: u64, max_bytes_object: u64) -> Self {
        Self {
            max_bytes_total,
            max_bytes_object,
            inner: Mutex::new(DCacheInner {
                bytes: 0,
                entries: LinkedHashMap::new(),
                pending_streams: HashMap::new(),
            }),
        }
    }
    /// On expiry of a buffered write, we stream it into a DCache entry at the same
    /// time as streaming it to the backend.
    pub fn insert(&self, etag: ETag, content_length: u64) -> Result<EntryStream<'_>, ()> {
        let mut inner = self.inner.lock();

        // Validate content_length
        if content_length > self.max_bytes_object {
            debug!(
                "Oversized object, not caching ({} > {})",
                content_length, self.max_bytes_object
            );
            return Err(());
        } else if content_length == 0 {
            debug!("Zero length object, not caching");
            return Err(());
        }

        // Reject null ETags, which appear in our journals when a server
        // was running with etag calculation disabled on puts.
        if etag == [0; 16] {
            debug!("Null Etag, not caching");
            return Err(());
        }

        // Enforce max_bytes_total
        while inner.bytes + content_length > self.max_bytes_total {
            match inner.entries.pop_front() {
                Some((victim_k, victim)) => {
                    info!("Trimmed ETag {:?}", victim_k);

                    inner.bytes -= victim.content_length;
                }
                None => {
                    // This means that the space quota is too full with
                    // in-flight EntryStreams (we removed all the complete
                    // Entrys and there still wasn't enough space for the insert)
                    debug!("Too many bytes in flight, not caching");
                    return Err(());
                }
            }
        }

        let entries = &mut inner.entries;
        let entry = entries.get_refresh(&etag);
        match entry {
            Some(_) => {
                // Never makes sense to overwrite an existing entry in a content-addressable store.
                // However, we have bumped it to the back of the LRU list
                // as an intentional side effect of the lookup, because we consider a write of
                // the content just as much a reason to retain it as a read of the content
                debug!("Already have etag, not caching");
                Err(())
            }
            None => match inner.pending_streams.get(&etag) {
                Some(_) => {
                    //Someone is already writing this
                    debug!("Write already in flight to this etag, not caching");
                    Err(())
                }
                None => {
                    inner.pending_streams.insert(etag, content_length);
                    inner.bytes += content_length;

                    Ok(EntryStream {
                        parent: self,
                        etag: etag.clone(),
                        chunks: Vec::new(),
                        content_length,
                        bytes: 0,
                    })
                }
            },
        }
    }

    fn abort_insert(&self, etag: &ETag) {
        let mut inner = self.inner.lock();
        let entries = &mut inner.pending_streams;
        if let Some(removed) = entries.remove(etag) {
            inner.bytes -= removed;
        }
    }

    fn complete_insert(&self, etag: &ETag, chunks: Vec<Bytes>) {
        let mut inner = self.inner.lock();
        match inner.pending_streams.remove(etag) {
            None => {
                // Rather odd to trim an Entry while it's still in Writing state, but
                // quite possible if something else very high priority pre-empted
                // us for the capacity.  Drop the data on the floor.
                warn!(
                    "DCache entry for {:?} dropped before it finished writing",
                    etag
                );
            }
            Some(content_length) => {
                info!(
                    "Completed DCache insert for etag {:?} content_length={}",
                    etag, content_length
                );
                let now = Utc::now();
                inner.entries.insert(
                    etag.clone(),
                    Arc::new(Entry {
                        chunks,
                        hit_count: AtomicU64::new(0),
                        _inserted_at: now.clone(),
                        hit_at: AtomicI64::new(now.timestamp_millis()),
                        content_length,
                    }),
                );
            }
        }
    }

    pub fn get(&self, etag: &ETag) -> Option<Arc<Entry>> {
        let mut inner = self.inner.lock();

        // Fetch and bump to back of LRU
        let entry = inner.entries.get_refresh(etag).map(|e| e.clone());

        // Update cache stats
        if let Some(entry) = entry.as_ref() {
            let now = Utc::now();
            entry
                .hit_count
                .fetch_add(1, std::sync::atomic::Ordering::Relaxed);
            entry
                .hit_at
                .store(now.timestamp_millis(), std::sync::atomic::Ordering::Relaxed);
        }

        entry
    }
}

#[cfg(test)]
mod test {
    use super::DCache;

    #[test]
    fn test_insert_get() {
        let c = DCache::new(8192, 8192);

        let etag = [1; 16];
        let content_length = 4096;

        let content: [u8; 4096] = [0xf0; 4096];
        let mut stream = c.insert(etag, content_length).expect("Should accept");

        // Key shouldn't be visible until written
        assert!(c.get(&etag).is_none());

        stream.write_slice(&content[0..2048]);

        // Key shouldn't be visible until *fully* written
        assert!(c.get(&etag).is_none());

        // New writes to same etag should be rejected
        assert!(c.insert(etag, content_length).is_err());

        stream.write_slice(&content[2048..]);

        // Key should be complete, visible
        let entry = c.get(&etag).expect("Should get");
        assert_eq!(entry.content_length, content_length);
        assert_eq!(entry.chunks.len(), 2);

        // Overwrites should be rejected
        assert!(c.insert(etag, content_length).is_err());
    }

    #[test]
    fn test_abort() {
        let c = DCache::new(8192, 8192);

        let etag = [1; 16];
        let content_length = 4096;

        let content: [u8; 4096] = [0xf0; 4096];
        let mut stream = c.insert(etag, content_length).expect("Should accept");

        stream.write_slice(&content[0..2048]);
        drop(stream);

        // A stream that was dropped before writing expected number of bytes
        // should not be visible
        assert!(c.get(&etag).is_none());
    }

    #[test]
    fn test_lru() {
        let c = DCache::new(8192, 8192);
        let etag_a = [1; 16];
        let etag_b = [2; 16];
        let etag_c = [3; 16];
        let content_length = 4096;
        let content: [u8; 4096] = [0xf0; 4096];

        c.insert(etag_a, content_length)
            .expect("Should accept")
            .write_slice(&content);
        c.insert(etag_b, content_length)
            .expect("Should accept")
            .write_slice(&content);
        c.insert(etag_c, content_length)
            .expect("Should accept")
            .write_slice(&content);

        // Should find that A was displaced from the cache to make way for C
        assert!(c.get(&etag_a).is_none());

        // B should not have been displaced, and C should have been successfully inserted
        assert!(c.get(&etag_b).is_some());
        assert!(c.get(&etag_c).is_some());

        // Now do the same thing, but with a get of A before the insert of C
        let c = DCache::new(8192, 8192);
        c.insert(etag_a, content_length)
            .expect("Should accept")
            .write_slice(&content);
        c.insert(etag_b, content_length)
            .expect("Should accept")
            .write_slice(&content);

        assert!(c.get(&etag_a).is_some());

        c.insert(etag_c, content_length)
            .expect("Should accept")
            .write_slice(&content);

        // This time B should have been displaced, and A retained because it was
        // accessed more recently
        assert!(c.get(&etag_b).is_none());
        assert!(c.get(&etag_a).is_some());
        assert!(c.get(&etag_c).is_some());
    }
}
