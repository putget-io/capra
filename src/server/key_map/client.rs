use std::borrow::Cow;
use std::collections::hash_map::DefaultHasher;
use std::collections::HashSet;
use std::fmt;
use std::hash::Hasher;
use std::sync::Arc;

use log::*;
use smallvec::SmallVec;
use tokio::sync::mpsc;

use crate::server::cluster_map::ClusterMap;
use crate::server::journal::{GlobalJournalId, JournalId, StreamFooter, StreamHeader};
use crate::server::types::{ObjectOp, OffsetList};

use super::remote::KeyMapShardRemote;
use super::shard::Shard;
use super::{KeyMapShard, PersistentValue};

use crate::server::key_map::{DeleteValue, ListCursor, PersistentValueOp, PutValue};
use tokio::task::JoinHandle;

/// The KeyMap is a distributed structure.  It is broken up into
/// key hash ranges stored in KeyMapShards across 1 or more nodes.  Each KeyMapShard
/// has an associated server accessible via grpc KeyMapShard service.
/// Each node also has a KeyMapClient instance which issues operations either to
/// local KeyMapShard instance fo shards on the same node as the client, or via
/// a KeyMapShardRemote grpc client for remote shards.

/// Writers must add themselves to KeyState.projected before they can be
/// told which journal to use for incoming data:
pub struct ProjectionHandle<'a> {
    bucket: Cow<'a, str>,
    key: Cow<'a, str>,

    // A ProjectionHandle always refers to a local journal
    journal_id: JournalId,

    complete: bool,

    // Fire the key down this channel on unclean drop, for background
    // cleanup
    cancel_tx: mpsc::UnboundedSender<(String, String)>,
}

impl ProjectionHandle<'_> {
    pub fn bucket(&self) -> &str {
        self.bucket.as_ref()
    }

    pub fn key(&self) -> &str {
        self.key.as_ref()
    }

    pub fn get_journal_id(&self) -> JournalId {
        self.journal_id
    }

    pub fn into_owned(mut self) -> ProjectionHandle<'static> {
        let r = ProjectionHandle {
            bucket: Cow::from(self.bucket.clone().into_owned()),
            key: Cow::from(self.key.clone().into_owned()),
            journal_id: self.journal_id,
            complete: self.complete,
            cancel_tx: self.cancel_tx.clone(),
        };
        self.complete = true; // Disable drop
        r
    }
}

/// Drop is kind of interesting when our projected state is on
/// a remote keymap shard.  We can't do an async await on a grpc to
/// cancel it on the remote side.  However, we don't logically need
/// to wait: we can fire+forget a cancellation to complete eventually.
impl Drop for ProjectionHandle<'_> {
    fn drop(&mut self) {
        if !self.complete {
            trace!("ProjectionHandle::drop: incomplete, cancelling");
            if self
                .cancel_tx
                .send((self.bucket.to_string(), self.key.to_string()))
                .is_err()
            {
                // Unexpected case, the KeyMap has been dropped while ProjectionHandles still existed?
                // Callers should drop any keymap-using tasks first.
                error!(
                    "Error cancelling for dropped ProjectionHandle {}:{}",
                    self.bucket, self.key
                );
            };
        } else {
            trace!("ProjectionHandle::drop: complete OK");
        }
    }
}

// Strictly this isn't a limit, it's just that if you have more shards than this
// then the SmallVec containing the shards will allocate.
pub const MAX_SHARDS: usize = 128;

enum ShardSlot {
    Local(Arc<KeyMapShard>),
    Remote(KeyMapShardRemote),
}

pub struct KeyMap {
    cluster_map: Arc<ClusterMap>,

    // TODO: currently, all nodes have a single shard and their shard is just
    // keys where the mod of the hash maps to to their index in `shards`.
    // Later, nodes should be able to have more than one shard (perhaps even
    // with each shard having an independent grpc server and running on a different
    // core), which will require the clustermap to provide more explicit information
    // about which shards are on which nodes.

    // A bit wasteful to take space for MAX_SHARDS irrespective of how
    // much we're using, but this is one of the hottest structures so
    // I don't want the pointer chase of a regular Vec

    // Q: Why both distinguishing between local and remote, why not just always RPC?
    // A: Because we care about small (1-3 nodes) system performance, and in that regime
    // the CPU savings from skipping the RPC for 1/nth of operations are significant.
    shards: SmallVec<[ShardSlot; MAX_SHARDS]>,

    cancel_queue_tx: mpsc::UnboundedSender<(String, String)>,
}

impl<'a> KeyMap {
    pub fn start(
        cluster_map: Arc<ClusterMap>,
        local_shard: &Arc<KeyMapShard>,
        rt_handle: &tokio::runtime::Handle,
    ) -> (Arc<Self>, JoinHandle<()>) {
        let mut shards = SmallVec::<[ShardSlot; MAX_SHARDS]>::new();
        for node in cluster_map.get_nodes() {
            if node.get_id() == cluster_map.get_id() {
                shards.push(ShardSlot::Local(local_shard.clone()));
            } else {
                let url = node.get_grpc_url();
                shards.push(ShardSlot::Remote(KeyMapShardRemote::new(url)))
            }
        }

        let (cancel_tx, cancel_rx) = mpsc::unbounded_channel();

        let me = Arc::new(Self {
            shards,
            cluster_map,
            cancel_queue_tx: cancel_tx,
        });

        let bg = Self::background(me.clone(), rt_handle, cancel_rx);

        (me, bg)
    }

    /// Stuff that happens in the background for the keymap, such
    /// as ProjectionHandle drops that emit cancellations we must service
    fn background(
        this: Arc<Self>,
        rt_handle: &tokio::runtime::Handle,
        mut cancel_rx: mpsc::UnboundedReceiver<(String, String)>,
    ) -> JoinHandle<()> {
        rt_handle.spawn(async move {
            while let Some((key, bucket)) = cancel_rx.recv().await {
                this.cancel_write(&key, &bucket).await;
            }
        })
    }

    // For any operation that acts on a particular key, find which
    // of our shards the key belongs to.
    fn get_shard(&self, bucket: &str, key: &str) -> &dyn Shard {
        let shard_slot = if self.shards.len() == 1 {
            &self.shards[0]
        } else {
            // TODO OPTIMIZATION: some code paths (notably write) pass through the KeyMap
            // twice, it would be nice to avoid hashing the key twice.
            // TODO OPTIMISATION: the default hashmap hasher is intended to be DOS resistant,
            // which we probably don't need: change to something cheaper.
            let mut h = DefaultHasher::new();

            h.write(bucket.as_bytes());
            h.write(key.as_bytes());
            &self.shards[h.finish() as usize & (self.shards.len() - 1)]
        };

        match shard_slot {
            ShardSlot::Local(l) => &**l,
            ShardSlot::Remote(r) => r,
        }
    }

    // TODO: listings will typically be many requests within the same
    // prefix.  Since it is not harmful to falsely return 'true' from this
    // function (it just causes a performance hit if the interception
    // of the request wasn't needed), we should ttl-cache the result if it was true.
    pub async fn prefix_dirty(&self, bucket: &str, prefix: &str) -> bool {
        // TODO: parallelise remote requests
        for shard in &self.shards {
            if match shard {
                ShardSlot::Local(kms) => kms.prefix_dirty(bucket, prefix).await,
                ShardSlot::Remote(rkms) => rkms.prefix_dirty(bucket, prefix).await,
            } {
                return true;
            }
        }

        false
    }

    #[cfg(test)]
    pub fn len(&self) -> usize {
        self.shards
            .iter()
            .map(|s| match s {
                ShardSlot::Local(l) => l.len(),
                ShardSlot::Remote(_r) => unimplemented!("Don't call len() with remote shards"),
            })
            .sum()
    }

    pub async fn list(
        &self,
        bucket: &str,
        cursor: Option<&ListCursor>,
        prefix: &str,
        delimiter: Option<&str>,
        max_keys: usize,
    ) -> (Vec<(String, PersistentValue)>, Vec<String>) {
        // TODO: parallelise remote requests
        let mut result_keys = Vec::new();
        let mut result_prefixes: HashSet<String> = HashSet::new();
        for shard in &self.shards {
            let (mut keys, prefixes) = match shard {
                ShardSlot::Local(kms) => {
                    kms.list(bucket, cursor, prefix, delimiter, max_keys).await
                }
                ShardSlot::Remote(rkms) => {
                    rkms.list(bucket, cursor, prefix, delimiter, max_keys).await
                }
            };
            result_keys.append(&mut keys);
            for p in prefixes {
                result_prefixes.insert(p);
            }
        }

        // Because we aggregated keys from many hash-addressed shards, our result
        // isn't going to be in key order unless we sort it now.
        if self.shards.len() > 1 {
            result_keys.sort_by(|a, b| a.0.cmp(&b.0));
        }

        // We don't sort the prefix result, because the caller is going to end
        // up sorting it anyway when they combine it with the backend's prefixes
        let result_prefixes = result_prefixes.into_iter().collect();

        (result_keys, result_prefixes)
    }

    pub async fn begin_write(
        &'a self,
        bucket: &'a str,
        key: &'a str,
        pref_journal: JournalId,
    ) -> Result<ProjectionHandle<'a>, GlobalJournalId> {
        let shard = self.get_shard(bucket, key);
        let journal_id = shard
            .begin_write(
                bucket,
                key,
                GlobalJournalId::new(self.cluster_map.get_id(), pref_journal),
            )
            .await;
        if journal_id.node_id == self.cluster_map.get_id() {
            Ok(ProjectionHandle {
                key: Cow::from(key),
                bucket: Cow::from(bucket),
                complete: false,
                journal_id: journal_id.journal_id,
                cancel_tx: self.cancel_queue_tx.clone(),
            })
        } else {
            // A remote journal: tell the caller where they should redirect to.
            Err(journal_id)
        }
    }

    pub async fn cancel_write(&self, bucket: &str, key: &str) {
        let shard = self.get_shard(bucket, key);
        shard.cancel_write(bucket, key).await
    }

    pub async fn complete_write(&self, mut handle: ProjectionHandle<'a>, pval: PersistentValue) {
        let shard = self.get_shard(handle.bucket(), handle.key());
        shard
            .complete_write(handle.bucket(), handle.key(), pval)
            .await;
        handle.complete = true; // Tip off handle not to cancel on drop
    }

    pub async fn get(&self, bucket: &str, key: &str) -> Option<(GlobalJournalId, PersistentValue)> {
        let shard = self.get_shard(bucket, key);
        shard.get(bucket, key).await
    }

    pub async fn replay(
        &self,
        journal_id: JournalId,
        offset_list: OffsetList,
        stream_header: StreamHeader<'_>,
        stream_footer: StreamFooter,
    ) {
        debug!(
            "replay() journal={} op={:?} key={}:{} offsets={:#10x}..{:#10x}",
            journal_id,
            stream_header.op,
            stream_header.bucket,
            stream_header.key,
            offset_list.first().unwrap().location,
            offset_list.last().unwrap().location
        );

        // Convert inputs into a PersistentValue
        let shard = self.get_shard(&stream_header.bucket, &stream_header.key);

        // Construct a PersistentValue
        let pv_op = match stream_header.op {
            ObjectOp::Put => PersistentValueOp::Put(PutValue {
                last_modified: stream_header.last_modified,
                content_length: stream_header.content_length,
                etag: stream_footer.into(),
            }),
            ObjectOp::Delete => PersistentValueOp::Delete(DeleteValue {
                last_modified: stream_header.last_modified,
            }),
        };
        let pv = PersistentValue {
            chunks: offset_list,
            op: pv_op,
        };

        shard
            .replay(
                GlobalJournalId::new(self.cluster_map.get_id(), journal_id),
                &stream_header.bucket,
                &stream_header.key,
                pv,
            )
            .await;
    }

    // Call this once a write to the backend has completed successfully.
    pub async fn expire(&self, journal_id: JournalId, bucket: &str, key: &str, last_offset: u64) {
        let shard = self.get_shard(bucket, key);
        shard
            .expire(
                GlobalJournalId::new(self.cluster_map.get_id(), journal_id),
                bucket,
                key,
                last_offset,
            )
            .await;
    }

    // Call this once a write to the backend has completed successfully.
    pub async fn trim(&self, journal_id: JournalId, tail: u64) {
        // TODO: should issue to remote shards in parallel
        for (i, shard) in (&self.shards).iter().enumerate() {
            debug!("trim {} {:#10x} (shard {})", journal_id, tail, i);
            match shard {
                ShardSlot::Local(kms) => {
                    kms.trim(
                        GlobalJournalId::new(self.cluster_map.get_id(), journal_id),
                        tail,
                    )
                    .await
                }
                ShardSlot::Remote(rkms) => {
                    rkms.trim(
                        GlobalJournalId::new(self.cluster_map.get_id(), journal_id),
                        tail,
                    )
                    .await
                }
            };
        }
    }
}

impl fmt::Debug for KeyMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "KeyMap shards={}", self.shards.len())
    }
}
