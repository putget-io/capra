use async_trait::async_trait;
use tonic;

use super::api;
use super::shard::Shard;
use super::PersistentValue;

use crate::server::journal::GlobalJournalId;
use crate::server::key_map::shard_server::{
    gjid_from_proto, gjid_to_proto, pval_from_proto, pval_to_proto,
};
use crate::server::key_map::ListCursor;

use tokio::sync::RwLock as TokioRwLock;

use log::warn;

type KeyMapShardClient = api::key_map_shard_client::KeyMapShardClient<tonic::transport::Channel>;

pub struct KeyMapShardRemote {
    url: String,
    client: TokioRwLock<Option<KeyMapShardClient>>,
}

impl KeyMapShardRemote {
    pub fn new(url: String) -> Self {
        Self {
            url,
            client: TokioRwLock::new(None),
        }
    }
}

impl KeyMapShardRemote {
    async fn get_client(
        &self,
    ) -> api::key_map_shard_client::KeyMapShardClient<tonic::transport::Channel> {
        {
            let read = self.client.read().await;
            if let Some(c) = &*read {
                return c.clone();
            }
        }

        // Fall through: not yet initialized
        {
            let mut write = self.client.write().await;

            // In case we raced with another thread for init
            if let Some(c) = &*write {
                return c.clone();
            }

            let client = loop {
                let r =
                    api::key_map_shard_client::KeyMapShardClient::connect(self.url.clone()).await;
                match r {
                    Ok(c) => {
                        break c;
                    }
                    Err(e) => {
                        warn!("Failed to connect to peer {}, retrying ({})", self.url, e);
                        tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
                    }
                }
            };

            *write = Some(client.clone());
            client
        }
    }
}

/// Convenience to enable required_field() in grpc handlers
/// when extracting fields that should return errors when missing.
///
/// TODO: once our client side has proper structured errors, don't panic!
fn required_field<T>(opt: Option<T>) -> T {
    opt.unwrap_or_else(|| panic!("Missing required field"))
}
#[async_trait]
impl Shard for KeyMapShardRemote {
    async fn begin_write(
        &self,
        bucket: &str,
        key: &str,
        prefer_journal: GlobalJournalId,
    ) -> GlobalJournalId {
        let req = api::BeginWriteRequest {
            bucket: bucket.to_string(),
            key: key.to_string(),
            preferred: Some(api::GlobalJournalId {
                node_id: prefer_journal.node_id,
                journal_id: prefer_journal.journal_id,
            }),
        };
        // FIXME: don't panic, propagate errors up to callers (HTTP handlers should either 503 or block
        // if a keymap rpc can't be completed), in this and all other client functions

        let mut client = self.get_client().await;
        let tonic_response = client.begin_write(req).await.expect("Unexpected RPC error");
        let response = tonic_response.into_inner();
        let gjid = required_field(response.bound_to);

        gjid_from_proto(gjid)
    }

    async fn cancel_write(&self, bucket: &str, key: &str) {
        let req = api::CancelWriteRequest {
            bucket: bucket.to_string(),
            key: key.to_string(),
        };
        let mut client = self.get_client().await;
        let _tonic_response = client
            .cancel_write(req)
            .await
            .expect("Unexpected RPC error");
    }

    async fn complete_write(&self, bucket: &str, key: &str, pval: PersistentValue) {
        let req = api::CompleteWriteRequest {
            bucket: bucket.to_string(),
            key: key.to_string(),
            value: Some(pval_to_proto(pval)),
        };
        let mut client = self.get_client().await;
        let _tonic_response = client
            .complete_write(req)
            .await
            .expect("Unexpected RPC error");
    }

    async fn get(&self, bucket: &str, key: &str) -> Option<(GlobalJournalId, PersistentValue)> {
        let req = api::GetRequest {
            bucket: bucket.to_string(),
            key: key.to_string(),
        };
        let mut client = self.get_client().await;
        let tonic_response = client.get(req).await.expect("Unexpected RPC error");
        let get_response = tonic_response.into_inner();
        get_response.found.map(|found| {
            let (gjid, pval) = (required_field(found.location), required_field(found.value));
            (
                gjid_from_proto(gjid),
                pval_from_proto(pval).expect("Invalid response"),
            )
        })
    }

    async fn replay(
        &self,
        journal_id: GlobalJournalId,
        bucket: &str,
        key: &str,
        pv: PersistentValue,
    ) {
        let req = api::ReplayRequest {
            journal: Some(gjid_to_proto(journal_id)),
            bucket: bucket.to_string(),
            key: key.to_string(),
            value: Some(pval_to_proto(pv)),
        };
        let mut client = self.get_client().await;
        client.replay(req).await.expect("Unexpected RPC error");
    }

    async fn expire(&self, journal_id: GlobalJournalId, bucket: &str, key: &str, last_offset: u64) {
        let req = api::ExpireRequest {
            journal: Some(gjid_to_proto(journal_id)),
            bucket: bucket.to_string(),
            key: key.to_string(),
            last_offset,
        };
        let mut client = self.get_client().await;
        client.expire(req).await.expect("Unexpected RPC error");
    }

    async fn trim(&self, journal_id: GlobalJournalId, tail: u64) {
        let req = api::TrimRequest {
            journal: Some(gjid_to_proto(journal_id)),
            tail,
        };
        let mut client = self.get_client().await;
        client.trim(req).await.expect("Unexpected RPC error");
    }

    async fn list(
        &self,
        bucket: &str,
        cursor: Option<&ListCursor>,
        prefix: &str,
        delimiter: Option<&str>,
        max_keys: usize,
    ) -> (Vec<(String, PersistentValue)>, Vec<String>) {
        let cursor = cursor.map(|c| api::ListCursor {
            cursor: c.cursor.clone(),
            is_prefix: c.is_prefix,
        });
        let req = api::ListRequest {
            bucket: bucket.to_string(),
            prefix: prefix.to_string(),
            delimiter: delimiter.map(|d| d.to_string()).unwrap_or(String::new()),
            cursor,
            max_keys: max_keys as u32,
        };
        let mut client = self.get_client().await;
        let resp = client
            .list(req)
            .await
            .expect("Unexpected RPC error")
            .into_inner();

        let keys = resp
            .keys
            .into_iter()
            .map(|lkv| {
                (
                    lkv.key,
                    pval_from_proto(required_field(lkv.value)).expect("Invalid response"),
                )
            })
            .collect();

        (keys, resp.prefixes)
    }

    async fn prefix_dirty(&self, bucket: &str, prefix: &str) -> bool {
        let req = api::PrefixDirtyRequest {
            bucket: bucket.to_string(),
            prefix: prefix.to_string(),
        };
        let mut client = self.get_client().await;
        let resp = client
            .prefix_dirty(req)
            .await
            .expect("Unexpected RPC error")
            .into_inner();
        resp.is_dirty
    }
}
