use std::sync::Arc;
use std::time::SystemTime;

use digest::Output;
use md5::Md5;
use tonic::{Request, Response, Status};

use crate::server::key_map::{
    DeleteValue, KeyMapShard, ListCursor, PersistentValue, PersistentValueOp, PutValue,
};

use super::api;
use super::shard::Shard;
use crate::server::journal::GlobalJournalId;
use crate::server::key_map::api::key_map_shard_server;
use crate::server::types::Offset;
use crate::util::grpc::required_field;

pub struct KeyMapShardService {
    shard: Arc<KeyMapShard>,
}

impl KeyMapShardService {
    pub fn new(shard: Arc<KeyMapShard>) -> Self {
        Self { shard }
    }
}

// FIXME: all the places protobuf is using a String inline, we could
// steal those strings in places where KeyMapShard will construct IndexKeys

#[tonic::async_trait]
impl key_map_shard_server::KeyMapShard for KeyMapShardService {
    async fn begin_write(
        &self,
        request: Request<api::BeginWriteRequest>,
    ) -> Result<Response<api::BeginWriteResponse>, Status> {
        let request = request.into_inner();

        let preferred = required_field(request.preferred.as_ref())?;

        let jid = self
            .shard
            .begin_write(
                &request.bucket,
                &request.key,
                GlobalJournalId::new(preferred.node_id, preferred.journal_id),
            )
            .await;
        Ok(Response::new(api::BeginWriteResponse {
            bound_to: Some(api::GlobalJournalId {
                node_id: jid.node_id,
                journal_id: jid.journal_id,
            }),
        }))
    }

    async fn cancel_write(
        &self,
        request: Request<api::CancelWriteRequest>,
    ) -> Result<Response<api::CancelWriteResponse>, Status> {
        let request = request.into_inner();

        self.shard.cancel_write(&request.bucket, &request.key).await;

        Ok(Response::new(api::CancelWriteResponse {}))
    }

    async fn complete_write(
        &self,
        request: Request<api::CompleteWriteRequest>,
    ) -> Result<Response<api::CompleteWriteResponse>, Status> {
        let request = request.into_inner();

        // Construct a native PersistentValue from the protobuf equivalent
        let proto_v = required_field(request.value)?;
        let pval = pval_from_proto(proto_v)?;

        self.shard
            .complete_write(&request.bucket, &request.key, pval)
            .await;

        Ok(Response::new(api::CompleteWriteResponse {}))
    }

    async fn get(
        &self,
        request: Request<api::GetRequest>,
    ) -> Result<Response<api::GetResponse>, Status> {
        let request = request.into_inner();

        // FIXME: if caller is going to redirect anyway on non-local data, maybe skip the
        // effort of cloning out the PersistentValue into the result unless the result is local?

        Ok(Response::new(api::GetResponse {
            found: self
                .shard
                .get(&request.bucket, &request.key)
                .await
                .map(|(gjid, pval)| api::GetResponseFound {
                    location: Some(gjid_to_proto(gjid)),
                    value: Some(pval_to_proto(pval)),
                }),
        }))
    }

    async fn replay(
        &self,
        request: Request<api::ReplayRequest>,
    ) -> Result<Response<api::ReplayResponse>, Status> {
        let request = request.into_inner();

        let journal = gjid_from_proto(required_field(request.journal)?);
        let value = pval_from_proto(required_field(request.value)?)?;

        self.shard
            .replay(journal, &request.bucket, &request.key, value)
            .await;

        Ok(Response::new(api::ReplayResponse {}))
    }

    async fn expire(
        &self,
        request: Request<api::ExpireRequest>,
    ) -> Result<Response<api::ExpireResponse>, Status> {
        let request = request.into_inner();

        self.shard
            .expire(
                gjid_from_proto(required_field(request.journal)?),
                &request.bucket,
                &request.key,
                request.last_offset,
            )
            .await;

        Ok(Response::new(api::ExpireResponse {}))
    }

    async fn trim(
        &self,
        request: Request<api::TrimRequest>,
    ) -> Result<Response<api::TrimResponse>, Status> {
        let request = request.into_inner();

        self.shard
            .trim(
                gjid_from_proto(required_field(request.journal)?),
                request.tail,
            )
            .await;

        Ok(Response::new(api::TrimResponse {}))
    }

    async fn prefix_dirty(
        &self,
        request: Request<api::PrefixDirtyRequest>,
    ) -> Result<Response<api::PrefixDirtyResponse>, Status> {
        let request = request.into_inner();
        Ok(Response::new(api::PrefixDirtyResponse {
            is_dirty: self
                .shard
                .prefix_dirty(&request.bucket, &request.prefix)
                .await,
        }))
    }

    async fn list(
        &self,
        request: Request<api::ListRequest>,
    ) -> Result<Response<api::ListResponse>, Status> {
        let request = request.into_inner();

        let cursor = request
            .cursor
            .map(|c| ListCursor::from_string(c.cursor, c.is_prefix));

        let delimiter: Option<&str> = if request.delimiter.is_empty() {
            None
        } else {
            Some(&request.delimiter)
        };

        let (keys, prefixes) = self
            .shard
            .list(
                &request.bucket,
                cursor.as_ref(),
                &request.prefix,
                delimiter,
                request.max_keys as usize,
            )
            .await;

        Ok(Response::new(api::ListResponse {
            keys: keys
                .into_iter()
                .map(|(k, v)| api::ListKeyValue {
                    key: k,
                    value: Some(pval_to_proto(v)),
                })
                .collect(),
            prefixes,
        }))
    }
}

pub fn pval_from_proto(proto_v: api::PersistentValue) -> Result<PersistentValue, Status> {
    let offsets = proto_v
        .offsets
        .into_iter()
        .map(|o| Offset::new(o.location, o.size as usize))
        .collect();
    let value = required_field(proto_v.value)?;
    let op = match value {
        api::persistent_value::Value::Put(put_v) => {
            let last_modified = SystemTime::from(required_field(put_v.last_modified)?);
            PersistentValueOp::Put(PutValue {
                last_modified: last_modified.into(),
                content_length: put_v.content_length,
                etag: Output::<Md5>::clone_from_slice(&put_v.etag[..]),
            })
        }
        api::persistent_value::Value::Delete(delete_v) => {
            let last_modified = SystemTime::from(required_field(delete_v.last_modified)?);
            PersistentValueOp::Delete(DeleteValue {
                last_modified: last_modified.into(),
            })
        }
    };

    Ok(PersistentValue {
        chunks: offsets,
        op,
    })
}

pub fn pval_to_proto(pval: PersistentValue) -> api::PersistentValue {
    let offsets = pval
        .chunks
        .iter()
        .map(|c| api::Offset {
            location: c.location,
            size: c.size,
        })
        .collect();
    api::PersistentValue {
        offsets,
        value: Some(match pval.op {
            PersistentValueOp::Put(put_value) => api::persistent_value::Value::Put(api::PutValue {
                last_modified: Some(SystemTime::from(put_value.last_modified).into()),
                content_length: put_value.content_length,
                etag: put_value.etag.to_vec(),
            }),
            PersistentValueOp::Delete(delete_value) => {
                api::persistent_value::Value::Delete(api::DeleteValue {
                    last_modified: Some(SystemTime::from(delete_value.last_modified).into()),
                })
            }
        }),
    }
}

pub fn gjid_from_proto(gjid: api::GlobalJournalId) -> GlobalJournalId {
    GlobalJournalId {
        node_id: gjid.node_id,
        journal_id: gjid.journal_id,
    }
}

pub fn gjid_to_proto(gjid: GlobalJournalId) -> api::GlobalJournalId {
    api::GlobalJournalId {
        node_id: gjid.node_id,
        journal_id: gjid.journal_id,
    }
}
