use smallstr::SmallString;
use std::fmt;

/// Fully qualified key: includes the bucket name as well as the key within the bucket.
#[derive(Eq, Hash, PartialEq, PartialOrd, Ord, Clone)]
pub struct IndexKey {
    // Note that these sizes are not limits, just allowances to enable 'small'
    // bucket names and keys to fit without an extra allocation.
    // One would prefer to make them neatly pad IndexKey out to 128b, but SmallVec's
    // Array trait is only implement for certain abitrary lengths!  (can lift this limit
    // once const generics are in stable Rust)
    pub bucket: SmallString<[u8; 36]>,
    pub key: SmallString<[u8; 36]>,
}

impl IndexKey {
    pub fn new(bucket: &str, key: &str) -> Self {
        Self {
            bucket: SmallString::from(bucket),
            key: SmallString::from(key),
        }
    }
}

impl fmt::Display for IndexKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.bucket, self.key)
    }
}
