use std::fmt;

use percent_encoding;
use percent_encoding::percent_decode;

/// What we prepend to our ListObjectsv2 continuation tokens to distinguish them from
/// the backend's continuation tokens
const CONTINUATION_MAGIC: &str = "capra-";

#[derive(PartialEq, Eq)]
pub struct ListCursor {
    pub cursor: String,
    pub is_prefix: bool,
}

impl ListCursor {
    pub fn new(cursor: &str, is_prefix: bool) -> Self {
        Self {
            cursor: cursor.to_string(),
            is_prefix,
        }
    }

    pub fn from_string(cursor: String, is_prefix: bool) -> Self {
        Self { cursor, is_prefix }
    }

    pub fn decode(token: &str) -> Option<Self> {
        if token.starts_with(CONTINUATION_MAGIC) && token.len() > CONTINUATION_MAGIC.len() + 2 {
            let start_after = &token[CONTINUATION_MAGIC.len()..token.len() - 2];
            let is_prefix = token[token.len() - 1..] == *"1";
            Some(Self::new(
                &percent_decode(start_after.as_bytes()).decode_utf8_lossy(),
                is_prefix,
            ))
        } else {
            None
        }
    }

    pub fn encode(&self) -> String {
        // This encoding could be made more robust by including e.g. a signature of
        // the concatenated magic and key.  However, it only has to be strong enough
        // to not collide with the leading bytes of other S3 implementations
        // that we might use as as backend.
        format!(
            "{}{}-{}",
            CONTINUATION_MAGIC,
            self.cursor,
            // percent_encoding::percent_decode(
            //     self.cursor.as_bytes(),
            //     percent_encoding::NON_ALPHANUMERIC
            // ),
            if self.is_prefix { 1 } else { 0 }
        )
    }
}

impl fmt::Debug for ListCursor {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}(is_prefix={})", self.cursor, self.is_prefix)
    }
}

#[cfg(test)]
mod test {
    use super::ListCursor;

    #[test]
    fn test_list_cursor() {
        assert_eq!(
            ListCursor::new("foo", true).encode(),
            "capra-foo-1".to_string()
        );
        assert_eq!(
            ListCursor::decode("capra-foo-1"),
            Some(ListCursor::new("foo", true))
        );

        // A token without our magic is None (i.e. pass it through to backend)
        assert_eq!(ListCursor::decode("foo"), None);
    }
}
