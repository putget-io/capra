pub mod api {
    tonic::include_proto!("keymap");
}

mod client;
mod cursor;
mod key;
mod remote;
mod shard;
mod shard_server;

pub use client::{KeyMap, ProjectionHandle};
pub use cursor::ListCursor;
pub use key::IndexKey;
pub use shard::KeyMapShard;
pub use shard::{DeleteValue, KeyState, PersistentValue, PersistentValueOp, PutValue};
pub use shard_server::KeyMapShardService;

#[cfg(test)]
pub mod test {
    use crate::server::cluster_map::ClusterMap;
    use crate::server::key_map::{KeyMap, KeyMapShard};
    use std::sync::Arc;

    /// Test helper for constructing a simple local keymap
    pub async fn key_map_for_test() -> KeyMap {
        let cluster_map = Arc::new(ClusterMap::new_local());
        let key_map_shard = Arc::new(KeyMapShard::new());
        let handle = tokio::runtime::Handle::current();
        let (key_map, _km_bg) = KeyMap::start(cluster_map, &key_map_shard, &handle);
        _km_bg.abort();
        drop(_km_bg);

        // Since tokio 1.7.0, calling abort() on a JoinHandle doesn't
        // drop the task immediately (even if it's not running): we must
        // yield to get the _km_bg task to drop.
        tokio::task::yield_now().await;

        Arc::try_unwrap(key_map).unwrap()
    }
}
