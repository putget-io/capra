use std::collections::btree_map::Entry;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::mem;
use std::mem::size_of;
use std::ops::Bound;
use std::ops::Bound::{Excluded, Included};

use ::digest::Output;
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use log::*;
use md5;
use parking_lot::{Mutex, RwLock, RwLockUpgradableReadGuard, RwLockWriteGuard};

use crate::server::journal::GlobalJournalId;
use crate::server::key_map::key::IndexKey;
use crate::server::key_map::ListCursor;
use crate::server::types::OffsetList;

/// The persistent state for a Key: this is the most up-to-date thing we have,
/// i.e. the most recent PUT or DELETE.  Dropped when the corresponding
/// journal entries are expired.
/// Includes most of same information as StreamHeader and StreamFooter, but does not
/// inline those structures because it would be duplicative to have the full key inside
/// KeyState as well as as the key in KeyMap.
/// (Immutable structure: written completely at the completion of a write request)
#[derive(Clone)]
pub struct PersistentValue {
    /// Reference to which journal chunks encode the entry for this value
    pub chunks: OffsetList,

    /// Fields describing current state of object, which depend on what most recent op was.
    pub op: PersistentValueOp,
}

#[derive(Clone)]
pub struct PutValue {
    pub last_modified: DateTime<Utc>,
    pub content_length: u64,
    pub etag: Output<md5::Md5>,
}

#[derive(Clone)]
pub struct DeleteValue {
    // Not strictly needed, but stored in case we are interested in checking
    // timestamp of deletion against anything else (memory is free because we're
    // in a union with PutValue)
    pub last_modified: DateTime<Utc>,
}

#[derive(Clone)]
pub enum PersistentValueOp {
    Put(PutValue),
    Delete(DeleteValue),
}

// Immutable structure: once you have one via an Arc, reads are always safe.
#[derive(Clone)]
pub struct KeyState {
    /// ALL the state for any given key is on the SAME journal.  That's a core
    /// purpose of what how+why we track this state, to ensure that we don't
    /// re-order writes to a key.
    pub journal_id: GlobalJournalId,

    /// There is always[1] either zero (on creation) or one (after first write
    /// is persistent, before expiry) persistent value.
    /// 1. During live operation, not during replay.  During replay, we instantiate
    /// KeyState in an already-peristent mode.
    pub persistent: Option<PersistentValue>,

    // Important: readers, having copied a value out of PeristentValue, must
    // separately contract with the Journal interface to ensure that the range
    // in question is not overwritten (i.e. that the journal tail doesn't advance
    // past it).
    /// The number of instances of ProjectionHandle alive
    /// (we need not store a list of them -- only need to know how many
    ///  are alive in order to keep KeyStates in existence and thereby
    ///  keep the key pinned to a particular JournalId to ensure write ordering)
    pub projected: usize,
}

// Projected state: an in progress write
// Persistent state: the most up to date thing we have (either a PUT or a DELETE)

// Readers touch no state other than the Arc reference count on the ObjectState
// they read.  They take a readguard to find the ObjectState, and then either
// drop out (if there's no persistent value), or take a reference to the

// FIXME: using inlined strings (SmallString) inside IndexKey is really nice for
// storing a huge KeyMap, but bloody painful on the BtreeMap interface for get(),
// because we have to copy out the whole key onto the stack (i.e. into an IndexKey)
// to pass into get().
// * If we define an IndexKeyShallow with &str arguments, then we can't still can't use this
// with BtreeMap, because they require that your key type is *borrowable* to your lookup type,
// which requires they have the same in-memory representation.
// * If we define a Cow<'a, str> type for the strings then that does potentially let us
// use &str types for lookup whilel storing clones of strings inside the key, BUT
// that leaves behind the SmallString inlining.
// * Using IndexKey is actually performance-adjacent to just concatenating
//   the bucket and key into a big String and using that :-/
// * Repeating the bucket name in every key entry is ram-wasteful anyway, as is
//   repeating the full key, rather than having a directory-like layout where
//   we just store the parts between slashes as keys.
// -> So the TODO here is to really totally rethink the design of the map into
//    someting less naive!

pub struct KeyMapShard {
    keys: RwLock<BTreeMap<IndexKey, KeyState>>,
    // Keys that are expired, by their latest chunk offset.
    // When we are notified that the tail has gone past the
    // last chunk of a key, we can maybe drop it from the
    // main `objects`
    pending_drop: RwLock<HashMap<GlobalJournalId, Mutex<BTreeMap<u64, IndexKey>>>>,
    // TODO: pending_drop's top level rwlock could be dispensed with if we knew
    // all GlobalJournalIds up front, which we could gather during the startup barrier
}

/// Generate a string which compares just greater than the input,
/// by incrementing the last character.  Input string may not be empty.
/// This is a pretty hacky thing to do, and longer term we might be better
/// off creating an IndexKey with some magic flags to act as a 'top of
/// bucket' comparator.
fn inc_string(s: &str) -> String {
    assert!(!s.is_empty());

    let mut c = s.chars().last().unwrap();
    c = std::char::from_u32(c as u32 + 1).unwrap_or(c);

    format!("{}{}", &s[0..s.len() - 1], c)
}

#[async_trait]
pub trait Shard: Send + Sync + 'static {
    async fn begin_write(
        &self,
        bucket: &str,
        key: &str,
        prefer_journal: GlobalJournalId,
    ) -> GlobalJournalId;

    async fn cancel_write(&self, bucket: &str, key: &str);

    async fn complete_write(&self, bucket: &str, key: &str, pval: PersistentValue);

    async fn get(&self, bucket: &str, key: &str) -> Option<(GlobalJournalId, PersistentValue)>;

    async fn replay(
        &self,
        journal_id: GlobalJournalId,
        bucket: &str,
        key: &str,
        pv: PersistentValue,
    );

    async fn expire(&self, journal_id: GlobalJournalId, bucket: &str, key: &str, last_offset: u64);

    async fn trim(&self, journal_id: GlobalJournalId, tail: u64);

    async fn list(
        &self,
        bucket: &str,
        cursor: Option<&ListCursor>,
        prefix: &str,
        delimiter: Option<&str>,
        max_keys: usize,
    ) -> (Vec<(String, PersistentValue)>, Vec<String>);

    async fn prefix_dirty(&self, bucket: &str, prefix: &str) -> bool;
}

impl KeyMapShard {
    pub fn new() -> Self {
        debug!("sizeof IndexKey: {}", size_of::<IndexKey>());

        Self {
            keys: RwLock::new(BTreeMap::new()),
            pending_drop: RwLock::new(HashMap::new()),
        }
    }

    pub fn len(&self) -> usize {
        self.keys.read().len()
    }

    fn with_dropmap<F, R>(&self, journal_id: &GlobalJournalId, callable: F) -> R
    where
        F: FnOnce(&mut BTreeMap<u64, IndexKey>) -> R,
    {
        // The pending_drop map's top level refers to journals by ID.  Usually
        // a journal will already be present, and we only need a shared read
        // lock on the top level.  In the rare initialization case, we have to upgrade
        // it to a write lock to insert the journal.

        let top_rlock = self.pending_drop.upgradable_read();
        match top_rlock.get(journal_id) {
            Some(m) => {
                let mut map = m.lock();
                callable(&mut map)
            }
            None => {
                let mut top_wlock = RwLockUpgradableReadGuard::upgrade(top_rlock);
                top_wlock
                    .entry(*journal_id)
                    .or_insert_with(|| Mutex::new(BTreeMap::new()));
                let top_rlock = RwLockWriteGuard::downgrade(top_wlock);
                let mut map = top_rlock.get(&journal_id).unwrap().lock();
                callable(&mut map)
            }
        }
    }

    fn drop(&self, journal_id: GlobalJournalId, ikey: IndexKey, last_chunk_offset: u64) {
        // OPTIMIZE: it's really nasty that the expirer will compete for the write
        // lock with actual latency-sensitive end user IO.  This is somewhat mitigated
        // by sharding the keyspace and by the expirer's self-throttling vs writes,
        // but still.  Expiring things is generally not urgent (it only blocks updating
        // the tail, nothing else).
        // It is not mandatory to expire things asap - if we leave something stale
        // then readers will just fail their attempt to lock the journal location and
        // fall back to proxying.
        //  - writers are only influenced inasmuchas they'll respect the JournalId
        //    of the existing KeyState, there's no correctness issue.
        //  - readers will discover their PersistentValue is invalid after they
        //    try to lock the location in the journal (the journal is responsible for
        //    noticing if they try and lock a location that is already expired), and
        //    fall back correctly to doing a passthrough read.
        //  - Obviously there is some extra memory consumption during the lag between
        //    advancing the tail and expiring the KeyMap entries.

        let mut map = self.keys.write();
        let mut entry = match map.entry(ikey) {
            Entry::Vacant(e) => {
                // A key being expired should always be in the KeyMap: it would have been
                // put there during replay.  However, in the event of something weird happening,
                // this should be harmless (if a key isn't in the map, we just don't need to
                // notify anyone that its journal data is being expired).
                warn!("Unexpected missing key={} in expire()", e.key());
                debug_assert!(false); // Crash out if we're testing, continue if prod.
                return;
            }
            Entry::Occupied(e) => e,
        };

        if entry.get().journal_id != journal_id {
            // This should never happen (Keys are sticky to a journal as long as they have
            // persistent data or projected requests) but this isn't severe enough to crash
            // for.  Drop out, and hope that the thing we're expiring is a phantom, and
            // the current KeyState is the valid thing.
            error!(
                "drop() key={} from journal={}, but existing KeyState is from journal={}",
                entry.key(),
                journal_id,
                entry.get().journal_id
            );
            debug_assert!(false); // Crash out if we're testing, continue if prod.
            return;
        }

        match &entry.get().persistent {
            None => {
                // Same as if we find no KeyState to begin with: this is unexpected, because
                // we should be expiring things that have previously completed their PUT and
                // been entered into the KeyMap (or superceded by something else persistent)
                warn!(
                    "Unexpected KeyState for key={}: persistent=None",
                    entry.key()
                );
                debug_assert!(false); // Crash out if we're testing, continue if prod.
                return;
            }
            Some(current_persistent) => {
                let current_last_location = current_persistent.chunks.last().unwrap().location;
                if current_last_location != last_chunk_offset {
                    // The Key's current peristent value is not the journal entry
                    // we're expiring, so nothing to do.
                    debug!(
                        "drop() journal={} key={}, retaining KeyState b/c it points to a different location {:#10x} (dropping {:10x})",
                        journal_id, entry.key(), current_last_location, last_chunk_offset
                    );
                    return;
                };
            }
        };

        // All checks passed: we have a persistent KeyState that needs expiring
        let entry_projected = entry.get().projected;
        if entry_projected == 0 {
            // Simply drop the KeyState: it has no persistent data to point to, and
            // no projected requests that would require us to keep it pinned to this JournalId
            debug!(
                "drop(): removing KeyState journal={} key={}",
                journal_id,
                entry.key(),
            );
            entry.remove();
        } else {
            // Another write operation is in flight!  Keep the KeyState in place (albeit with
            // no persistent value) and it will be populated by that other operation when
            // they complete (or dropped when they cancel).
            debug!(
                "drop(): retaining key={} for {} projected writes",
                entry.key(),
                entry_projected
            );
            entry.get_mut().persistent = None;
        }
    }
}

#[async_trait]
impl Shard for KeyMapShard {
    /// If KeyState doesn't already exist, create it with the preferred GlobalJournalId.  If
    /// it already exists and is on the same node as prefer_journal, then increment existing
    /// record's projected counter, and return the journal id.  If it already exists but
    /// the journal is on a different node than the preferred one, then return the journal id,
    /// but do not increment its projected counter.
    ///
    /// There is potential to optimize (skip) this in the single-journal case, where the answer
    /// to "which journal" is always the same, and there can be no concerns over write
    /// ordering between journals.
    async fn begin_write(
        &self,
        bucket: &str,
        key: &str,
        prefer_journal: GlobalJournalId,
    ) -> GlobalJournalId {
        let mut keys = self.keys.write();
        let key_state: &mut KeyState =
            keys.entry(IndexKey::new(bucket, key))
                .or_insert_with(|| KeyState {
                    projected: 0,
                    persistent: None,
                    journal_id: prefer_journal,
                });

        if key_state.journal_id.node_id == prefer_journal.node_id {
            // This key was on the node the caller wanted: they are going to write it.
            key_state.projected += 1;
        }

        return key_state.journal_id;
    }

    /// Called by ProjectionHandle if it is dropped without having been
    /// marked complete: indicates that something went wrong in the code
    /// handling a request
    async fn cancel_write(&self, bucket: &str, key: &str) {
        let mut map = self.keys.write();
        let ikey = IndexKey::new(bucket, key);
        let key_state = match map.get_mut(&ikey) {
            Some(ks) => ks,
            None => {
                panic!(
                    "cancel_write: missing key {}, should never drop projected keys!",
                    key
                );
            }
        };

        assert!(key_state.projected > 0);
        if key_state.projected == 1 && key_state.persistent.is_none() {
            // No persistent data and we were the only projection?  KeyState is now defunct.
            map.remove(&ikey);
        } else {
            // A request is in flight: keep the KeyState alive to maintain the pinning
            // of Key->Journal (and because the ProjectionHandle will want to touch this
            // KeyState when the request is complete)
            key_state.projected -= 1;
        }
    }

    async fn complete_write(&self, bucket: &str, key: &str, pval: PersistentValue) {
        let mut map = self.keys.write();
        let key_state = match map.get_mut(&IndexKey::new(bucket, key)) {
            Some(ks) => ks,
            None => {
                panic!(
                    "complete_write: missing key {}, should have been created in write_project!",
                    key
                );
            }
        };

        // Update projected state
        assert!(key_state.projected > 0);
        key_state.projected -= 1;

        // Update persistent state
        key_state.persistent = Some(pval)
    }

    /// IMPORTANT: the PersistentValue returned here is NOT guaranteed to refer to
    /// a readable location in the journal (i.e. it might already have been expired).
    /// The caller is responsible for passing the location onwards into ChunkJournal
    /// for locking, which will check it's a valid location.  If it isn't, that's
    /// equivalent to this function returning None.
    async fn get(&self, bucket: &str, key: &str) -> Option<(GlobalJournalId, PersistentValue)> {
        match self.keys.read().get(&IndexKey::new(bucket, key)) {
            None => None,
            Some(ks) => match &ks.persistent {
                Some(p) => Some((ks.journal_id, p.clone())),
                None => None,
            },
        }
    }

    async fn replay(
        &self,
        journal_id: GlobalJournalId,
        bucket: &str,
        key: &str,
        pv: PersistentValue,
    ) {
        let key_state = KeyState {
            journal_id,
            projected: 0,
            persistent: Some(pv),
        };

        // This is a simple insert, but use entry API so that we can do some
        // sanity checking in the replacement case.
        let ikey = IndexKey::new(bucket, key);

        let mut map = self.keys.write();
        let entry = map.entry(ikey);
        match entry {
            Entry::Vacant(e) => {
                e.insert(key_state);
            }
            Entry::Occupied(mut e) => {
                let old_journal_id = e.get().journal_id;
                if old_journal_id != journal_id {
                    // We should never be racing replays of the same key from different journals
                    // (strict write ordering per-key!).  If this happens it's a bug, but not
                    // one we want to crash in prod for.
                    error!(
                        "During replay, saw key={} on different journals {} and {}",
                        e.key(),
                        old_journal_id,
                        journal_id
                    );
                    debug_assert!(false);
                }

                e.insert(key_state);
            }
        };
    }

    // Call this once a write to the backend has completed successfully.
    async fn expire(&self, journal_id: GlobalJournalId, bucket: &str, key: &str, last_offset: u64) {
        self.with_dropmap(&journal_id, |dropmap| {
            let existing = dropmap.insert(last_offset, IndexKey::new(bucket, key));
            // Expirer should never call us twice for the same offset+key
            assert!(existing.is_none());
            debug!(
                "expire: {}:{} at {}:{:#10x} ({} pending trim this journal)",
                bucket,
                key,
                journal_id,
                last_offset,
                dropmap.len()
            );
        });
    }

    /// The caller must ensure that the expired location is *persistently* expired, i.e.
    /// the journal's persistent tail pointer has been safely written.  This is because
    /// removing the KeyState releases the pin of the key to a particular journal, so
    /// subsequent writes will go into a different journal, and we must ensure that
    /// the same Key is never replayable from two different jouranls in order to retain
    /// strict ordering of writes.
    async fn trim(&self, journal_id: GlobalJournalId, tail: u64) {
        let to_drop = self.with_dropmap(&journal_id, |dropmap| {
            let mut tmp = dropmap.split_off(&tail);
            mem::swap(&mut tmp, &mut *dropmap);
            tmp
        });

        let n_drop = to_drop.len();

        // OPTIMIZE: this is an unbounded N operation, and currently we thrash in
        // and out of each shard's lock on each key.  We should gather up all the keys
        // for each shard, and then grab the lock for the shard and dump them through
        // in batches (periodically yielding the lock and the task to let others progress)
        for (offset, index_key) in to_drop {
            self.drop(journal_id, index_key, offset);
        }

        debug!(
            "trim: dropping {} up to {}:{:#10x} after size={}",
            n_drop,
            journal_id,
            tail,
            self.len()
        );
    }

    async fn prefix_dirty(&self, bucket: &str, prefix: &str) -> bool {
        let keys = self.keys.read();

        // Not everything in the KeyMap is a dirty key: some
        // entries are one of:
        // - expired but not yet trimmed (i.e. contains valid data but we could ignore
        //   it and go to the backend if we wanted.
        // - projected but not yet dirty (i.e. a PUT started but didn't finish yet)
        // But for simplicity we do a pessimistic check that just says there are dirty
        // keys if there are any keys in the prefix - that way no need to cross ref with pending_drop.
        let mut iter = keys.range(IndexKey::new(bucket, prefix)..);
        while let Some((ik, _v)) = iter.next() {
            if ik.key.starts_with(prefix) && ik.bucket == bucket {
                return true;
            } else {
                // We have gone beyond the prefix or beyond the bucket
                break;
            }
        }

        false
    }

    /// Input ranges include the bucket, but results just include the key, because this
    /// function will always be used on bucket-local listings (e.g. S3 ListObjectsV2)
    /// max_keys is a target that may be exceeded
    /// This is a listing of *dirty* keys only: i.e. those we must contributed to
    /// a ListObjectsv2 response from the KeyMap, overlaid on keys from the backend.
    async fn list(
        &self,
        bucket: &str,
        cursor: Option<&ListCursor>,
        prefix: &str,
        delimiter: Option<&str>,
        max_keys: usize,
    ) -> (Vec<(String, PersistentValue)>, Vec<String>) {
        let lower_kbound = if let Some(cursor) = &cursor {
            if cursor.is_prefix {
                // Skip past all keys starting with the prefix
                Included(IndexKey::new(bucket, &inc_string(&cursor.cursor)))
            } else {
                // Start immediately after the key
                Excluded(IndexKey::new(bucket, &cursor.cursor))
            }
        } else {
            // Start from prefix
            Included(IndexKey::new(bucket, prefix))
        };

        let mut result_keys = Vec::new();
        let mut result_prefixes: HashSet<String> = HashSet::new();

        let keys = self.keys.read();
        for (k, v) in keys.range((lower_kbound, Bound::Unbounded)) {
            if k.bucket != bucket {
                debug!("get_dirty_keys: wrong bucket, dropping out {}", k);
                break;
            }
            if !k.key.starts_with(prefix) {
                debug!("get_dirty_keys: beyond prefix, dropping out {}", k);
                break;
            }
            if v.persistent.is_none() {
                // This is a projected write, not yet dirty, skip it.
                debug!("get_dirty_keys: projected, dropping {}", k);
                continue;
            }

            // This is a little inefficient when we hit a common prefix: we continue
            // to iterate through keys within the prefix and discount each one individually,
            // whereas we could drop out of our range
            if let Some(delimiter) = delimiter {
                if k.key.len() > prefix.len() {
                    if let Some(i) = k.key[prefix.len()..].find(delimiter) {
                        // Finding the first delimiter /after/ the prefix part of the key,
                        // then the candidate common_prefix match is the part from the start
                        // of the key up to+including that delimiter that we found.
                        let my_prefix = &k.key[0..prefix.len() + i + 1];
                        let pv = v.persistent.as_ref().unwrap();
                        if let PersistentValueOp::Put(_) = pv.op {
                            debug!(
                                "get_dirty_keys: taken as common_prefix {} ({})",
                                k, my_prefix
                            );
                            result_prefixes.insert(my_prefix.to_string());
                            // As soon as we have a common prefix to yield, drop out
                            // so that we will start iterating again from /after/
                            // the prefix, rather than proceeding through a bunch of
                            // keys within the prefix.
                            break;
                        } else {
                            // A delete which is within a prefix,
                            // so will not be included in output.
                            debug!("get_dirty_keys: skipping {} (prefix {})", k, my_prefix);
                            continue;
                        }
                    }
                }
            }

            // An un-prefixed key, or we have no delimiter: include the full key in the result
            debug!("get_dirty_keys: including {}", k);
            result_keys.push((k.key.to_string(), v.persistent.as_ref().unwrap().clone()));
            if result_keys.len() + result_prefixes.len() >= max_keys {
                break;
            }
        }

        (result_keys, result_prefixes.into_iter().collect())
    }
}

#[cfg(test)]
mod test {
    use super::inc_string;

    #[test]
    fn test_inc_string() {
        assert_eq!(inc_string("alice"), "alicf");
    }
}
