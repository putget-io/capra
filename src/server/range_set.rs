use std::collections::BTreeSet;
use std::fmt;
use std::fmt::Display;

pub struct RangeSet<T: Ord + Default + Copy + fmt::LowerHex> {
    items: BTreeSet<(T, T)>,

    // Fast lookup of lowest value, for pop_if
    min: Option<(T, T)>,
}

/// A specialized rangeset for tracking non overlapping IOs in flight
/// where deletions always span whole contiguous ranges.
impl<T: Ord + Default + Copy + fmt::LowerHex> RangeSet<T> {
    pub fn new() -> Self {
        Self {
            items: BTreeSet::new(),
            min: None,
        }
    }
    pub fn insert(&mut self, mut from: T, mut to: T) {
        assert!(to > from);
        if let Some((b1, b2)) = self.items.range(..(from, T::default())).next_back() {
            if *b2 == from {
                let v1 = *b1;
                let v2 = *b2;
                self.items.remove(&(v1, v2));
                from = v1;
            }
        }

        if let Some((v1, v2)) = self.items.range((to, T::default())..).next() {
            if *v1 == to {
                let v1 = *v1;
                let v2 = *v2;
                to = v2;
                self.items.remove(&(v1, v2));
            }
        }

        let inserted = self.items.insert((from, to));
        assert!(inserted);

        match &self.min {
            None => self.min = Some((from, to)),
            Some(prev) => {
                if from <= prev.0 {
                    self.min = Some((from, to))
                }
            }
        }
    }

    fn update_min(&mut self) {
        match self.items.iter().next() {
            None => self.min = None,
            Some((v1, v2)) => self.min = Some((*v1, *v2)),
        }
    }

    fn _delete(&mut self, from: T, to: T) {
        assert!(to > from);
        let removed = self.items.remove(&(from, to));
        self.update_min();
        assert!(removed);
    }

    fn _is_empty(&self) -> bool {
        self.items.is_empty()
    }

    fn _len(&self) -> usize {
        self.items.len()
    }

    // See if the first range starts at `expect` and if so, pop
    // it and return its upper value.
    pub fn pop_if(&mut self, expect: T) -> Option<T> {
        match self.min {
            None => None,
            Some((v1, v2)) => {
                if v1 == expect {
                    let r = self.items.remove(&(v1, v2));
                    assert!(r);
                    self.update_min();
                    Some(v2)
                } else {
                    None
                }
            }
        }
    }
}

impl<T: Ord + Default + Copy + fmt::LowerHex> Display for RangeSet<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        let mut first = true;
        for i in &self.items {
            if !first {
                write!(f, ",")?;
            } else {
                first = false;
            }
            write!(f, "{:#010x}..{:#010x}", i.0, i.1)?;
        }
        write!(f, "]")
    }
}

#[test]
fn test_simple() {
    let mut rs = RangeSet::<u64>::new();
    rs.insert(0x100, 0x200);
    assert!(!rs._is_empty());
    rs.insert(0x300, 0x400);
    assert_eq!(rs._len(), 2);
    rs.insert(0x200, 0x300);
    assert_eq!(rs._len(), 1);
    rs._delete(0x100, 0x400);
    assert!(rs._is_empty());
}

#[test]
fn test_pop() {
    let mut rs = RangeSet::<u64>::new();

    rs.insert(0x100, 0x200);
    assert_eq!(rs.pop_if(0x50), None);
    assert_eq!(rs.pop_if(0x150), None);
    assert_eq!(rs.pop_if(0x100), Some(0x200));

    rs.insert(0x300, 0x400);
    assert_eq!(rs.pop_if(0x250), None);
    assert_eq!(rs.pop_if(0x350), None);
    assert_eq!(rs.pop_if(0x300), Some(0x400));

    rs.insert(0x100, 0x200);
    rs.insert(0x0, 0x100);
    assert_eq!(rs.pop_if(0x0), Some(0x200));

    rs.insert(0x0, 0x100);
    rs.insert(0x100, 0x200);
    assert_eq!(rs.pop_if(0x0), Some(0x200));
}
