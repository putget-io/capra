use std::path::Path;
use std::process::exit;
use std::sync::Arc;

use log::*;

use crate::server::cluster_map::ClusterMap;
use crate::server::options::ServerOptions;

mod backend;
mod core_tracker;
mod decoder;
mod frontend;
mod key_map;
mod range_set;
#[cfg(test)]
mod test;
mod types;

// Persistence code is public for the format tool to use
pub mod io_engine;
pub mod journal;

mod cluster_map;
mod dcache;
mod options;
mod process;
pub mod s3_types;
mod supervisor;

pub fn main(args: &[String]) {
    let options = ServerOptions::parse(args).unwrap_or_else(|e| {
        error!("Invalid Argument: {}", e);
        exit(-1);
    });

    let cluster_map = Arc::new(match &options.cluster_map {
        Some(path_str) => {
            let r = ClusterMap::from_file(&Path::new(&path_str));
            match r {
                Err(e) => {
                    error!("Cluster Map error: {:?}", e);
                    exit(-1);
                }
                Ok(cm) => cm,
            }
        }
        None => ClusterMap::new_local(),
    });

    // IOEngine autoselection requires a path to probe for functionality: make the reasonable
    // assumption that all journal shards will be of the same type and on the same filesystem
    // if they're not block devices, so just pass in the first one.
    let exemplar_shard = &options.journal_shards[0];
    let io_builder = match io_engine::get_builder(&options.engine, Path::new(exemplar_shard)) {
        Ok(e) => e,
        Err(e) => {
            error!("Failed to initialize I/O engine: {}", e);
            exit(-1);
        }
    };

    let r = if cluster_map.get_nodes().len() < 2 {
        supervisor::LocalSupervisor::new(options, cluster_map, io_builder).block()
    } else {
        if cluster_map.am_leader() {
            let sv = supervisor::LeaderSupervisor::new(options, cluster_map, io_builder);
            supervisor::LeaderSupervisor::block(sv)
        } else {
            let sv = supervisor::FollowerSupervisor::new(options, cluster_map, io_builder);
            supervisor::FollowerSupervisor::block(sv)
        }
    };

    if r.is_err() {
        exit(-1);
    }
}
