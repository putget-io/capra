#[macro_use]
mod util;

mod bench;
mod format;
mod server;

use env_logger::TimestampPrecision;
use log::LevelFilter;
use std::env;
use std::process::exit;

fn usage(program_name: &str) {
    println!("Usage: {} <server|bench|format> ... ", program_name);
    println!("See {} <server|bench|format> --help for more", program_name);
}

fn main() {
    env_logger::builder()
        .format_timestamp(Some(TimestampPrecision::Micros))
        .filter_level(LevelFilter::Warn)
        .parse_env("CAPRA_LOG")
        .init();

    let strings: Vec<String> = env::args().collect();

    let program_name = &strings[0];
    if strings.len() < 2 {
        usage(program_name);
        exit(-1);
    }

    let mode: &str = &strings[1];
    let mode_args = &strings[2..];
    match mode {
        "server" => {
            server::main(mode_args);
        }
        "bench" => {
            bench::main(mode_args).expect("Error running bench");
        }
        "format" => {
            format::main(mode_args);
        }
        _ => {
            usage(program_name);
            exit(-1)
        }
    }
}
