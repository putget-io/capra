#[macro_use]
pub mod grpc;
pub mod http;

// These used to live here, link them through for convenience
pub use capralib::{auth, digest, format};

// Round up to the nearest interval of 'align'
// (align must be a power of two)
pub fn round_to_pow2(input: usize, align: usize) -> usize {
    if input & (align - 1) > 0 {
        input + align & (!(align - 1))
    } else {
        input
    }
}
