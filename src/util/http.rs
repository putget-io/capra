use std::convert::Infallible;

use capralib::digest::{parse_md5, Md5Digest};
use http::{Response, StatusCode};
use hyper::{Body, HeaderMap};

use log::warn;

/// When you want to get a header and will treat a non-integer value as absent
pub fn _get_header_int(header_map: &HeaderMap, key: &str) -> Option<u64> {
    match header_map.get(key) {
        Some(v) => v.to_str().ok().and_then(|v| v.parse().ok()),
        None => None,
    }
}

/// When you want to get a header and will treat an un-decodable value as absent
pub fn get_header_str<'a>(header_map: &'a HeaderMap, key: &'a str) -> Option<&'a str> {
    header_map.get(key).and_then(|hv| hv.to_str().ok())
}

// Read the binary representation of ETag out of an HTTP response header,
// for cases where we want to look it up in a structure (like DCache) that
// uses the binary form.
pub fn get_etag<T>(response: &Response<T>) -> Option<Md5Digest> {
    let etag = response
        .headers()
        .get("ETag")
        .map(|hv| hv.to_str().unwrap_or(""));
    if let Some(etag) = etag {
        // Tolerate either quoted (correct) or unquoted
        let etag = if etag.len() == 34 { &etag[1..33] } else { etag };

        if let Ok(etag) = parse_md5(etag) {
            Some(etag)
        } else {
            warn!("Malformed ETag header '{}'", etag);
            None
        }
    } else {
        None
    }
}

pub fn empty_response(status: StatusCode) -> Result<Response<Body>, Infallible> {
    return Ok(Response::builder()
        .status(status)
        .body(Body::empty())
        .unwrap());
}

pub fn status_response(status: StatusCode) -> Response<Body> {
    Response::builder()
        .status(status)
        .body(Body::empty())
        .unwrap()
}
