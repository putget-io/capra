use tonic::Status;

/// Convenience to enable required_field()? in grpc handlers
/// when extracting fields that should return errors when missing.
///
/// Protobuf 3 doesn't implement required fields, to make future changes
/// possible: only use required_field when it doesn't make sense to
/// handle an absent value.
pub fn required_field<T>(opt: Option<T>) -> Result<T, Status> {
    opt.map(|v| v)
        .ok_or(Status::invalid_argument("Missing required field"))
}

//#[macro_export]
macro_rules! decode_enum {
    ($a:ty, $b:expr) => {
        <$a>::from_i32($b).ok_or_else(|| ::tonic::Status::invalid_argument("Invalid enum value"))?
    };
}
