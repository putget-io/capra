use args::Args;
use getopts::Occur;
use log::*;
use std::env;
use std::process::exit;

use bytefmt;

use crate::server::io_engine::FileType::DoesNotExist;
use crate::server::io_engine::{resolve_path, FileType};
use crate::server::journal::chunk_journal::{ChunkJournalInner, DEFAULT_JOURNAL_SIZE};
use crate::server::journal::io_buffer::IO_ALIGN;
use ioctls::blkgetsize64;
use std::fs::OpenOptions;
use std::io::ErrorKind;
use std::os::unix::io::AsRawFd;
use std::path::PathBuf;

/// A utility to prepare block devices for use as journal shards
pub fn main(input_args: &[String]) {
    let mut args = Args::new(&env::args().nth(0).unwrap()[..], "Formatter");
    args.option(
        "s",
        "size",
        "Size of journal (file size if creating, else range of existing file/device to use",
        "SIZE",
        Occur::Optional,
        None,
    );

    args.option(
        "p",
        "path",
        "Path to file or block device",
        "PATH",
        Occur::Req,
        None,
    );

    args.parse(input_args).expect("Bad argument");

    let path: PathBuf = args.value_of("path").unwrap();
    let size = (args.optional_value_of("size").unwrap() as Option<String>).map(|s| {
        // Parse out size string and clamp it to aligned size
        bytefmt::parse(s).expect("Invalid size") & !(IO_ALIGN as u64 - 1)
    });

    match do_format(&path, size) {
        Err(e) => {
            error!("Failed to format at {}: {}", path.display(), e);
            exit(-1);
        }
        Ok(()) => {}
    }
}

pub fn do_format(path: &PathBuf, size: Option<u64>) -> std::io::Result<()> {
    let file_type = resolve_path(path)?;

    if file_type == FileType::Regular {
        // While we *could* go ahead and 'format' an existing regular file, it's
        // a risky thing to do: if someone wanted this we expect they would have
        // removed the existing file to recreate it.  The only existing files
        // we are happy to overwrite are block devices.
        return Err(std::io::Error::from(ErrorKind::AlreadyExists));
    }

    let size = match size {
        Some(size) => size,
        None => {
            if file_type == FileType::Block {
                let fd = OpenOptions::new().read(true).open(path)?;
                let mut blksz: u64 = 0;
                let r = unsafe { blkgetsize64(fd.as_raw_fd(), &mut blksz) };
                if r != 0 {
                    return Err(std::io::Error::from_raw_os_error(r));
                }
                info!(
                    "Read block device size {} for {}",
                    bytefmt::format(blksz),
                    path.display()
                );
                blksz
            } else {
                DEFAULT_JOURNAL_SIZE
            }
        }
    };

    if file_type == DoesNotExist {
        // A path that doesn't exist, create a journal file
        ChunkJournalInner::create(&path, size)?;
    } else {
        // A block device, or a file that already existed
        println!(
            "Formatting {} to size {}",
            path.display(),
            bytefmt::format(size)
        );
        ChunkJournalInner::format(&path, size)?;
    }

    Ok(())
}
