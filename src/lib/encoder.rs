use futures::Stream;
use log::*;
use partial_min_max::min;
use std::mem;
use std::pin::Pin;
use std::task::{Context, Poll};

use hyper::body::Bytes;

use super::s3_client_auth::{ClientAuthKeyState, ClientAuthRequestState};
use crate::auth::sign_string;
use crate::auth::{get_chunk_string_to_sign, AuthError};
use crate::digest::{Sha256, Sha256DigestStr};
use ::digest::Digest;

#[derive(Debug)]
pub enum DecodeErrorKind {
    Hyper(hyper::Error),
    Auth(AuthError),
    System(std::io::Error),
    Internal,
}

pub struct DecodeError {
    pub kind: DecodeErrorKind,
}

impl From<hyper::Error> for DecodeError {
    fn from(e: hyper::Error) -> Self {
        Self {
            kind: DecodeErrorKind::Hyper(e),
        }
    }
}

impl From<AuthError> for DecodeError {
    fn from(e: AuthError) -> Self {
        Self {
            kind: DecodeErrorKind::Auth(e),
        }
    }
}

impl From<std::io::Error> for DecodeError {
    fn from(e: std::io::Error) -> Self {
        Self {
            kind: DecodeErrorKind::System(e),
        }
    }
}

impl std::fmt::Debug for DecodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self.kind)
    }
}

impl std::fmt::Display for DecodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self.kind)
    }
}

impl std::error::Error for DecodeError {}

pub struct ChunkedEncoder<S: Stream<Item = Result<Bytes, DecodeError>>> {
    inner: Pin<Box<S>>,
    chunk_size: usize,
    // FIXME: if re-encoding an incoming chunked PUT during proxy(), we should
    // calculate the SHA256 once and re-use it, instead of calculating it both
    // during un-chunking and re-chunking.
    content_hash: Sha256,
    content_cursor: u64,
    total_length: u64,
    buffer: Vec<u8>,
    prev_chunk_sig: Sha256DigestStr,
    daily_auth_state: ClientAuthKeyState,
    request_auth_state: ClientAuthRequestState,

    sealed: bool,

    complete_chunks_tx: std::sync::mpsc::Sender<Result<Bytes, DecodeError>>,
    complete_chunks_rx: std::sync::mpsc::Receiver<Result<Bytes, DecodeError>>,
}

impl<S: Stream<Item = Result<Bytes, DecodeError>>> futures::Stream for ChunkedEncoder<S> {
    type Item = Result<Bytes, DecodeError>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        if let Ok(next_val) = self.complete_chunks_rx.try_recv() {
            return Poll::Ready(Some(next_val));
        }

        // Loop until we get a value to yield, or the input stream
        // goes Pending/None.
        loop {
            match self.inner.as_mut().poll_next(cx) {
                Poll::Ready(Some(Err(e))) => return Poll::Ready(Some(Err(e))),
                Poll::Ready(Some(Ok(bytes))) => {
                    self.write(&bytes[..]);
                }
                Poll::Ready(None) => return Poll::Ready(None),
                Poll::Pending => return Poll::Pending,
            };

            if let Ok(next_val) = self.complete_chunks_rx.try_recv() {
                return Poll::Ready(Some(next_val));
            }
        }
    }
}

impl<S: Stream<Item = Result<Bytes, DecodeError>>> ChunkedEncoder<S> {
    pub fn new(
        inner: S,
        daily_auth_state: ClientAuthKeyState,
        request_auth_state: ClientAuthRequestState,
        seed_sig: &str,
        total_length: u64,
        chunk_size: usize,
    ) -> Self {
        let mut buffer = Vec::with_capacity(chunk_size);
        buffer.resize(chunk_size, 0);
        let (complete_chunks_tx, complete_chunks_rx) =
            std::sync::mpsc::channel::<Result<Bytes, DecodeError>>();

        Self {
            inner: Box::pin(inner),
            chunk_size,
            content_hash: Sha256::new(),
            content_cursor: 0,
            total_length,
            buffer,
            prev_chunk_sig: Sha256DigestStr::from_str(seed_sig),
            daily_auth_state,
            request_auth_state,
            sealed: false,
            complete_chunks_tx,
            complete_chunks_rx,
        }
    }

    fn buffer_cursor(&self) -> usize {
        (self.content_cursor % self.chunk_size as u64) as usize
    }

    fn swap(&mut self) -> Vec<u8> {
        let mut buffer = Vec::with_capacity(self.chunk_size);
        buffer.resize(self.chunk_size, 0);
        mem::swap(&mut self.buffer, &mut buffer);
        buffer
    }

    fn flush(&mut self, this_chunk_size: usize) {
        debug!("ChunkedEncoder.flush");

        let content_hash = mem::replace(&mut self.content_hash, Sha256::new());
        let chunk_hash = content_hash.finalize();

        let string_to_sign = get_chunk_string_to_sign(
            &self.request_auth_state.x_amz_date,
            &self.daily_auth_state.scope,
            &self.prev_chunk_sig,
            chunk_hash,
        );

        let chunk_signature = sign_string(&string_to_sign, self.daily_auth_state.signing_key);

        let header = format!(
            "{:x};chunk-signature={}\r\n",
            this_chunk_size, &chunk_signature
        )
        .into_bytes();

        let footer = ("\r\n").as_bytes();

        let buffer = self.swap();
        let body = &buffer[0..this_chunk_size];

        // TODO: compose directly into a vector with header+footer spaces, to
        // avoid this copy.
        let mut output = Vec::with_capacity(header.len() + body.len() + footer.len());
        output.extend_from_slice(&header[..]);
        output.extend_from_slice(body);
        output.extend_from_slice(footer);
        self.complete_chunks_tx
            .send(Ok(Bytes::from(output)))
            .expect("Rx closed?");
        debug!(
            "ChunkedEncoder.flush: {} bytes signature={}",
            this_chunk_size, chunk_signature
        );

        self.prev_chunk_sig = chunk_signature;
    }

    fn seal(&mut self) {
        debug!("ChunkedEncoder.seal");
        // Should only be called once we have seen all our data
        assert_eq!(self.content_cursor, self.total_length);

        // Trick flush() into seeing size as zero.
        self.content_cursor = 0;

        self.sealed = true;

        // Emit an empty chunk.
        self.flush(0)
    }

    fn write(&mut self, bytes: &[u8]) {
        assert!(!self.sealed);

        let mut input_cursor: usize = 0;

        // While we have input bytes
        while input_cursor < bytes.len() {
            let buffer_cursor = self.buffer_cursor();
            assert!(buffer_cursor <= self.chunk_size);
            let take_bytes = min(bytes.len() - input_cursor, self.chunk_size - buffer_cursor);

            debug!(
                "ChunkedEncoder.write: taking {} Input cursor {}/{} Buffer cursor {}/{}",
                take_bytes,
                input_cursor,
                bytes.len(),
                buffer_cursor,
                self.buffer.len()
            );

            self.buffer[buffer_cursor..buffer_cursor + take_bytes]
                .copy_from_slice(&bytes[input_cursor..input_cursor + take_bytes]);

            self.content_hash
                .update(&self.buffer[buffer_cursor..buffer_cursor + take_bytes]);
            input_cursor += take_bytes;
            self.content_cursor += take_bytes as u64;

            debug!(
                "ChunkedEncoder.write: Progress {}/{}",
                self.content_cursor, self.total_length
            );

            if self.content_cursor == self.total_length {
                // All done!  Flush data, then write a zero sized chunk
                // as required by protocol.
                let chunk_size = if self.content_cursor % self.chunk_size as u64 == 0 {
                    self.chunk_size
                } else {
                    self.buffer_cursor()
                };
                self.flush(chunk_size);
                self.seal();
            } else if self.content_cursor % self.chunk_size as u64 == 0 {
                // We have filled up our buffer, flush it out as a signed chunk.
                self.flush(self.chunk_size);
            }
        }
    }
}
