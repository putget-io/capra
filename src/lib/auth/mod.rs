//mod chunked_encode_stream;

use std::fmt::Write;
use std::result::Result;

use super::digest::Sha256;
use hmac;
use hmac::{Mac, NewMac};
use http::request::Parts;
use log::*;
use smallstr::SmallString;
use smallvec::SmallVec;

use super::digest;
use super::digest::{sha_str, Sha256Digest, Sha256DigestStr};
use crate::digest::sha_eq;
use ::digest::{Digest, Output};
use hyper::Uri;
use percent_encoding::CONTROLS;
use std::cell::RefCell;
use std::collections::BTreeMap;

// Using BtreeMap for efficiency when we have just a single entry
// (FIXME: we should use some kind of packed/baked map structure, as typically
//  we'll have a largely static, probably quite small set of elements)
pub type SecretMap = BTreeMap<String, String>;

// This is the magic value in the AWS/S3 protocol to be
// used when a requestor chooses not to sign their payload.
const UNSIGNED_PAYLOAD: &str = "UNSIGNED-PAYLOAD";

// Magic X-Amz-Content-Sha256 value used to indicate that HMACs will
// be inline with a chunked payload.
pub const STREAMING_PAYLOAD: &str = "STREAMING-AWS4-HMAC-SHA256-PAYLOAD";

type Sha256Hmac = hmac::Hmac<Sha256>;

// Set of characters forbidden in query strings in canonical requests
// From https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html:
// """
// * Do not URI-encode any of the unreserved characters that RFC 3986
//   defines: A-Z, a-z, 0-9, hyphen ( - ), underscore ( _ ), period ( . ), and tilde ( ~ ).
// * Percent-encode all other characters with %XY, where X and Y are hexadecimal characters (0-9 and uppercase A-F). For example, the space character must be encoded as %20 (not using '+', as some encoding schemes do) and extended UTF-8 characters must be in the form %XY%ZA%BC.
// * Double-encode any equals ( = ) characters in parameter values.
// """

pub const QUERY_ENCODE_CHARS: &percent_encoding::AsciiSet = &percent_encoding::NON_ALPHANUMERIC
    .remove(b'=')
    .remove(b'-')
    .remove(b'_')
    .remove(b'.')
    .remove(b'~');

pub const KEY_ENCODE_CHARS: &percent_encoding::AsciiSet = &percent_encoding::NON_ALPHANUMERIC
    .remove(b'/')
    .remove(b'-')
    .remove(b'_')
    .remove(b'.')
    .remove(b'~');

/// Sort the query string from an URI, as required by AWSv4 when generating
/// a canonical request.
/// Fast in the normal case of URIs with no query string, else slow and allocates.
pub fn maybe_rewrite_query<'a>(rewritten: &'a mut String, input: &'a str) -> &'a str {
    if input.is_empty() {
        input
    } else {
        trace!("Rewriting query string, before: {}", input);
        let mut query_parts: SmallVec<[&str; 4]> = input.split("&").collect();
        query_parts[..].sort();

        let encoded: Vec<String> = if is_percent_encoded(input) {
            query_parts.iter().map(|q| q.to_string()).collect()
        } else {
            // TODO: rewrite equals in value part to double equals
            query_parts
                .iter()
                .map(|q| {
                    percent_encoding::percent_encode(q.as_bytes(), &QUERY_ENCODE_CHARS).to_string()
                })
                .collect()
        };
        *rewritten = encoded.join("&");
        trace!("Rewriting query string, after:  {}", rewritten);
        rewritten
    }
}

struct StringStream(String);

impl StringStream {
    fn new() -> Self {
        Self(String::new())
    }
}

impl std::io::Write for StringStream {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.0.push_str(&String::from_utf8_lossy(buf));
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

fn is_percent_encoded(raw: &str) -> bool {
    let decoded = percent_encoding::percent_decode(raw.as_bytes());
    let a = decoded.count();
    a != raw.len()
}

pub fn canonical_uri(uri: &Uri) -> percent_encoding::PercentEncode<'_> {
    let raw_path = uri.path();

    // Depressingly, the only way to check if a string is already URL-encoded is to try decoding it,
    // and clients can give us either encoded or raw URLs.
    if is_percent_encoded(raw_path) {
        percent_encoding::percent_encode(raw_path.as_bytes(), CONTROLS)
    } else {
        let encoded = percent_encoding::percent_encode(raw_path.as_bytes(), &KEY_ENCODE_CHARS);
        encoded
    }
}

/// Write the canonical request WITHOUT the last line (the content hash),
/// which should be written later as either a content hash or the magic
/// value for chunked bodies.
fn write_canonical_request<W: std::io::Write>(
    out: &mut W,
    header: &Parts,
    canonical_headers: &str,
    signed_header_names: &str,
) {
    // A String slot to rewrite the query string if necessary.
    // Performance: This only allocates if we actually do the rewrite. Uusually for an already-slow
    // request like a ListObjectsV2.  PutObject never has query parms, and GetObject usually
    // doesn't.
    let mut rewritten_canonical_query_string = String::new();
    let canonical_query_string = maybe_rewrite_query(
        &mut rewritten_canonical_query_string,
        header.uri.query().unwrap_or(""),
    );

    write!(
        out,
        r#"{http_request_method}
{canonical_uri}
{canonical_query_string}
{canonical_headers}

{signed_headers}
"#,
        http_request_method = header.method.as_str(),
        canonical_uri = canonical_uri(&header.uri),
        canonical_query_string = canonical_query_string,
        canonical_headers = canonical_headers,
        signed_headers = signed_header_names
    )
    .expect("Error writing canonical request");

    trace!("Canonical Request:");
    trace!(
        r#"{http_request_method}
{canonical_uri}
{canonical_query_string}
{canonical_headers}

{signed_headers}
"#,
        http_request_method = header.method.as_str(),
        canonical_uri = canonical_uri(&header.uri),
        canonical_query_string = canonical_query_string,
        canonical_headers = canonical_headers,
        signed_headers = signed_header_names
    );
}

/// Write the CanonicalHeaders part of a canonical request
fn write_canonical_headers<W: std::io::Write>(
    out: &mut W,
    header: &Parts,
    signed_headers: &SmallVec<[SmallString<[u8; 32]>; 4]>,
) -> Result<(), AuthError> {
    for (i, http_header) in signed_headers.iter().enumerate() {
        if let Some(header_val) = header.headers.get(http_header.as_str()) {
            if i == signed_headers.len() - 1 {
                // Last one, no trailing newline
                write!(out, "{}:{}", http_header, header_val.to_str().unwrap())
                    .expect("Header write error")
            } else {
                write!(out, "{}:{}\n", http_header, header_val.to_str().unwrap())
                    .expect("Header write error")
            }
        } else {
            return Err(AuthError::new(format!(
                "Missing signed header {}",
                http_header
            )));
        }
    }

    Ok(())
}

// AWS auth is based on HMAC of both headers and
// body of an HTTP request.
// When processing request, we have an initial step
// that loads headers, and then subsequent steps that
// happen later with buffer data.
// Although this class is mainly about authorization, it is
// also where we validate content hashes (as the hash is used
// for both request correctness and authentication).
pub struct AuthorizedRequest {
    finalized: bool,

    // x-amz-content-sha256
    claimed_content_hash: Option<Sha256DigestStr>,

    // authorization[Signature]
    signature: Sha256DigestStr,

    // Our running hash of content, either of the whole request
    // or of the current chunk if doing chunked encoding.
    content_hash: Option<Sha256>,

    // Cache signing key because in chunked encoding it is used many times
    // TODO: perhaps even cache this across requests: the timestamp in it
    // only has second resolution, so for a single client doing 1000s of requests,
    // they will re-use the same signing key 1000s of times.
    signing_key: Option<Sha256Digest>,

    // Fields for chunked encodings
    prev_chunk_sig: Sha256DigestStr,

    canonical_request_ctx: Option<Sha256>,

    // authorization[Credential][0]
    secret_id: SmallString<[u8; 128]>,

    // authorization[Credential][1..]
    credential_scope: SmallString<[u8; 64]>,

    // x-amz-date
    amzdate: SmallString<[u8; 16]>,
}

#[derive(Debug)]
pub struct AuthError {
    reason: String,
}

impl AuthError {
    fn new(reason: String) -> AuthError {
        return AuthError { reason };
    }

    pub fn get_reason(&self) -> &String {
        &self.reason
    }
}

const MAX_ACCESS_KEY_ID: usize = 128;

fn sign(inkey: &[u8], indata: &[u8]) -> Sha256Digest {
    let mut ctx = Sha256Hmac::new_from_slice(inkey).unwrap();
    ctx.update(indata);
    ctx.finalize().into_bytes().into()
}

struct CachedSigningKey {
    signing_key: Sha256Digest,
    // e.g. 20200101
    date: SmallVec<[u8; 8]>,
    // e.g. ap-northeast-3
    region: SmallVec<[u8; 14]>,
    // e.g. s3
    service: SmallVec<[u8; 2]>,
    // Variable length: use leftover bytes after rounding up to 96
    secret: SmallVec<[u8; 40]>,
}

// A typical application will do many requests within the same second using the same
// credentials -- the vast majority of these will have the same signing key
// from one request to the next.
thread_local! (static CACHED_SIGNING_KEY: RefCell<Option<CachedSigningKey>> = RefCell::new(None));

pub fn get_signing_key(secret: &[u8], date: &[u8], region: &[u8], service: &[u8]) -> Sha256Digest {
    let cached_key = CACHED_SIGNING_KEY.with(|rc| {
        let rc = rc.borrow();
        if let Some(cached) = &*rc {
            if cached.date.as_slice() == date
                && cached.secret.as_slice() == secret
                && cached.region.as_slice() == region
                && cached.service.as_slice() == service
            {
                Some(cached.signing_key)
            } else {
                None
            }
        } else {
            None
        }
    });

    if let Some(key) = cached_key {
        // Cache hit
        key
    } else {
        // Cache miss, calculate the signing key
        let mut extended_secret: SmallVec<[u8; MAX_ACCESS_KEY_ID + 4]> = SmallVec::new();

        extended_secret.extend_from_slice("AWS4".as_bytes());
        extended_secret.extend_from_slice(secret);

        let key = sign(
            &sign(
                &sign(&sign(&extended_secret[..], date)[..], region)[..],
                service,
            )[..],
            "aws4_request".as_bytes(),
        );

        CACHED_SIGNING_KEY.with(|rc| {
            let mut rc = rc.borrow_mut();
            *rc = Some(CachedSigningKey {
                secret: SmallVec::from(secret),
                date: SmallVec::from(date),
                region: SmallVec::from(region),
                service: SmallVec::from(service),
                signing_key: key,
            })
        });

        key
    }
}

/// An example in testing is of length 133.  A 256 byte buffer should be enough
/// to handle typical traffic without incurrent an allocation.
pub fn get_string_to_sign(
    amzdate: &str,
    scope: &str,
    canonical_request_digest: &Sha256Digest,
) -> SmallString<[u8; 256]> {
    let mut r = SmallString::new();
    write!(
        &mut r,
        r#"AWS4-HMAC-SHA256
{amzdate}
{scope}
{canonical_request_digest}"#,
        amzdate = amzdate,
        scope = scope,
        canonical_request_digest = sha_str(canonical_request_digest)
    )
    .expect("Failed to format in get_string_to_sign");
    r
}

pub fn get_chunk_seed_signature(
    signing_key: &[u8],
    canonical_request_digest: &Sha256Digest,
    amzdate: &str,
    scope: &str,
) -> Sha256DigestStr {
    let string_to_sign = format!(
        r#"AWS4-HMAC-SHA256
{amzdate}
{scope}
{canonical_request_digest}"#,
        amzdate = amzdate,
        scope = scope,
        canonical_request_digest = sha_str(canonical_request_digest)
    );
    debug!(
        "Seed StringToSign: {} bytes: {}",
        string_to_sign.len(),
        string_to_sign
    );

    digest::sha_str(&sign(signing_key, string_to_sign.as_bytes()))
}

pub fn get_chunk_string_to_sign(
    amzdate: &str,
    scope: &str,
    prev_chunk_sig: &str,
    chunk_hash: Output<Sha256>,
) -> String {
    let chunk_hash: Sha256Digest = chunk_hash.into();
    format!(
        r#"AWS4-HMAC-SHA256-PAYLOAD
{amzdate}
{scope}
{prev_sig}
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
{chunk_hash_str}"#,
        amzdate = amzdate,
        scope = scope,
        prev_sig = prev_chunk_sig,
        chunk_hash_str = sha_str(&chunk_hash)
    )
}

pub fn sign_string(string_to_sign: &str, signing_key: Sha256Digest) -> Sha256DigestStr {
    digest::sha_str(&sign(&signing_key, string_to_sign.as_bytes()))
}

pub trait Authorizer: Send + Sync {
    fn new(header: &Parts, secrets: &SecretMap) -> Result<Self, AuthError>
    where
        Self: Sized;
    fn finalize(&mut self) -> Result<(), AuthError>;
    fn update(&mut self, bytes: &[u8]);
    fn update_chunked(&mut self, chunk_sig: String) -> Result<(), AuthError>;

    fn get_secret_id(&self) -> Option<&str>;

    // Enabled means this authorizer is really doing something.  PUT handler
    // needs to know this to work out whether to bother running concurrently with other checksums.
    fn enabled(&self) -> bool;

    // For robustness: if a function thinks that a request should already have
    // been authenticated, it can use this in an assertion.
    fn finalized(&self) -> bool;
}

pub struct UnsignedRequest(bool);

impl Authorizer for UnsignedRequest {
    fn new(_header: &Parts, _secrets: &SecretMap) -> Result<Self, AuthError> {
        Ok(Self(false))
    }

    fn finalize(&mut self) -> Result<(), AuthError> {
        self.0 = true;
        Ok(())
    }
    fn update(&mut self, _bytes: &[u8]) {}
    fn update_chunked(&mut self, _chunk_sig: String) -> Result<(), AuthError> {
        Ok(())
    }

    fn get_secret_id(&self) -> Option<&str> {
        None
    }

    fn enabled(&self) -> bool {
        false
    }

    fn finalized(&self) -> bool {
        self.0
    }
}

impl AuthorizedRequest {
    fn calc_signing_key(&mut self, secret: &str) {
        //HMAC(HMAC(HMAC(HMAC("AWS4" + kSecret,"20150830"),"us-east-1"),"iam"),"aws4_request")

        // We need credential scope in both string-concatenated form (elsewhere) and in
        // separated form (here).  A split is pretty cheap as long as we're just
        // stepping through &strs.
        let mut scope_parts = self.credential_scope.split("/");

        debug!("calc_signing_key for {}", self.secret_id);
        self.signing_key = Some(get_signing_key(
            secret.as_bytes(),
            scope_parts.next().unwrap().as_bytes(),
            scope_parts.next().unwrap().as_bytes(),
            scope_parts.next().unwrap().as_bytes(),
        ));
    }

    fn finalize_nonchunked(&mut self) -> Result<(), AuthError> {
        let content_hash = self.content_hash.take().unwrap();
        let mut content_hash_str = sha_str(content_hash.finalize().as_ref());

        // Did the requestor include a hash? Validate that the
        // content matched it.
        if let Some(ref request_hash) = self.claimed_content_hash {
            if request_hash == UNSIGNED_PAYLOAD {
                // If caller has opted not to sign their payload, we will respect that
                // in our signature calculation.  The header signature calculation still
                // needs to happen.
                content_hash_str = SmallString::from_str(UNSIGNED_PAYLOAD);
            } else if content_hash_str != *request_hash {
                error!(
                    "Finalize Hash Claimed/Actual: {}/{}",
                    request_hash, content_hash_str
                );
                return Err(AuthError::new("Invalid content hash".to_string()));
            }
        }

        let mut canonical_request_ctx = self.canonical_request_ctx.take().unwrap();
        canonical_request_ctx.update(content_hash_str.as_bytes());
        let canonical_request_digest = canonical_request_ctx.finalize().into();

        let string_to_sign = get_string_to_sign(
            &self.amzdate,
            &self.credential_scope,
            &canonical_request_digest,
        );

        let signing_key = self.signing_key.as_ref().unwrap();

        let signature = sign(signing_key, string_to_sign.as_bytes());
        if !sha_eq(&signature, &self.signature) {
            Err(AuthError::new("Request Signature mismatch".to_string()))
        } else {
            Ok(())
        }
    }
}

impl Authorizer for AuthorizedRequest {
    fn new(header: &Parts, secrets: &SecretMap) -> Result<Self, AuthError> {
        // Note on Authorization header: the auth type and the rest are /always/ space
        // separated, the subsequent parts are comma separated but there may also
        // be spaces after the commas!  boto3 includes the spaces after commas, mc
        // doesn't.

        /*
        Example headers from an S3 compatible client:
        host 127.0.0.1:1234
        user-agent MinIO (linux; amd64) minio-go/v7.0.6 mc/2020-10-03T02:54:56Z
        authorization AWS4-HMAC-SHA256
            Credential=minioadmin/20201202/us-east-1/s3/aws4_request,
            SignedHeaders=host;x-amz-content-sha256;x-amz-date,
            Signature=5bdabc4a37f894c9bb21c57084e9a65ac46d9e84ea07051df760310d2ed8ad18
        header x-amz-content-sha256 e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
        header x-amz-date 20201202T140923Z
        */

        // It is optional for requests to provide this, so we store an Option
        let claimed_content_hash =
            if let Some(claimed_sha256_headervalue) = header.headers.get("x-amz-content-sha256") {
                if claimed_sha256_headervalue == STREAMING_PAYLOAD {
                    debug!("Streaming payload auth mode");
                    None
                } else {
                    if let Ok(strhash) = claimed_sha256_headervalue.to_str() {
                        // Copying out into string because of weird way that hyper's
                        // request gets moved when we take its body later.
                        Some(Sha256DigestStr::from_str(strhash))
                    } else {
                        None
                    }
                }
            } else {
                None
            };

        // FIXME: validate amzdate vs. the credential scope
        // (https://docs.aws.amazon.com/general/latest/gr/sigv4-date-handling.html)
        // FIXME: X-AMZ-Date is optional if Date contains an iso8601 value
        let amzdate = if let Some(amzdate_hv) = header.headers.get("x-amz-date") {
            if let Ok(amzdate_str) = amzdate_hv.to_str() {
                amzdate_str
            } else {
                return Err(AuthError::new("Corrupt X-AMZ-Date header".to_string()));
            }
        } else {
            return Err(AuthError::new("Missing X-AMZ-Date header".to_string()));
        };

        let mut secret_id: Option<SmallString<[u8; 128]>> = None;
        let mut credential_scope: Option<SmallString<[u8; 64]>> = None;
        let mut signed_headers: SmallVec<[SmallString<[u8; 32]>; 4]> = SmallVec::new();
        let mut signature: Option<Sha256DigestStr> = None;

        let mut signed_headers_raw_str: Option<&str> = None;

        if let Some(authorization) = header.headers.get("authorization") {
            if let Ok(authorization_str) = authorization.to_str() {
                if authorization_str.len() == 0 {
                    return Err(AuthError::new("Empty authorization header".to_string()));
                }

                let mut space_tokens = authorization_str.split(" ");
                let auth_type = space_tokens.next().unwrap();
                if auth_type != "AWS4-HMAC-SHA256" {
                    return Err(AuthError::new(format!("Unknown auth type {}", auth_type)));
                }

                let comma_tokens = authorization_str[auth_type.len() + 1..].split(",");

                for t in comma_tokens {
                    let stripped = match t.strip_prefix(" ") {
                        Some(s) => s,
                        None => t,
                    };

                    let mut kv_tokens = stripped.split("=");
                    let key = kv_tokens.next().unwrap();
                    if let Some(val) = kv_tokens.next() {
                        if key == "Credential" {
                            // To calculate a signing key, we need the date,region,service separately
                            // To generate chunk signing strings, we need the full string of
                            // 20130606/us-east-1/s3/aws4_request

                            let mut cred_parts = val.split("/");
                            secret_id = Some(SmallString::from_str(cred_parts.next().unwrap()));
                            credential_scope = Some(SmallString::from_str(
                                &val[secret_id.as_ref().unwrap().len() + 1..],
                            ));
                        } else if key == "SignedHeaders" {
                            signed_headers =
                                val.split(";").map(|x| SmallString::from_str(x)).collect();
                            signed_headers_raw_str = Some(val);
                        } else if key == "Signature" {
                            signature = Some(Sha256DigestStr::from_str(val));
                        }
                    } else {
                        return Err(AuthError::new(format!("Invalid auth pair {}", stripped)));
                    }
                }
            } else {
                return Err(AuthError::new(format!("Can't decode authorization header")));
            }
        } else {
            return Err(AuthError::new(format!("No authorization header")));
        }

        // Check for missing mandatory headers.  From this point onwards we will
        // call unwrap on these without fear of failure.
        if signed_headers.is_empty()
            || signature == None
            || secret_id == None
            || credential_scope == None
        {
            error!("Dumping request headers on malformed request:");
            for (k, v) in header.headers.iter() {
                warn!("  {} : {}", k.to_string(), v.to_str().unwrap());
            }
            return Err(AuthError::new(format!("Incomplete auth headers")));
        }

        //let mut canonical_headers_str: SmallString<[u8; 256]> = SmallString::new();
        let mut canonical_headers_str = StringStream::new();
        write_canonical_headers(&mut canonical_headers_str, header, &signed_headers)?;

        let mut canonical_request_ctx = Sha256::new();
        write_canonical_request(
            &mut canonical_request_ctx,
            &header,
            &canonical_headers_str.0,
            signed_headers_raw_str.unwrap(),
        );

        let secret_id = secret_id.unwrap();

        let secret = match secrets.get(secret_id.as_str()) {
            Some(s) => s,
            None => {
                return Err(AuthError::new(format!(
                    "Unknown access_key_id {}",
                    secret_id
                )))
            }
        };

        let mut inst = AuthorizedRequest {
            finalized: false,
            credential_scope: credential_scope.unwrap(),
            secret_id,
            amzdate: SmallString::from_str(amzdate),
            signature: signature.unwrap(),
            claimed_content_hash,
            content_hash: Some(digest::Sha256::new()),
            signing_key: None,
            prev_chunk_sig: Sha256DigestStr::new(),
            canonical_request_ctx: Some(canonical_request_ctx),
        };

        // TODO: expose public method for all these calculations, so that caller
        // can first create the structure, then submit their first IOs to disk,
        // then do the auth calculation in the background
        inst.calc_signing_key(&secret);

        Ok(inst)
    }

    fn get_secret_id(&self) -> Option<&str> {
        Some(self.secret_id.as_str())
    }

    fn update_chunked(&mut self, chunk_sig: String) -> Result<(), AuthError> {
        // Our self.content_hash contains the hash of the
        // bytes of the chunk we're just finishing.
        let signing_key = self.signing_key.as_ref().unwrap();

        if self.prev_chunk_sig.is_empty() {
            // First chunk.  Calculate the seed.
            let mut canonical_request_ctx = self.canonical_request_ctx.take().unwrap();
            canonical_request_ctx.update("STREAMING-AWS4-HMAC-SHA256-PAYLOAD");

            let seed_signature = get_chunk_seed_signature(
                signing_key.as_ref(),
                &canonical_request_ctx.finalize().into(),
                &self.amzdate,
                &self.credential_scope,
            );

            debug!("SeedSignature: {}", seed_signature);

            // Compare seed signature with the Authorization header
            if seed_signature != self.signature {
                return Err(AuthError::new("Signature mismatch".to_string()));
            }

            self.prev_chunk_sig = seed_signature;
        }

        let content_hash = self.content_hash.replace(digest::Sha256::new()).unwrap();
        let chunk_hash_str = sha_str(content_hash.finalize().as_ref());
        debug!("update_chunked: calculated content hash {}", chunk_hash_str);

        let chunk_string_to_sign = format!(
            r#"AWS4-HMAC-SHA256-PAYLOAD
{amzdate}
{scope}
{prev_sig}
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
{chunk_hash_str}"#,
            amzdate = self.amzdate,
            scope = self.credential_scope,
            prev_sig = self.prev_chunk_sig,
            chunk_hash_str = chunk_hash_str
        );

        debug!(
            "Chunk StringToSign: {} bytes: {}",
            chunk_string_to_sign.len(),
            chunk_string_to_sign
        );

        let chunk_signature = digest::sha_str(&sign(signing_key, chunk_string_to_sign.as_bytes()));

        debug!(
            "update_chunked: calculated chunk_signature {}",
            chunk_signature
        );
        debug!("update_chunked: claimed chunk_signature {}", chunk_sig);

        if chunk_signature != chunk_sig {
            return Err(AuthError::new("Chunk Signature mismatch".to_string()));
        } else {
            self.prev_chunk_sig = chunk_signature;
            Ok(())
        }
    }

    fn update(&mut self, bytes: &[u8]) {
        self.content_hash.as_mut().unwrap().update(bytes);
    }

    fn finalize(&mut self) -> Result<(), AuthError> {
        assert!(!self.finalized);
        self.finalized = true;

        // At end of request, we only have to validate if this was a non-chunked
        // request.  Chunked requests were validated on each chunk.
        if self.prev_chunk_sig.is_empty() {
            self.finalize_nonchunked()
        } else {
            Ok(())
        }
    }

    fn enabled(&self) -> bool {
        true
    }
    fn finalized(&self) -> bool {
        self.finalized
    }
}

#[cfg(test)]
mod test {
    use super::write_canonical_request;
    use crate::auth::{write_canonical_headers, StringStream};
    use http::Method;
    use hyper::{Body, Request, Uri};
    use smallstr::SmallString;
    use smallvec::SmallVec;
    use std::io::Write;

    const EMPTY_STRING_SHA256: &str =
        "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

    #[test]
    fn test_canonical_req_1() {
        let content_hash = EMPTY_STRING_SHA256;
        let expected = r#"DELETE
/capra/0_1

host:localhost:5678
x-amz-content-sha256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
x-amz-date:20210709T145142Z

host;x-amz-content-sha256;x-amz-date
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"#;

        let raw_signed_headers = "host;x-amz-content-sha256;x-amz-date";
        let uri = Uri::from_static("/capra/0_1");
        let req = Request::builder()
            .uri(uri)
            .method(Method::DELETE)
            .header("Host", "localhost:5678")
            .header("x-amz-content-sha256", content_hash)
            .header("x-amz-date", "20210709T145142Z")
            .header("SignedHeaders", raw_signed_headers)
            .header(
                "Credential",
                "minioadmin/20210709/us-east-1/s3/aws4_request",
            )
            .header("Signature", "foobarbaz")
            .body(Body::empty())
            .unwrap();
        let (header, _body) = req.into_parts();

        let mut signed_headers_items: SmallVec<[SmallString<[u8; 32]>; 4]> = SmallVec::new();
        for sh in raw_signed_headers.split(";") {
            signed_headers_items.push(sh.into())
        }
        let mut canonical_headers = StringStream::new();
        write_canonical_headers(&mut canonical_headers, &header, &signed_headers_items).unwrap();

        let mut canonical_request = StringStream::new();
        write_canonical_request(
            &mut canonical_request,
            &header,
            &canonical_headers.0,
            &raw_signed_headers,
        );

        canonical_request.write(content_hash.as_bytes()).unwrap();

        assert_eq!(canonical_request.0, expected)
    }

    #[test]
    fn test_canonical_req_2() {
        let expected = r#"GET
/capra/
delimiter=&encoding-type=url&fetch-owner=true&list-type=2&max-keys=1000&prefix=pHdx5hPD&start-after=pHdx5hPD%2F2799.0IWU%287NeHezsE0vr.rnd
host:localhost:5678
x-amz-content-sha256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
x-amz-date:20210712T125849Z

host;x-amz-content-sha256;x-amz-date
e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"#;

        let raw_signed_headers = "host;x-amz-content-sha256;x-amz-date";
        let uri = Uri::from_static("/capra/?delimiter=&fetch-owner=true&encoding-type=url&list-type=2&max-keys=1000&prefix=pHdx5hPD&start-after=pHdx5hPD/2799.0IWU(7NeHezsE0vr.rnd");
        let req = Request::builder()
            .uri(uri)
            .method(Method::GET)
            .header("Host", "localhost:5678")
            .header("x-amz-content-sha256", EMPTY_STRING_SHA256)
            .header("x-amz-date", "20210712T125849Z")
            .header("SignedHeaders", raw_signed_headers)
            .header(
                "Credential",
                "minioadmin/20210712/us-east-1/s3/aws4_request",
            )
            .header("Signature", "foobarbaz")
            .body(Body::empty())
            .unwrap();
        let (header, _body) = req.into_parts();

        let mut signed_headers_items: SmallVec<[SmallString<[u8; 32]>; 4]> = SmallVec::new();
        for sh in raw_signed_headers.split(";") {
            signed_headers_items.push(sh.into())
        }
        let mut canonical_headers = StringStream::new();
        write_canonical_headers(&mut canonical_headers, &header, &signed_headers_items).unwrap();

        let mut canonical_request = StringStream::new();
        write_canonical_request(
            &mut canonical_request,
            &header,
            &canonical_headers.0,
            &raw_signed_headers,
        );

        canonical_request
            .write(EMPTY_STRING_SHA256.as_bytes())
            .unwrap();

        assert_eq!(canonical_request.0, expected)
    }
}
