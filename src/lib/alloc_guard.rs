use log::error;
use std::alloc::System;

use stats_alloc;

#[global_allocator]
static GLOBAL: &stats_alloc::StatsAlloc<System> = &stats_alloc::INSTRUMENTED_SYSTEM;

/// This is tracking GLOBAL allocator stats, so it is not safe to use in any
/// concurernt code.  You probably only want to use this in microbenchmarks.
pub struct AllocGuard {
    name: &'static str,
    permitted: usize,
    advisory: bool,
    initial: stats_alloc::Stats,
}

impl AllocGuard {
    pub fn new(name: &'static str, permitted: usize, advisory: bool) -> Self {
        Self {
            name,
            permitted,
            advisory,
            initial: GLOBAL.stats(),
        }
    }
}

impl Drop for AllocGuard {
    fn drop(&mut self) {
        let delta = GLOBAL.stats() - self.initial;
        if delta.allocations > self.permitted {
            let msg = format!(
                "AllocGuard[{}] Too many allocs {} > {}",
                self.name, delta.allocations, self.permitted
            );
            if self.advisory {
                error!("{}", msg);
            } else {
                panic!("{}", msg);
            }
        }
    }
}
