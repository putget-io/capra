use log::*;

// TODO: perhaps optionally dump buffers out to binary files for use in
// analysis/testcases?
// TODO: make this a macro that can be compiled out based on
pub fn dump_buffer(level: log::Level, bytes: &[u8]) {
    if !log_enabled!(level) {
        return;
    }

    let line_count = bytes.len() / 16;
    for line in 0..line_count + 1 {
        if line == line_count && bytes.len() % 16 == 0 {
            // No trailing bytes
            continue;
        }

        let byte_range = if line == line_count {
            &bytes[line * 16..]
        } else {
            &bytes[line * 16..line * 16 + 16]
        };

        let txt = byte_range
            .into_iter()
            .map(|b| format!("{:02x}", b))
            .collect::<Vec<String>>()
            .join(" ");
        let ascii_text = byte_range
            .into_iter()
            .map(|b| *b as char)
            .map(|c| {
                if c.is_ascii_alphanumeric() || c.is_ascii_punctuation() {
                    c
                } else {
                    '.'
                }
            })
            .collect::<String>();
        log!(level, "{:08x} | {} |{}|", line * 16, txt, ascii_text);
    }
}
