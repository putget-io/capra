use std::any::type_name;
use tokio::sync::mpsc;
use tokio::sync::Mutex as TokioMutex;

use log::*;

/// Sometimes, we want to pass a pooled object (like an IO buffer) into a new
/// task, which doesn't have a reference to the original pool.  To enable
/// doing this and reeasing the object, the Releaser class is cloneable and Sync/Send.
pub struct SimplePoolReleaser<T: Send + Sync> {
    inner_tx: mpsc::Sender<T>,
}

impl<T: Send + Sync> SimplePoolReleaser<T> {
    pub fn release(&self, t: T) {
        match self.inner_tx.try_send(t) {
            Err(_) => {
                // If the parent pool is dropped, then it's fine to drop this
                // value on the floor: all the parent would have done on destruction
                // is drop it anyway.  However, this should generally only happen during
                // shutdown, so it's a warning
                warn!(
                    "SimplePoolReleaser<{}>.release: parent closed, dropping",
                    type_name::<T>(),
                );
            }
            Ok(_) => {
                debug!("SimplePoolReleaser<{}>.release", type_name::<T>(),);
            }
        }
    }
}

pub struct SimplePool<T: Send + Sync> {
    inner_tx: mpsc::Sender<T>,
    inner_rx: TokioMutex<mpsc::Receiver<T>>,
    capacity: usize,

    // If we were created with a new_steal, this is the parent's release
    // channel to send items back to when we're dropped.
    parent: Option<mpsc::Sender<T>>,
}

pub struct ItemHandle<T: Send + Sync> {
    // This is an option to enable drop() to take it, but
    // that could be avoided with some unsafe{} magic.
    inner: Option<T>,
    releaser: mpsc::Sender<T>,
}

impl<T: Send + Sync + std::fmt::Display> std::fmt::Display for ItemHandle<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.inner.as_ref().unwrap().fmt(f)
    }
}

unsafe impl<T: Send + Sync> StableAddress for ItemHandle<T> {}

impl<T: Send + Sync> Deref for ItemHandle<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.inner.as_ref().unwrap()
    }
}

impl<T: Send + Sync> DerefMut for ItemHandle<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner.as_mut().unwrap()
    }
}

impl<T: Send + Sync> Drop for ItemHandle<T> {
    fn drop(&mut self) {
        let r = self.releaser.try_send(self.inner.take().unwrap());
        if r.is_err() {
            // This should only happen during shutdowns or panics, where a pool might be dropped
            // before all the tasks that had buffers are dropped.
            warn!(
                "Parent pool is dropped, dropped {} SimplePool item",
                type_name::<T>(),
            )
        }
    }
}

/// An async allocator for simple fixed size collections of objects
impl<T: Send + Sync> SimplePool<T> {
    pub fn releaser(&self) -> SimplePoolReleaser<T> {
        SimplePoolReleaser {
            inner_tx: self.inner_tx.clone(),
        }
    }

    pub fn get_capacity(&self) -> usize {
        self.capacity
    }

    /// A bulk acquirer, to avoid thrashing the rx mutex on
    /// many individual acquire calls.  No bulk release function
    /// is needed because release is just a push to a queue.
    ///
    /// It's called steal because the use case is another SimplePool
    /// stealing some entries for itself.
    async fn steal_many(&self, n: usize, into: &Self) {
        let mut rx = self.inner_rx.lock().await;
        for _i in 0..n {
            let v = match (*rx).recv().await {
                None => panic!("SimplePool sender closed unexpectedly"),
                Some(v) => v,
            };
            into.release(v)
        }
    }

    /// The 'smart' variant wraps the object in an auto-releasing handle.  This
    /// is much more robust in code paths that spawn tokio tasks that take ownership
    /// of items, because it reliably handles releasing items if the task panics or
    /// fails to explicitly release things.
    pub async fn acquire_smart(&self) -> ItemHandle<T> {
        let t = self.acquire().await;
        ItemHandle {
            releaser: self.inner_tx.clone(),
            inner: Some(t),
        }
    }

    /// Retrieve a single value.  If none are available, wait until something
    /// is released by someone else.
    pub async fn acquire(&self) -> T {
        let v = {
            let mut rx = match self.inner_rx.try_lock() {
                Ok(guard) => guard,
                Err(_) => self.inner_rx.lock().await,
            };

            match (*rx).recv().await {
                None => panic!("SimplePool sender closed unexpectedly"),
                Some(v) => v,
            }
        };
        v
    }

    /// Return a value that was obtained with acquire.  Never use this
    /// to try and push new values into a pool - it will hang upon hitting
    /// the channel size limit used during new()
    pub fn release(&self, v: T) {
        // A non-async try_send, because our queue is always large enough
        // to receive one of our values (assumes callers aren't maliciously inventing
        // new values)
        // (and tx+rx are owned by the same struct, so never have to worry about
        //  rx being dropped)
        match self.inner_tx.try_send(v) {
            Err(_) => panic!("Pool receiver closed unexpectedly"),
            Ok(_) => {}
        }
    }

    pub fn new(mut values: Vec<T>) -> Self {
        let len = values.len();
        Self::new_with(len, || values.pop().unwrap())
    }

    pub async fn new_steal(from: &Self, n: usize) -> Self {
        let (tx, rx) = mpsc::channel(n);
        let mut inst = Self {
            inner_tx: tx,
            inner_rx: TokioMutex::new(rx),
            capacity: n,
            parent: Some(from.inner_tx.clone()),
        };

        from.steal_many(n, &mut inst).await;
        inst
    }

    pub fn new_with<F>(len: usize, mut f: F) -> Self
    where
        F: FnMut() -> T,
    {
        let (tx, rx) = mpsc::channel(len);
        for _i in 0..len {
            let v = f();
            match tx.try_send(v) {
                Err(_) => assert!(false),
                _ => {}
            }
        }
        Self {
            inner_tx: tx,
            inner_rx: TokioMutex::new(rx),
            capacity: len,
            parent: None,
        }
    }

    /// This must be the last thing you do with the SimplePool - it closes
    /// the internal channel.  There must be no other tasks trying to acqure
    /// from this pool.
    pub fn drain_into(&self, dest: &Self) {
        // We unwrap a try_lock here because callers are responsible for only
        // calling this after they've aborted any other tasks that might be
        // trying to access it.
        let mut rx = self.inner_rx.try_lock().unwrap();

        rx.close();

        // This use of now_or_never is a workaround for missing try_recv
        // https://github.com/tokio-rs/tokio/pull/3263
        // (see alsohttps://github.com/tokio-rs/tokio/issues/3350)
        loop {
            let v = match rx.recv().now_or_never().unwrap() {
                None => break,
                Some(v) => v,
            };
            dest.release(v);
        }
    }

    #[cfg(test)]
    pub async fn drain(self) -> Vec<T> {
        let mut rx = self.inner_rx.try_lock().unwrap();
        rx.close();
        let mut r = Vec::new();
        loop {
            let v = match rx.recv().await {
                None => break,
                Some(v) => v,
            };
            r.push(v);
        }
        r
    }
}

impl<T: Send + Sync> Drop for SimplePool<T> {
    fn drop(&mut self) {
        if let Some(parent) = &self.parent {
            // We were created with borrowed items from another pool:
            // release them to the parent pool
            let mut rx = self.inner_rx.try_lock().unwrap();
            rx.close();

            let mut drained: usize = 0;
            loop {
                // When we run out of items, we may /either/ get a non-complete
                // future, or get a complete future with a None in it.
                let item = match rx.recv().now_or_never() {
                    None => break,
                    Some(v) => match v {
                        Some(v) => v,
                        None => break,
                    },
                };

                drained += 1;
                if parent.try_send(item).is_err() {
                    // This should only happen during shutdowns or panics, where a pool might be dropped
                    // before all the tasks that had buffers are dropped.
                    warn!(
                        "Parent pool is dropped, dropped {} SimplePool item",
                        type_name::<T>(),
                    );
                }
            }

            if drained < self.capacity {
                let msg = format!(
                    "SimplePool dropped with buffers in flight, only got {}/{}",
                    drained, self.capacity
                );
                error!("{}", msg);
                panic!("{}", msg);
            }
        }
    }
}

use futures::FutureExt;
#[cfg(test)]
use ntest::timeout;
use owning_ref::StableAddress;
use std::ops::{Deref, DerefMut};
#[cfg(test)]
use tokio;

#[tokio::test]
#[timeout(1000)]
async fn test_simple() {
    let values: Vec<u32> = (0..10).collect();
    let sp = SimplePool::new(values);

    let n = sp.acquire().await;
    assert_eq!(n, 9);
    sp.release(n);
    assert_eq!(sp.drain().await.len(), 10);
}

#[tokio::test]
#[timeout(1000)]
async fn test_releaser() {
    let values: Vec<u32> = (0..10).collect();
    let sp = SimplePool::new(values);
    let releaser = sp.releaser();

    let n = sp.acquire().await;
    assert_eq!(n, 9);
    releaser.release(n);
    assert_eq!(sp.drain().await.len(), 10);
}
