use crate::digest::{sha_str, Sha256Digest, Sha256DigestStr};
use crate::s3_client_auth::{
    sign_request, sign_request_chunked, ClientAuthConfig, ClientAuthKeyState,
    ClientAuthRequestState,
};
use chrono::prelude::*;
use chrono::{DateTime, Utc};
use http::request::Builder;
use http::Uri;
use hyper::body::Bytes;
use hyper::http::uri::PathAndQuery;
use hyper::{Body, Method, Request};

const EMPTY_STRING_SHA256: &str =
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

/// An S3 client specialized for benchmarks.
///
/// This client is particularly useful for benchmarking, because it
/// notices if your payload digest is the same as last time, and
/// saves the cost of crypto work if so, enabling driving much more
/// benchmark traffic from a given CPU resource.
pub struct S3Client {
    unsigned: bool,

    key_state: ClientAuthKeyState,

    last_request_t: Option<DateTime<Utc>>,
    last_request_state: Option<ClientAuthRequestState>,
    last_payload_digest: Option<Sha256Digest>,

    auth_config: ClientAuthConfig,

    base_url: Uri,
}

impl S3Client {
    pub fn new(base_url: Uri, auth_config: ClientAuthConfig, unsigned: bool) -> Self {
        let now = Utc::now();
        Self {
            base_url,
            key_state: ClientAuthKeyState::new(&now, &auth_config),
            auth_config,
            last_payload_digest: None,
            unsigned,
            last_request_state: None,
            last_request_t: None,
        }
    }

    fn with_auth_state<F, G>(&mut self, f: F, payload_digest: Option<&Sha256Digest>) -> G
    where
        F: FnOnce(&str, &ClientAuthKeyState, &ClientAuthRequestState) -> G,
    {
        let now = Utc::now();

        let digest_str = payload_digest.map(|s| sha_str(s));
        let digest_str_ref = digest_str
            .as_ref()
            .map(|s| s.as_str())
            .unwrap_or(EMPTY_STRING_SHA256);

        if !self.key_state.is_fresh(now) {
            self.key_state = ClientAuthKeyState::new(&now, &self.auth_config);
        }

        let reuse_auth_state = if self.last_payload_digest.as_ref() == payload_digest {
            match self.last_request_t {
                None => false,
                Some(t) => t.round_subsecs(0) == now.round_subsecs(0),
            }
        } else {
            false
        };

        if !reuse_auth_state {
            self.last_request_t = Some(now);
            self.last_request_state = Some(ClientAuthRequestState::new(
                &now,
                &self.auth_config.host,
                digest_str_ref,
            ));
            self.last_payload_digest = payload_digest.map(|d| *d);
        }
        let request_state = self.last_request_state.as_ref().unwrap();

        f(digest_str_ref, &self.key_state, request_state)
    }

    fn sign_request_chunked(&mut self, builder: Builder) -> (Builder, Sha256DigestStr) {
        if self.unsigned {
            return (builder, Sha256DigestStr::new());
        }

        self.with_auth_state(
            |_digest_str, key_state, request_state| {
                sign_request_chunked(builder, key_state, request_state)
            },
            None,
        )
    }

    fn sign_request(&mut self, builder: Builder, payload_digest: Option<&Sha256Digest>) -> Builder {
        if self.unsigned {
            return builder;
        }
        self.with_auth_state(
            |digest_str, key_state, request_state| {
                sign_request(builder, digest_str, key_state, request_state)
            },
            payload_digest,
        )
    }

    pub fn post(
        &mut self,
        path_and_query: PathAndQuery,
        content: Bytes,
        content_digest: &Sha256Digest,
    ) -> Request<Body> {
        let uri = Uri::builder()
            .scheme(self.base_url.scheme().unwrap().clone())
            .authority(self.base_url.authority().unwrap().clone())
            .path_and_query(path_and_query)
            .build()
            .unwrap();

        let mut req_builder = Request::builder()
            .method(Method::POST)
            .uri(uri)
            .header("Content-Type", "application/octet-stream")
            .header("Content-Length", &content.len().to_string())
            .header("Host", &self.auth_config.host);

        req_builder = self.sign_request(req_builder, Some(content_digest));
        req_builder.body(Body::from(content)).unwrap()
    }

    pub fn put(
        &mut self,
        bucket: &str,
        key: &str,
        payload: Bytes,
        payload_digest: &Sha256Digest,
    ) -> Request<Body> {
        let uri = format!("{}{}/{}", self.base_url, bucket, key);
        let mut req_builder = Request::builder()
            .method(Method::PUT)
            .uri(uri)
            .header("Content-Type", "application/octet-stream")
            .header("Content-Length", &payload.len().to_string())
            .header("Host", &self.auth_config.host);

        req_builder = self.sign_request(req_builder, Some(payload_digest));
        // TODO...
        //let (req_builder, seed_sig) = self.sign_request_chunked(req_builder);
        //let chunked_stream = ChunkedEncodeStr

        req_builder.body(Body::from(payload)).unwrap()
    }

    pub fn get(&mut self, bucket: &str, key: &str) -> Request<Body> {
        let uri = format!("{}{}/{}", self.base_url, bucket, key);
        let mut req_builder = Request::builder()
            .method(Method::GET)
            .uri(uri)
            .header("Host", &self.auth_config.host);
        req_builder = self.sign_request(req_builder, None);

        req_builder.body(Body::empty()).unwrap()
    }

    pub fn delete(&mut self, bucket: &str, key: &str) -> Request<Body> {
        let uri = format!("{}{}/{}", self.base_url, bucket, key);
        let mut req_builder = Request::builder()
            .method(Method::DELETE)
            .uri(uri)
            .header("Host", &self.auth_config.host);
        req_builder = self.sign_request(req_builder, None);
        req_builder.body(Body::empty()).unwrap()
    }
}
