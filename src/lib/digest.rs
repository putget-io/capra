use smallstr::SmallString;

// Alias Sha256 to either RustCrypto or ring implementation depending on feature
#[cfg(feature = "ringhash")]
pub use ring_compat::digest::Sha256;
#[cfg(not(feature = "ringhash"))]
pub use sha2::Sha256;

const SHA256_OUTPUT_LEN: usize = 32;
pub type Sha256Digest = [u8; SHA256_OUTPUT_LEN];
pub type Sha256DigestStr = SmallString<[u8; 64]>;

const MD5_OUTPUT_LEN: usize = 16;
pub type Md5Digest = [u8; 16];

#[inline(always)]
fn octet_to_char(i: u8) -> u8 {
    if i < 0xa {
        // '0'...
        0x30 + i
    } else {
        // 'a'...
        0x61 - 10 + i
    }
}

/// This is measured as about 10x faster than using a generic formatter
/// helper like the hex-slice crate (our 87ns vs their 900ns)
pub fn sha_str(input: &Sha256Digest) -> Sha256DigestStr {
    let mut r = Sha256DigestStr::with_capacity(64);

    unsafe {
        let v = r.as_mut_vec();
        v.set_len(64);
        for i in 0..32 {
            let n: u8 = input[i];
            v[i * 2] = octet_to_char(n >> 4);
            v[i * 2 + 1] = octet_to_char(n & 0xf);
        }
    }

    r
}

/// Compare a binary digest with a hex-printed digest.  Useful
/// because we calculate our own digests as binary arrays, but
/// digests come off the wire as hex-printed strings.
pub fn sha_eq(lhs: &Sha256Digest, rhs: &Sha256DigestStr) -> bool {
    let v = rhs.as_bytes();
    for i in 0..32 {
        let n: u8 = lhs[i];
        if v[i * 2] != octet_to_char(n >> 4) || v[i * 2 + 1] != octet_to_char(n & 0xf) {
            return false;
        }
    }

    true
}

pub fn parse_md5(input: &str) -> Result<Md5Digest, ()> {
    if input.len() != MD5_OUTPUT_LEN * 2 {
        Err(())
    } else {
        let mut md5: Md5Digest = [0; MD5_OUTPUT_LEN];
        for i in 0..MD5_OUTPUT_LEN {
            md5[i] = u8::from_str_radix(&input[i * 2..i * 2 + 2], 16).map_err(|_| ())?;
        }
        Ok(md5)
    }
}
