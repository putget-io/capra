use chrono::prelude::*;
use hmac;
use hmac::{Mac, NewMac};

use log::*;

use crate::auth::{
    canonical_uri, get_signing_key, get_string_to_sign, maybe_rewrite_query, STREAMING_PAYLOAD,
};
use crate::digest::{sha_str, Sha256, Sha256Digest, Sha256DigestStr};
use ::digest::Digest;

type Sha256Hmac = hmac::Hmac<Sha256>;

pub struct ClientAuthConfig {
    pub region: String,
    pub access_key_id: String,
    pub secret_access_key: String,
    pub host: String,
}

pub struct ClientAuthKeyState {
    updated_at: DateTime<Utc>,
    pub signing_key: Sha256Digest,
    pub scope: String,
    pub credential: String,
}

const SIGNED_HEADERS: [&str; 3] = ["host", "x-amz-content-sha256", "x-amz-date"];

// Key State, which only changes when the auth config changes
// or when the date changes.
impl ClientAuthKeyState {
    pub fn new(now: &DateTime<Utc>, auth_config: &ClientAuthConfig) -> Self {
        let updated_at: DateTime<Utc> = now.clone();

        debug!("Generating new signing key at {}", now);

        let signing_key = get_signing_key(
            auth_config.secret_access_key.as_bytes(),
            now.format("%Y%m%d").to_string().as_bytes(),
            auth_config.region.as_bytes(),
            "s3".as_bytes(),
        );

        let scope = format!(
            "{}/{}/s3/aws4_request",
            now.format("%Y%m%d"),
            auth_config.region
        );

        let credential = format!("{}/{}", auth_config.access_key_id, scope);

        Self {
            updated_at,
            signing_key,
            scope,
            credential,
        }
    }

    pub fn is_fresh(&self, now: DateTime<Utc>) -> bool {
        return self.updated_at.date_naive() == now.date_naive();
    }
}

// Request state, which changes with each request content, but equally
// might be cacheable between requests if sending the same content
// again and again in a benchmark.  In the latter case, only needs
// regenerating when the walltime second changes.
pub struct ClientAuthRequestState {
    updated_at: DateTime<Utc>,
    pub x_amz_date: String,
    pub signed_headers_joined: String,
    pub canonical_headers: String,
}

impl ClientAuthRequestState {
    pub fn new(now: &DateTime<Utc>, host: &str, payload_digest: &str) -> Self {
        debug!("Generating new request auth state at {}", now);

        let x_amz_date = now.format("%Y%m%dT%H%M%SZ").to_string();

        let mut signed_header_lines: Vec<String> = Vec::with_capacity(SIGNED_HEADERS.len());
        signed_header_lines.push(format!("host:{}", host));
        signed_header_lines.push(format!("x-amz-content-sha256:{}", &payload_digest));
        signed_header_lines.push(format!("x-amz-date:{}", x_amz_date));

        Self {
            updated_at: now.clone(),
            x_amz_date,
            canonical_headers: signed_header_lines.join("\n"),
            signed_headers_joined: SIGNED_HEADERS.join(";"),
        }
    }

    /// Use this when adding additional x-amz headers on a request,
    /// like x-amz-decoded-content-length for chunked PUT
    /// FIXME: this is a whacky API.  Really new() ought to take
    /// a HeaderMap reference and build the list of headers.
    pub fn signed_header(&mut self, key: &str, val: &str) {
        self.signed_headers_joined.push(';');

        self.signed_headers_joined.push_str(key);

        self.canonical_headers.push('\n');
        self.canonical_headers.push_str(key);
        self.canonical_headers.push(':');
        self.canonical_headers.push_str(val);
    }

    pub fn is_fresh(&self, now: DateTime<Utc>) -> bool {
        return self.updated_at.round_subsecs(0) == now.round_subsecs(0);
    }
}

pub fn canonical_request_client(
    req_builder: &hyper::http::request::Builder,
    request_auth_state: &ClientAuthRequestState,
    payload_digest: &str,
) -> String {
    let mut rewritten_canonical_query_string = String::new();
    let input_query = req_builder.uri_ref().unwrap().query().unwrap_or("");
    let canonical_query_string =
        maybe_rewrite_query(&mut rewritten_canonical_query_string, input_query);

    let r = format!(
        r#"{http_request_method}
{canonical_uri}
{canonical_query_string}
{canonical_headers}

{signed_headers}
{payload_digest}"#,
        http_request_method = req_builder.method_ref().unwrap().as_str(),
        canonical_uri = canonical_uri(req_builder.uri_ref().unwrap()),
        canonical_query_string = canonical_query_string,
        canonical_headers = request_auth_state.canonical_headers,
        signed_headers = request_auth_state.signed_headers_joined,
        payload_digest = payload_digest
    );
    r
}

pub fn sign_request_chunked(
    mut req_builder: hyper::http::request::Builder,
    daily_auth_state: &ClientAuthKeyState,
    request_auth_state: &ClientAuthRequestState,
) -> (hyper::http::request::Builder, Sha256DigestStr) {
    req_builder = req_builder.header("Content-Encoding", "aws-chunked");
    sign_request_inner(
        req_builder,
        STREAMING_PAYLOAD,
        daily_auth_state,
        request_auth_state,
    )
}

pub fn sign_request(
    req_builder: hyper::http::request::Builder,
    payload_digest: &str,
    daily_auth_state: &ClientAuthKeyState,
    request_auth_state: &ClientAuthRequestState,
) -> hyper::http::request::Builder {
    sign_request_inner(
        req_builder,
        payload_digest,
        daily_auth_state,
        request_auth_state,
    )
    .0
}

fn sign_request_inner(
    mut req_builder: hyper::http::request::Builder,
    payload_digest: &str,
    daily_auth_state: &ClientAuthKeyState,
    request_auth_state: &ClientAuthRequestState,
) -> (hyper::http::request::Builder, Sha256DigestStr) {
    req_builder = req_builder
        .header("x-amz-content-sha256", payload_digest)
        .header("x-amz-date", &request_auth_state.x_amz_date);

    let canonical_request =
        canonical_request_client(&req_builder, request_auth_state, payload_digest);

    debug!(
        "Canonical request {} {}",
        canonical_request.len(),
        canonical_request
    );

    let mut hasher = Sha256::new();
    hasher.update(canonical_request.as_bytes());
    let canonical_request_digest: Sha256Digest = hasher.finalize().into();
    let string_to_sign = get_string_to_sign(
        &request_auth_state.x_amz_date,
        &daily_auth_state.scope,
        &canonical_request_digest,
    );

    let mut signature = Sha256Hmac::new_from_slice(&daily_auth_state.signing_key).unwrap();
    signature.update(string_to_sign.as_bytes());
    let signature = sha_str(&signature.finalize().into_bytes().as_ref());

    let authorization_header = format!(
        "AWS4-HMAC-SHA256 Credential={},SignedHeaders={},Signature={}",
        daily_auth_state.credential, request_auth_state.signed_headers_joined, signature
    );

    (
        req_builder.header("authorization", authorization_header),
        signature.into(),
    )
}
