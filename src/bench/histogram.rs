use std::ops::AddAssign;

use partial_min_max::{max, min};

pub struct FineHistogram {
    // Lowest bucket value, in `unit`s
    min: usize,
    // Higest bucket value, in `unit`s
    max: usize,
    unit: f32,
    total: u64,
    bins: Vec<u32>,
    // Smallest value seen in an insert (may be smaller than min bucket!)
    min_value: f32,
    // Largest value seen in an insert (may be larger than max bucket!)
    max_value: f32,
}

/*
 * For benchmarks where we expect a rather tight range of latencies, e.g. an IO benchmark
 * which will give results ranging from a drive limit (e.g. 20us) up to a reasonable
 * bandwidth-bounded limit (e.g. 4000us for 4MB at 1gbps).
 *
 * If you want coarser resolution, just set a different unit (e.g. while we might use
 * 0.000001 for us, if we want 1us bucket we can use 0.00001
 */
impl FineHistogram {
    pub fn new(unit: f32, min: usize, max: usize) -> FineHistogram {
        assert!(max > min);

        return FineHistogram {
            min,
            max,
            unit,
            total: 0,
            bins: vec![0; (max - min + 1) as usize],
            min_value: 0.0,
            max_value: 0.0,
        };
    }

    pub fn get_min_value(&self) -> f32 {
        return self.min_value;
    }

    pub fn get_max_value(&self) -> f32 {
        return self.max_value;
    }

    pub fn insert(&mut self, value: f32) {
        if value > self.max_value {
            self.max_value = value;
        }

        if value < self.min_value || self.min_value == 0.0 {
            self.min_value = value
        }

        let value_i = (value / self.unit) as usize;

        let bin = if value_i < self.min {
            0
        } else if value_i > self.max {
            self.bins.len() - 1
        } else {
            (value_i - self.min) as usize
        };

        self.bins[bin] += 1;
        self.total += 1;

        // Avoid overflowing, and stay under half so that
        // we can reliably sum two histograms together without
        // overflowing in the process.
        if self.bins[bin] >= u32::MAX / 2 {
            self.halve();
        }
    }

    /// When a bucket is getting close to the representation limit of
    /// our bin value type, we halve everything to keep our histogram shape
    /// while bounding the magnitude of the values.  Worst case happens once
    /// every 2 billion inserts.
    fn halve(&mut self) {
        for v in &mut self.bins {
            *v = *v / 2
        }
        self.total /= 2
    }

    pub fn percentile(&self, pct: f32) -> f32 {
        let count = (pct * self.total as f32) as u64;
        let mut k: u64 = 0;
        for (i, bin) in self.bins.iter().enumerate() {
            k += *bin as u64;
            if k > count || i == self.bins.len() - 1 {
                let value = (i + self.min) as f32 * self.unit;
                return value;
            }
        }

        unreachable!();
    }
}

impl AddAssign for FineHistogram {
    fn add_assign(&mut self, rhs: FineHistogram) {
        assert_eq!(self.min, rhs.min);
        assert_eq!(self.max, rhs.max);
        assert_eq!(self.unit, rhs.unit);

        if self.min_value > 0.0 {
            self.min_value = min(self.min_value, rhs.min_value);
        } else {
            self.min_value = rhs.min_value;
        }

        self.max_value = max(self.max_value, rhs.max_value);

        self.total += rhs.total;
        let mut halve = false;
        for i in 0..self.bins.len() {
            self.bins[i] += rhs.bins[i];
            if self.bins[i] >= u32::MAX / 2 {
                halve = true;
            }
        }

        if halve {
            self.halve();
        }
    }
}
