use capralib::digest::Sha256Digest;
use core::marker::{Send, Sync};
use core::option::Option;
use core::option::Option::{None, Some};

use digest::Digest;
use hyper::body::Bytes;
use rand::{thread_rng, RngCore};
use sha2::Sha256;
use std::iter::Iterator;

// KeyIterator generates Strings for keys
pub trait KeyIterator: Iterator<Item = String> + Clone + Send + Sync + 'static {}
impl<T> KeyIterator for T where T: Iterator<Item = String> + Clone + Send + Sync + 'static {}
unsafe impl<I: KeyIterator> Send for SameObjectIter<I> {}
unsafe impl<I: KeyIterator> Sync for SameObjectIter<I> {}

#[derive(Clone)]
pub struct KeyContent {
    pub payload: Bytes,
    pub sha256: Sha256Digest,
    pub etag: String,
}

// KeyContent is the PUT/GET payload and its checksums
impl KeyContent {
    fn new(payload: Bytes) -> Self {
        let mut sha256 = Sha256::new();
        sha256.update(&payload);

        // Calculate MD5 of payload to validate the ETag we get back from the server
        let mut md5_ctx = md5::Md5::new();
        md5_ctx.update(&payload);

        Self {
            payload,
            sha256: sha256.finalize().into(),
            etag: format!("\"{:x}\"", md5_ctx.finalize()),
        }
    }
}

unsafe impl Send for KeyContent {}
unsafe impl Sync for KeyContent {}

// KeyWork ties together a key and its body
pub struct KeyWork {
    pub key: String,
    pub content: KeyContent,
}

pub trait KeyWorkIterator: Iterator<Item = KeyWork> + Sync + Send + 'static {
    fn box_clone(&self) -> Box<dyn KeyWorkIterator>;
}

/// An iterator that gives you the same object indefinitely
#[derive(Clone)]
pub struct SameObjectIter<I: KeyIterator> {
    keys: I,
    template: KeyContent,
}

impl<I: KeyIterator> SameObjectIter<I> {
    pub fn new(keys: I, payload_size: usize) -> Self {
        let mut payload_bytes: Vec<u8> = Vec::with_capacity(payload_size);
        payload_bytes.resize(payload_size, 0);
        thread_rng().fill_bytes(&mut payload_bytes[0..payload_size]);
        let payload: Bytes = Bytes::copy_from_slice(&payload_bytes[0..payload_size]);

        // Constructing KeyContent early, so that we only sha256/md5 once
        // at startup instead of on each KeyWork that we yield.

        Self {
            keys,
            template: KeyContent::new(payload),
        }
    }
}

impl<I: KeyIterator> KeyWorkIterator for SameObjectIter<I> {
    fn box_clone(&self) -> Box<dyn KeyWorkIterator> {
        return Box::new(self.clone());
    }
}

impl<I: KeyIterator> Iterator for SameObjectIter<I> {
    type Item = KeyWork;
    fn next(&mut self) -> Option<Self::Item> {
        match self.keys.next() {
            Some(key) => Some(KeyWork {
                key,

                content: self.template.clone(),
            }),
            None => None,
        }
    }
}

/// An iterator that gives you slightly different objects
/// every time, where most of their content might still be identicaly, but
/// their etags will be different.
#[derive(Clone)]
pub struct VaryObjectIterator<I: KeyIterator> {
    keys: I,
    payload_size: usize,
}

impl<I: KeyIterator> KeyWorkIterator for VaryObjectIterator<I> {
    fn box_clone(&self) -> Box<dyn KeyWorkIterator> {
        return Box::new(self.clone());
    }
}

unsafe impl<I: KeyIterator> Send for VaryObjectIterator<I> {}
unsafe impl<I: KeyIterator> Sync for VaryObjectIterator<I> {}

impl<I: KeyIterator> VaryObjectIterator<I> {
    pub fn new(keys: I, payload_size: usize) -> Self {
        Self { keys, payload_size }
    }

    fn gen_content(&self, key: &str) -> KeyContent {
        assert!(key.len() <= self.payload_size);

        let grow_by = self.payload_size - key.len();

        let mut content = Vec::with_capacity(self.payload_size);

        content.extend_from_slice(key.as_bytes());
        content.extend(std::iter::repeat(0xf0).take(grow_by));

        KeyContent::new(Bytes::from(content))
    }
}

impl<I: KeyIterator> Iterator for VaryObjectIterator<I> {
    type Item = KeyWork;

    fn next(&mut self) -> Option<Self::Item> {
        match self.keys.next() {
            Some(key) => {
                let content = self.gen_content(&key);
                Some(KeyWork { key, content })
            }
            None => None,
        }
    }
}
