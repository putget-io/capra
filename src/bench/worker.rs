use std::env;
use std::fmt::Write;
use std::net::IpAddr;
use std::ops::AddAssign;
use std::time::{Duration, Instant};

use futures::stream::StreamExt;
use http::Uri;
use hyper::client::connect::Connect;
use hyper::client::{Client, HttpConnector, ResponseFuture};
use hyper::header::HeaderMap;
use hyper::http::response::Parts;
use hyper::{Body, Request, Response};
use hyper_tls::HttpsConnector;
use log::*;
use md5;
use md5::Digest;
use rand::prelude::*;

use capralib::digest::{Sha256, Sha256Digest};
use capralib::s3_client::S3Client;
use capralib::s3_client_auth::ClientAuthConfig;

use crate::util::format::dump_buffer;

use super::histogram::FineHistogram;
use super::main::{BenchOptions, WorkerAction};
use crate::bench::content::KeyWork;

// How many requests to run before we start contributing latency statistics
// to the histogram.
const WARMUP_REQUESTS: usize = 16;

pub struct WorkerResult {
    pub latency: FineHistogram,
    pub error_count: u32,
    pub duration: Duration,
    pub request_count: usize,
    pub byte_count: usize,
}

impl WorkerResult {
    pub fn new() -> WorkerResult {
        return WorkerResult {
            latency: FineHistogram::new(0.000001, 20, 10000),
            error_count: 0,
            duration: Duration::from_millis(0),
            request_count: 0,
            byte_count: 0,
        };
    }

    pub fn report(&self) -> String {
        let mut result = String::new();

        write!(&mut result, "min: {}\n", self.latency.get_min_value()).expect("");
        write!(&mut result, "10pc: {}\n", self.latency.percentile(0.1)).expect("");
        write!(&mut result, "50pc: {}\n", self.latency.percentile(0.5)).expect("");
        write!(&mut result, "90pc: {}\n", self.latency.percentile(0.9)).expect("");
        write!(&mut result, "99pc: {}\n", self.latency.percentile(0.99)).expect("");
        write!(&mut result, "max: {}\n", self.latency.get_max_value()).expect("");

        write!(&mut result, "Runtime: {}s\n", self.duration.as_secs_f64()).expect("");
        write!(
            &mut result,
            "Requests: {}/s\n",
            self.request_count as f64 / self.duration.as_secs_f64()
        )
        .expect("");

        let mbps = ((self.byte_count as f64) / (1024.0 * 1024.0)) / self.duration.as_secs_f64();
        let gbps = (mbps / 1024.0) * 8.0;
        write!(
            &mut result,
            "Bandwidth: {:.3}MB/s ({:.1}gbps))\n",
            mbps, gbps
        )
        .expect("");

        result
    }
}

impl AddAssign for WorkerResult {
    fn add_assign(&mut self, rhs: WorkerResult) {
        self.error_count += rhs.error_count;
        self.latency += rhs.latency;
        // When combining results, assume they are concurrent, so durations
        // are averaged rather than summed
        self.duration = (self.duration + rhs.duration) / 2;
        self.request_count += rhs.request_count;
        self.byte_count += rhs.byte_count;
    }
}

fn get_header<'a>(headers: &'a HeaderMap, k: &str) -> Option<&'a str> {
    let header_value = match headers.get(k) {
        Some(v) => v,
        None => return None,
    };

    match header_value.to_str() {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}

/// For simple dev CLI tools, where an empty default might not work, but it's
/// fine as long as we complained to stderr
fn env_or_log_error(var: &str, default: &str) -> String {
    match env::var(var) {
        Ok(v) => v,
        Err(_) => {
            if default.is_empty() {
                error!("Environment variable {} must be set", var);
            }
            default.to_string()
        }
    }
}

// Wrap hyper Client in a trait so that I can use it in `dyn` references
// where the Client's C can be either HttpConnector or HttpsConnector
trait GenericClient: Send + Sync + 'static {
    fn request(&self, req: Request<Body>) -> ResponseFuture;
}

impl<C: Connect + Send + Sync + Clone + 'static> GenericClient for Client<C, Body> {
    fn request(&self, req: Request<Body>) -> ResponseFuture {
        Client::request(self, req)
    }
}

pub struct Worker {
    options: BenchOptions,
    my_id: usize,
    result: WorkerResult,
}

struct MultiClient {
    s3_clients: Vec<S3Client>,
    rng: ThreadRng,
}

// Wrapper around multiple S3Clients that use different URIs to access the
// same logical endpoint (e.g. multiple capra server instances in a cluster)
impl MultiClient {
    fn new(endpoints: &Vec<Uri>, unsigned: bool) -> Self {
        let mut clients = Vec::new();
        for e in endpoints {
            let host = e.host().unwrap();

            let auth_config = ClientAuthConfig {
                region: env_or_log_error("AWS_DEFAULT_REGION", "us-east-1"),
                access_key_id: env_or_log_error("AWS_ACCESS_KEY_ID", ""),
                secret_access_key: env_or_log_error("AWS_SECRET_ACCESS_KEY", ""),
                host: host.to_string(),
            };

            let s3_client = S3Client::new(e.clone(), auth_config, unsigned);

            clients.push(s3_client);
        }

        Self {
            s3_clients: clients,
            rng: thread_rng(),
        }
    }

    // Pick a client and run the callable on it
    fn with_client<F, R>(&mut self, f: F) -> R
    where
        F: FnOnce(&mut S3Client) -> R,
    {
        let i = if self.s3_clients.len() > 1 {
            self.rng.gen::<usize>() % self.s3_clients.len()
        } else {
            0
        };
        let client = &mut self.s3_clients[i];
        f(client)
    }
}

unsafe impl Send for MultiClient {}
unsafe impl Sync for MultiClient {}

impl Worker {
    pub fn new(options: BenchOptions, my_id: usize) -> Self {
        Self {
            options,
            my_id,
            result: WorkerResult::new(),
        }
    }

    pub async fn execute<K>(
        mut self,
        key_iter: K,
        action: WorkerAction,
    ) -> Result<WorkerResult, Box<dyn std::error::Error + Send + Sync>>
    where
        K: Iterator<Item = KeyWork>,
    {
        info!("Worker {} running batch of action {:?}", self.my_id, action);

        let mut http_connector = HttpConnector::new();
        if self.options.local_address.len() > 0 {
            http_connector.set_local_address(Some(self.options.local_address.parse::<IpAddr>()?));
        }
        let client =
            Client::builder().build::<_, Body>(HttpsConnector::new_with_connector(http_connector));

        let mut s3_clients = MultiClient::new(&self.options.endpoints, self.options.unsigned);

        let t1 = Instant::now();
        for (i, key_work) in key_iter.enumerate() {
            let (key, digest, etag, content) = (
                key_work.key,
                key_work.content.sha256,
                key_work.content.etag,
                key_work.content.payload,
            );

            self.result.request_count += 1;
            if action != WorkerAction::Delete {
                self.result.byte_count += content.len();
            }

            let (duration, valid) = match action {
                WorkerAction::Write => {
                    let req = s3_clients
                        .with_client(|c| c.put(&self.options.bucket, &key, content, &digest));

                    // Timed execution of request
                    let t0 = Instant::now();
                    let put_result = client.request(req).await;
                    let duration = t0.elapsed();
                    info!("{}: PUT {} {}us", self.my_id, key, duration.as_micros());
                    (duration, self.validate_put(put_result, &key, &etag))
                }
                WorkerAction::Read => {
                    let t0 = Instant::now();
                    let req = s3_clients.with_client(|c| c.get(&self.options.bucket, &key));
                    let get_result = client.request(req).await;
                    // Timing includes consuming the response: if verification is on, that
                    // also includes hashing.  Turn verification off for good GET performance measurements.
                    let valid = self.validate_get(get_result, &key, &etag, &digest).await;
                    let duration = t0.elapsed();
                    info!("{}: GET {} {}us", self.my_id, key, duration.as_micros());
                    (duration, valid)
                }
                WorkerAction::Delete => {
                    let t0 = Instant::now();
                    let req = s3_clients.with_client(|c| c.delete(&self.options.bucket, &key));
                    let delete_result = client.request(req).await;
                    let duration = t0.elapsed();
                    info!("{}: DELETE {} {}us", self.my_id, key, duration.as_micros());
                    (duration, self.validate_delete(delete_result, &key))
                }
            };
            if !valid {
                self.result.error_count += 1;
            }

            if i >= WARMUP_REQUESTS {
                self.result.latency.insert(duration.as_secs_f32());
            }
        }
        self.result.duration = t1.elapsed();

        return Ok(self.result);
    }

    fn validate_etag(&self, key: &str, head: &Parts, expected_etag: &str) -> bool {
        if self.options.no_verify {
            return true;
        }

        match head.headers.get("ETag").and_then(|v| v.to_str().ok()) {
            Some(resp_etag) => {
                if expected_etag != resp_etag {
                    error!(
                        "ETag mismatch on response: {} vs expected {}",
                        resp_etag, expected_etag
                    );
                    false
                } else {
                    true
                }
            }
            None => {
                error!(
                    "Missing ETag on response key={} expected={}",
                    key, expected_etag
                );
                false
            }
        }
    }

    fn validate_put(
        &mut self,
        put_result: hyper::Result<Response<Body>>,
        key: &str,
        expected_etag: &str,
    ) -> bool {
        let result_ok = match put_result {
            Ok(response) => {
                if response.status().as_u16() < 300 {
                    debug!("resp {}", response.status());
                    let (head, _body) = response.into_parts();
                    self.validate_etag(key, &head, expected_etag)
                } else {
                    warn!("resp {}", response.status());
                    false
                }
            }
            Err(e) => {
                warn!("PUT error on key {}: {}", key, e);
                false
            }
        };

        result_ok
    }

    async fn consume_response(&mut self, mut body: Body, expect_bytes: u64) -> bool {
        let mut byte_count: u64 = 0;
        loop {
            let read = match body.next().await {
                None => {
                    break;
                }
                Some(result) => result,
            };

            match read {
                Ok(b) => {
                    byte_count += b.len() as u64;
                }
                Err(e) => {
                    error!("Read error: {}", e);
                    return false;
                }
            };
        }

        if byte_count != expect_bytes {
            error!("short response {} expected {}", byte_count, expect_bytes);
            false
        } else {
            true
        }
    }

    fn validate_delete(&mut self, delete_result: hyper::Result<Response<Body>>, key: &str) -> bool {
        match delete_result {
            Ok(response) => !(response.status() == 200 || response.status() == 204),
            Err(e) => {
                error!("Delete error {} on key={}", e, key);
                false
            }
        }
    }

    async fn validate_get(
        &mut self,
        get_result: hyper::Result<Response<Body>>,
        key: &str,
        expected_etag: &str,
        payload_digest: &Sha256Digest,
    ) -> bool {
        let mut get_resp_body = Vec::<u8>::with_capacity(self.options.request_size);

        let response = match get_result {
            Ok(response) => response,
            Err(e) => {
                error!("read-back error {} on key={}", e, key);
                return false;
            }
        };

        let (head, mut body) = response.into_parts();

        let content_length = match get_header(&head.headers, "content-length") {
            Some(cl) => cl,
            None => {
                error!("Missing content-length on response");
                "missing"
            }
        };
        let content_length_i: u64 = content_length.parse().unwrap();

        if !self.validate_etag(key, &head, expected_etag) {
            return false;
        }

        if self.options.no_verify {
            return self.consume_response(body, content_length_i).await;
        }

        if head.status != 200 {
            error!("read-back error {}", head.status);
            return false;
        }

        for (k, v) in head.headers.iter() {
            debug!(
                "GET response header {} : {}",
                k.to_string(),
                v.to_str().unwrap()
            );
        }

        let mut hasher = Sha256::new();
        let mut response_body_size: u64 = 0;
        'per_read: loop {
            let read = match body.next().await {
                None => {
                    error!(
                        "Unexpected end of body after {:#10x} bytes",
                        response_body_size
                    );
                    break 'per_read;
                }
                Some(result) => result,
            };

            let buffer = match read {
                Ok(b) => b,
                Err(e) => {
                    error!("Error reading response for key {}: {}", key, e);
                    return false;
                }
            };

            debug!("Got {} bytes", buffer.len());
            get_resp_body.extend_from_slice(&buffer[..]);
            response_body_size += buffer.len() as u64;
            hasher.update(&buffer[..]);

            if response_body_size >= content_length_i {
                break;
            }
        }

        if response_body_size != content_length_i {
            error!(
                "GET body size {} doesn't match content length {} for key {}",
                response_body_size, content_length_i, key
            )
        }

        let calculated: Sha256Digest = hasher.finalize().into();
        if calculated != *payload_digest {
            error!("GET returned bad data for key {}", key);

            dump_buffer(log::Level::Error, &get_resp_body[..]);
            false
        } else {
            debug!("Successful GET for key {}", key);
            true
        }
    }
}
