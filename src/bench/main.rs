use std::convert::TryFrom;
use std::env;
use std::process::exit;

use args::{Args, ArgsError};
use getopts::Occur;
use tokio;

use super::worker::{Worker, WorkerResult};
use crate::bench::content::{KeyWorkIterator, SameObjectIter, VaryObjectIterator};
use args::validations::Validation;
use http::Uri;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum WorkerAction {
    Read = 0x1,
    Write = 0x2,
    Delete = 0x4,
}

type Actions = u8;

#[derive(Clone)]
pub struct BenchOptions {
    pub request_count: usize,
    pub request_size: usize,
    pub workers: usize,
    pub endpoints: Vec<Uri>,
    pub local_address: String,
    pub unsigned: bool,
    pub no_verify: bool,
    pub bucket: String,
    pub actions: Actions,
    pub content: String,
}

struct EnumValidation {
    valid: Vec<String>,
}

impl EnumValidation {
    fn new(values: Vec<String>) -> Self {
        Self {
            valid: values.into_iter().map(|v| v.to_lowercase()).collect(),
        }
    }
}

impl Validation for EnumValidation {
    type T = String;

    fn error(&self, value: &Self::T) -> ArgsError {
        let joined: String = self.valid.join(",");
        ArgsError::new("", &format!("'{}' not one of {}", value, joined))
    }

    fn is_valid(&self, value: &Self::T) -> bool {
        let value = value.to_lowercase();
        for i in &self.valid {
            if &value == i {
                return true;
            }
        }

        false
    }
}

impl BenchOptions {
    fn parse(input_args: &[String]) -> Result<BenchOptions, ArgsError> {
        let mut args = Args::new(
            &env::args().nth(0).unwrap()[..],
            "Capra test/benchmarking tool",
        );

        args.flag("h", "help", "Print the usage menu");

        args.option(
            "c",
            "count",
            "How many requests to send",
            "COUNT",
            Occur::Optional,
            Some(String::from("10000")),
        );
        args.option(
            "w",
            "workers",
            "How many concurrent requests",
            "WORKERS",
            Occur::Optional,
            Some(String::from("16")),
        );
        args.option(
            "e",
            "endpoint",
            "URI to HTTP endpoint",
            "URI",
            Occur::Optional,
            Some(String::from("http://localhost:1234")),
        );
        args.option(
            "b",
            "bucket",
            "Bucket name",
            "BUCKET",
            Occur::Optional,
            Some("caprabench".into()),
        );
        args.option(
            "s",
            "size",
            "Request payload size",
            "BYTES",
            Occur::Optional,
            Some(String::from("1MB")),
        );
        args.option(
            "l",
            "local-address",
            "Local IP to bind on",
            "URI",
            Occur::Optional,
            Some(String::from("0.0.0.0")),
        );
        args.flag(
            "u",
            "unsigned",
            "do not calculate awsv4 authentication headers",
        );
        args.flag(
            "n",
            "no-verify",
            "do not validate responses (etags, checksums)",
        );

        args.option(
            "t",
            "content",
            "How to generate object contents, default 'same'",
            "same|vary",
            Occur::Optional,
            Some("same".into()),
        );

        args.option(
            "a",
            "actions",
            "Which of Write(w) Read(r) Delete(d) phases to run, default 'wrd'",
            "[w][r][d]",
            Occur::Optional,
            Some("wrd".into()),
        );

        args.parse(input_args)?;

        if args.value_of("help")? {
            eprintln!("{}", args.full_usage());
            std::process::exit(0);
        }

        let sz_str: String = args.value_of("size")?;
        let sz_parsed = bytefmt::parse(sz_str);
        let sz = match sz_parsed {
            Ok(s) => s,
            Err(_) => return Err(ArgsError::new("", "Invalid size string")),
        };

        let endpoints_str: String = args.value_of("endpoint")?;
        let mut endpoints = Vec::new();
        for e in endpoints_str.split(",") {
            let uri = Uri::try_from(e)
                .map_err(|e| ArgsError::new("", &format!("Invalid URL '{}'", e)))?;
            endpoints.push(uri);
        }

        let mut actions: Actions = 0;
        let actions_str: String = args.value_of("actions")?;
        for c in actions_str.chars() {
            match c {
                'w' => actions |= WorkerAction::Write as Actions,
                'r' => actions |= WorkerAction::Read as Actions,
                'd' => actions |= WorkerAction::Delete as Actions,
                _ => return Err(ArgsError::new("", &format!("Invalid action '{}'", c))),
            }
        }
        if actions == 0 {
            return Err(ArgsError::new("", "Must specify at least on action"));
        }

        return Ok(BenchOptions {
            request_count: args.value_of("count")?,
            workers: args.value_of("workers")?,
            endpoints,
            request_size: sz as usize,
            local_address: args.value_of("local-address")?,
            unsigned: args.value_of("unsigned")?,
            no_verify: args.value_of("no-verify")?,
            bucket: args.value_of("bucket")?,
            content: args.validated_value_of(
                "content",
                &[Box::new(EnumValidation::new(vec![
                    "same".into(),
                    "vary".into(),
                ]))],
            )?,
            actions,
        });
    }
}

async fn run_phase<F>(
    options: &BenchOptions,
    name: &str,
    action: WorkerAction,
    mut key_fn: F,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>>
where
    F: FnMut(usize) -> Box<dyn KeyWorkIterator> + Send + Sync,
{
    let worker_params = (0..options.workers).into_iter().map(|i| (i, key_fn(i)));

    // Spawn workers
    let mut handles = vec![];
    for (i, iter) in worker_params {
        let worker = Worker::new(options.clone(), i);
        handles.push(tokio::spawn(
            async move { worker.execute(iter, action).await },
        ));
    }

    // Join and sum results.
    let mut agg_result: Option<WorkerResult> = None;
    for h in handles {
        let worker_result = h.await?.expect("Worker error");
        match &mut agg_result {
            None => agg_result = Some(worker_result),
            Some(w) => *w += worker_result,
        }
    }
    let agg_result = agg_result.unwrap();

    // Output results
    println!("{}:\n{}", name, agg_result.report());
    if agg_result.error_count > 0 {
        exit(-1);
    }

    Ok(())
}

#[tokio::main]
pub async fn entry(input_args: &[String]) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let options = BenchOptions::parse(input_args).expect("Invalid argument");
    let n_per_worker = options.request_count / options.workers;

    // Generator functions to output a KeyWork iterator for each worker
    let mut worker_iters: Vec<Box<dyn KeyWorkIterator>> = Vec::new();
    for i in 0..options.workers {
        let key_iter = (0..n_per_worker)
            .into_iter()
            .map(move |j| format!("{}_{}", i, j));

        let work_iter: Box<dyn KeyWorkIterator> = match options.content.as_str() {
            "same" => Box::new(SameObjectIter::new(key_iter, options.request_size)),
            "vary" => Box::new(VaryObjectIterator::new(key_iter, options.request_size)),
            _ => panic!("Invalid content spec '{}'", options.content),
        };

        worker_iters.push(work_iter);
    }

    if options.actions & WorkerAction::Write as Actions != 0 {
        run_phase(&options, "Write", WorkerAction::Write, |i| {
            worker_iters[i].box_clone()
        })
        .await?;
    }

    if options.actions & WorkerAction::Read as Actions != 0 {
        run_phase(&options, "Read", WorkerAction::Read, |i| {
            worker_iters[i].box_clone()
        })
        .await?;
    }

    if options.actions & WorkerAction::Delete as Actions != 0 {
        run_phase(&options, "Delete", WorkerAction::Delete, |i| {
            worker_iters[i].box_clone()
        })
        .await?;
    }

    return Ok(());
}
