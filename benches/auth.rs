use criterion::{black_box, criterion_group, criterion_main, Criterion};

use hyper::body::Bytes;
use hyper::http::request::Parts;

use chrono::Utc;
use hyper::{Body, Method, Request};
use rand::{thread_rng, RngCore};

use ::digest::Digest;
use capralib::auth::{AuthorizedRequest, Authorizer, SecretMap};
use capralib::digest::{sha_str, Sha256, Sha256Digest};
use capralib::s3_client_auth::{
    sign_request, ClientAuthConfig, ClientAuthKeyState, ClientAuthRequestState,
};

use capralib::alloc_guard::AllocGuard;

fn run_authorizer<A: Authorizer>(header: &Parts, secrets: &SecretMap, body: &Bytes) {
    AllocGuard::new("auth_body", 0, false);
    let mut a = A::new(header, secrets).unwrap();
    a.update(body);
    a.finalize().expect("Auth failed!");
}

fn run_authorizer_initonly<A: Authorizer>(header: &Parts, secrets: &SecretMap) {
    AllocGuard::new("auth_init", 0, false);
    A::new(header, secrets).unwrap();
}

pub fn criterion_benchmark(c: &mut Criterion) {
    fn build_request(request_size: usize) -> (Parts, Bytes) {
        let request_size_str = request_size.to_string();
        let mut payload_bytes: Vec<u8> = Vec::with_capacity(request_size);
        payload_bytes.resize(request_size, 0);
        thread_rng().fill_bytes(&mut payload_bytes[0..request_size]);

        let payload: Bytes = Bytes::copy_from_slice(&payload_bytes[0..request_size]);
        assert_eq!(payload.len(), request_size as usize);

        let mut hasher = Sha256::new();
        hasher.update(&payload);
        let payload_digest = format!("{:x}", hasher.finalize());

        let auth_config = ClientAuthConfig {
            region: "us-east-1".to_string(),
            access_key_id: "testid".to_string(),
            secret_access_key: "testkey".to_string(),
            host: "testhost".to_string(),
        };

        let init_now = Utc::now();
        let daily_auth_state = ClientAuthKeyState::new(&init_now, &auth_config);
        let request_auth_state =
            ClientAuthRequestState::new(&init_now, &auth_config.host, &payload_digest);

        let mut req_builder = Request::builder()
            .method(Method::PUT)
            .uri("http://testhost/testbucket/testobject")
            .header("Content-Type", "application/octet-stream")
            .header("Content-Length", &request_size_str)
            .header("Host", &auth_config.host);

        req_builder = sign_request(
            req_builder,
            &payload_digest,
            &daily_auth_state,
            &request_auth_state,
        );
        let req = req_builder
            .body(Body::from(payload.clone()))
            .expect("Error constructing request");
        let (head, _body) = req.into_parts();
        (head, payload)
    }

    let mut secret_map = SecretMap::new();
    secret_map.insert("testid".to_string(), "testkey".to_string());

    c.bench_function("auth_check_init", |b| {
        let (head, _payload) = build_request(0x1000);
        b.iter(|| {
            run_authorizer_initonly::<AuthorizedRequest>(&head, &secret_map);
        })
    });

    c.bench_function("auth_check_smallbody", |b| {
        let (head, payload) = build_request(0x1000);
        b.iter(|| {
            run_authorizer::<AuthorizedRequest>(&head, &secret_map, &payload);
        })
    });

    c.bench_function("auth_check_bigbody", |b| {
        let (head, payload) = build_request(0x10000);
        b.iter(|| {
            run_authorizer::<AuthorizedRequest>(&head, &secret_map, &payload);
        })
    });

    // TODO compare this with the old hex print
    c.bench_function("auth_sha_str", |b| {
        let digest: Sha256Digest = [0; 32];
        b.iter(|| {
            sha_str(black_box(&digest));
        })
    });
}
//
// fn alternate_measurement() -> Criterion<criterion_alloc::AllocMeasurement> {
//     Criterion::default().with_measurement(criterion_alloc::AllocMeasurement)
// }
//
// criterion_group!(name = benches;
// config = alternate_measurement();
// targets = criterion_benchmark);
criterion_group! {benches, criterion_benchmark}
criterion_main!(benches);
