use criterion::{black_box, criterion_group, criterion_main, Criterion};

use std::fmt;

use capralib::simple_pool::SimplePool;
use std::time::Instant;

async fn take_release<T>(pool: &mut SimplePool<T>)
where T: Send + Sync + fmt::Display{
    let i = pool.acquire().await;
    pool.release(i);
}

pub fn criterion_benchmark(c: &mut Criterion) {
    let executor = tokio::runtime::Builder::new_current_thread().build().unwrap();
    c.bench_function("take_release", |b| {
        b.to_async(&executor).iter_custom(|iters| {
            async move {
                let mut pool = SimplePool::<u64>::new_with(16384, || 123);
                let t1 = Instant::now();
                for _i in 0..iters {
                    black_box(take_release(black_box(&mut pool)).await);
                }
                t1.elapsed()
            }
        })
    });
}

criterion_group! {benches, criterion_benchmark}
criterion_main!(benches);
