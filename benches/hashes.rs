use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};

use capralib::digest;
use crypto::digest::Digest as CryptoDigest;
use crypto::md5::Md5 as CryptoMd5;
use crypto::sha2::Sha256 as CryptoSha256;

// md5 crate
use md55;

// md-5 crate
use ::digest::Digest;
use md5;

use sodiumoxide::crypto::hash::sha256 as NaO2Sha256;

use ring::digest as RingDigest;
use sha2::Sha256 as Sha2Sha256;

pub fn digest_n(buf: &[u8]) {
    let mut ctx = digest::Sha256::new();
    ctx.update(buf);
    let _digest = ctx.finalize();
}

pub fn md5_n(buf: &[u8]) {
    let mut ctx = md5::Md5::new();
    ctx.update(buf);
    ctx.finalize();
}

pub fn md55_n(buf: &[u8]) {
    let mut ctx = md55::Context::new();
    ctx.consume(buf);
    ctx.compute();
}

pub fn rust_crypto_md5(buf: &[u8]) {
    let mut ctx = CryptoMd5::new();
    ctx.input(buf);
    let mut result: [u8; 16] = [0; 16];
    ctx.result(&mut result);
}

pub fn rust_crypto_sha256(buf: &[u8]) {
    let mut ctx = CryptoSha256::new();
    ctx.input(buf);
    let mut result: [u8; 32] = [0; 32];
    ctx.result(&mut result);
}

pub fn nao2_sha256(buf: &[u8]) {
    let mut ctx = NaO2Sha256::State::new();
    ctx.update(buf);
    let _digest = ctx.finalize();
}

pub fn ring_sha256(buf: &[u8]) {
    let mut ctx = RingDigest::Context::new(&RingDigest::SHA256);
    ctx.update(buf);
    let _digest = ctx.finish();
}

pub fn sha2_sha256(buf: &[u8]) {
    let mut ctx = Sha2Sha256::new();
    ctx.update(buf);
    let _digest = ctx.finalize();
}

pub fn criterion_benchmark(c: &mut Criterion) {
    {
        let mut group = c.benchmark_group("digest");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| digest_n(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }
    {
        let mut group = c.benchmark_group("md-5");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| md5_n(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }
    {
        let mut group = c.benchmark_group("md5");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| md55_n(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }

    {
        let mut group = c.benchmark_group("rust_crypto_md5");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| rust_crypto_md5(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }

    {
        let mut group = c.benchmark_group("rust_crypto_sha256");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| rust_crypto_sha256(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }

    {
        let mut group = c.benchmark_group("nao2_sha256");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| nao2_sha256(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }

    {
        let mut group = c.benchmark_group("sha2_sha256");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| sha2_sha256(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }

    {
        let mut group = c.benchmark_group("ring_sha256");
        for size in [0x1000, 0x4000, 0x10000].iter() {
            let mut buffer: Vec<u8> = Vec::with_capacity(*size);
            buffer.resize(*size, 0xf0);
            group.throughput(Throughput::Bytes((*size) as u64));
            group.bench_with_input(BenchmarkId::from_parameter(size), size, |b, &_size| {
                b.iter(|| ring_sha256(black_box(&buffer.as_slice())))
            });
        }
        group.finish();
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
